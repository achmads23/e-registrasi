<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acara', function (Blueprint $table) {
            $table->increments('id_acr');
            $table->string('nama_acr');
            $table->string('tempat_acr');
            $table->string('bagi')->default('-');
            $table->string('lingkungan')->default('-');
            $table->date('tgl_acr');
            $table->date('akhir_tgl_acr');
            $table->time('pukul');
            $table->string('id_opds')->nullable();
            $table->text('deskripsi')->nullable();
            $table->dateTime('tanggal_awal')->nullable();
            $table->dateTime('tanggal_akhir')->nullable();
            $table->dateTime('tampil_awal')->nullable();
            $table->dateTime('tampil_akhir')->nullable();
            $table->string('code')->nullable();
            $table->integer('kuota_umum')->default(0);
            $table->integer('kuota_undangan')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acara');
    }
}
