<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('judul');
            $table->text('question');
            $table->string('file_question')->nullable();
            $table->text('answer')->nullable();
            $table->text('keterangan')->nullable();
            $table->integer('publish')->default(1);
            $table->string('file_answer')->nullable();
            $table->dateTime('answered_at')->nullable();
            $table->string('rating')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threads');
    }
}
