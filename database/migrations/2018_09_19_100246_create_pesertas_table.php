<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesertas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_acara');
            $table->integer('id_pegawai');
            $table->integer('id_opd');
            $table->string('nip')->nullable();
            $table->string('nama')->nullable();;
            $table->string('jenis_kelamin')->nullable();
            $table->string('asal_instansi')->nullable();;
            $table->string('golongan')->nullable();;
            $table->longText('foto');
            $table->string('qrcode');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('jenis')->default('undangan');
            $table->integer('status');
            $table->integer('inap')->default(0);
            $table->integer('sesuai_spt')->default(0);
            $table->integer('notif')->default(0);
            $table->longText('file')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('email')->nullable();
            $table->dateTime('check_in')->nullable();
            $table->string('token_check_in')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesertas');
    }
}
