<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OpdsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(RegenciesTableSeeder::class);
        $this->call(VillagesTableSeeder::class);
        $this->call(PesertasTableSeeder::class);
        $this->call(AcaraTableSeeder::class);
        $this->call(DetailUsersTableSeeder::class);
        // $this->call(PegawaisTableSeeder::class);
        $this->call(AhlisTableSeeder::class);
        $this->call(FilesTableSeeder::class);
        $this->call(MigrationsTableSeeder::class);
        $this->call(ThreadsTableSeeder::class);
    }
}