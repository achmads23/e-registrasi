<?php

use Illuminate\Database\Seeder;
use App\Pegawai;
use Carbon\Carbon;

class PegawaisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        $file = fopen("database/seeds/csv/pegawais.csv","r");
        $i = 0;
        $last_id = null;
        while (!feof($file)) {
            $value = (fgetcsv($file, 0, ';'));
            if ($i > 0) {
                $Pegawai = new Pegawai;
                if(isset($value[3]))
                    $Pegawai->kode_satker = $value[3];
                if(isset($value[4])) 
                    $Pegawai->nama_satker = $value[4];
                if(isset($value[5])) 
                    $Pegawai->nip = $value[5];
                if(isset($value[6])) 
                    $Pegawai->nama = $value[6];
                if(isset($value[7])) 
                    $Pegawai->jk = $value[7];
                if(isset($value[8])) 
                    $Pegawai->gol = $value[8];
                if(isset($value[9])) 
                    $Pegawai->ruang = $value[9];
                $Pegawai->created_by = 1;
                try {
                  $Pegawai->save();
                } catch (Exception $e) {
                  // print_r($e);
                }
            }
            $i++;
        }
        fclose($file);

        
        
    }
}