<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 0,
                'name' => 'Perencanaan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'name' => 'Penganggaran',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 0,
                'name' => 'Penatausahaan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 0,
                'name' => 'Akuntansi',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => '2018-10-30 15:01:45',
                'created_at' => NULL,
                'updated_at' => '2018-10-30 15:01:45',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 0,
                'name' => 'Aset',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 1,
                'name' => 'RPJMD',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 1,
                'name' => 'RKPD',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 1,
                'name' => 'Indikator Kinerja',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 1,
                'name' => 'Renstra/Renja',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 2,
                'name' => 'RKA',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 2,
                'name' => 'DPA',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 2,
                'name' => 'KUA/PPAS',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 3,
                'name' => 'Penerimaan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 3,
                'name' => 'Pengeluaran',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 4,
                'name' => 'Sistem Akuntansi',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'parent_id' => 4,
                'name' => 'BAS',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'parent_id' => 5,
                'name' => 'Pemanfaatan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'parent_id' => 5,
                'name' => 'Penatausahaan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'parent_id' => 5,
                'name' => 'Pemusnahan/Pemindahtanganan',
                'created_by' => 0,
                'updated_by' => NULL,
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}