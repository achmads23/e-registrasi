<?php

use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('files')->delete();
        
        \DB::table('files')->insert(array (
            0 => 
            array (
                'id' => 1,
                'jenis' => 'file',
                'file' => 'storage/files/xtwP6MfT6XAVlse1Z6AzA7aPyONpaiNH15oEAmmw.jpeg',
                'create_by' => 39,
                'created_at' => '2018-12-13 05:24:59',
                'updated_at' => '2018-12-13 05:24:59',
            ),
            1 => 
            array (
                'id' => 2,
                'jenis' => 'file',
                'file' => 'storage/files/7XvfvBF1kmvkCx1RBURPuURA3q7TUcoS16yONUC8.jpeg',
                'create_by' => 7,
                'created_at' => '2018-12-13 09:29:24',
                'updated_at' => '2018-12-13 09:29:24',
            ),
            2 => 
            array (
                'id' => 3,
                'jenis' => 'file',
                'file' => 'storage/files/C2n2HRDyXlf9lYSGJrGGH60LxaLxYhegr367mGp8.jpeg',
                'create_by' => 7,
                'created_at' => '2018-12-13 09:29:51',
                'updated_at' => '2018-12-13 09:29:51',
            ),
            3 => 
            array (
                'id' => 4,
                'jenis' => 'file',
                'file' => 'storage/files/TfhvZ5r2IjObOVo0JIV0lb3AyntbF6tP2gtPlZxL.jpeg',
                'create_by' => 7,
                'created_at' => '2018-12-13 09:50:33',
                'updated_at' => '2018-12-13 09:50:33',
            ),
            4 => 
            array (
                'id' => 5,
                'jenis' => 'file',
                'file' => 'storage/files/Ii80o5ur8hNEkoLV4JYZ5Lv8GOsjLXHohf8vLUbX.jpeg',
                'create_by' => 7,
                'created_at' => '2018-12-13 10:07:43',
                'updated_at' => '2018-12-13 10:07:43',
            ),
            5 => 
            array (
                'id' => 6,
                'jenis' => 'file',
                'file' => 'storage/files/GZXpwv2rm3GMkPB7HCMxswPjm3hGmYK2tAMnHOg7.jpeg',
                'create_by' => 39,
                'created_at' => '2018-12-13 10:38:06',
                'updated_at' => '2018-12-13 10:38:06',
            ),
            6 => 
            array (
                'id' => 7,
                'jenis' => 'file',
                'file' => 'storage/files/UL1jVRaF0WXXBt2m8IpuCmZZfazYKydzAdtAUbbA.jpeg',
                'create_by' => 7,
                'created_at' => '2018-12-13 10:48:30',
                'updated_at' => '2018-12-13 10:48:30',
            ),
        ));
        
        
    }
}