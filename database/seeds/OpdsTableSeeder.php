<?php

use Illuminate\Database\Seeder;

class OpdsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('opds')->delete();
        
        \DB::table('opds')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_opd' => '1010100',
                'opd' => 'DINAS PENDIDIKAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            1 => 
            array (
                'id' => 2,
                'id_opd' => '1020101',
                'opd' => 'DINAS KESEHATAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            2 => 
            array (
                'id' => 3,
                'id_opd' => '1020102',
                'opd' => 'RUMAH SAKIT UMUM KARSA HUSADA BATU',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            3 => 
            array (
                'id' => 4,
                'id_opd' => '1020103',
                'opd' => 'RUMAH SAKIT PARU JEMBER',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            4 => 
            array (
                'id' => 5,
                'id_opd' => '1020104',
                'opd' => 'RUMAH SAKIT PARU DUNGUS MADIUN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            5 => 
            array (
                'id' => 6,
                'id_opd' => '1020105',
                'opd' => 'RUMAH SAKIT KUSTA KEDIRI',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            6 => 
            array (
                'id' => 7,
                'id_opd' => '1020106',
                'opd' => 'RUMAH SAKIT KUSTA SUMBERGLAGAH MOJOKERTO',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            7 => 
            array (
                'id' => 8,
                'id_opd' => '1020107',
                'opd' => 'RUMAH SAKIT MATA MASYARAKAT JAWA TIMUR',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            8 => 
            array (
                'id' => 9,
                'id_opd' => '1020108',
                'opd' => 'RUMAH SAKIT PARU SURABAYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            9 => 
            array (
                'id' => 10,
                'id_opd' => '1020109',
                'opd' => 'RUMAH SAKIT UMUM MOHAMMAD NOER PAMEKASAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            10 => 
            array (
                'id' => 11,
                'id_opd' => '1020110',
                'opd' => 'RUMAH SAKIT PARU MANGUHARJO MADIUN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            11 => 
            array (
                'id' => 12,
                'id_opd' => '1020111',
                'opd' => 'AKADEMI KEPERAWATAN MADIUN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            12 => 
            array (
                'id' => 13,
                'id_opd' => '1020112',
                'opd' => 'AKADEMI GIZI SURABAYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            13 => 
            array (
                'id' => 14,
                'id_opd' => '1020113',
                'opd' => 'UPT PELATIHAN KESEHATAN MASYARAKAT MURNAJATI LAWANG',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            14 => 
            array (
                'id' => 15,
                'id_opd' => '1020200',
                'opd' => 'RUMAH SAKIT UMUM DR. SOETOMO SURABAYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            15 => 
            array (
                'id' => 16,
                'id_opd' => '1020300',
                'opd' => 'RUMAH SAKIT UMUM DR. SAIFUL ANWAR MALANG',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            16 => 
            array (
                'id' => 17,
                'id_opd' => '1020400',
                'opd' => 'RUMAH SAKIT UMUM DR. SOEDONO MADIUN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            17 => 
            array (
                'id' => 18,
                'id_opd' => '1020500',
                'opd' => 'RUMAH SAKIT HAJI SURABAYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            18 => 
            array (
                'id' => 19,
                'id_opd' => '1020600',
                'opd' => 'RUMAH SAKIT JIWA MENUR SURABAYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            19 => 
            array (
                'id' => 20,
                'id_opd' => '1030100',
                'opd' => 'DINAS PEKERJAAN UMUM BINA MARGA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            20 => 
            array (
                'id' => 21,
                'id_opd' => '1030200',
                'opd' => 'DINAS PEKERJAAN UMUM SUMBER DAYA AIR',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            21 => 
            array (
                'id' => 22,
                'id_opd' => '1030300',
                'opd' => 'DINAS PERUMAHAN RAKYAT, KAWASAN PERMUKIMAN DAN CIPTA KARYA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            22 => 
            array (
                'id' => 23,
                'id_opd' => '1050100',
                'opd' => 'SATUAN POLISI PAMONG PRAJA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            23 => 
            array (
                'id' => 24,
                'id_opd' => '1060100',
                'opd' => 'DINAS SOSIAL',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            24 => 
            array (
                'id' => 25,
                'id_opd' => '1070100',
                'opd' => 'DINAS TENAGA KERJA DAN TRANSMIGRASI',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            25 => 
            array (
                'id' => 26,
                'id_opd' => '1080100',
                'opd' => 'DINAS PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK, DAN KEPENDUDUKAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            26 => 
            array (
                'id' => 27,
                'id_opd' => '1090100',
                'opd' => 'DINAS PERTANIAN DAN KETAHANAN PANGAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            27 => 
            array (
                'id' => 28,
                'id_opd' => '1090101',
                'opd' => 'UPT PENGEMBANGAN BENIH PADI',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            28 => 
            array (
                'id' => 29,
                'id_opd' => '1090102',
                'opd' => 'UPT PENGEMBANGAN BENIH PALAWIJA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            29 => 
            array (
                'id' => 30,
                'id_opd' => '1090103',
                'opd' => 'UPT PENGEMBANGAN BENIH HORTIKULTURA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            30 => 
            array (
                'id' => 31,
                'id_opd' => '1090104',
                'opd' => 'UPT PENGEMBANGAN AGRIBISNIS TANAMAN PANGAN DAN HORTIKULTURA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            31 => 
            array (
                'id' => 32,
                'id_opd' => '1110100',
                'opd' => 'DINAS LINGKUNGAN HIDUP',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            32 => 
            array (
                'id' => 33,
                'id_opd' => '1130100',
                'opd' => 'DINAS PEMBERDAYAAN MASYARAKAT DAN DESA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            33 => 
            array (
                'id' => 34,
                'id_opd' => '1150100',
                'opd' => 'DINAS PERHUBUNGAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            34 => 
            array (
                'id' => 35,
                'id_opd' => '1160100',
                'opd' => 'DINAS KOMUNIKASI DAN INFORMATIKA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            35 => 
            array (
                'id' => 36,
                'id_opd' => '1170100',
                'opd' => 'DINAS KOPERASI, USAHA KECIL DAN MENENGAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            36 => 
            array (
                'id' => 37,
                'id_opd' => '1180100',
                'opd' => 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            37 => 
            array (
                'id' => 38,
                'id_opd' => '1190100',
                'opd' => 'DINAS KEPEMUDAAN DAN OLAHRAGA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            38 => 
            array (
                'id' => 39,
                'id_opd' => '1220100',
                'opd' => 'DINAS KEBUDAYAAN DAN PARIWISATA',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            39 => 
            array (
                'id' => 40,
                'id_opd' => '1230100',
                'opd' => 'DINAS PERPUSTAKAAN DAN KEARSIPAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            40 => 
            array (
                'id' => 41,
                'id_opd' => '2010100',
                'opd' => 'DINAS KELAUTAN DAN PERIKANAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            41 => 
            array (
                'id' => 42,
                'id_opd' => '2010101',
                'opd' => 'UPT PELABUHAN DAN PENGELOLAAN SUMBER DAYA KELAUTAN DAN PERIKANAN MAYANGAN ',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            42 => 
            array (
                'id' => 43,
                'id_opd' => '2010102',
                'opd' => 'UPT PELABUHAN DAN PENGELOLAAN SUMBER DAYA KELAUTAN DAN PERIKANAN TAMPERAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            43 => 
            array (
                'id' => 44,
                'id_opd' => '2010103',
                'opd' => 'UPT PELABUHAN DAN PENGELOLAAN SUMBER DAYA KELAUTAN DAN PERIKANAN PONDOKDADAP',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            44 => 
            array (
                'id' => 45,
                'id_opd' => '2030100',
                'opd' => 'DINAS PERKEBUNAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            45 => 
            array (
                'id' => 46,
                'id_opd' => '2030200',
                'opd' => 'DINAS PETERNAKAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            46 => 
            array (
                'id' => 47,
                'id_opd' => '2040100',
                'opd' => 'DINAS KEHUTANAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            47 => 
            array (
                'id' => 48,
                'id_opd' => '2050100',
                'opd' => 'DINAS ENERGI DAN SUMBER DAYA MINERAL',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            48 => 
            array (
                'id' => 49,
                'id_opd' => '2070100',
                'opd' => 'DINAS PERINDUSTRIAN DAN PERDAGANGAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            49 => 
            array (
                'id' => 50,
                'id_opd' => '3010100',
                'opd' => 'KEPALA DAERAH DAN WAKIL KEPALA DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            50 => 
            array (
                'id' => 51,
                'id_opd' => '3030108',
                'opd' => 'SEKRETARIAT DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            51 => 
            array (
                'id' => 52,
                'id_opd' => '3040100',
                'opd' => 'SEKRETARIAT DPRD',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            52 => 
            array (
                'id' => 53,
                'id_opd' => '3050100',
                'opd' => 'INSPEKTORAT',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            53 => 
            array (
                'id' => 54,
                'id_opd' => '3060100',
                'opd' => 'BADAN PERENCANAAN PEMBANGUNAN DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            54 => 
            array (
                'id' => 55,
                'id_opd' => '3070100',
                'opd' => 'BADAN PENDAPATAN DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            55 => 
            array (
                'id' => 56,
                'id_opd' => '3070200',
                'opd' => 'BADAN PENGELOLA KEUANGAN DAN ASET DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            56 => 
            array (
                'id' => 57,
                'id_opd' => '3080100',
                'opd' => 'BADAN KEPEGAWAIAN DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            57 => 
            array (
                'id' => 58,
                'id_opd' => '3090100',
                'opd' => 'BADAN PENDIDIKAN DAN PELATIHAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            58 => 
            array (
                'id' => 59,
                'id_opd' => '3100100',
                'opd' => 'BADAN PENELITIAN DAN PENGEMBANGAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            59 => 
            array (
                'id' => 60,
                'id_opd' => '3110100',
                'opd' => 'BADAN PENGHUBUNG DAERAH PROVINSI',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            60 => 
            array (
                'id' => 61,
                'id_opd' => '3120100',
                'opd' => 'BADAN KESATUAN BANGSA DAN POLITIK',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            61 => 
            array (
                'id' => 62,
                'id_opd' => '3130100',
                'opd' => 'BADAN PENANGGULANGAN BENCANA DAERAH',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            62 => 
            array (
                'id' => 63,
                'id_opd' => '3140100',
                'opd' => 'BADAN KOORDINASI WILAYAH PEMERINTAHAN DAN PEMBANGUNAN I MADIUN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            63 => 
            array (
                'id' => 64,
                'id_opd' => '3140200',
                'opd' => 'BADAN KOORDINASI WILAYAH PEMERINTAHAN DAN PEMBANGUNAN II BOJONEGORO',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            64 => 
            array (
                'id' => 65,
                'id_opd' => '3140300',
                'opd' => 'BADAN KOORDINASI WILAYAH PEMERINTAHAN DAN PEMBANGUNAN III MALANG',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            65 => 
            array (
                'id' => 66,
                'id_opd' => '3140400',
                'opd' => 'BADAN KOORDINASI WILAYAH PEMERINTAHAN DAN PEMBANGUNAN IV PAMEKASAN',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
            66 => 
            array (
                'id' => 67,
                'id_opd' => '3140500',
                'opd' => 'BADAN KOORDINASI WILAYAH PEMERINTAHAN DAN PEMBANGUNAN V JEMBER',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-24 10:22:34',
                'updated_at' => '2018-11-24 10:22:34',
            ),
        ));
        
        
    }
}