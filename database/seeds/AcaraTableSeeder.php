<?php

use Illuminate\Database\Seeder;

class AcaraTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('acara')->delete();
        
        \DB::table('acara')->insert(array (
            0 => 
            array (
                'id_acr' => 5,
                'nama_acr' => 'Bimbingan Teknis Tematik Perencanaan Keuangan Desa I',
                'tempat_acr' => 'UPT PPK',
                'tgl_acr' => '2018-11-29',
                'id_opds' => '',
                'deskripsi' => NULL,
                'tanggal_awal' => '2018-11-28 14:17:54',
                'tanggal_akhir' => '2018-11-29 14:17:54',
                'tampil_awal' => '2018-11-27 14:17:54',
                'tampil_akhir' => '2018-11-30 14:17:54',
                'code' => 'SG06',
                'kuota_umum' => 40,
                'kuota_undangan' => 40,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2018-11-27 08:11:14',
                'updated_at' => '2018-11-28 14:17:54',
                'pukul' => '07:30:00',
                'bagi' => 'DESA',
                'lingkungan' => 'PEMERINTAH PROVISI JAWA TIMUR',
                'akhir_tgl_acr' => '2018-11-28',
            ),
            1 => 
            array (
                'id_acr' => 6,
                'nama_acr' => 'Tematik Perencanaan Keuangan Desa II',
                'tempat_acr' => 'UPT PPK',
                'tgl_acr' => '2018-12-03',
                'id_opds' => '',
                'deskripsi' => NULL,
                'tanggal_awal' => '2018-11-27 10:12:05',
                'tanggal_akhir' => '2018-12-03 10:12:05',
                'tampil_awal' => '2018-11-27 10:12:05',
                'tampil_akhir' => '2018-12-04 10:12:05',
                'code' => 'UK08',
                'kuota_umum' => 40,
                'kuota_undangan' => 40,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2018-11-27 08:22:10',
                'updated_at' => '2018-11-30 10:12:05',
                'pukul' => '07:30:00',
            'bagi' => 'Tim Penguatan Kapasitas Aparatur Kabupaten (PKAK)',
                'lingkungan' => 'Pemerintah Kabupaten/Kota',
                'akhir_tgl_acr' => '2018-11-30',
            ),
            2 => 
            array (
                'id_acr' => 7,
                'nama_acr' => 'Bimbingan Teknis Tematik Perencanaan Keuangan Desa III',
                'tempat_acr' => 'UPT PPK',
                'tgl_acr' => '2018-12-05',
                'id_opds' => '',
                'deskripsi' => NULL,
                'tanggal_awal' => '2018-11-27 08:24:05',
                'tanggal_akhir' => '2018-12-04 08:24:05',
                'tampil_awal' => '2018-11-27 08:24:05',
                'tampil_akhir' => '2018-12-06 08:24:05',
                'code' => 'UC24',
                'kuota_umum' => 40,
                'kuota_undangan' => 40,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-11-27 08:24:05',
                'updated_at' => '2018-11-27 08:24:05',
                'pukul' => '07:30:00',
                'bagi' => '-',
                'lingkungan' => '-',
                'akhir_tgl_acr' => NULL,
            ),
            3 => 
            array (
                'id_acr' => 9,
                'nama_acr' => 'Bimbingan Teknis Test BLUD',
                'tempat_acr' => 'Bromo',
                'tgl_acr' => '2018-12-01',
                'id_opds' => '1;2;',
                'deskripsi' => NULL,
                'tanggal_awal' => '2018-12-01 04:52:01',
                'tanggal_akhir' => '2018-12-02 04:52:01',
                'tampil_awal' => '2018-12-01 04:52:01',
                'tampil_akhir' => '2018-12-02 04:52:01',
                'code' => 'BN63',
                'kuota_umum' => 10,
                'kuota_undangan' => 30,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2018-12-01 04:46:21',
                'updated_at' => '2018-12-01 04:52:01',
                'pukul' => '08:00:00',
                'bagi' => 'Bendahara Penerimaan',
                'lingkungan' => 'Pemerintah Provinsi Jawa Timur',
                'akhir_tgl_acr' => '2018-12-02',
            ),
        ));
        
        
    }
}