<?php

use Illuminate\Database\Seeder;

class AhlisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ahlis')->delete();
        
        \DB::table('ahlis')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_user' => 35,
                'id_category' => 9,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-10 12:09:09',
                'updated_at' => '2018-12-10 12:09:09',
            ),
            1 => 
            array (
                'id' => 2,
                'id_user' => 36,
                'id_category' => 6,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2018-12-13 03:16:28',
                'updated_at' => '2018-12-13 07:38:21',
            ),
            2 => 
            array (
                'id' => 5,
                'id_user' => 41,
                'id_category' => 6,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-13 10:12:00',
                'updated_at' => '2018-12-13 10:12:00',
            ),
            3 => 
            array (
                'id' => 4,
                'id_user' => 38,
                'id_category' => 6,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-13 04:02:14',
                'updated_at' => '2018-12-13 04:02:14',
            ),
        ));
        
        
    }
}