<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 19,
                'migration' => '2014_10_12_000000_create_detail_users_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 20,
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 1,
            ),
            2 => 
            array (
                'id' => 21,
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 1,
            ),
            3 => 
            array (
                'id' => 22,
                'migration' => '2018_07_15_173554_create_districts_table',
                'batch' => 1,
            ),
            4 => 
            array (
                'id' => 23,
                'migration' => '2018_07_15_173554_create_regencies_table',
                'batch' => 1,
            ),
            5 => 
            array (
                'id' => 24,
                'migration' => '2018_07_15_173554_create_villages_table',
                'batch' => 1,
            ),
            6 => 
            array (
                'id' => 25,
                'migration' => '2018_07_15_174818_create_provinces_table',
                'batch' => 1,
            ),
            7 => 
            array (
                'id' => 26,
                'migration' => '2018_09_17_100246_create_acaras_table',
                'batch' => 1,
            ),
            8 => 
            array (
                'id' => 27,
                'migration' => '2018_09_17_100246_create_opds_table',
                'batch' => 1,
            ),
            9 => 
            array (
                'id' => 28,
                'migration' => '2018_09_17_100246_create_pegawais_table',
                'batch' => 1,
            ),
            10 => 
            array (
                'id' => 29,
                'migration' => '2018_09_19_100246_create_pesertas_table',
                'batch' => 1,
            ),
            11 => 
            array (
                'id' => 30,
                'migration' => '2018_10_08_071121_create_user_verifications_table',
                'batch' => 1,
            ),
            12 => 
            array (
                'id' => 37,
                'migration' => '2018_10_11_095138_create_categories_table',
                'batch' => 2,
            ),
            13 => 
            array (
                'id' => 32,
                'migration' => '2018_10_11_105412_create_threads_table',
                'batch' => 1,
            ),
            14 => 
            array (
                'id' => 33,
                'migration' => '2018_10_22_094126_create_responders_table',
                'batch' => 1,
            ),
            15 => 
            array (
                'id' => 34,
                'migration' => '2018_10_22_094126_create_settings_table',
                'batch' => 1,
            ),
            16 => 
            array (
                'id' => 35,
                'migration' => '2018_10_22_100303_create_files_table',
                'batch' => 1,
            ),
            17 => 
            array (
                'id' => 36,
                'migration' => '2018_10_30_125328_create_ahlis_table',
                'batch' => 1,
            ),
        ));
        
        
    }
}