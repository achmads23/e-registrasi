<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'label' => 'monitor',
                'value' => '6',
                'created_by' => 0,
                'created_at' => NULL,
                'updated_at' => '2018-12-01 05:55:07',
            ),
        ));
        
        
    }
}