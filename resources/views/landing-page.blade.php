@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')
<div class="text-center row-zone" style="margin: 100px 0px 20px 0px;">
  <a href="{{route('bimtek')}}" class='btn-zone'>
    <img src="{{asset('images/bimtek.png')}}">
    <h4 class="text-black"><b>Bimtek</b></h4>
  </a>
  <a href="{{route('klinik-center')}}" class='btn-zone'>
    <img src="{{asset('images/Klinik Center.png')}}">
    <h4 class="text-black"><b>Klinik Center</b></h4>
  </a>
  <a href="{{route('pendampingan')}}" class='btn-zone'>
    <img src="{{asset('images/Pendampingan.png')}}">
    <h4 class="text-black"><b>Pendampingan</b></h4>
  </a>
  <a href="http://uptlpkd.bpkad.jatimprov.go.id/" class='btn-zone' target="_blank">
    <img src="{{asset('images/Informasi.png')}}">
    <h4 class="text-black"><b>Informasi</b></h4>
  </a>
</div>
<div class="text-center">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK - BPKAD Provinsi Jawa Timur</a></strong>
</div>
@endsection

@section('js')
<script>
$(function() {
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
