@extends('layouts.main')

@section('title', 'Acara')
@include('plugins.eonasdan-datepicker')
@include('plugins.timepicker')

@section('title-content')
Acara
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Acara</a></li>
<li class="active">Buat Acara</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/acara')}}">List</a></li>
  <li class="active"><a href="{{URL::to('/admin/acara/create')}}">Buat Acara</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/acara')}}">
      {{ csrf_field() }}
      <div class="form-group">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-sm-10">
          <select name="opd[]" class="select-opd" placeholder="Pilih OPD" multiple="">
            @foreach ($opds as $opd)
              <option value="{{$opd->id}}">{{$opd->opd}}</option>
            @endforeach
            
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10">
          <input type="text" name="nama_acr" value="{{ old('nama_acr')}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Deskripsi Acara</label>
        <div class="col-sm-10">
          <textarea class="form-control" name="deskripsi" placeholder="Deskripsi Acara"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Tempat</label>
        <div class="col-sm-10">
          <input type="text" name="tempat_acr" value="{{ old('tempat_acr')}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-3">
          <input type="text" name="tgl_acr" value="{{ old('tgl_acr')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Acara" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="akhir_tgl_acr" value="{{ old('akhir_tgl_acr')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Acara" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10">
          <input type="text" name="pukul" value="{{ old('pukul')}}" class="form-control timepicker" placeholder="Waktu Acara" required>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-sm-2 control-label">Waktu Pendaftaran</label>
        <div class="col-sm-3">
          <input type="text" name="tanggal_awal" value="{{ old('tanggal_awal')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Awal" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="tanggal_akhir" value="{{ old('tanggal_akhir')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Akhir" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Durasi Tampil</label>
        <div class="col-sm-3">
          <input type="text" name="tampil_awal" value="{{ old('tampil_awal')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Tampil Awal" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="tampil_akhir" value="{{ old('tampil_akhir')}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Tampil Akhir" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Kuota Undangan</label>
        <div class="col-md-3 col-sm-10">
          <input type="text" name="kuota_undangan" value="{{ old('kuota_undangan')}}" class="form-control text-right" placeholder="0" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Kuota Umum</label>
        <div class="col-md-3 col-sm-10">
          <input type="text" name="kuota_umum" value="{{ old('kuota_umum')}}" class="form-control text-right" placeholder="0" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-10">
          Untuk Keperluan Dokumen
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Bagi</label>
        <div class="col-sm-10">
          <input type="text" name="bagi" value="{{ old('bagi')}}" class="form-control" placeholder="Bagi" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Di Lingkungan</label>
        <div class="col-sm-10">
          <input type="text" name="lingkungan" value="{{ old('lingkungan')}}" class="form-control" placeholder="Di Lingkungan" required>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary button-submit">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  $("ul.chosen-choices").css({ "overflow": 'auto', "max-height": "200px" });

  selectopd = $('.select-opd').selectize({})[0].selectize;
  selectopd.setValue({{old('opd')}});

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
