@extends('layouts.main')

@section('title', 'Acara')

@section('title-content')
Acara
@endsection

@include('plugins.eonasdan-datepicker')
@include('plugins.datatable')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Acaraa</a></li>
<li class="active">Show</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/acara')}}">List</a></li>
  @if(Auth::user()->role == ADMIN)
  <li class=""><a href="{{URL::to('/admin/acara/create')}}">Buat Acara</a></li>
  @endif
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class='row'>
        <div class='col-md-6'>
          <div class="row">
            <label class="col-sm-5 control-label">OPD</label>
            <div class="col-sm-7 control-label">
              @if(count($acara->opds) > 0)
                @foreach ($acara->opds as $key => $opd)
                  {{$key+1}}. {{$opd->opd}}<br>  
                @endforeach
              @else 
              -
              @endif
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Nama Acara</label>
            <div class="col-sm-7 control-label">
              {{$acara->nama_acr}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Deskripsi Acara</label>
            <div class="col-sm-7 control-label">
              {{$acara->deskripsi}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Tanggal Acara</label>
            <div class="col-sm-7 control-label">
              {{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Waktu Acara</label>
            <div class="col-sm-7 control-label">
              {{time_view($acara->pukul)}} WIB
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Tanggal Pendaftaran</label>
            <div class="col-sm-7 control-label">
              {{date_view($acara->tanggal_awal)}} - {{date_view($acara->tanggal_akhir)}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Durasi Tampil</label>
            <div class="col-sm-7 control-label">
              {{date_view($acara->tampil_awal)}} - {{date_view($acara->tampil_akhir)}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Kode Undangan</label>
            <div class="col-sm-7 control-label">
              {{$acara->code}}
            </div>
          </div>
          <br>
          <div class="row">
            <label class="col-sm-5 control-label">Kuota Undangan</label>
            <div class="col-sm-7 control-label">
              {{$acara->kuota_undangan}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Kuota Umum</label>
            <div class="col-sm-7 control-label">
              {{$acara->kuota_umum}}
            </div>
          </div>
          <br>
          <div class="row">
            <label class="col-sm-5 control-label" style="font-weight: normal;font-style: italic">Keperluan Dokumen</label>
            <div class="col-sm-7 control-label">
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Bagi</label>
            <div class="col-sm-7 control-label">
              {{$acara->bagi}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Di Lingkungan</label>
            <div class="col-sm-7 control-label">
              {{$acara->lingkungan}}
            </div>
          </div>
        </div>
        <div class='col-md-6'>
          <div class="row">
            <label class="col-sm-7 control-label">Jumlah Peserta</label>
            <div class="col-sm-5 control-label">
              {{$terdaftar}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-7 control-label">Jumlah Peserta yang Datang</label>
            <div class="col-sm-5 control-label">
              {{$check_in}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-7 control-label">Jumlah Peserta yang Sesuai SPT</label>
            <div class="col-sm-5 control-label">
              {{$sesuai_spt}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-7 control-label">Jumlah Peserta yang Tidak Sesuai SPT</label>
            <div class="col-sm-5 control-label">
              {{$tidak_sesuai_spt}}
            </div>
          </div>
          <br>
          <div class="row">
            <label class="col-sm-7 control-label">Jumlah Peserta Inap</label>
            <div class="col-sm-5 control-label">
              {{$inap}}
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-6 control-label" style="margin-bottom:5px">
              <a  style="width: 100%;text-align: left" data-toggle="modal" data-target="#exampleModal" class="btn btn-gray border--blue"><i class="fa fa-print"></i> Undangan - Kode Undangan</a>
            </div>
            <div class="col-lg-6 control-label" style="margin-bottom:5px">
              <a  style="width: 100%;text-align: left" data-toggle="modal" data-target="#modalUndanganUmum" class="btn btn-gray border--blue"><i class="fa fa-print"></i> Undangan - Pilih Acara</a>
            </div>
            <div class="col-lg-6 control-label" style="margin-bottom:5px">
              <a  style="width: 100%;text-align: left" target="_blank" href="{{route('admin.cetak-daftar-hadir',['id' => $acara->id_acr])}}" class="btn btn-gray border--blue"><i class="fa fa-print"></i> Daftar Hadir</a>
            </div>
            <div class="col-lg-6 control-label" style="margin-bottom:5px">
              <a  style="width: 100%;text-align: left" target="_blank" href="{{route('admin.cetak-konfirmasi-peserta',['id' => $acara->id_acr])}}" class="btn btn-gray border--blue"><i class="fa fa-print"></i> Konfirmasi Peserta</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
      <a href="{{route('bimtek.umum')}}" class="btn bg-yellow">Daftarkan Peserta Umum</a>
      <form style="display: inline-block;" method="POST" action="{{route('bimtek.pendaftaran.index')}}">
        <input type="hidden" name="token" value="{{$acara->code}}">
        {{ csrf_field()}}
        <button type="submit" class="btn bg-orange">Daftarkan Peserta Undangan</button>
      </form>
      <form style="display: inline-block;" method="POST" action="{{route('admin.acara.monitor',['id' => $acara->id_acr])}}">
        {{ csrf_field()}}
        <button type="submit" class="btn btn-info monitor">Tampilkan ke Monitor</button>
      </form>
    @if(Auth::user()->role == ADMIN)
      <form style="display: inline-block;" id="hapus" method="POST" action="{{URL::to('/admin/acara/'.$acara->id_acr)}}">
        {{ method_field('DELETE') }}
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Hapus</button>
        <a href="{{URL::to('/admin/acara/'.$acara->id_acr.'/edit')}}" class="btn btn-success">Ubah</a>
      </form>
    </div>
    @endif
  <!-- /.box-footer -->
</div>

<div class="box box-success">
  <div class="box-header with-border">
    <b>Peserta</b>
  </div>
  <div class="box-body">
    <div class="table-responsive">
    <table class='table' id="table-peserta">
      <thead>
        <tr>
          <th>No</th>
          <th></th>
          <th>Jenis</th>
          <th class="filter">NIP</th>
          <th class="filter">Nama</th>
          <th>L/P</th>
          <th class="filter">Opd</th>
          <th>Foto</th>
          <th>Check In</th>
          <th>Sesuai SPT</th>
          <th>Notif</th>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th><input type="text" class='form-control' id="search-nip"placeholder="NIP"></th>
          <th><input type="text" class='form-control' id="search-nama"placeholder="Nama"></th>
          <th></th>
          <th><input type="text" class='form-control' id="search-opd"placeholder="OPD"></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($acara->peserta) > 0)
        @php $count = 1; @endphp
        @foreach ($acara->peserta as $peserta)
          @if($peserta->status)
          <tr>
            <td>{{$count}}</td>
            <td>
              <form class="form-horizontal" method="POST" action="{{route('pendaftaran.download')}}">
                {{ csrf_field() }}
                <input type="hidden" value="{{$peserta->id}}" name="id">
                <button type="submit" class="btn btn-primary btn-block btn-flat btn-xs"><i class="fa fa-download"></i></button>
              </form>
            </td>
            <td nowrap>{{$peserta->jenis == 'grup-undangan' ? 'Grup Undangan' : ($peserta->jenis == 'grup-umum' ? 'Grup Umum': ucfirst($peserta->jenis))}}</td>

            <td>{{$peserta->id_pegawai == 0 ? $peserta->nip : $peserta->pegawai->nip}}</td>
            <td>{{$peserta->nama ?? $peserta->pegawai->nama}}</td>
            <td>{{$peserta->jenis_kelamin?? '-'}}</td>
            <td>{{$peserta->asal_instansi ?? $peserta->opd->opd}}</td>
            <td><a href="{{asset($peserta->foto)}}" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-file-image-o"></i></a> </td>
            <td nowrap>
              @if($peserta->check_in)
              {{datetime_view($peserta->check_in)}}
              @else
                <input type="hidden" value="{{$peserta->id}}" class="peserta-id">
                <button class='btn btn-xs btn-success check_in'>Check In</button>
              @endif
            </td>
            <td class="zone-spt" nowrap>
                @if($peserta->check_in)
                <input type="hidden" value="{{$peserta->id}}" class="peserta-id">
                <input type="hidden" value="{{$peserta->sesuai_spt}}" class="spt-default">
                <label class='{{$peserta->sesuai_spt == 0 ? 'radio_spt' : ''}}' style="margin-right: 15px">
                <input type="radio" value="1" name="sesuai_spt{{$peserta->id}}" {{$peserta->sesuai_spt == 1? 'checked' :''}}  {{$peserta->sesuai_spt == 0 ? '' : 'disabled'}}> Sesuai
                </label>
                <label class='{{$peserta->sesuai_spt == 0 ? 'radio_spt' : ''}}'>
                <input type="radio" value="-1" name="sesuai_spt{{$peserta->id}}" {{$peserta->sesuai_spt == -1? 'checked' :''}}  {{$peserta->sesuai_spt == 0 ? '' : 'disabled'}}> Tidak Sesuai
                </label>
                @endif
            </td>
            <td class="text-center zone-notif">
              @if($peserta->check_in)
              <input type="hidden" value="{{$peserta->id}}" class="peserta-id">
              <input type="checkbox" name="notif" {{$peserta->notif? 'checked' :''}} class='notify'>
              @endif
            </td>
          </tr>
          @php $count++; @endphp
          @endif
        @endforeach
      @else
        <tr>
          <td colspan="7" class="text-center"><i>Peserta belum ada</i></td>
        </tr>
      @endif
    </tbody>
    </table>
  </div>
  </div>
  <!-- /.box-footer -->
</div>

<div class="modal fade" id="modalUndanganUmum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form target="_blank" action="{{route('admin.cetak-undangan-umum',['id' => $acara->id_acr])}}" method="GET">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cetak Undangan Umum</h5>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label class="control-label">Kepada</label>
            <input type="text" name="kepada" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="control-label">Jumlah orang</label>
            <input type="number" name="jumlah" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="control-label">Nomor Surat</label>
            <input type="text" name="nomor_surat" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="control-label">Tanggal Surat</label>
            <input type="text" name="tgl_surat" class="form-control eonasdan-datepicker" required>
          </div>
          <div class="form-group">
            <label class="control-label">Tanda Tangan</label>
            <div class="radio">
              <label style="margin-right: 10px"><input type="radio" value="1" name="ttd">Ya</label>
              <label><input type="radio" value="0" name="ttd" checked>Tidak</label>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form target="_blank" action="{{route('admin.cetak-undangan-opd',['id' => $acara->id_acr])}}" method="GET">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Cetak Undangan OPD</h5>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label class="control-label">Kepada</label>
            <input type="text" name="kepada" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="control-label">Nomor Surat</label>
            <input type="text" name="nomor_surat" class="form-control" required>
          </div>
          <div class="form-group">
            <label class="control-label">Tanggal Surat</label>
            <input type="text" name="tgl_surat" class="form-control eonasdan-datepicker" required>
          </div>
          <div class="form-group">
            <label class="control-label">Tanda Tangan</label>
            <div class="radio">
              <label style="margin-right: 10px"><input type="radio" value="1" name="ttd">Ya</label>
              <label><input type="radio" value="0" name="ttd" checked>Tidak</label>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('js') @parent
<script>
  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  })
  $(function () {
    bind_btn();
  
    @if(count($acara->peserta) > 0)
    var table = $('#table-peserta').DataTable( {
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   true,
        ordering: false
    } );
 
    // Filter event handler

    $('#search-nip').on( 'keyup', function () {
        table
            .columns( 3 )
            .search( this.value )
            .draw();
    });
    $('#search-nama').on( 'keyup', function () {
        table
            .columns( 4 )
            .search( this.value )
            .draw();
    });
    $('#search-opd').on( 'keyup', function () {
        table
            .columns( 6 )
            .search( this.value )
            .draw();
    });

    $('.check_in').on('click', function(event) {
      td = $(this).closest('td');
      that = $(this);
      id_peserta = td.find('.peserta-id').val();
      $.ajax({
        url: '{{route('admin.force_check_in')}}',
        type: 'POST',
        data: {
          _token: '{{ csrf_token() }}',
          id_peserta: id_peserta
        },
      })
      .done(function(response) {
        $(that).closest('tr').find('.zone-spt').html('<input type="hidden" value="'+id_peserta+'" class="peserta-id"><input type="hidden" value="" class="spt-default"><label class="radio_spt" style="margin-right: 15px"><input type="radio" value="1" name="sesuai_spt'+id_peserta+'"> Sesuai</label><label class="radio_spt"><input type="radio" value="-1" name="sesuai_spt'+id_peserta+'"> Tidak Sesuai</label>');
        $(that).closest('tr').find('.zone-notif').html('<input type="hidden" value="'+id_peserta+'" class="peserta-id"><input type="checkbox" name="notif" class="notify">');
        $(that).closest('td').html(response.data);
        bind_btn();
      })
      .fail(function(response) {
        console.log(response);
        if(response.code == 500){
          swal("Ooops..", response.message, "error");
        } else {
          swal("Ooops..", "Internal Server Error", "error");
        }
      });
    });
    @endif

    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menghapus data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  function bind_btn(){
    $('.radio_spt').on('click', function(event) {
      td = $(this).closest('td');
      that = $(this);
      value = $(this).find('input').val();
      id_peserta = td.find('.peserta-id').val();
      console.log(value);
      if(value == 1){
        swal({
          title: "Apakah anda yakin mengaktivasi akun ini ?",
          text: "Data yang sudah disimpan tidak akan bisa diubah kembali",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#00a65a",
          confirmButtonText: "Ya!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(inputValue){
          if(inputValue){
            $.ajax({
              url: '{{route('admin/cek-spt')}}',
              type: 'POST',
              data: {
                _token: '{{ csrf_token() }}',
                id_peserta: id_peserta,
                value: value
              },
            })
            .done(function(response) {
              console.log(response);
              $(that).parent().find('.radio_spt').unbind();
              def = td.find('.spt-default').val();
              td.find('input[type="radio"]').prop('disabled',true);
            })
            .fail(function(response) {
              console.log(response);
              if(response.code == 500){
                swal("Ooops..", response.message, "error");
              } else {
                swal("Ooops..", "Internal Server Error", "error");
              }
              
              def = td.find('.spt-default').val();
              td.find('input[type="radio"]').prop('checked',false);
              td.find('input[value="'+def+'"]').prop('checked',true);
            });
          } else {
            def = td.find('.spt-default').val();
            td.find('input[type="radio"]').prop('checked',false);
          }
        });
      } else {
        swal({
          title: "Apakah anda yakin tidak mengaktivasi akun ini ?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(inputValue){
          if(inputValue){
            $.ajax({
              url: '{{route('admin/cek-spt')}}',
              type: 'POST',
              data: {
                _token: '{{ csrf_token() }}',
                id_peserta: id_peserta,
                value: value
              },
            })
            .done(function(response) {
              console.log('dor');
            })
            .fail(function(response) {
              console.log(response);
              if(response.code == 500){
                swal("Ooops..", response.message, "error");
              } else {
                swal("Ooops..", "Internal Server Error", "error");
              }
              
              def = td.find('.spt-default').val();
              td.find('input[type="radio"]').prop('checked',false);
              td.find('input[value="'+def+'"]').prop('checked',true);
            });
          } else {
            def = td.find('.spt-default').val();
            td.find('input[type="radio"]').prop('checked',false);
          }
        });
      }
    });

    $('.notify').on('click', function(event) {
      td = $(this).closest('td');
      that = $(this);
      id_peserta = td.find('.peserta-id').val();
      $.ajax({
        url: '{{route('admin.notify')}}',
        type: 'POST',
        data: {
          _token: '{{ csrf_token() }}',
          id_peserta: id_peserta
        },
      })
      .done(function(response) {
        $(that).closest('td').find('.notify').prop('checked', response.data);
      })
      .fail(function(response) {
        console.log(response);
        if(response.code == 500){
          swal("Ooops..", response.message, "error");
        } else {
          swal("Ooops..", "Internal Server Error", "error");
        }
      });
    });
  }

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('monitor_success'))
  swal("Acara berhasil ditampilkan ke monitor", "Note: Jika monitor tidak merespon, refresh page pada monitor", "success");
  @endif
  
</script>
@endsection
