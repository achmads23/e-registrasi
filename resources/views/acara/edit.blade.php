@extends('layouts.main')

@section('title', 'Acara')
@include('plugins.eonasdan-datepicker')
@include('plugins.timepicker')

@section('title-content')
Acara
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Acara</a></li>
<li class="active">Edit Acara</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/acara')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/acara/create')}}">Buat Acara</a></li>
  <li class="active"><a>Edit</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/acara/'.$acara->id_acr)}}">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="form-group">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-sm-10">
          <select name="opd[]" class="select-opd" placeholder="Pilih OPD" multiple>
            @php
              $id_opds = $acara->id_opds;
              $id_opds = explode(';', $id_opds);
            @endphp

            @foreach ($opds as $opd)
              <option value="{{$opd->id}}" {{in_array($opd->id,$id_opds)?'selected':''}}>{{$opd->opd}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10">
          <input type="text" name="nama_acr" value="{{ old('nama_acr',$acara->nama_acr)}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Deskripsi Acara</label>
        <div class="col-sm-10">
          <textarea class="form-control" name="deskripsi" placeholder="Deskripsi Acara">{{ old('deskripsi',$acara->deskripsi)}}</textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Tempat</label>
        <div class="col-sm-10">
          <input type="text" name="tempat_acr" value="{{ old('tempat_acr',$acara->tempat_acr)}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-3">
          <input type="text" name="tgl_acr" value="{{ old('tgl_acr',date_view($acara->tgl_acr))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Acara" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="akhir_tgl_acr" value="{{ old('akhir_tgl_acr',date_view($acara->akhir_tgl_acr))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Acara" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10">
          <input type="text" name="pukul" value="{{ old('pukul',$acara->pukul)}}" class="form-control timepicker" placeholder="Waktu Acara" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Waktu Pendaftaran</label>
        <div class="col-sm-3">
          <input type="text" name="tanggal_awal" value="{{ old('tanggal_awal',date_view($acara->tanggal_awal))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Awal" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="tanggal_akhir" value="{{ old('tanggal_akhir',date_view($acara->tanggal_akhir))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Akhir" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Durasi Tampil</label>
        <div class="col-sm-3">
          <input type="text" name="tampil_awal" value="{{ old('tampil_awal',date_view($acara->tampil_awal))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Tampil Awal" required>
        </div>
        <div class="col-sm-1 text-center">
          <label class="control-label">Sampai</label>
        </div>
        <div class="col-sm-4">
          <input type="text" name="tampil_akhir" value="{{ old('tampil_akhir',date_view($acara->tampil_akhir))}}" class="form-control eonasdan-datepicker" placeholder="Tanggal Tampil Akhir" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Kuota Undangan</label>
        <div class="col-md-3 col-sm-10">
          <input type="text" name="kuota_undangan" value="{{ old('kuota_undangan',$acara->kuota_undangan)}}" class="form-control text-right" placeholder="0" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Kuota Umum</label>
        <div class="col-md-3 col-sm-10">
          <input type="text" name="kuota_umum" value="{{ old('kuota_umum',$acara->kuota_umum)}}" class="form-control text-right" placeholder="0" required>
        </div>
      </div>
       <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-10">
          Untuk Keperluan Dokumen
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Bagi</label>
        <div class="col-sm-10">
          <input type="text" name="bagi" value="{{ old('bagi',$acara->bagi)}}" class="form-control" placeholder="Bagi" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Di Lingkungan</label>
        <div class="col-sm-10">
          <input type="text" name="lingkungan" value="{{ old('lingkungan',$acara->lingkungan)}}" class="form-control" placeholder="Di Lingkungan" required>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary button-submit">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  selectopd = $('.select-opd').selectize({})[0].selectize;

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
