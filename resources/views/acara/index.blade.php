@extends('layouts.main')

@section('title', 'Acara')
@include('plugins.datatable')
@section('title-content')
Acara
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Acara</li>
<li class="active">List</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/acara')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/acara/create')}}">Buat Acara</a></li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th >No</th>
          <th class="col-md-3">Nama Acara</th>
          <th class="col-md-2">Tanggal Acara</th>
          <th class="col-md-3">Tanggal Pendaftaran</th>
          <th class="col-md-1">Kode Undangan</th>
          <th class="text-center">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($acara as $key => $d)
        <tr style="{{$monitor->value == $d->id_acr ? 'background-color:green !important; color:white !important;' : ''}}">
          <td>{{$key+1}}</td>
          <td>
            {{$d->nama_acr}}</td>
          <td>{{date_view($d->tgl_acr)}} - {{date_view($d->akhir_tgl_acr)}}</td>
          <td>{{date_view($d->tanggal_awal)}} - {{date_view($d->tanggal_akhir)}}</td>
          <td>{{$d->code}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/acara/'.$d->id_acr)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection