@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')
<div class="login-box disable-margin-top">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h3 class="text-center disable-margin">Registrasi Sukses</h3>
    <br>
    <img src="{{asset($peserta->qrcode)}}" width="100%">
    <br>
    <br>
    <p class="text-center">Kode Pesanan</p>
    <table style="font-size:20px;margin:auto;">
      <tr>
        <td>Username</td>
        <td>:</td>
        <td style="font-weight: bold;padding-left: 10px;">{{$peserta->username}}</td>
      </tr>
      <tr>
        <td>Password</td>
        <td>:</td>
        <td style="font-weight: bold;padding-left: 10px;">{{$peserta->password}}</td>
      </tr>
    </table>
    <hr>
    <div class="row">
      <div class="col-xs-6">
        <a href="{{URL::to('/')}}" class="btn btn-default btn-block btn-flat">
          <i class="fa fa-chevron-left"></i> Kembali Ke Awal
        </a>
      </div>
      <div class="col-xs-6">
        <form class="form-horizontal" method="POST" action="{{route('pendaftaran.download')}}">
          {{ csrf_field() }}
          <input type="hidden" value="{{$peserta->id}}" name="id">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Download <i class="fa fa-download"></i></button>
        </form>
      </div>
      <br>
      <!-- /.col -->
    </div>
  </div>
  <br>
  <div class="text-center" style="margin-bottom: 100px">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
    </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
