@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="col-md-6 col-md-offset-3">
  <div class="box" style="border-top:none">
    <div class="box-body">
      <div class='col-md-12'>
      <form class="form-horizontal" method="POST" action="{{route('bimtek.pendaftaran.register')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class='text-center'>
        <h4 class="disable-margin"><b><i>Selamat Datang Calon Peserta</i></b></h4>
        </div>
        <br>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Kode Undangan</label>
          <div class="col-sm-8 control-label">
            {{$acara->code}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Acara</label>
          <div class="col-sm-8 control-label">
            {{$acara->nama_acr}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Tempat Acara</label>
          <div class="col-sm-8 control-label">
            {{$acara->tempat_acr}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Tanggal Acara</label>
          <div class="col-sm-8 control-label">
            {{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Waktu Acara</label>
          <div class="col-sm-8 control-label">
            {{time_view($acara->pukul)}} WIB
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">Tanggal Pendaftaran</label>
          <div class="col-sm-8 control-label">
            {{date_view($acara->tanggal_awal) . ' - ' . date_view($acara->tanggal_akhir)}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-sm-4 control-label disable-padding-left">OPD</label>
          <div class="col-sm-8 control-label">
            @if(count($acara->opds) > 0)
              @foreach ($acara->opds as $key => $opd)
              {{$key+1}}. {{$opd->opd}}<br>  
              @endforeach
            @else 
              -
            @endif
          </div>
        </div>
        <hr>
        <div class="form-group">
          <label class="col-sm-4 control-label">NIP<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <input type="text" name="nip" id="nip" value="{{ old('nip')}}" class="form-control" placeholder="NIP" required>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Nama<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <input type="text" name="nama" id="nama" placeholder="Nama" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Jenis Kelamin<span class="text-red">*</span></label>
          <div class="col-sm-8 control-label">
            <label>
              <input type="radio" name="jenis_kelamin" value="L"> Laki-laki
            </label>
            <br>
            <label>
              <input type="radio" name="jenis_kelamin" value="P"> Perempuan
            </label>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Asal Instansi<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <input type="text" name="asal_instansi" id="asal_instansi" placeholder="Asal Instansi" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Golongan<span class="text-red">*</span></label>
          <div class="col-sm-8 row-golongan">
            <select name="golongan" class="select-golongan" id='golongan' placeholder="Gol">
                <option value="" selected disabled>Gol</option>
                <option value="IA">IA</option>
                <option value="IB">IB</option>
                <option value="IC">IC</option>
                <option value="ID">ID</option>
                <option value="IIA">IIA</option>
                <option value="IIB">IIB</option>
                <option value="IIC">IIC</option>
                <option value="IID">IID</option>
                <option value="IIIA">IIIA</option>
                <option value="IIIB">IIIB</option>
                <option value="IIIC">IIIC</option>
                <option value="IIID">ID</option>
                <option value="IVA">IVA</option>
                <option value="IVB">IVB</option>
                <option value="IVC">IVC</option>
                <option value="IVD ">IVD</option>
              </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Jabatan<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <select name="jabatan" class="select-jabatan" id="jabatan" placeholder="Pilih Jabatan" required>
              <option value="PA">PA</option>
              <option value="KPA">KPA</option>
              <option value="PPK">PPK</option>
              <option value="BP ">BP </option>
              <option value="BPP">BPP</option>
              <option value="PPKOM">PPKOM</option>
              <option value="PPTK">PPTK</option>
              <option value="Administrasi Keuangan">Administrasi Keuangan</option>
              <option value="Pengurus Barang Aset">Pengurus Barang Aset</option>
              <option value="Pengurus Barang Persediaan">Pengurus Barang Persediaan</option>
              <option value="Pemegang Barang Aset">Pemegang Barang Aset</option>
              <option value="Lain-lain">Lain-lain</option>
            </select>
            <div id="input-jabatan" class="hidden">
              <input type="text" name="jabatan_text" class="form-control" placeholder="Jabatan">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Email<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <input type="email" name="email" class="form-control"  id="email" placeholder="Email" required>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">No HP<span class="text-red">*</span></label>
          <div class="col-sm-8">
            <input type="text" name="hp"  id="hp" class="form-control" placeholder="No HP" required>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Kartu Identitas</label>
          <div class="col-sm-8">
            <input type="file" name="foto" class="form-control">
            <input type="hidden" id="code" name="code" value="{{$acara->code}}">
          </div>
        </div>
        <hr>
        <div class="form-group text-center">
          <label class="col-sm-12">Kami menyediakan tempat menginap, apakah anda ingin menginap ?</label>
        </div>
        <div class="form-group text-center">
          <label class="col-sm-3 col-sm-offset-3">
            <input type="radio" name="inap" value="1"> Ya
          </label>
          <label class="col-sm-3">
            <input type="radio" name="inap" value="0" checked> Tidak
          </label>
        </div>
      </form>
      <div class="box-footer text-right">
          <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
          <button type="button" id="simpan" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
  <div class="text-center" style="margin-bottom: 100px">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>
<script>
  $(function() {
    selectjabatan = $('.select-jabatan').selectize({})[0].selectize;
    selectjabatan.setValue({{old('jabatan')}});
    selectjabatan.on('change', function() {
      var value = selectjabatan.getValue();
      if(value == 'Lain-lain'){
        $('#input-jabatan').removeClass('hidden');
      } else {
        $('#input-jabatan').addClass('hidden');
      }
    });

    selectgolongan = $('.select-golongan').selectize({})[0].selectize;
    selectgolongan.setValue({{old('golongan')}});

    $('#simpan').on('click',function(){
      if($('#nip').val() && $('#jabatan').val() && $('#email').val() && $('#hp').val() && $('#nama').val() && $('#asal_instansi').val() && $('#golongan').val() && $('input[name="jenis_kelamin"]:checked').val()){
    		$( ".loading" ).show();
    		$('form').submit();
  	  } else{
        swal("Ooops!", "Data tidak lengkap", "warning");
      }
      
    });
    $('.has-error').keypress(function(){
      $(this).removeClass('has-error');
      $('#error-message').remove();
      $(this).find('.help-block').hide();
    });

    $('#nip').keyup(function(){
      let nip = $(this).val().replace(/\s/g, '');
      if($(this).val() && nip.length == 18){
        $( ".loading" ).show();
        $.ajax({
          url: '{{route('ajax-cek-peserta')}}',
          type: 'POST',
          data: {_token:'{{ csrf_token() }}',nip: nip, code:$('#code').val()},
        })
        .done(function(data) {
          console.log(data);
          $( ".loading" ).hide();
          if(data.data != false){
            if(data.data.is_peserta){
              if(data.data.peserta.status == 1){
                swal({
                  text: "NIP sudah terdaftar sebagai peserta!",
                  icon: "warning",
                  buttons: {
                    cancel: "Coba Lagi",
                    catch: {
                      text: "Lihat",
                      value: "catch",
                    },
                  },
                })
                .then((value) => {
                  switch (value) {
                    case "catch":
                      window.location = '{{route('pendaftaran.peserta')}}' + '/' + data.data.peserta.id;
                      break;
                 
                    default:
                      $('#nip').val('');
                      $('#asal_instansi').val('');
                      $('#golongan').val('');
                      $('#nama').val('');
                  }
                });
              } else if(data.data.peserta.status == 0){
                swal({
                  text: "NIP sedang dalam proses verifikasi!",
                  icon: "warning",
                  buttons: {
                    cancel: "Tutup",
                    catch: {
                      text: "Kembali ke Awal",
                      value: "catch",
                    },
                  },
                }).then((value) => {
                  switch (value) {
                    case "catch":
                      window.location = '{{route('bimtek-scan')}}';
                      break;
                 
                    default:
                      $('#nip').val('');
                      $('#asal_instansi').val('');
                      $('#golongan').val('');
                      $('#nama').val('');
                  }
                });
              } else if(data.data.peserta.status == -1){
                swal({
                  text: "Verifikasi NIP anda ditolak!",
                  icon: "warning",
                  buttons: {
                    cancel: "Tutup",
                    catch: {
                      text: "Kembali ke Awal",
                      value: "catch",
                    },
                  },
                }).then((value) => {
                  switch (value) {
                    case "catch":
                      window.location = '{{route('bimtek-scan')}}';
                      break;
                 
                    default:
                      $('#nip').val('');
                      $('#asal_instansi').val('');
                      $('#golongan').val('');
                      $('#nama').val('');
                  }
                });
              }
            } else {
              if(data.data.pegawai || {{count($acara->opds)}} == 0){
                $('#nama').val(data.data.pegawai.nama);
                $('#asal_instansi').val(data.data.pegawai.opd.opd);
                $('#golongan').val(data.data.pegawai.gol);
              } else {
                swal({
                  title:'Oooops!',
                  text: "OPD anda ("+ data.data.opd +") tidak terdaftar!",
                  icon: "warning",
                  buttons: {
                    cancel: "tutup",
                    catch: {
                      text: "Bukan OPD anda? Klik Disini",
                      value: "konfirmasi",
                    },
                  },
                }).then((value) => {
                  switch (value) {
                    case "konfirmasi":
                      window.location.href = '{{route('pendaftaran.konfirmasi_perubahan_opd')}}' + "?nip=" + nip + '&code=' + $('#code').val();
                      break;
                    default:
                      $('#nip').val('');
                      $('#asal_instansi').val('');
                      $('#golongan').val('');
                      $('#nama').val('');
                  }
                });
                $('#asal_instansi').val('');
                $('#golongan').val('');
                $('#nama').val('');
              }
            }
          }
        })
        .fail(function(error) {
          $( ".loading" ).hide();
          console.log(error)
          console.log("error");
        })
        .always(function() {
          $( ".loading" ).hide();
          console.log("complete");
        });
      } else {
        $('#asal_instansi').val('');
        $('#golongan').val('');
        $('#nama').val('');
      }
    });
  });
</script>

@endsection
