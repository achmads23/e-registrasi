@extends('layouts.empty')

@section('title', 'Pendaftaran')


@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="col-md-6 col-md-offset-3">
  <div class="box" style="border-top:none">
    <div class="box-body">
      <div class='col-md-12'>
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class='text-center'>
            <h4 class="disable-margin"><b><i>Konfirmasi Perbedaan OPD</i></b></h4>
          </div>
          <br>
          <div class="form-group disable-margin">
            <label class="col-sm-3 control-label">NIP</label>
            <div class="col-sm-9 control-label">
              {{$pegawai->nip}}
            </div>
          </div>
          <div class="form-group disable-margin">
            <label class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9 control-label">
              {{$pegawai->nama}}
            </div>
          </div>
          <div class="form-group disable-margin">
            <label class="col-sm-3 control-label">OPD</label>
            <div class="col-sm-9 control-label">
              {{$pegawai->opd->opd}}
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label class="col-sm-3 control-label">OPD Terbaru<span class="text-red">*</span></label>
            <div class="col-sm-9">
              <select name="opd" class="select-opd" id="opd" placeholder="Pilih OPD" required>
                @foreach ($opds as $opd)
                <option value="{{$opd->id}}">{{$opd->opd}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">File SK<span class="text-red">*</span></label>
            <div class="col-sm-9">
              <input type="file" name="filesk" class="form-control" id="filesk">
              <input type="hidden" id="code" name="code" value="{{$acara->code}}">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Jabatan<span class="text-red">*</span></label>
            <div class="col-sm-9">
              <select name="jabatan" class="select-jabatan" id="jabatan" placeholder="Pilih Jabatan" required>
                <option value="PA">PA</option>
                <option value="KPA">KPA</option>
                <option value="PPK">PPK</option>
                <option value="BP ">BP </option>
                <option value="BPP">BPP</option>
                <option value="PPKOM">PPKOM</option>
                <option value="PPTK">PPTK</option>
                <option value="Administrasi Keuangan">Administrasi Keuangan</option>
                <option value="Pengurus Barang Aset">Pengurus Barang Aset</option>
                <option value="Pengurus Barang Persediaan">Pengurus Barang Persediaan</option>
                <option value="Pemegang Barang Aset">Pemegang Barang Aset</option>
                <option value="Lain-lain">Lain-lain</option>
              </select>
              <div id="input-jabatan" class="hidden">
                <input type="text" name="jabatan_text" class="form-control" placeholder="Jabatan">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Kartu Identitas</label>
            <div class="col-sm-9">
              <input type="file" name="foto" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Email<span class="text-red">*</span></label>
            <div class="col-sm-9">
              <input type="email" name="email" class="form-control"  id="email" required placeholder="Email">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">No HP<span class="text-red">*</span></label>
            <div class="col-sm-9">
              <input type="text" name="hp"  id="hp" class="form-control" required placeholder="No HP">
            </div>
          </div>
          <hr>
        <div class="form-group text-center">
          <label class="col-sm-12">Kami menyediakan tempat menginap, apakah anda ingin menginap ?</label>
        </div>
        <div class="form-group text-center">
          <label class="col-sm-3 col-sm-offset-3">
            <input type="radio" name="inap" value="1"> Ya
          </label>
          <label class="col-sm-3">
            <input type="radio" name="inap" value="0" checked> Tidak
          </label>
        </div>
          <div class="box-footer text-right" style="padding:0 !important">
            <a href="{{route('bimtek.undangan')}}" class="btn btn-default">Batal</a>
            <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="text-center" style="margin-bottom: 100px">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>

<script>
  $(function() {
    $("ul.chosen-choices").css({ "overflow": 'auto', "max-height": "200px" });

     $('#simpan').on('click',function(){
 

      if($('opd').val() && $('filesk').val() && $('jabatan').val() && $('email').val() && $('hp').val()){
        $( ".loading" ).show();
        $('form').submit();
      } else{
        swal("Ooops!", "Data tidak lengkap", "warning");
      }
      
    });

    selectjabatan = $('.select-jabatan').selectize({})[0].selectize;
    selectjabatan.setValue({{old('jabatan')}});
    selectjabatan.on('change', function() {
      var value = selectjabatan.getValue();
      if(value == 'Lain-lain'){
        $('#input-jabatan').removeClass('hidden');
      } else {
        $('#input-jabatan').addClass('hidden');
      }
    });

    selectopd = $('.select-opd').selectize({})[0].selectize;
    selectopd.setValue({{old('opd')}});
  });


</script>

@endsection
