@extends('layouts.main')

@section('title', 'Konfirmasi Peserta')

@section('title-content')
Konfirmasi Peserta
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Konfirmasi Peserta</a></li>
<li class="active">Detail</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/konfirmasi-peserta')}}">List</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10 control-label">
          {{$peserta->pegawai->nip}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta->acara->nama_acr}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-10 control-label">
          {{date_view($peserta->acara->tgl_acr)}} - {{date_view($peserta->acara->akhir_tgl_acr)}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10 control-label">
          {{time_view($acara->pukul)}} WIB
        </div>
      </div>
      @if($peserta->pegawai->opd->opd == $peserta->opd->opd)
      <div class="row">
        <label class="col-sm-2 control-label">OPD Lama</label>
        <div class="col-sm-10 control-label">
          {{$peserta->pegawai->opd->opd}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">OPD Baru</label>
        <div class="col-sm-10 control-label">
          {{$peserta->opd->opd}}
        </div>
      </div>
      @else
      <div class="row">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-sm-10 control-label">
          {{$peserta->opd->opd}}
        </div>
      </div>
      @endif
      <div class="row">
        <label class="col-sm-2 control-label">File SK</label>
        <div class="col-sm-10 control-label">
          <a href="{{ asset($peserta->file) }}" target="_blank">Download File SK</a>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nomor HP</label>
        <div class="col-sm-10 control-label">
          {{$peserta->no_telp}}
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
      <form style="display: inline !important;" id="hapus" method="POST" action="{{URL::to('/admin/konfirmasi-peserta/'.$peserta->id.'/tolak')}}">
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Verifikasi Ditolak</button>
      </form>
      <form style="display: inline !important;" id="setuju" method="POST" action="{{URL::to('/admin/konfirmasi-peserta/'.$peserta->id.'/setuju')}}">
        {{ csrf_field()}}
        <button type="button" class="btn btn-success setuju">Verifikasi Diterima</button>
      </form>
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menolak data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Tolak!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  $(function () {
    $(".setuju").click(function(){
      swal({
        title: "Apakah yakin menyetujui data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#00a65a",
        confirmButtonText: "Ya, Setuju!",
        closeOnConfirm: false
      },
      function(){
        $('form#setuju').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
