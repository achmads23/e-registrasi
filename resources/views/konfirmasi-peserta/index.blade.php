@extends('layouts.main')

@section('title', 'Konfirmasi Peserta')
@include('plugins.datatable')
@section('title-content')
Konfirmasi Peserta
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Konfirmasi Peserta</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/konfirmasi-peserta')}}">List</a></li>
  </li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th style="width:5%">No</th>
          <th class="col-md-2">NIP</th>
          <th class="col-md-2">Nama Acara</th>
          <th class="col-md-3">OPD Baru</th>
          <th class="col-md-2">Waktu Pendaftaran</th>
          <th style="width:5%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pesertas as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->pegawai->nama}}</td>
          <td>{{$d->acara->nama_acr}}</td>
          <td>{{$d->opd->opd}}</td>
          <td>{{date_view($d->created_at)}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/konfirmasi-peserta/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="box">
  <div class="box-header with-border">
    Verifikasi Ditolak
  </div>
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th style="width:5%">No</th>
          <th class="col-md-2">NIP</th>
          <th class="col-md-2">Nama Acara</th>
          <th class="col-md-3">OPD Baru</th>
          <th class="col-md-2">Waktu Verifikasi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pesertas_ditolak as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->pegawai->nama}}</td>
          <td>{{$d->acara->nama_acr}}</td>
          <td>{{$d->opd->opd}}</td>
          <td>{{date_view($d->updated_at)}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('tolak_success'))
  swal("Menolak verifikasi data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('setuju_success'))
  swal("Menyetujui verifikasi data berhasil", "Data berhasil disimpan", "success");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection