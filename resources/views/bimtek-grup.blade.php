@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')

<div class="text-center" style="margin: 100px 0px 0px 0px;">
    <h4><b>Bimbingan Teknis</b></h4>
</div>
<div class="text-center row-zone" style="margin: 0px 0px 0px 0px;">
  <br>
  <a href="{{route('bimtek.grup-undangan')}}" class='btn-zone'>
    <img src="{{asset('images/Undangan.png')}}">
    <h4 class="text-black"><b>Grup Undangan</b></h4>
  </a>
  <a href="{{route('bimtek.grup-umum')}}" class='btn-zone'>
    <img src="{{asset('images/umum.png')}}">
    <h4 class="text-black"><b>Grup Umum</b></h4>
  </a>
</div>
<div class="text-center" style="margin: 0px 0px 30px 0px;">
  <a href="{{route('index')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
</div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script>
$(function() {
  @if (session('kuota_habis'))
  swal("Oooops!", "Kuota Habis", "error");
  @endif

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
