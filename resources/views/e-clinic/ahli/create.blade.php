@php use App\User; @endphp
@extends('layouts.main')

@section('title', 'Ahli')

@section('title-content')
Ahli
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>eClinic</li>
<li>Ahli</li>
<li class="active">Buat Akun Ahli</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/e-clinic/ahli')}}">List</a></li>
  <li class="active"><a href="{{URL::to('/admin/e-clinic/ahli/create')}}">Buat Akun Ahli</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/e-clinic/ahli')}}">
      {{ csrf_field() }}

      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      <div class="form-group">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
          <input type="text" name="nip" value="{{ old('nip')}}" class="form-control" placeholder="NIP" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" name="name" value="{{ old('name')}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
          <label class="col-sm-2 control-label">Jenis Kelamin</label>
          <div class="col-sm-10 control-label">
            <label style='margin-right: 10px'>
              <input type="radio" name="jenis_kelamin" value="L" {{ old("jenis_kelamin") == 'L' ? 'checked' : ''}}> Laki-laki
            </label>
            <label>
              <input type="radio" name="jenis_kelamin" value="P" {{ old("jenis_kelamin") == 'P' ? 'checked' : ''}}> Perempuan
            </label>
          </div>
        </div>
      <div class="form-group @if(session()->get('email_error')) has-error @endif">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="text" name="email" value="{{ old('email')}}" class="form-control" placeholder="Email" required>
          @if(session()->get('email_error'))
          <span class="help-block">{{session()->get('email_error')}}</span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Asal Instansi</label>
        <div class="col-sm-10">
          <input type="text" name="opd" value="{{ old('opd')}}" class="form-control" placeholder="Asal Instansi" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
          <input type="text" name="jabatan" value="{{ old('jabatan')}}" class="form-control" placeholder="Jabatan" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">HP</label>
        <div class="col-sm-10">
          <input type="text" name="hp" value="{{ old('hp')}}" class="form-control" placeholder="HP" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
          <input type="password" name="password" value="{{ old('password')}}" class="form-control" placeholder="Password" required>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">Kategori</label>
        <div class="col-sm-10">
          <select type="text" name="category[]" value="" class="selectize-category" placeholder="Pilih Kategori" required multiple="true">
            <option value=""></option>
            {!! $categories !!}
          </select>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  selectizecategory = $('.selectize-category').selectize({})[0].selectize;
  selectizecategory.setValue({{old('category')}});

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
