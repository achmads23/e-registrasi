@extends('layouts.main')

@section('title', 'Ahli')
@include('plugins.datatable')
@section('title-content')
Ahli
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>eClinic</li>
<li>Ahli</li>
<li class="active">List</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/e-clinic/ahli')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/e-clinic/ahli/create')}}">Buat Akun Ahli</a></li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th width="10%">No</th>
          <th width="20%" >Nama Ahli</th>
          <th width="30%">Jumlah Kategori</th>
          <th width="20%">Status</th>
          <th width="10%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $key => $d)
        {{-- {{dd($d)}} --}}
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->name}}</td>
          <td>
            {{$d->n_category}}
          </td>
          <td>{{$d->is_verified ? 'Aktif' : 'Email Belum Divalidasi'}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/e-clinic/ahli/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection