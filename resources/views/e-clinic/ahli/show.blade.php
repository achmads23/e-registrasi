@extends('layouts.main')

@section('title', 'Ahli')

@section('title-content')
Ahli
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>eClinic</li>
<li>Ahli</li>
<li class="active">Detil</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/e-clinic/ahli')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/e-clinic/ahli/create')}}">Buat Akun Ahli</a></li>
  <li class="active"><a href="#">Detil</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->nip}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Ahli</label>
        <div class="col-sm-10 control-label">
          {{$user->name}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan'}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10 control-label">
          {{$user->email}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Asal Instansi</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->opd?? '-'}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->jabatan?? '-'}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">HP</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->hp?? '-'}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Kategori</label>
        <div class="col-sm-10 control-label">
          <ol>
          @foreach ($user->ahli->categories as $element)
            <li>{{$element}}</li>
          @endforeach
          </ol>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10 control-label">
          {{$user->is_verified == 1? 'Aktif': ( $user->is_verified == 0? 'Belum Aktif' : ($user->is_verified == -1? 'Non-Aktif': ''))}}
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
      <form id="hapus" method="POST" action="{{URL::to('/admin/e-clinic/ahli/'.$user->id)}}" style="float:right">
        {{ method_field('DELETE') }}
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Hapus</button>
        <a href="{{URL::to('/admin/e-clinic/ahli/'.$user->id.'/edit')}}" class="btn btn-success">Ubah</a>
      </form>
      @if($user->is_verified == 1)
      <form id="non-aktif" method="POST" action="{{URL::to('/admin/e-clinic/ahli/'.$user->id . '/non-aktifkan')}}" style="float:right;margin-right: 4px">
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger non-aktif">Non Aktifkan</button>
      </form>
      @endif

      @if($user->is_verified == -1 || $user->is_verified == 0)
      <form id="aktif" method="POST" action="{{URL::to('/admin/e-clinic/ahli/'.$user->id . '/aktifkan')}}"  style="float:right;margin-right: 4px">
        {{ csrf_field()}}
        <button type="button" class="btn btn-success aktif">Aktifkan</button>
      </form>
      @endif
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menghapus data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
    $(".aktif").click(function(){
      swal({
        title: "Apakah anda yakin mengaktifkan akun ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#00a65a",
        confirmButtonText: "Ya, Aktifkan",
        closeOnConfirm: false
      },
      function(){
        $('form#aktif').submit();
      });
    });
    $(".non-aktif").click(function(){
      swal({
        title: "Apakah anda yakin menon-aktifkan akun ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Non Aktifkan",
        closeOnConfirm: false
      },
      function(){
        $('form#non-aktif').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('aktif_success'))
  swal("Ahli berhasil diaktifkan", "Data berhasil disimpan", "success");
  @endif
  @if (session('non_aktif_success'))
  swal("Ahli berhasil dinonaktifkan", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
