@extends('layouts.main')

@section('title', 'Laporan')
@include('plugins.datepicker')
@section('title-content')
e-Clinic Laporan
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>eClinic</li>
<li class="active">Laporan</li>
@endsection

@section('content')
<div class='row'>
  <div class='col-md-6'>
    <ul class="nav nav-pills">
      <li class="active"><a href="{{URL::to('/admin/e-clinic/laporan')}}">Laporan</a></li>
    </ul>
    <div class="box box-success">
      <form class="form-horizontal" method="POST" action="{{URL::to('/admin/e-clinic/laporan')}}" target="_blank">
          {{ csrf_field() }}
        <div class="box-body">
          <div class="form-group">
            <label class="col-md-4 control-label">Pilih Ahli</label>
            <div class="col-lg-6 col-md-8">
              <select name="ahli" id="select-ahli" required>
                <option selected disabled>Pilih Ahli</option>
                @foreach ($ahlis as $ahli)
                  <option value="{{$ahli->id}}">{{$ahli->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Periode</label>
            <div class="col-lg-3 col-md-4 control-label">
              <input type="text" name="bulan" placeholder="Bulan" class='form-control datepickermonth' required>  
            </div>
            <div class="col-lg-3 col-md-4 control-label">
              <input type="text" name="tahun" placeholder="Tahun" class='form-control datepickeryear' required> 
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="form-group">
            <label class="col-lg-4 col-md-4 control-label"></label>
            <div class="col-lg-8 col-md-8 control-label">
              <button type="submit" class="btn btn-primary">Download</button>
            </div>
          </div>
          
        </div>
      </form>
    </div>
  </div>
  <div class='col-md-6'>
    <ul class="nav nav-pills">
      <li class="active"><a href="{{URL::to('/admin/e-clinic/laporan')}}">Rekap</a></li>
    </ul>
    <div class="box box-success">
      <form class="form-horizontal" method="POST" action="{{URL::to('/admin/e-clinic/rekap')}}" target="_blank">
          {{ csrf_field() }}
        <div class="box-body">
          <div class="form-group">
            <label class="col-lg-4 col-md-4 control-label">Periode</label>
            <div class="col-lg-3 col-md-4 control-label">
              <input type="text" name="bulan" placeholder="Bulan" class='form-control datepickermonth' required>  
            </div>
            <div class="col-lg-3 col-md-4 control-label">
              <input type="text" name="tahun" placeholder="Tahun" class='form-control datepickeryear' required> 
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="form-group">
            <label class="col-lg-4 col-md-4 control-label"></label>
            <div class="col-lg-8 col-md-8 control-label">
              <button type="submit" class="btn btn-primary">Download</button>
            </div>
          </div>
          
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  selectizeahli = $('#select-ahli').selectize({})[0].selectize;

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
