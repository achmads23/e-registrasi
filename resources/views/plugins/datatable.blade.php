@section('css') @parent
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.css"/>

{{-- <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.css')}}"> --}}
@endsection

@section('js-plugin') @parent
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.js"></script>
{{-- <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script> --}}
@endsection