@section('css') @parent
<link rel="stylesheet/less" type="text/css" href="{{asset('bower_components/bootstrap-timepicker/css/timepicker.less')}}">
@endsection

@section('js-plugin') @parent
<script src="{{asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
<script>
settimepicker = function() {
  $(function(){
    $('.timepicker').timepicker({
      minuteStep:1,
      showMeridian:false,
      explicitMode:true
    });
  })
};
settimepicker();
</script>
@endsection