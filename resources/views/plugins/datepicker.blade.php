@section('js-plugin') @parent
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
<script>
$(function(){
  $('.datepicker').datepicker({
    autoclose: true,
    horizontal: 'auto',
    vertical: 'bottom',
    format: "dd-mm-yyyy"
  });
  $(".datepickermonth").datepicker( {
    format: "m",
    startView: "months", 
    minViewMode: "months",
  });
  $(".datepickeryear").datepicker( {
    format: "yyyy",
    startView: "years", 
    minViewMode: "years"
  });
  $(".datepickerdate").datepicker( {
    format: "dd",
    startView: "days", 
    minViewMode: "days"
  });
})
</script>
@stop