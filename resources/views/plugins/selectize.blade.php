@section('css') @parent
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
<style type="text/css">
  .form-inline .selectize-control {
    margin-bottom: -5px;
  }
</style>
@endsection

@section('js-plugin') @parent
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>
@endsection