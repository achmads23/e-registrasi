@section('css') @parent
<link rel="stylesheet" href="{{asset('bower_components/AdminLTE/plugins/iCheck/minimal/_all.css')}}">
@endsection

@section('js-plugin') @parent
<script src="{{asset('bower_components/AdminLTE/plugins/iCheck/icheck.min.js')}}"></script>
@endsection