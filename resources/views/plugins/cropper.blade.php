@section('css') @parent
<link rel="stylesheet" type="text/css" href="{{asset('cropper/cropper.min.css')}}">
@stop

@section('js-plugin') @parent
<script src="{{asset('cropper/cropper.min.js')}}"></script>
@stop