@section('css') @parent
<style type="text/css">
.parsley-error {
  border: 1px solid #e80000 !important;
}

.parsley-errors-list {
  margin: 2px 0 3px;
  padding: 0;
  list-style-type: none;
  font-size: 0.9em;
  line-height: 0.9em;
  opacity: 0;

  transition: all .3s ease-in;
  -o-transition: all .3s ease-in;
  -moz-transition: all .3s ease-in;
  -webkit-transition: all .3s ease-in;
}

.parsley-errors-list.filled {
  color: #e80000;
  opacity: 1;
}

</style>
@endsection

@section('js-plugin') @parent
<script type="text/javascript" src="{{asset('js/parsley.js')}}"></script>
@endsection