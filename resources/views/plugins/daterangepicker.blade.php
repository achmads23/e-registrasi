@section('css') @parent
<link href="{{asset('bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endsection

@section('js-plugin') @parent
<script type="text/javascript" src="{{asset('bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js')}}"></script>
@endsection