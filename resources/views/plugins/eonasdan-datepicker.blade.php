@section('css') @parent
<link rel="stylesheet" href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" />
@endsection

@section('js-plugin') @parent
<script type="text/javascript" src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script>
$(function(){
  $('.eonasdan-datetimepicker').datetimepicker({
    format: 'DD-MM-Y HH:mm',
  });
  $('.eonasdan-datepicker').datetimepicker({
    format: 'DD-MM-Y',
  });
})
</script>
@endsection