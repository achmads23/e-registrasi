@extends('layouts.empty-no-lola')

@section('title', 'Pendaftaran')

@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="text-center">
  <br>
  <h2 class="disable-margin">SIRA</h2>
  <h4>SISTEM INFORMASI REGISTRASI</h4>
</div>
<br>
<div class="col-md-10 col-md-offset-1">
  <div class="box" style="border-top:none">
    <div class="box-body">
      <div class='col-md-12'>
      <form class="form-horizontal" method="POST">
        <div class='text-center'>
        <h4 class="disable-margin"><b>MONITOR</b></h4>
        </div>
        <br>
        @if($acara)
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Kode Undangan</label>
              <div class="col-sm-8 control-label">
                {{$acara->code}}
              </div>
            </div>
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Acara</label>
              <div class="col-sm-8 control-label">
                {{$acara->nama_acr}}
              </div>
            </div>
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Tempat Acara</label>
              <div class="col-sm-8 control-label">
                {{$acara->tempat_acr}}
              </div>
            </div>
          </div>
          <div class="col-sm-6">
             <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Tanggal Acara</label>
              <div class="col-sm-8 control-label">
                {{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}
              </div>
            </div>
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Waktu Acara</label>
              <div class="col-sm-8 control-label">
                {{time_view($acara->pukul)}} WIB
              </div>
            </div>
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">Tanggal Pendaftaran</label>
              <div class="col-sm-8 control-label">
                {{date_view($acara->tanggal_awal) . ' - ' . date_view($acara->tanggal_akhir)}}
              </div>
            </div>
            <div class="form-group disable-margin">
              <label class="col-sm-4 control-label disable-padding-left">OPD</label>
              <div class="col-sm-8 control-label">
                <?php foreach ($acara->opds as $key => $opd): ?>
                {{$key+1}}. {{$opd->opd}}<br>  
                <?php endforeach ?>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="table-responsive">
        <table class="table table-striped" style='font-size:20px;'>
          <tr>
            <th>Nama</th>
            <th>L/P</th>
            <th>Status</th>
          </tr>
          @if(count($acara->peserta) > 0)
            @php $count = 1; @endphp
              @foreach ($acara->peserta as $peserta)
                @if($peserta->check_in != NULL)
                <tr>
                  <td>{{$peserta->nama ?? $peserta->pegawai->nama }}</td>
                  <td>{{$peserta->jenis_kelamin ?? '-' }}</td>
                  <td width="30%">
                    @if($peserta->notif)
                      @if($peserta->sesuai_spt == -1)
                        <span class='text-red'><b>Tidak Aktif, Harap menuju Front Desk</b></span>
                      @endif
                    @else 
                      @if($peserta->sesuai_spt)
                        <span class='text-green'><b>AKTIF</b></span>
                      @elseif($peserta->sesuai_spt == 0)
                        Check In
                      @endif
                    @endif
                  </td>
                </tr>
                @endif
              @endforeach
            @endif
        </table>
        </div>
        @else
          <div class="row text-center" style="height: 300px">
            <h4><i>Tidak ada acara yang ditampilkan</i></h4>
          </div>
        @endif
    </div>
  </div>
</div>
<div class="text-center" style="margin-bottom: 100px" id="footer">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>
<script>

  if($(window).height() == $(document).height()){
    setTimeout(location.reload(), 10000);
  }
  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  }

  $("html,body").animate({ scrollTop: $('body').prop("scrollHeight")}, 10000);
  window.onscroll = function(ev) {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
       location.reload();
    }
  };
</script>

@endsection
