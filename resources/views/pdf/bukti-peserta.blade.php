<div style="text-align:center">
<h3>UPT PPK</h3>
<br>
<br>
<h3>Registrasi Sukses</h3>
<table style="font-size:20px;margin:auto;">
  <tr>
    <td>NIP</td>
    <td>:</td>
    <td style="font-weight: bold;padding-left: 10px;">{{$peserta->pegawai ? $peserta->pegawai->nip : $peserta->nip}}</td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td style="font-weight: bold;padding-left: 10px;">{{$peserta->nama}}</td>
  </tr>
  <tr>
    <td>Username</td>
    <td>:</td>
    <td style="font-weight: bold;padding-left: 10px;">{{$peserta->username}}</td>
  </tr>
  <tr>
    <td>Password</td>
    <td>:</td>
    <td style="font-weight: bold;padding-left: 10px;">{{$peserta->password}}</td>
  </tr>
</table>
<br>
<p>Gunakan qr code dibawah ini untuk absensi kehadiran</p>
<img src="{{public_path($peserta->qrcode)}}" width='50%'>
</div>