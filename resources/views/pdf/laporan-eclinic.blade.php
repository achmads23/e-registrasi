<div style="text-align: center; border-bottom: 2px solid black;font-family:'Sans-serif';font-weight: bold">
	<h2 style="line-height: 0px;">LAPORAN</h2><br>
	<h3 style="line-height: 0px;">E-CLINIK ANDROID KEUANGAN</h3><br>
	<h3 style="line-height: 0px;">UPT-LABORATORIUM PENGELOLAAN KEUANGAN DAERAH</h3><br>
	<h3 style="line-height: 0px;">BPKAD PROVINSI JAWA TIMUR</h3><br><br>
</div>
<div style="padding:15px 0px;border-bottom: 2px solid black;">
<table>
<tr>
	<td style="width:100px">Nama</td>
	<td>:</td>
	<td>{{$user->name}}</td>
</tr>
<tr>
	<td style="width:100px">NIP</td>
	<td>:</td>
	<td>{{$user->detail->nip}}</td>
</tr>
<tr>
	<td style="width:100px">Jabatan</td>
	<td>:</td>
	<td>{{$user->detail->jabatan}}</td>
</tr>
<tr>
	<td style="width:100px">Instansi</td>
	<td>:</td>
	<td>{{$user->detail->opd}}</td>
</tr>
<tr>
	<td style="width:100px">No Telpon</td>
	<td>:</td>
	<td>{{$user->detail->hp}}</td>
</tr>
<tr>
	<td style="width:100px">E-Mail</td>
	<td>:</td>
	<td>{{$user->email}}</td>
</tr>
<tr>
	<td style="width:100px">Masa</td>
	<td>:</td>
	<td>{{ucfirst($month)}} {{ucfirst($year)}}</td>
</tr>
<tr>
	<td style="width:100px">Bidang</td>
	<td>:</td>
	<td>
		@foreach ($user->ahli->categories as $key => $element)
            {{$key +1 }}. {{$element}}<br>
        @endforeach
	</td>
</tr>
<tr>
	<td style="width:100px">Kinerja</td>
	<td>:</td>
	<td>Menjawab {{$from}} dari {{$max}} Pertanyaan</td>
</tr>
</table>
</div>
<div style="padding:15px 0px;border-bottom: 2px solid black;">
	<ol>
	@foreach ($responders as $responder)
			<li>
				<table>
					<tr>
						<td valign="top">Pertanyaan</td>
						<td valign="top">:</td>
						<td>
							{{$responder->thread->question}} oleh {{$responder->thread->user ? $responder->thread->user->name : '-'}}
							<br>
							{{datetime_view($responder->thread->created_at)}}
						</td>
					</tr>
					<tr>
						<td valign="top">Jawaban</td>
						<td valign="top">:</td>
						<td>
							{{$responder->answer}}
							<br>
							{{datetime_view($responder->answered_at)}}
						</td>
					</tr>
				</table>
			</li>
	@endforeach
	</ol>
</div>