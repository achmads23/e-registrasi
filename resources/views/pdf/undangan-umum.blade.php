<div>
  <table>
    <tr>
      <td>
        <img src="{{asset('images/logo-jatim.png')}}" height='150px'>
      </td>
      <td style="text-align: center; width:550px;">
        <span style="font-size: 18px">PEMERINTAH DAERAH PROVINSI JAWA TIMUR</span><br>
        <span style="font-size: 18px">BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">UNIT PELAKSANA TEKNIS LABORATORIUM</span><br>
        <span style="font-size: 18px;font-weight: bold">PENGELOLAAN KEUANGAN DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">Jl. Sikatan No.10 Telp. 3554069 Fax. 3554052</span><br>
        <span style="font-size: 18px;font-weight: bold">SURABAYA  60175</span>
      </td>
    </tr>
  </table>
  <div>
  <table>
    <tr>
      <td>
      </td>
      <td>
      </td>
      <td>Surabaya, {{$date}} {{$month}} {{$year}}</td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td valign='top'>
              Nomor
            </td>
            <td valign='top'>:</td>
            <td style="word-wrap: break-word !important;"> 
              900/{{$nomor_surat}}/203.7/{{$year}}
            </td>
          </tr>
          <tr>
            <td>
              Sifat
            </td>
            <td>:</td>
            <td> 
              Segera
            </td>
          </tr>
          <tr>
            <td>
              Lampiran
            </td>
            <td>:</td>
            <td> 
              @if(count($acara->opds) > 0)
              1 (satu) berkas
              @else
              -
              @endif
            </td>
          </tr>
          <tr>
            <td valign="top">
              Perihal
            </td>
            <td valign="top">:</td>
            <td valign="top" >
              <div style='border-bottom:1px solid black;'>
                <b> {{$acara->nama_acr}}</b>
              </div>
            </td>
          </tr>
        </table>
      </td>
      <td style='width: 300px;'>
      </td>
      <td>
        <table style="margin-left: 150px;" >
          <tr>
            <td>
              Kepada
            </td>
          </tr>
          <tr>
            <td style="width:200px;">
              Yth. Sdr. {{$kepada}}
            </td>
          </tr>
          <tr>
            <td>
              <span>di</span>
            </td>
          </tr>
          <tr>
            <td style='text-align: center;'>
              <b><u>TEMPAT</u></b>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>

  <br>
  <br>
  <div>
    <div style='text-align: justify;padding-left:80px;width:630px'>
      <p style="text-indent: 50px;margin:0;">Menindaklanjuti Surat Edaran Bapak Sekretaris Daerah Provinsi Jawa Timur tanggal 15 Januari 2018 Nomor 005/311/203.7/2018 tentang usulan Peserta Bimbingan Teknis dan dalam rangka meningkatkan kualitas Pengelolaan Keuangan Daerah, maka kami bermaksud mengadakan Kegiatan Bimbingan Teknis terkait Pengelolaan Keuangan Daerah.  </p>
      <p style="text-indent: 50px;margin:0;">Sehubungan dengan hal tersebut dimohon kesediaan Bapak/Ibu untuk mendaftarkan dan menugaskan ASN atau PTTPK sebanyak {{$jumlah}} orang melalui aplikasi <b>Sistem Informasi Registrasi (SIRA)</b> pada website <span style="color:blue"><b>http://layanan.uptlpkd.bpkad.jatimprov.go.id</b></span> atau aplikasi <b>Android SIRA</b> yang dapat diunduh di <b>Google Play Store</b>.</p>
      <br>
      <b>Catatan : </b>
      <table>
        <tr>
          <td valign="top">1.</td>
          <td style='text-align: justify;'><b>Registrasi</b> dapat dilakukan hanya melalui aplikasi <b>SIRA</b> dan paling lambat 2 (dua) hari sebelum pelaksanaan kegiatan (jadwal dapat dilihat pada aplikasi <b>SIRA</b>)</td>
        </tr>
        <tr>
          <td valign="top">2.</td>
          <td style='text-align: justify;'>Setelah registrasi akan memperoleh <b>Kode Pesanan</b> dan harap dibawa untuk diaktivasi pada saat Bimbingan Teknis</td>
        </tr>
        <tr>
          <td valign="top">3.</td>
          <td style='text-align: justify;'>UPT PPK menyediakan sarana penginapan untuk 40 (empat puluh) peserta bimtek dari luar daerah</td>
        </tr>
        <tr>
          <td valign="top">4.</td>
          <td style='text-align: justify;'>Biaya perjalanan dinas dari dan ke UPT PPK ditanggung oleh masing-masing OPD/Instansi pengirim.</td>
        </tr>
      </table>
      <p>Demikian untuk menjadikan maklum.</p>
    </div>
  </div>
  <div>
    <table>
      <tr>
        <td valign="bottom" style='width: 350px'>
          <b>Tembusan:</b><br>
          Yth. Bp. Kepala Badan (sebagai laporan)
        </td>
        <td style='text-align:center;'>
          @if($ttd)
          <img src="{{asset('images/ttd.png')}}" style="z-index:999999; position:absolute;width:300px;top:-20px;">
          @endif
          KEPALA UPT-LABORATORIUM PENGELOLAAN <br>KEUANGAN DAERAH PROVINSI JAWA TIMUR
          <br>
          <br>
          <br>
          <br>
          <div>
            <b><u>Ir. MOCHAMAD ISMANTO, MM</u></b>
            <br>
            P e m b i n a<br>
            NIP. 19680627 199803 1 007
          </div>

        </td>
      </tr>
    </table>
  </div>
</div>
@if(count($acara->opds) > 0)
<div style="page-break-before: always;">
  <p><b><u>Lampiran Surat:</u></b></p>
  Yth. Sdr.<br>

  @foreach ($acara->opds as $key => $opd)
  {{$key+1}} {{ucwords(strtolower($opd->opd))}}<br>
  @endforeach
</div>
@endif
