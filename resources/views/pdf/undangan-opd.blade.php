<div>
  <table>
    <tr>
      <td>
        <img src="{{asset('images/logo-jatim.png')}}" height='150px'>
      </td>
      <td style="text-align: center; width:550px;">
        <span style="font-size: 18px">PEMERINTAH DAERAH PROVINSI JAWA TIMUR</span><br>
        <span style="font-size: 18px">BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">UNIT PELAKSANA TEKNIS LABORATORIUM</span><br>
        <span style="font-size: 18px;font-weight: bold">PENGELOLAAN KEUANGAN DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">Jl. Sikatan No.10 Telp. 3554069 Fax. 3554052</span><br>
        <span style="font-size: 18px;font-weight: bold">SURABAYA  60175</span>
      </td>
    </tr>
  </table>
</div>
<div>
  <table>
    <tr>
      <td>
      </td>
      <td>
      </td>
      <td>Surabaya, {{$date}} {{$month}} {{$year}}</td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td valign='top'>
              Nomor
            </td>
            <td valign='top'>:</td>
            <td style="word-wrap: break-word !important;"> 
              900/{{$nomor_surat}}/203.7/{{$year}}
            </td>
          </tr>
          <tr>
            <td>
              Sifat
            </td>
            <td>:</td>
            <td> 
              Segera
            </td>
          </tr>
          <tr>
            <td>
              Lampiran
            </td>
            <td>:</td>
            <td> 
              @if(count($acara->opds) > 0)
              1 (satu) berkas
              @else
              -
              @endif
            </td>
          </tr>
          <tr>
            <td valign="top">
              Perihal
            </td>
            <td valign="top">:</td>
            <td valign="top" >
              <div style='border-bottom:1px solid black;'>
                <b> {{$acara->nama_acr}}</b>
              </div>
            </td>
          </tr>
        </table>
      </td>
      <td style='width: 280px;'>
      </td>
      <td>
        <table>
          <tr>
            <td>
              Kepada
            </td>
          </tr>
          <tr>
            <td style="width:200px;">
              Yth. Sdr. {{$kepada}}
            </td>
          </tr>
          <tr>
            <td>
              <span>di</span>
            </td>
          </tr>
          <tr>
            <td style='text-align: center;'>
              <b><u>TEMPAT</u></b>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
<br>
<br>
<div>
  <div style='text-align: justify;padding-left:80px;width:630px'>
    <p style="text-indent: 50px;margin:0;">Menindaklanjuti Peraturan Gubernur Jawa Timur Nomor 101 Tahun 2016 tentang Nomenklatur, Susunan Organisasi, Uraian Tugas dan Fungsi serta Tata Kerja Unit Pelaksana Teknis Badan Pengelola Keuangan dan Aset Daerah Provinsi Jawa Timur serta Surat Edaran Bapak Sekretaris Daerah Provinsi Jawa Timur tanggal 15 Januari 2018 Nomor : 005/311/203.7/2018 tentang usulan Peserta Bimbingan Teknis, maka dalam rangka meningkatkan kualitas Pengelolaan Keuangan Daerah perlu dilakukan {{ucwords(strtolower($acara->nama_acr))}} bagi {{ucwords(strtolower($acara->bagi))}} di lingkungan {{ucwords(strtolower($acara->lingkungan))}} pada: </p>
    <table>
      <tr>
        <td>
          Hari / Tanggal
        </td>
        <td>:</td>
        <td>{{$day}} / {{$tgl}} {{$bulan}} {{$tahun}}</td>
      </tr>
      <tr>
        <td>
          Pukul
        </td>
        <td>:</td>
        <td>{{time_view($acara->pukul)}} WIB</td>
      </tr>
      <tr>
        <td>
          Tempat
        </td>
        <td>:</td>
        <td>{{$acara->tempat_acr}} Kantor UPT PPK</td>
      </tr>
      <tr>
          <td>
          </td>
          <td></td>
          <td>Jl.  Sikatan  No.  10  Surabaya</td>
        </tr>
      <tr>
        <td>
          <b>Kode Undangan</b>
        </td>
        <td>:</td>
        <td>{{$acara->code}}</td>
      </tr>
    </table>
    <br>
    <p style="text-indent: 50px;margin:0;">Sehubungan dengan hal tersebut dimohon kesediaan Bapak/Ibu untuk menugaskan pegawai yang bersangkutan mengikuti kegiatan dimaksud.</p>
    <b>Catatan : </b>
    <table>
      <tr>
        <td valign="top">1.</td>
        <td style='text-align: justify;'>Harap melakukan <b>registrasi</b> paling lambat 2 (dua) hari sebelum pelaksanaan kegiatan Bimbingan Teknis, dengan memasukkan <b>Kode Undangan</b> pada aplikasi <b>Sistem Informasi Registrasi (SIRA)</b> melalui website <b>layanan.uptlpkd.bpkad.jatimprov.go.id</b> untuk mendapatkan <b>Kode Pesanan</b></td>
      </tr>
      <tr>
        <td valign="top">2.</td>
        <td style='text-align: justify;'><b>Kode Pesanan</b> harap dibawa pada saat hadir untuk diaktivasi</td>
      </tr>
      <tr>
        <td valign="top">3.</td>
        <td style='text-align: justify;'>Pakaian Peserta (menyesuaikan seragam yang berlaku pada hari tersebut)</td>
      </tr>
      <tr>
        <td valign="top">4.</td>
        <td style='text-align: justify;'>UPT PPK menyediakan sarana penginapan untuk 40 (empat puluh) peserta bimtek dari luar daerah</td>
      </tr>
      <tr>
        <td valign="top">5.</td>
        <td style='text-align: justify;'>Biaya perjalanan dinas dari dan ke UPT PPK ditanggung oleh masing-masing OPD/Instansi pengirim.</td>
      </tr>
    </table>
    <p>Demikian untuk menjadikan maklum.</p>
  </div>
</div>
<div>
  <table>
    <tr>
      <td valign="bottom" style='width: 350px'>
        <b>Tembusan:</b><br>
        Yth. Bp. Kepala Badan (sebagai laporan)
      </td>
      <td style='text-align:center;'>
        @if($ttd)
        <img src="{{asset('images/ttd.png')}}" style="z-index:999999; position:absolute;width:300px;top:-20px;">
        @endif
        KEPALA UPT-LABORATORIUM PENGELOLAAN <br>KEUANGAN DAERAH PROVINSI JAWA TIMUR
        <br>
        <br>
        <br>
        <br>
        <div>
          <b><u>Ir. MOCHAMAD ISMANTO, MM</u></b>
          <br>
          P e m b i n a<br>
          NIP. 19680627 199803 1 007
        </div>

      </td>
    </tr>
  </table>
</div>
@if(count($acara->opds) > 0)
<div style="page-break-before: always;">
<p><b><u>Lampiran Surat:</u></b></p>
Yth. Sdr.<br>

@foreach ($acara->opds as $key => $opd)
{{$key+1}} {{ucwords(strtolower($opd->opd))}}<br>
@endforeach
</div>
@endif
