<div style="text-align: center; font-family:'Sans-serif';font-weight: bold">
	<h2 style="line-height: 0px;">LAPORAN</h2><br>
	<h3 style="line-height: 0px;">E-CLINIK ANDROID KEUANGAN</h3><br>
	<h3 style="line-height: 0px;">UPT-LABORATORIUM PENGELOLAAN KEUANGAN DAERAH</h3><br>
	<h3 style="line-height: 0px;">BPKAD PROVINSI JAWA TIMUR</h3><br>
	<h3 style="line-height: 0px;">PER {{strtoupper($month)}} {{strtoupper($year)}}</h3><br><br>
</div>
<div style="padding:15px 0px;">
<table style="border-collapse: collapse;border:1px solid black;">
	<thead>
		<tr>
			<th style="text-overflow: ellipsis;width:10px;padding:5px;border:1px solid black;">No</th>
			<th style="text-overflow: ellipsis;padding:5px;border:1px solid black;width:165px">Nama</th>
			<th style="text-overflow: ellipsis;padding:5px;border:1px solid black;width:165px">Instansi</th>
			<th style="text-overflow: ellipsis;padding:5px;border:1px solid black;">Bidang</th>
			<th style="text-overflow: ellipsis;width:25px;padding:5px;border:1px solid black;">Pertanyaan</th>
			<th style="text-overflow: ellipsis;width:25px;padding:5px;border:1px solid black;">Jawaban</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $key => $user)
			<tr>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$key+1}}</td>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$user ? $user->name : '-'}}</td>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$user ? $user->detail->opd : '-'}}</td>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">
			          @foreach ($user->ahli->categories as $key => $element)
			            * {{$element}}<br>
			          @endforeach
				</td>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$user ? $user->from : '-'}}</td>
				<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$user ? $user->max : '-'}}</td>
			</tr>
		@endforeach
		<tr>
			<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;text-align: center" colspan="4">Jumlah</td>
			<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$from}}</td>
			<td  style="text-overflow: ellipsis;padding:5px;border:1px solid black;">{{$max}}</td>
		</tr>
	</tbody>
</table>
</div>
