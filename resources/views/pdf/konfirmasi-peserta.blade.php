<div style="text-align:center">
<span style='font-size:20px'><b>DAFTAR KONFIRMASI PESERTA</b></span>
<br>
{{strtoupper($acara->nama_acr)}} <br>BAGI {{strtoupper($acara->bagi)}}
<br>
DI LINGKUNGAN {{strtoupper($acara->lingkungan)}}
<br>
<span style="font-size: 15px">{{$day}}, {{$date}} {{$month}} {{$year}}</span>
<br>
<br>
<table style="border-collapse: collapse;border:1px solid black;">
  <tr>
    <th style="border:1px solid black;padding:10px;width: 20px; text-align: center;" >NO.</th>
    <th style="border:1px solid black;padding:10px;width: 204px; text-align: center;" >NAMA</th>
    <th style="border:1px solid black;padding:10px;width: 204px; text-align: center;" >NIP</th>
    <th style="border:1px solid black;padding:10px;width: 204px; text-align: center;" >INSTANSI</th>
  </tr>
  @foreach ($acara->peserta as $key => $peserta)
    <tr>
      <td style="border:1px solid black;padding:10px;width: 20px; text-align: center;" >{{$key + 1}}</td>
      <td style="border:1px solid black;padding:10px;width: 204px; text-align: left;" >{{$peserta->nama}}</td>
      <td style="border:1px solid black;padding:10px;width: 204px; text-align: center;" >{{$peserta->nip}}</td>
      <td style="border:1px solid black;padding:10px;width: 204px; text-align: center;" >{{$peserta->asal_instansi}}</td>
    </tr>
  @endforeach

</table>