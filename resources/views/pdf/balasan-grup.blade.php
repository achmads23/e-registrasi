<div>
  <table>
    <tr>
      <td>
        <img src="{{asset('images/logo-jatim.png')}}" height='150px'>
      </td>
      <td style="text-align: center; width:550px;">
        <span style="font-size: 18px">PEMERINTAH DAERAH PROVINSI JAWA TIMUR</span><br>
        <span style="font-size: 18px">BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">UNIT PELAKSANA TEKNIS LABORATORIUM</span><br>
        <span style="font-size: 18px;font-weight: bold">PENGELOLAAN KEUANGAN DAERAH</span><br>
        <span style="font-size: 18px;font-weight: bold">Jl. Sikatan No.10 Telp. 3554069 Fax. 3554052</span><br>
        <span style="font-size: 18px;font-weight: bold">SURABAYA  60175</span>
      </td>
    </tr>
  </table>
  <table>
    <tr>
      <td>
      </td>
      <td>
      </td>
      <td>Surabaya, {{$date}} {{$month}} {{$year}}</td>
    </tr>
    <tr>
      <td>
        <table>
          <tr>
            <td valign='top'>
              Nomor
            </td>
            <td valign='top'>:</td>
            <td style="word-wrap: break-word !important;"> 
              900/{{$nomor_surat}}/203.7/{{$year}}
            </td>
          </tr>
          <tr>
            <td>
              Sifat
            </td>
            <td valign="top">:</td>
            <td> 
              Segera
            </td>
          </tr>
          <tr>
            <td>
              Lampiran
            </td>
            <td valign="top">:</td>
            <td> 
              @if(count($acara->opds) > 0)
              1 (satu) berkas
              @else
              -
              @endif
            </td>
          </tr>
          <tr>
            <td valign="top">
              Perihal
            </td>
            <td valign="top">:</td>
            <td valign="top" >
              <div style='border-bottom:1px solid black;'>
                <b> {{$acara->nama_acr}}</b>
              </div>
            </td>
          </tr>
        </table>
      </td>
      <td style='width: 280px;'>
      </td>
      <td>
        <table>
          <tr>
            <td>
              Kepada
            </td>
          </tr>
          <tr>
            <td style="width:200px;">
              Yth. {{$pesertas[0]->user->detail->jenis_kelamin == 'L' ? 'Bapak ': ($pesertas[0]->user->detail->jenis_kelamin == 'P' ? 'Ibu ': 'Bapak/Ibu')}} {{$pesertas[0]->user->name}}
            </td>
          </tr>
          <tr>
            <td>
              <span>di</span>
            </td>
          </tr>
          <tr>
            <td style='text-align: center;'>
              <b><u>TEMPAT</u></b>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>
  <br>
  <div>
    <div style='text-align: justify;padding-left:80px;width:630px'>
      <p style="text-indent: 50px;margin:0;text-align: justify;">Menindaklanjuti permohonan Bp./Ibu melalui aplikasi Sistem Informasi Registrasi (SIRA), dengan ini diinformasikan bahwa permohonan dimaksud dapat <b>Disetujui</b> dan kegiatan tersebut akan dilaksanakan pada : </p>
      <table>
        <tr>
          <td valign="top">
            Hari / Tanggal
          </td>
          <td valign="top">:</td>
          <td>{{$day}} / {{$tgl}} {{$bulan}} {{$tahun}}</td>
        </tr>
        <tr>
          <td valign="top">
            Pukul
          </td>
          <td valign="top">:</td>
          <td>{{time_view($acara->pukul)}} WIB</td>
        </tr>
        <tr>
          <td valign="top">
            Tempat
          </td>
          <td valign="top">:</td>
          <td>Kantor UPT PPK {{$acara->tempat_acr}}</td>
        </tr>
        <tr>
          <td>
          </td>
          <td></td>
          <td>Jl.  Sikatan  No.  10  Surabaya</td>
        </tr>
        <tr>
          <td>Pakaian
          </td>
          <td valign="top">:</td>
          <td>Pakaian Dinas yang berlaku pada hari tersebut</td>
        </tr>
        <tr>
          <td valign="top">Biaya
          </td>
          <td valign="top">:</td>
          <td>Biaya perjalanan dinas ditanggung oleh masing-masing OPD / Instansi / Pribadi</td>
        </tr>
        <tr>
          <td>
            <b>Kode Pesanan</b>
          </td>
          <td valign="top">:</td>
          <td>Terlampir (harap diserahkan pada petugas untuk diaktivasi)</td>
        </tr>
      </table>
      <p>Demikian untuk menjadikan maklum.</p>
    </div>
  </div>
  <div>
    <table>
      <tr>
        <td valign="bottom" style='width: 350px'>
          <b>Tembusan:</b><br>
          Yth. Bp. Kepala Badan (sebagai laporan)
        </td>
        <td style='text-align:center;'>
          @if($ttd)
          <img src="{{asset('images/ttd.png')}}" style="z-index:999999; position:absolute;width:300px;top:-20px;">
          @endif
          KEPALA UPT-LABORATORIUM PENGELOLAAN <br>KEUANGAN DAERAH PROVINSI JAWA TIMUR
          <br>
          <br>
          <br>
          <br>
          <div>
            <b><u>Ir. MOCHAMAD ISMANTO, MM</u></b>
            <br>
            P e m b i n a<br>
            NIP. 19680627 199803 1 007
          </div>

        </td>
      </tr>
    </table>
  </div>
</div>

<div style="text-align:left;page-break-before: always;">
<span style='font-size:15px'><b><u>Lampiran Surat:</u></b></span>
</div>
<br>
<div style="text-align:center">
<span style='font-size:20px'><b>DAFTAR PESERTA</b></span>
<br>
<span style='font-size:15px'>
{{strtoupper($acara->nama_acr)}} <br>BAGI {{strtoupper($acara->bagi)}}
<br>
DI LINGKUNGAN {{strtoupper($acara->lingkungan)}}
<br>
</span>
<span style="font-size: 15px">{{$day}}, {{$tgl}} {{$bulan}} {{$tahun}}</span>
<br>
<br>
<div style="text-align: left;">
  <b>Instansi : {{$pesertas[0]->asal_instansi}}
  </b>
</div>
<table style="border-collapse: collapse;border:1px solid black;">
  <tr>
    <th style="border:1px solid black;padding:10px;width: 20px; text-align: center;" >NO.</th>
    <th style="border:1px solid black;padding:10px;width: 135px; text-align: center;" >NAMA</th>
    <th style="border:1px solid black;padding:10px;width: 20px; text-align: center;" >L/P</th>
    <th style="border:1px solid black;padding:10px;width: 135px; text-align: center;" >NIP</th>
    <th style="border:1px solid black;padding:10px;width: 145px; text-align: center;" >Username</th>
    <th style="border:1px solid black;padding:10px;width: 145px; text-align: center;" >Password</th>
  </tr>
  @foreach($pesertas as $key => $peserta)
    <tr>
      <td style="border:1px solid black;padding:10px;width: 20px; text-align: center;" >1</td>
      <td style="border:1px solid black;padding:10px;width: 135px;" >{{$peserta->nama}}</td>
      <td style="border:1px solid black;padding:10px;width: 20px;" >{{$peserta->jenis_kelamin}}</td>
      <td style="border:1px solid black;padding:10px;width: 135px; text-align: center;" >{{$peserta->nip}}</td>
      <td style="border:1px solid black;padding:10px;width: 145px; text-align: center;" >{{$peserta->username}}</td>
      <td style="border:1px solid black;padding:10px;width: 145px; text-align: center;" >{{$peserta->password}}</td>
    </tr>
  @endforeach
</table>
