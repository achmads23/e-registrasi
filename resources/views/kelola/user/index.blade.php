@extends('layouts.main')

@section('title', 'Kelola Data Master')
@include('plugins.datatable')
@section('title-content')
Kelola Data User
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>User</li>
<li class="active">List</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/kelola/user')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/kelola/user/create')}}">Buat Baru</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form method="get" action="{{URL::to('/admin/kelola/user')}}">
      <div class="table-responsive">
      <table class="table table-striped DataTable">
        <thead>
          <tr class="judul-kolom">
            <th >No</th>
            <th class="col-md-3">Nama</th>
            <th class="col-md-2">Email</th>
            <th class="col-md-2">Tanggal dibuat</th>
            <th class="col-md-1">Role</th>
            <th class="col-md-1">Status</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($user as $key => $d)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$d->name}}</td>
            <td>{{$d->email}}</td>
            <td>{{datetime_view($d->created_at)}}</td>
            <td>{{$d->role == PEGAWAI? PEGAWAI_TXT : ($d->role == ADMIN? ADMIN_TXT: ($d->role == USER? USER_TXT: ''))}}</td>
            <td>{{$d->is_verified ? 'Aktif' : 'Email Belum Divalidasi'}}</td>
            <td class="text-center">
              <a href="{{URL::to('/admin/kelola/user/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection