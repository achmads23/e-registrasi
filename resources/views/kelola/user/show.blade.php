@extends('layouts.main')

@section('title', 'Kelola Data Master')

@section('title-content')
Kelola Data User
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>User</li>
<li class="active">Show</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/kelola/user')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/kelola/user/create')}}">Buat Baru</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">Nama User</label>
        <div class="col-sm-10 control-label">
          {{$user->name}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-10 control-label">
          {{$user->jenis_kelamin == 'L' ? 'Laki-laki' : ($user->jenis_kelamin == 'P' ? 'Perempuan' : '-')}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Email</label>
        <div class="col-sm-10 control-label">
          {{$user->email}}
        </div>
      </div>
      @if(isset($user->detail) && $user->detail)
      <div class="row">
        <label class="col-sm-2 control-label">Zone</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->region}} <i>({{$user->detail->area}})</i>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->opd}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">No HP</label>
        <div class="col-sm-10 control-label">
          {{$user->detail->hp}}
        </div>
      </div>
      @endif

      <div class="row">
        <label class="col-sm-2 control-label">Role</label>
        <div class="col-sm-10 control-label">
          {{$user->role == PEGAWAI? PEGAWAI_TXT : ($user->role == ADMIN? ADMIN_TXT: ($user->role == USER? USER_TXT: ''))}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10 control-label">
          {{$user->is_verified ? 'Aktif' : 'Email Belum Divalidasi'}}
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
      <form id="hapus" method="POST" action="{{URL::to('/admin/kelola/user/'.$user->id)}}">
        {{ method_field('DELETE') }}
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Hapus</button>
        <a href="{{URL::to('/admin/kelola/user/'.$user->id.'/edit')}}" class="btn btn-success">Ubah</a>
      </form>
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menghapus data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
