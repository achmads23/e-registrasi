@extends('layouts.main')

@section('title', 'Kelola Data Master')

@section('title-content')
Kelola Data User
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>User</li>
<li class="active">Ubah</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/kelola/user')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/kelola/user/create')}}">Buat Baru</a></li>
  <li class="active"><a>Ubah</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/kelola/user/'.$user->id)}}">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" name="name" class="form-control" placeholder="Nama" value="{{old('name', $user->name) }}" required>
        </div>
      </div>
      <div class="form-group @if(session()->get('email_error')) has-error @endif">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
          <input type="text" name="email" class="form-control" placeholder="Email" value="{{old('email',$user->email)}}" required>
          @if(session()->get('email_error'))
          <span class="help-block">{{session()->get('email_error')}}</span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
          <input type="text" name="password" class="form-control" placeholder="Password">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Role</label>
        <div class="col-sm-10">
          <select type="text" name="role" class="selectize-role" placeholder="Pilih Role" required>
            <option value=""></option>
            <option value="{{ADMIN}}">{{ADMIN_TXT}}</option>
            <option value="{{PEGAWAI}}">{{PEGAWAI_TXT}}</option>
            <option value="{{USER}}">{{USER_TXT}}</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10">
          <select type="text" name="status" value="{{ old('status')}}" class="selectize-status" placeholder="Pilih Status" required>
            <option value=""></option>
            <option value="1">Aktif</option>
            <option value="0">Belum Aktif</option>
          </select>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  selectizerole = $('.selectize-role').selectize({})[0].selectize;
  selectizerole.setValue({{old('role',$user->role)}});

  selectizestatus = $('.selectize-status').selectize({})[0].selectize;
  selectizestatus.setValue({{old('status',$user->is_verified)}});

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
