@extends('layouts.main')

@section('title', 'Kelola Data Master')
@include('plugins.datatable')
@section('title-content')
Kelola Data Pegawai
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>Pegawai</li>
<li class="active">List</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/kelola/pegawai')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/kelola/pegawai/create')}}">Buat Baru</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form method="get" action="{{URL::to('/admin/kelola/pegawai')}}">
      <div class="table-responsive">
      <table class="table table-striped DataTable">
        <thead>
          <tr class="judul-kolom">
            <th width="5%">No</th>
            <th class="col-md-2">NIP</th>
            <th class="col-md-3">Nama</th>
            <th width="8%">JK</th>
            <th width="8%">Gol</th>
            <th>OPD</th>
            <th class="text-center">Aksi</th>
          </tr>
          <form>  
            <tr class="judul-kolom">
              <th width="5%"></th>
              <th class="col-md-2"><input type="text" placeholder='NIP' name="nip" class="form-control" value="{{$inputs['nip']??''}}"></th>
              <th class="col-md-3"><input type="text" placeholder="Nama" name="nama" class="form-control" value="{{$inputs['nama']?? ''}}"></th>
              <th width="8%">
                <select name="jk" class='form-control'>
                  <option value=''>L/P</option> 
                  <option value='L' {{isset($inputs['jk']) && $inputs['jk'] == 'L' ? 'selected' : ''}}>L</option> 
                  <option value='P' {{isset($inputs['jk']) && $inputs['jk'] == 'P' ? 'selected' : ''}}>P</option> 
                </select>
              </th>
              <th width="8%"><input type="text" placeholder="Gol" name="golongan" class="form-control" value="{{$inputs['golongan']??''}}"></th>
              <th><input type="text" name="opd" placeholder="OPD" class="form-control" value="{{$inputs['opd']??''}}"></th></th>
              <th class="text-center" >
                <a href="{{URL::to('/admin/kelola/pegawai')}}" class="btn btn-default"><i class='fa fa-close'></i></a>
                <button type="submit" class="btn btn-primary"><i class='fa fa-search'></i></button>
              </th>
            </form>
          </tr>
        </thead>
        <tbody>
          @php $i = ($pegawais->currentpage() - 1) * 20 + 1; @endphp
          @foreach ($pegawais as $key => $d)
          <tr>
            <td>{{$i++}}</td>
            <td>{{$d->nip}}</td>
            <td>{{$d->nama}}</td>
            <td>{{$d->jk}}</td>
            <td>{{$d->gol}}{{$d->ruang}}</td>
            <td>{{$d->opd? $d->opd->opd : '-'}}</td>
            <td class="text-center">
              <a href="{{URL::to('/admin/kelola/pegawai/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      </div>
      <div class='text-right'>
        {{ $pegawais->appends(request()->input())->links() }}
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    // var table = $('.DataTable').DataTable( {
    // } );
  });
</script>
@endsection