@extends('layouts.main')

@section('title', 'Kelola Data Master')

@section('title-content')
Kelola Data Pegawai
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>Pegawai</li>
<li class="active">Show</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/kelola/pegawai')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/kelola/pegawai/create')}}">Buat Baru</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="pegawai">
      <div class="row">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->nip}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->nama}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->opd? $pegawai->opd->opd : '-'}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Satker</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->nama_satker}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->jk}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Golongan</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->gol}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Ruang</label>
        <div class="col-sm-10 control-label">
          {{$pegawai->ruang}}
        </div>
      </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
      <form id="hapus" method="POST" action="{{URL::to('/admin/kelola/pegawai/'.$pegawai->id)}}">
        {{ method_field('DELETE') }}
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Hapus</button>
        <a href="{{URL::to('/admin/kelola/pegawai/'.$pegawai->id.'/edit')}}" class="btn btn-success">Ubah</a>
      </form>
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menghapus data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
