@extends('layouts.main')

@section('title', 'Kelola Data Master')

@section('title-content')
Kelola Data Pegawai
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li></i>Kelola Data</a></li>
<li>Pegawai</li>
<li class="active">Ubah</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li><a href="{{URL::to('/admin/kelola/pegawai')}}">List</a></li>
  <li><a href="{{URL::to('/admin/kelola/pegawai/create')}}">Buat Baru</a></li>
  <li class="active"><a>Ubah</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/kelola/pegawai/'.$pegawai->id)}}">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="form-group @if(session()->get('nip_error')) has-error @endif">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10">
          <input type="text" name="nip" value="{{ old('nip',$pegawai->nip)}}" class="form-control" placeholder="NIP" required>
          @if(session()->get('nip_error'))
          <span class="help-block">{{session()->get('nip_error')}}</span>
          @endif
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">OPD</label>
        <div class="col-md-6 col-sm-8">
          <select name="opd" class="select-opd" placeholder="Pilih OPD" required>
            @foreach ($opds as $opd)
              <option value="{{$opd->id}}">{{$opd->opd}}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Satuan Kerja</label>
        <div class="col-sm-10">
          <input type="text" name="satker" value="{{ old('satker',$pegawai->nama_satker)}}" class="form-control" placeholder="Satuan Kerja" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" name="nama" value="{{ old('nama',$pegawai->nama)}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-md-6 col-sm-8">
          <select name="jk" class="select-jk" placeholder="Pilih Jenis Kelamin" required>
            <option value="L">Laki-laki</option>
            <option value="P">Perempuan</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Golongan</label>
        <div class="col-sm-10">
          <input type="text" name="gol" value="{{ old('gol',$pegawai->gol)}}" class="form-control" placeholder="Golongan" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Ruang</label>
        <div class="col-sm-10">
          <input type="text" name="ruang" value="{{ old('ruang',$pegawai->ruang)}}" class="form-control" placeholder="Ruang">
        </div>
      </div>
      
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  
  selectopd = $('.select-opd').selectize({})[0].selectize;
  selectopd.setValue('{{old('opd',$pegawai->opd? $pegawai->opd->id : '')}}');

  selectjk = $('.select-jk').selectize({})[0].selectize;
  selectjk.setValue('{{old('jk',$pegawai->jk)}}');

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });
})
</script>
@endsection
