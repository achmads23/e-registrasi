@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')

<div class="text-center" style="margin: 100px 0px 0px 0px;">
    <h4><b>Klinik Center</b></h4>
</div>
<div class="text-center" style="margin: 0px 0px 0px 0px; justify-content: center;
    display: flex;">
  <p style="text-align: justify;" id="klinik-center">
    Merupakan produk satu-satunya di Indonesia, yang dimiliki UPT-LPKD. Klinik Center Pengelolaan Keuangan Daerah berbentuk konsultasi bagi seluruh pemerintah daerah di seluruh Indonesia yang ingin menyelesaikan permasalahan keuangan yang dihadapi. Seiring dengan perkembangan tekhnologi pemerintah Provinsi Jawa Timur tidak henti melakukan berbagai macam inovasi salah satunya adalah Klinik Konsultasi Pengelolaan Keuangan Daerah. Inovasi ini bermaksud untuk memberi kemudahan kepada Client/User terutama pemerintah di Indonesia terkait permasalahan - permasalahan yang sering terjadi dan susah untuk menemukan penyelesaiannya.
  </p>
</div>
<div class="text-center" style="margin: 10px 0px 30px 0px;">
  <a href="{{route('index')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
</div>
@endsection

@section('js')
<script>
$(function() {
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
