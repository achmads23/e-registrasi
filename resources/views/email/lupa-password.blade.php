<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{$jenis_kelamin == 'L' ? 'Bapak ': ($jenis_kelamin == 'P' ? 'Ibu ': 'Bapak/Ibu')}} {{ $name }},
    <br>
    Klik link dibawah ini untuk mereset password anda:
    <br>
    <a href="{{ route('reset-password', ['token' => $verification_code])}}">Reset Password</a>
	<br>
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>