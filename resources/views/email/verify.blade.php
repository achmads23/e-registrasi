<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{ $name }},
    <br>
    Terimakasih sudah mendaftar.
    <br>
    Selesaikan pendaftaran anda dengan klik link dibawah ini:
    <br>
    <a href="{{ url('user/verify', $verification_code)}}">Selesaikan</a>
	<br>
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>