<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{$jenis_kelamin == 'L' ? 'Bapak ': ($jenis_kelamin == 'P' ? 'Ibu ': 'Bapak/Ibu')}} {{ $name }},
    <br>
    You are receiving this email because we received a password reset request for your account.
    <br>
    We already reset your password. and this is your password
    <br>
    Password: <b>{{$password}}</b>
    <br>
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>