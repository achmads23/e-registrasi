<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{ $name }},
    <br>
    Terimakasih telah menjadi ahli {{$category}}. Gunakan email dan password dibawah ini untuk login pada aplikasi.
    <br>
    <br>
    Email : {{$email}}
    <br>
    Password : {{$password}}
    <br>
    <br>
    Klik link dibawah ini untuk aktivasi akun:
    <br>
    <a href="{{ url('user/verify', $verification_code)}}">Selesaikan</a>
	<br>
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>