<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{ $name }},
    <br>
    Maaf pendaftaran anda sebagai peserta dalam acara {{$nama_acr}} ditolak, silahkan hubungi UPT PPK untuk keterangan lebih lanjut.
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>