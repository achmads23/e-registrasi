<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{ $name }},
    <br>
    Selamat pendaftaran grup anda sebagai peserta dalam acara {{$nama_acr}} pada tanggal {{$tgl_acr}} di {{$tempat_acr}} diterima.
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>