<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<div>
    Halo {{$jenis_kelamin == 'L' ? 'Bapak ': ($jenis_kelamin == 'P' ? 'Ibu ': 'Bapak/Ibu')}} {{ $name }},
    <br>
    Selamat anda telah menjadi peserta dalam acara {{$nama_acr}} pada tanggal {{$tgl_acr}} di {{$tempat_acr}}.

    Berikut kode pesanan yang dapat anda gunakan pada saat acara : 
    <br>
    <br>
    Username : {{$username}}
    <br>
    Password : {{$password}}
    <br>
    <br>
    Terima Kasih,
    <br>
    Administrator UPT PPK - BPKAD Provinsi Jawa Timur.
</div>

</body>
</html>