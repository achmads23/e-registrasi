@extends('layouts.empty')

@section('title', 'Pendaftaran')


@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="col-md-6 col-md-offset-3">
  <div class="box" style="border-top:none">
    <div class="box-body">
      <div class='col-md-12'>
        <div class='text-center'>
        <h4 class="disable-margin">Acara</h4>
        <br>
        </div>
        <table class="table table-striped">
          <tr>
            <th>No</th>
            <th>Acara</th>
            <th>Tanggal Acara</th>
            <th>Waktu</th>
            <th>Tanggal Pendaftaran</th>
            <th class="text-center"><i class='fa fa-cog'></i></th>
          </tr>
          @foreach($acaras as $key => $acara)
            <tr>
              <td>{{$key + 1}}</td>
              <td>{{$acara->nama_acr}}</td>
              <td>{{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}</td>
              <td>{{time_view($acara->pukul)}}</td>
              <td>{{date_view($acara->tanggal_awal)}} - {{date_view($acara->tanggal_akhir)}}</td>
              <td class="text-center">
                <a href="{{route('bimtek.acara.show',['id' => $acara->id_acr])}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
          @endforeach
        </table>
      </form>
    </div>
  </div>
</div>
<div class="text-center" style="margin: 0px 0px 30px 0px;">
  <a href="{{route('bimtek')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center" style="margin-bottom: 100px">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>

<script>
  $(function() {

  });


</script>

@endsection
