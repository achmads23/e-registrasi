@extends('layouts.empty')

@section('title', 'Pendaftaran')


@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop
@section('content')
<div class="col-md-6 col-md-offset-3">
  <div class="box" style="border-top:none">
    <div class="box-body">
      <div class='col-md-12'>
        <div class='text-center'>
        <h4 class="disable-margin">Acara</h4>
        <br>
        </div>
        <div class='row'>
        <div class='col-md-12'>
          <div class="row">
            <label class="col-sm-5 control-label">OPD</label>
            <div class="col-sm-7 control-label">
              @if(count($acara->opds) > 0)
                @foreach ($acara->opds as $key => $opd)
                  {{$key+1}}. {{$opd->opd}}<br>  
                @endforeach
              @else 
              -
              @endif
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Nama Acara</label>
            <div class="col-sm-7 control-label">
              {{$acara->nama_acr}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Deskripsi Acara</label>
            <div class="col-sm-7 control-label">
              {{$acara->deskripsi}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Tanggal Acara</label>
            <div class="col-sm-7 control-label">
              {{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Waktu Acara</label>
            <div class="col-sm-7 control-label">
              {{time_view($acara->pukul)}} WIB
            </div>
          </div>
          <div class="row">
            <label class="col-sm-5 control-label">Tanggal Pendaftaran</label>
            <div class="col-sm-7 control-label">
              {{date_view($acara->tanggal_awal)}} - {{date_view($acara->tanggal_akhir)}}
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="text-center" style="margin: 0px 0px 30px 0px;">
  <a href="{{route('bimtek.acara.index')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center" style="margin-bottom: 100px">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>

<script>
  $(function() {

  });


</script>

@endsection
