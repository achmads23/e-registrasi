@extends('layouts.main')

@section('title', 'Konfirmasi Peserta Umum')

@section('title-content')
Konfirmasi Peserta Umum
@endsection

@include('plugins.eonasdan-datepicker')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Konfirmasi Peserta Umum</a></li>
<li class="active">Detail</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{route('admin.bimtek.umum')}}">List</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">NIP</label>
        <div class="col-sm-10 control-label">
          {{$peserta->nip}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10 control-label">
          {{$peserta->nama}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-10 control-label">
          {{$peserta->jenis_kelamin == 'L' ? 'Bapak ': ($peserta->jenis_kelamin == 'P' ? 'Ibu ': '-')}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Asal Instansi</label>
        <div class="col-sm-10 control-label">
          {{$peserta->asal_instansi}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10 control-label">
          {{$peserta->jabatan}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Golongan</label>
        <div class="col-sm-10 control-label">
          {{$peserta->golongan}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10 control-label">
          {{$peserta->email}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nomor HP</label>
        <div class="col-sm-10 control-label">
          {{$peserta->no_hp}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Kartu identitas</label>
        <div class="col-sm-10 control-label">
          <a href="{{ asset($peserta->foto) }}" target="_blank">Download Kartu Identitas</a>
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">File SK</label>
        <div class="col-sm-10 control-label">
          <a href="{{ asset($peserta->file) }}" target="_blank">Download File SK</a>
        </div>
      </div>
      <br>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta->acara->nama_acr}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-10 control-label">
          {{date_view($peserta->acara->tgl_acr)}} - {{date_view($peserta->acara->akhir_tgl_acr)}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta->acara->pukul}}
        </div>
      </div>
    <form class="form-horizontal" style="display: inline !important;" id="konfirmasi" method="POST" action="{{URL::to('/admin/konfirmasi-umum/'.$peserta->id)}}">
        {{ csrf_field()}}
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Ganti Acara<span class="text-red">*</span></label>
        <div class="col-md-4">
          <select name="acara" class="select-acara" id="acara" placeholder="Pilih Acara" required>
            @foreach($acaras as $key => $acara)
            <option value="{{$acara->id_acr}}">
              {{$acara->nama_acr}} | {{date_view($acara->tgl_acr)}} | {{$acara->tempat_acr}}
            </option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Nomor Surat<span class="text-red">*</span></label>
        <div class="col-md-4">
          <input type="text" name="nomor_surat" class="form-control" required>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Tanggal Surat<span class="text-red">*</span></label>
        <div class="col-md-4">
          <input type="text" name="tgl_surat" class="form-control eonasdan-datepicker" required>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Tanda Tangan<span class="text-red">*</span></label>
        <div class="col-md-4">
          <div class="radio">
            <label style="margin-right: 10px"><input type="radio" value="1" name="ttd">Ya</label>
            <label><input type="radio" value="0" name="ttd" checked>Tidak</label>
          </div>
        </div>
      </div>
      <input type="hidden" name="result" value="0" id="result">
    </form>
    
    
      
    </div>
    <!-- /.box-body -->
    <div class="box-footer text-right">
        <button type="button" class="btn btn-danger hapus">Verifikasi Ditolak</button>
        <button type="button" class="btn btn-success setuju">Verifikasi Diterima</button>
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  selectacara = $('.select-acara').selectize({})[0].selectize;
  selectacara.setValue({{old('acara')}});
  
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menolak data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Tolak!",
        closeOnConfirm: false
      },
      function(){
        $('#result').val(0);
        $('form#konfirmasi').submit();
      });
    });
  });

  $(function () {
    $(".setuju").click(function(){
      swal({
        title: "Apakah yakin menyetujui data ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#00a65a",
        confirmButtonText: "Ya, Setuju!",
        closeOnConfirm: false
      },
      function(){
        $('#result').val(1);
        $('form#konfirmasi').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
