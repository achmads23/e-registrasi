@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')

<div class="text-center" style="margin: 100px 0px 0px 0px;">
    <h4><b>Bimbingan Teknis
</b></h4>
</div>
<div class="text-center" style="margin: 0px 0px 0px 0px; justify-content: center;
    display: flex;">
  <p style="text-align: justify;" id="klinik-center">
    Sebagaimana diketahui, pelatihan maupun Bimbingan Teknis (bimtek), merupakan kegiatan pelatihan dan pengembangan pengetahuan serta kemampuan yang dapat digunakan untuk memecahkan masalah yang dihadapi oleh setiap individu maupun institusi tertentu.Sehingga dengan mengikuti Bimtek diharapkan setiap individu maupun institusi tertentu, baik swasta maupun lembaga pemerintahan, dapat mengambil sebuah manfaat dengan  berorientasi  pada  kinerja.
  </p>
</div>
<div class="text-center" style="margin: 10px 0px 30px 0px;">
  <a href="{{route('index')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
</div>
@endsection

@section('js')
<script>
$(function() {
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
