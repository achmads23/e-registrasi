@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')
<div class="login-box disable-margin-top">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Masukkan Kode Undangan</p>
    @if(session()->get('token_error'))
    <p class="text-center text-danger italic" id="error-message">{{session()->get('token_error')}}</p>
    @endif
    <form method="POST" action="{{ route('bimtek.pendaftaran.index') }}">
        {{ csrf_field() }}
      <div class="form-group @if(session()->get('token_error')) has-error @endif has-feedback">
        <input id="text" type="text" class="form-control" placeholder="Kode Undangan" name="token" value="{{ old('token') }}" required autofocus>
        <span class="fa fa-key form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Lanjut <i class="fa fa-chevron-right"></i></button>
            <button type="button" class="btn btn-success btn-block btn-flat" data-toggle="modal" data-target="#exampleModal">
              Lihat Panduan
            </button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color: rgba(0, 0, 0, 0) !important;box-shadow: none">
      <div class="modal-body">
        <img src="{{asset('images/e-Registrasi.png')}}" width="100%">
      </div>
      <div class="modal-footer text-center" style="border-top:none">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  <br>
  <div class="text-center text-black">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script>
  @if (session('kuota_habis'))
  swal("Oooops!", "Kuota Undangan Habis", "error");
  @endif

  @if (session('pegawai_tidak_ditemukan'))
  swal("Oooops!", "Maaf, data pegawai tidak ditemukan", "error");
  @endif

  @if (session('acara_tidak_ditemukan'))
  swal("Oooops!", "Maaf, acara tidak ditemukan", "error");
  @endif
  
  $(function() {
    $('.has-error').keypress(function(){
      $(this).removeClass('has-error');
      $('#error-message').remove();
      $(this).find('.help-block').hide();
    });
  });
</script>
  @endsection
