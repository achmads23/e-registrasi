
@extends('layouts.empty')

@section('title', 'Pendaftaran')

@include('plugins.parsley')

@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="col-md-12">
  <div class="box" style="border-top:none; width: 100%;">
    <div class="box-body">
      <div class='col-md-12'>
        <form class="form-horizontal" method="POST" id="daftar-grup-undangan" action="{{route('bimtek.grup-undangan.register')}}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="jumlah_rombongan" class="jumlah_rombongan" value="1">
          <input type="hidden" name="code" id="code" value="{{$acara->code}}">
          <div class='text-center'>
            <h4 class="disable-margin"><b><i>Daftar Undangan</i></b></h4>
          </div>
          <br>

          @if (session('message_success'))
          <div class="callout callout-warning">
            <p>{{session('message_success')}}</p>
          </div>
          @endif
          <div class="form-group">
            <label class="col-lg-2 col-md-2 col-sm-3 control-label" style="text-align: left">OPD<span class="text-red">*</span></label>
            <div class="col-lg-10 col-md-10 col-sm-9 control-label">
              <b>{{Auth::user()->detail->opd}}</b>
            </div>
          </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Kode Undangan</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{$acara->code}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Acara</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{$acara->nama_acr}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Tempat Acara</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{$acara->tempat_acr}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Tanggal Acara</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{date_view($acara->tgl_acr)}} - {{date_view($acara->akhir_tgl_acr)}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Waktu Acara</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{time_view($acara->pukul)}} WIB
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">Tanggal Pendaftaran</label>
          <div class="col-lg-10 col-md-10 col-sm-9 control-label">
            {{date_view($acara->tanggal_awal) . ' - ' . date_view($acara->tanggal_akhir)}}
          </div>
        </div>
        <div class="form-group disable-margin">
          <label class="col-lg-2 col-md-2 col-sm-3 control-label disable-padding-left" style="text-align: left">OPD</label>
          <div class="col-sm-8 control-label">
            @if(count($acara->opds) > 0)
              @foreach ($acara->opds as $key => $opd)
              {{$key+1}}. {{$opd->opd}}<br>  
              @endforeach
            @else 
              -
            @endif
          </div>
        </div>
        <hr>
          <table class='table table-hover'>
            <thead>
              <tr>
                <th>NIP<span class="text-red">*</span></th>
                <th>Nama<span class="text-red">*</span></th>
                <th>Kelamin<span class="text-red">*</span></th>
                <th>Gol<span class="text-red">*</span></th>
                <th>Jabatan<span class="text-red">*</span></th>
                <th>Email<span class="text-red">*</span></th>
                <th>No HP<span class="text-red">*</span></th>
                <th>Kartu Identitas</th>
                <th>Inap<span class="text-red">*</span></th>
                <th><i class="fa fa-cog"></i></th>
              </tr>
            </thead>
            <tbody class="body-rombongan">
              <tr class="hidden">
                <td width="200px">
                  <input type="text" name="nip[]" class="form-control check-nip" placeholder="NIP" data-parsley-errors-messages-disabled>    
                </td>
                <td>
                  <input type="text" name="nama[]" placeholder="Nama" class="form-control input-nama" data-parsley-errors-messages-disabled>
                </td>
                <td width="100px">
                  <select name="jenis_kelamin[]" class="form-control input-jenis-kelamin" data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>L/P</option>
                    <option value="L">L</option>
                    <option value="P">P</option>
                  </select>
                </td>
                <td width="70px" class="row-golongan">
                  <select name="golongan[]" class="select-golongan input-golongan" placeholder="Gol" data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>Gol</option>
                    <option value="IA">IA</option>
                    <option value="IB">IB</option>
                    <option value="IC">IC</option>
                    <option value="ID">ID</option>
                    <option value="IIA">IIA</option>
                    <option value="IIB">IIB</option>
                    <option value="IIC">IIC</option>
                    <option value="IID">IID</option>
                    <option value="IIIA">IIIA</option>
                    <option value="IIIB">IIIB</option>
                    <option value="IIIC">IIIC</option>
                    <option value="IIID">ID</option>
                    <option value="IVA">IVA</option>
                    <option value="IVB">IVB</option>
                    <option value="IVC">IVC</option>
                    <option value="IVD ">IVD</option>
                  </select>
                </td>
                <td width="250px" class="row-jabatan">
                  <select name="jabatan[]" class="select-jabatan" placeholder="Jabatan" data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>Jabatan</option>
                    <option value="PA">PA</option>
                    <option value="KPA">KPA</option>
                    <option value="PPK">PPK</option>
                    <option value="BP ">BP </option>
                    <option value="BPP">BPP</option>
                    <option value="PPKOM">PPKOM</option>
                    <option value="PPTK">PPTK</option>
                    <option value="Administrasi Keuangan">Administrasi Keuangan</option>
                    <option value="Pengurus Barang Aset">Pengurus Barang Aset</option>
                    <option value="Pengurus Barang Persediaan">Pengurus Barang Persediaan</option>
                    <option value="Pemegang Barang Aset">Pemegang Barang Aset</option>
                    <option value="Lain-lain">Lain-lain</option>
                  </select>
                  <div class="hidden input-jabatan">
                    <input type="text" name="jabatan_text[]" class="form-control i-jabatan" placeholder="Jabatan" data-parsley-errors-messages-disabled>
                  </div>
                </td>
                <td>
                  <input type="email" name="email[]" class="form-control"  placeholder="Email" data-parsley-errors-messages-disabled>
                </td>
                <td width="150px">
                  <input type="text" name="hp[]"  class="form-control" placeholder="No HP" data-parsley-errors-messages-disabled>
                </td>
                <td width="133px">
                  <input type="file" name="foto[]" class="form-control file-input" data-parsley-errors-messages-disabled>
                  <div class="hidden control-label">
                    <i class="fa fa-file"></i><span class='nama-file'></span> <i class="fa fa-trash text-danger ganti-file"></i>
                  </div>
                </td>
                <td width="100px">
                  <select name="inap[]" class="form-control" data-parsley-errors-messages-disabled>
                    <option value="1">Inap</option>
                    <option value="0" selected>Tidak</option>
                  </select>
                </td>
                <td style="display:flex;align-items: center;">
                  <a href="javascript:void(0)" class="btn btn-sm btn-success tambah-peserta"><i class="fa fa-plus"></i></a>
                  <a href="javascript:void(0)" class="btn btn-sm btn-danger hapus-peserta hidden"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <tr>
                <td width="200px">
                  <input type="text" name="nip[]" class="form-control check-nip" placeholder="NIP" required data-parsley-errors-messages-disabled>    
                </td>
                <td>
                  <input type="text" name="nama[]" placeholder="Nama" class="form-control input-nama" required data-parsley-errors-messages-disabled>
                </td>
                <td width="100px">
                  <select name="jenis_kelamin[]" class="form-control input-jenis-kelamin" required data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>L/P</option>
                    <option value="L">L</option>
                    <option value="P">P</option>
                  </select>
                </td>
                <td width="70px" class="row-golongan">
                  <select name="golongan[]" class="select-golongan input-golongan" placeholder="Gol" required data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>Gol</option>
                    <option value="IA">IA</option>
                    <option value="IB">IB</option>
                    <option value="IC">IC</option>
                    <option value="ID">ID</option>
                    <option value="IIA">IIA</option>
                    <option value="IIB">IIB</option>
                    <option value="IIC">IIC</option>
                    <option value="IID">IID</option>
                    <option value="IIIA">IIIA</option>
                    <option value="IIIB">IIIB</option>
                    <option value="IIIC">IIIC</option>
                    <option value="IIID">ID</option>
                    <option value="IVA">IVA</option>
                    <option value="IVB">IVB</option>
                    <option value="IVC">IVC</option>
                    <option value="IVD ">IVD</option>
                  </select>
                </td>
                <td width="250px" class="row-jabatan">
                  <select name="jabatan[]" class="select-jabatan" placeholder="Jabatan" data-parsley-errors-messages-disabled>
                    <option value="" selected disabled>Jabatan</option>
                    <option value="PA">PA</option>
                    <option value="KPA">KPA</option>
                    <option value="PPK">PPK</option>
                    <option value="BP ">BP </option>
                    <option value="BPP">BPP</option>
                    <option value="PPKOM">PPKOM</option>
                    <option value="PPTK">PPTK</option>
                    <option value="Administrasi Keuangan">Administrasi Keuangan</option>
                    <option value="Pengurus Barang Aset">Pengurus Barang Aset</option>
                    <option value="Pengurus Barang Persediaan">Pengurus Barang Persediaan</option>
                    <option value="Pemegang Barang Aset">Pemegang Barang Aset</option>
                    <option value="Lain-lain">Lain-lain</option>
                  </select>
                  <div class="hidden input-jabatan">
                    <input type="text" name="jabatan_text[]" class="form-control i-jabatan" placeholder="Jabatan" data-parsley-errors-messages-disabled>
                  </div>
                </td>
                <td>
                  <input type="email" name="email[]" class="form-control"  placeholder="Email" required data-parsley-errors-messages-disabled>
                </td>
                <td width="150px">
                  <input type="text" name="hp[]"  class="form-control" placeholder="No HP" required data-parsley-errors-messages-disabled>
                </td>
                <td width="133px">
                  <input type="file" name="foto[]" class="form-control file-input" required data-parsley-errors-messages-disabled>
                  <div class="hidden control-label">
                    <i class="fa fa-file"></i><span class='nama-file'></span> <i class="fa fa-trash text-danger ganti-file"></i>
                  </div>
                </td>
                <td width="100px">
                  <select name="inap[]" class="form-control" required data-parsley-errors-messages-disabled>
                    <option value="1">Inap</option>
                    <option value="0" selected>Tidak</option>
                  </select>
                </td>
                <td style="display:flex;align-items: center;">
                  <a href="javascript:void(0)" class="btn btn-sm btn-success tambah-peserta"><i class="fa fa-plus"></i></a>
                </td>
              </tr>
            </tbody>
          </table> 
          <div class="box-footer text-right">
            <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
            <button type="button" class="btn btn-primary simpan">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="text-center" style="margin-bottom: 100px">
    <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>
<script>
  @if (session('kuota_habis'))
  swal("Oooops!", "Kuota Undangan Habis", "error");
  @endif
  $(function() {

    function bindCheckNIP(elem){
      console.log(elem);
      if(elem) {
        $element = elem;
      } else {
        $element = $('.check-nip');
      }
      $element.keyup(function(){
        let that = $(this);
        let nip = $(this).val().replace(/\s/g, '');
        if($(this).val() && nip.length == 18){
          $( ".loading" ).show();
          $.ajax({
            url: '{{route('ajax-cek-peserta')}}',
            type: 'POST',
            data: {_token:'{{ csrf_token() }}',nip: nip, code:$('#code').val()},
          })
          .done(function(data) {
            console.log(data);
            $( ".loading" ).hide();
            if(data.data != false){
              if(data.data.is_peserta){
                if(data.data.peserta.status == 1){
                  swal({
                    text: "NIP sudah terdaftar sebagai peserta!",
                    icon: "warning",
                    buttons: {
                      cancel: "Kembali",
                    },
                  })
                  .then((value) => {
                    switch (value) {
                      default:
                        $('#nip').val('');
                        $('#asal_instansi').val('');
                        $('#golongan').val('');
                        $('#nama').val('');
                    }
                  });
                } else if(data.data.peserta.status == 0){
                  swal({
                    text: "NIP sedang dalam proses verifikasi!",
                    icon: "warning",
                    buttons: {
                      cancel: "Kembali",
                      catch: {
                        text: "Kembali ke Awal",
                        value: "catch",
                      },
                    },
                  }).then((value) => {
                    switch (value) {
                      default:
                        $('#nip').val('');
                        $('#asal_instansi').val('');
                        $('#golongan').val('');
                        $('#nama').val('');
                    }
                  });
                } else if(data.data.peserta.status == -1){
                  swal({
                    text: "Verifikasi NIP anda ditolak!",
                    icon: "warning",
                    buttons: {
                      cancel: "Kembali",
                    },
                  }).then((value) => {
                    switch (value) {
                      default:
                        $('#nip').val('');
                        $('#asal_instansi').val('');
                        $('#golongan').val('');
                        $('#nama').val('');
                    }
                  });
                }
              } else {
                console.log('masuk sini');
                console.log(data.data.pegawai.gol + data.data.pegawai.ruang);
                if(data.data.pegawai || {{count($acara->opds)}} == 0){
                  tr = that.closest('tr');
                  tr.find('.input-nama').val(data.data.pegawai.nama);
                  tr.find('.input-jenis-kelamin').val(data.data.pegawai.jk).change();
                  tr.find('.input-golongan')[0].selectize.setValue(data.data.pegawai.gol + data.data.pegawai.ruang);
                } else {
                  swal({
                    title:'Oooops!',
                    text: "OPD "+ nip +" ("+ data.data.opd +") tidak terdaftar!",
                    icon: "warning",
                    buttons: {
                      cancel: "tutup",
                    },
                  }).then((value) => {
                    switch (value) {
                      default:
                        $('#nip').val('');
                        $('#asal_instansi').val('');
                        $('#golongan').val('');
                        $('#nama').val('');
                    }
                  });
                  $('#asal_instansi').val('');
                  $('#golongan').val('');
                  $('#nama').val('');
                }
              }
            }
          })
          .fail(function(error) {
            $( ".loading" ).hide();
            console.log(error)
            console.log("error");
          })
          .always(function() {
            $( ".loading" ).hide();
            console.log("complete");
          });
        } else {
          $('#asal_instansi').val('');
          $('#golongan').val('');
          $('#nama').val('');
        }
      });
    }

    bindCheckNIP(false)

    $('.simpan').on('click', function(event) {
       $('form#daftar-grup-undangan').parsley().whenValidate().done(function(){
            $('form#daftar-grup-undangan').submit();
        });
    });

    $('form#daftar-grup-undangan').parsley();

    select = $('tr:not(.hidden)').find('.select-jabatan').selectize({})[0].selectize;
    $('tr:not(.hidden)').find('.select-jabatan').on('change', function() {
      var value = $(this).val();
      parent = $(this).closest('td');
      if(value == 'Lain-lain'){
        parent.find('.input-jabatan input').prop('required',true);
        parent.find('.input-jabatan').removeClass('hidden');
      } else {
        parent.find('.input-jabatan').addClass('hidden');
        parent.find('.input-jabatan input').prop('required',false);
      }
    });

    $('tr:not(.hidden)').find("input:file").change(function (){
        var fileName = $(this).val();
        parent = $(this).closest('td');
        parent.find('.file-input').addClass('hidden');
        parent.find('div').removeClass('hidden');
        parent.find('.nama-file').html(' File Selected');
      });

    $('tr:not(.hidden)').find('.ganti-file').on('click', function(event) {
      var fileName = $(this).val();
      parent = $(this).closest('td');
      parent.find('input').val('');
      parent.find('.file-input').removeClass('hidden');
      parent.find('div').addClass('hidden');
    });


    $('select.select-acara').parsley({
        required: true,
        trigger: 'focusout',
        classHandler: function(el){
            elem = el.$element.closest(".form-group");
            return elem.find('.selectize-input');
        },
    });

    $('select.select-acara').on('change', function(event) {
      $(this).parsley().validate();
    });


    $('tr:not(.hidden)').find('.select-golongan').selectize({})[0].selectize;
    $('tr:not(.hidden)').find('select.select-jabatan').parsley({
        required: true,
        trigger: 'focusout',
        classHandler: function(el){
            elem = el.$element.closest(".row-jabatan");
            return elem.find('.selectize-input');
        },
    });

    $('tr:not(.hidden)').find('select.select-golongan').parsley({
        required: true,
        trigger: 'focusout',
        classHandler: function(el){
            elem = el.$element.closest(".row-golongan");
            return elem.find('.selectize-input');
        },
    });

    $('tr:not(.hidden)').find("input[type='select-one']").attr('required',false);

    $('tr:not(.hidden)').find('.select-jabatan').on('change', function(event) {
      $(this).parsley().validate();
    });

    $('tr:not(.hidden)').find('.select-golongan').on('change', function(event) {
      $(this).parsley().validate();
    });

    $('.hapus-peserta').on('click', function(event) {
      jum = parseInt($('.jumlah_rombongan').val()) - 1;
      $('.jumlah_rombongan').val(jum);
      tr = $(this).closest('tr');
      tr.remove();
    });

    $('.tambah-peserta').on('click', function(event) {
      jum = parseInt($('.jumlah_rombongan').val()) + 1;
      $('.jumlah_rombongan').val(jum);
      var tr = $('.body-rombongan tr:first-child');
      elemBaru = tr.clone().appendTo( ".body-rombongan" );

      elemBaru.removeClass('hidden');

      elemBaru.find('.select-jabatan').selectize({})[0].selectize;
      elemBaru.find('.select-jabatan').on('change', function() {
        var value = $(this).val();
        parent = $(this).closest('td');
        if(value == 'Lain-lain'){
          parent.find('.input-jabatan').removeClass('hidden');
          parent.find('.input-jabatan input').prop('required',true);
        } else {
          parent.find('.input-jabatan').addClass('hidden');
          parent.find('.input-jabatan input').prop('required',false);
        }
      });

      elemBaru.find('.tambah-peserta').addClass('hidden');
      elemBaru.find('.hapus-peserta').removeClass('hidden');

      elemBaru.find('.hapus-peserta').on('click', function(event) {
        jum = parseInt($('.jumlah_rombongan').val()) - 1;
      $('.jumlah_rombongan').val(jum);
        tr = $(this).closest('tr');
        tr.remove();
      });

      elemBaru.find("input:file").change(function (){
        var fileName = $(this).val();
        parent = $(this).closest('td');
        parent.find('.file-input').addClass('hidden');
        parent.find('div').removeClass('hidden');
        parent.find('.nama-file').html(' File Selected');
      });

      elemBaru.find('.ganti-file').on('click', function(event) {
        var fileName = $(this).val();
        parent = $(this).closest('td');
        parent.find('input').val('');
        parent.find('.file-input').removeClass('hidden');
        parent.find('div').addClass('hidden');
      });
      elemBaru.find("input:not('.i-jabatan'):not([type='select-one'])").attr('required',true);
      
        elemBaru.find('select.select-jabatan').parsley({
          required: true,
          trigger: 'focusout',
          classHandler: function(el){
              elem = el.$element.closest(".row-jabatan");
              return elem.find('.selectize-input');
          },
      });

      elemBaru.find('.select-jabatan').on('change', function(event) {
        $(this).parsley().validate();
      });

      elemBaru.find('.select-golongan').selectize({})[0].selectize;

      bindCheckNIP(elemBaru.find('.check-nip'))

      elemBaru.find('select.select-golongan').parsley({
          required: true,
          trigger: 'focusout',
          classHandler: function(el){
              elem = el.$element.closest(".row-golongan");
              return elem.find('.selectize-input');
          },
      });

      elemBaru.find('.select-golongan').on('change', function(event) {
        $(this).parsley().validate();
      });
    });
  });
</script>

@endsection
