@extends('layouts.main')

@section('title', 'Konfirmasi Undangan')

@section('title-content')
Konfirmasi Undangan
@endsection

@include('plugins.eonasdan-datepicker')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Konfirmasi Undangan</a></li>
<li class="active">Detail</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{route('admin.bimtek.grup-undangan')}}">List</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">Grup</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->name}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Region</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->region}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Asal Instansi</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->opd}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->email}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nomor HP</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->hp}}
        </div>
      </div>
      <br>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->acara->nama_acr}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->acara->pukul}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-10 control-label">
          {{date_view($peserta[0]->acara->tgl_acr)}} - {{date_view($peserta[0]->acara->akhir_tgl_acr)}}
        </div>
      </div>
      <form class="form-horizontal">
      <hr>
      <div class="table-responsive">
      <table class='table table-hover'>
        <thead>
          <tr>
            <th>NIP<span class="text-red">*</span></th>
            <th>Nama<span class="text-red">*</span></th>
            <th>Kelamin<span class="text-red">*</span></th>
            <th>Gol<span class="text-red">*</span></th>
            <th>Jabatan<span class="text-red">*</span></th>
            <th>Email<span class="text-red">*</span></th>
            <th>No HP<span class="text-red">*</span></th>
            <th>Kartu Identitas</th>
            <th>Inap<span class="text-red">*</span></th>
          </tr>
        </thead>
        <tbody class="body-rombongan">
          @foreach ($peserta as $p)
            <tr>
              <td>{{$p->nip}}</td>
              <td>{{$p->nama}}</td>
              <td>{{$p->jenis_kelamin?? '-'}}</td>
              <td>{{$p->golongan}}</td>
              <td>{{$p->jabatan}}</td>
              <td>{{$p->email}}</td>
              <td>{{$p->no_hp}}</td>
              <td><a href="{{ asset($p->foto) }}" target="_blank"><i class='fa fa-download'></i> Download</a></td>
              <td>{{$p->inap? 'Ya':'Tidak'}}</td>
            </tr>  
          @endforeach
          
        </tbody>
      </table>
      </div>  
    </form>

    <form class="form-horizontal" style="display: inline !important;" id="konfirmasi" method="POST" action="{{URL::to('/admin/konfirmasi-grup-undangan/'.$peserta[0]->created_by. '/'. $peserta[0]->acara->id_acr)}}">
        {{ csrf_field()}}
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Nomor Surat<span class="text-red">*</span></label>
        <div class="col-md-4" required>
          <input type="text" name="nomor_surat" class="form-control">
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Tanggal Surat<span class="text-red">*</span></label>
        <div class="col-md-4">
          <input type="text" name="tgl_surat" class="form-control eonasdan-datepicker" required>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-sm-2 control-label" style='text-align: left;'>Tanda Tangan<span class="text-red">*</span></label>
        <div class="col-md-4">
          <div class="radio">
            <label style="margin-right: 10px"><input type="radio" value="1" name="ttd">Ya</label>
            <label><input type="radio" value="0" name="ttd" checked>Tidak</label>
          </div>
        </div>
      </div>
      <input type="hidden" name="result" value="0" id="result">
    </form>
      
    </div>
    <!-- /.box-body -->
  </div>
  <div class="box-footer text-right">
      <button type="button" class="btn btn-danger hapus">Verifikasi Ditolak</button>
      <button type="button" class="btn btn-success setuju">Verifikasi Diterima</button>
    </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah yakin menolak data ini?",
        text: "memproses dan mengirim surat balasan akan membutuhkan waktu, mohon menunggu.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Tolak!",
        closeOnConfirm: false
      },
      function(){
        $( ".loading" ).show();
        $('#result').val(0);
        $('form#konfirmasi').submit();
      });
    });
  });

  $(function () {
    $(".setuju").click(function(){
      swal({
        title: "Apakah yakin menyetujui data ini?",
        text: "memproses dan mengirim surat balasan akan membutuhkan waktu, mohon menunggu.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#00a65a",
        confirmButtonText: "Ya, Setuju!",
        closeOnConfirm: false
      },
      function(){
        $( ".loading" ).show();
        $('#result').val(1);
        $('form#konfirmasi').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
</script>
@endsection
