@extends('layouts.main')

@section('title', 'Konfirmasi Undangan')

@section('title-content')
Konfirmasi Undangan
@endsection

@include('plugins.eonasdan-datepicker')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Konfirmasi Undangan</a></li>
<li class="active">Detail</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{route('admin.bimtek.grup-undangan')}}">List</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        <label class="col-sm-2 control-label">Grup</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->name}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Region</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->region}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Asal Instansi</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->opd}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->email}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Nomor HP</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->user->detail->hp}}
        </div>
      </div>
      <br>
      <div class="row">
        <label class="col-sm-2 control-label">Nama Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->acara->nama_acr}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Waktu Acara</label>
        <div class="col-sm-10 control-label">
          {{$peserta[0]->acara->pukul}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Tanggal Acara</label>
        <div class="col-sm-10 control-label">
          {{date_view($peserta[0]->acara->tgl_acr)}} - {{date_view($peserta[0]->acara->akhir_tgl_acr)}}
        </div>
      </div>
      <div class="row">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-10 control-label">
          @if($peserta[0]->status == -1)
          <span class='text-red'><b>Verifikasi Ditolak</b></span>
          @elseif($peserta[0]->status == 1)
          <span class='text-green'><b>Verifikasi Diterima</b></span>
          @endif
        </div>
      </div>
      <form class="form-horizontal">
      <hr>
      <div class="table-responsive">
      <table class='table table-hover'>
        <thead>
          <tr>
            <th>NIP<span class="text-red">*</span></th>
            <th>Nama<span class="text-red">*</span></th>
            <th>Kelamin<span class="text-red">*</span></th>
            <th>Gol<span class="text-red">*</span></th>
            <th>Jabatan<span class="text-red">*</span></th>
            <th>Email<span class="text-red">*</span></th>
            <th>No HP<span class="text-red">*</span></th>
            <th>Kartu Identitas</th>
            <th>Inap<span class="text-red">*</span></th>
          </tr>
        </thead>
        <tbody class="body-rombongan">
          @foreach ($peserta as $p)
            <tr>
              <td>{{$p->nip}}</td>
              <td>{{$p->nama}}</td>
              <td>{{$p->jenis_kelamin?? '-'}}</td>
              <td>{{$p->golongan}}</td>
              <td>{{$p->jabatan}}</td>
              <td>{{$p->email}}</td>
              <td>{{$p->no_hp}}</td>
              <td><a href="{{ asset($p->foto) }}" target="_blank"><i class='fa fa-download'></i> Download</a></td>
              <td>{{$p->inap? 'Ya':'Tidak'}}</td>
            </tr>  
          @endforeach
          
        </tbody>
      </table>  
      </div>
    </form>
    </div>
    <!-- /.box-body -->
  </div>
  <div class="box-footer text-right">
      <a href="{{URL::previous()}}" class="btn btn-default">Kembali</a>
    </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('js')
@endsection
