@extends('layouts.main')

@section('title', 'Konfirmasi Peserta Undangan')
@include('plugins.datatable')
@section('title-content')
Konfirmasi Peserta Undangan
@endsection

@section('css') @parent
 
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Konfirmasi Peserta Undangan</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{route('admin.bimtek.grup-undangan')}}">List</a></li>
  </li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th style="width:5%">No</th>
          <th class="col-md-2">Grup</th>
          <th class="col-md-2">Acara</th>
          <th class="col-md-2">Region</th>
          <th class="col-md-2">OPD</th>
          <th class="col-md-2">Telp</th>
          <th class="col-md-3">Jumlah Peserta Grup</th>
          <th class="col-md-2">Waktu Pendaftaran</th>
          <th style="width:5%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pesertas as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->user->name}}</td>
          <td>{{$d->acara->nama_acr}}</td>
          <td>{{$d->user->detail->region}}</td>
          <td>{{$d->user->detail->opd}}</td>
          <td>{{$d->user->detail->hp}}</td>
          <td>{{$d->jumlah}}</td>
          <td>{{date_view($d->created_at)}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/konfirmasi-grup-undangan/'.$d->created_by.'/'.$d->acara->id_acr)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="box">
  <div class="box-header with-border">
    Verifikasi Diterima
  </div>
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th style="width:5%">No</th>
          <th class="col-md-2">Grup</th>
          <th class="col-md-2">Region</th>
          <th class="col-md-2">OPD</th>
          <th class="col-md-2">Telp</th>
          <th class="col-md-3">Jumlah Peserta Grup</th>
          <th class="col-md-2">Waktu Pendaftaran</th>
          <th style="width:5%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pesertas_diterima as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->user->name}}</td>
          <td>{{$d->user->detail->region}}</td>
          <td>{{$d->user->detail->opd}}</td>
          <td>{{$d->user->detail->hp}}</td>
          <td>{{$d->jumlah}}</td>
          <td>{{date_view($d->created_at)}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/konfirmasi-grup-undangan/'.$d->created_by.'/'.$d->acara->id_acr . '/detail')}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

<div class="box">
  <div class="box-header with-border">
    Verifikasi Ditolak
  </div>
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th style="width:5%">No</th>
          <th class="col-md-2">Grup</th>
          <th class="col-md-2">Region</th>
          <th class="col-md-2">OPD</th>
          <th class="col-md-2">Telp</th>
          <th class="col-md-3">Jumlah Peserta Grup</th>
          <th class="col-md-2">Waktu Pendaftaran</th>
          <th style="width:5%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pesertas_ditolak as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$d->user->name}}</td>
          <td>{{$d->user->detail->region}}</td>
          <td>{{$d->user->detail->opd}}</td>
          <td>{{$d->user->detail->hp}}</td>
          <td>{{$d->jumlah}}</td>
          <td>{{date_view($d->created_at)}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/konfirmasi-grup-undangan/'.$d->created_by.'/'.$d->acara->id_acr . '/detail')}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('js/download.js')}}"></script>
<script>
  @if(session('filename'))
  window.open('{{ URL::to(session('filename')) }}');
  @endif
  @if (session('tolak_success'))
  swal("Menolak verifikasi data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('setuju_success'))
  swal("Menyetujui verifikasi data berhasil", "Data berhasil disimpan", "success");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection