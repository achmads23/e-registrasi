@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')

<div class="text-center" style="margin: 100px 0px 0px 0px;">
    <h4><b>Pendampingan Teknis</b></h4>
</div>
<div class="text-center" style="margin: 0px 0px 0px 0px; justify-content: center;
    display: flex;">
  <p style="text-align: justify;" id="klinik-center">
    Merupakan salah satu produk dari UPT PPK yang menjadi kunci utama bagi OPD Pemerintah Provinsi Jawa Timur,  untuk menyelesaikan permasalahan tentang Pengelolaan Keuangan Daerah yang ada di organisasi.
  </p>
</div>
<div class="text-center" style="margin: 10px 0px 30px 0px;">
  <a href="{{route('index')}}">
    <h4 class="text-black disable-margin"><i class="fa fa-chevron-left"></i> Kembali</h4>
  </a>
</div>
<div class="text-center">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
</div>
@endsection

@section('js')
<script>
$(function() {
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
