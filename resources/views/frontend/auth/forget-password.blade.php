@extends('layouts.empty')

@section('title', 'Login')

@section('content')
<div class="login-box disable-margin-top">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Lupa password</p>

    
    @if (session('forget_password_success'))
    <div class="callout callout-success">
      <p><b>Sukses</b> <br>Mohon cek email anda segera.</p>
    </div>
    @endif

    @if (session('user_not_found'))
    <div class="callout callout-danger">
      <p><b>Gagal</b> <br>User tidak ditemukan.</p>
    </div>
    @endif

    <form method="POST" action="{{ route('post-forget-password') }}">
        {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Kirim</button>
        </div>
        <div class="col-xs-12 text-center" style="margin-top: 10px;">
          Kembali ke halaman login?  <a href="{{route('public-login')}}"> Login</a>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
