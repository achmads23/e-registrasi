@extends('layouts.empty')

@section('title', 'Login')

@section('content')
<div class="login-box disable-margin-top">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    
    @if (session('login_failed'))
    <div class="callout callout-danger">
      <p><b>Login failed!</b> <br>Invalid username and/or password.</p>
    </div>
    @endif

    @if (session('register_success'))
    <div class="callout callout-success">
      <p><b>Register success!</b> <br>Please check your email to activate your account.</p>
    </div>
    @endif

    @if (session('reset_password_success'))
    <div class="callout callout-success">
      <p><b>Sukses</b> <br>Gunakan password baru anda untuk login.</p>
    </div>
    @endif

    <form method="POST" action="{{ route('public-login') }}">
        {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block text-red">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class='form-group text-right'>
        <a href="{{route('forget-password')}}"><i class="glyphicon glyphicon-lock"></i> Lupa password?</a>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <div class="col-xs-12 text-center" style="margin-top: 10px;">
          Belum punya akun?  <a href="{{route('public-register')}}"> Daftar</a>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
