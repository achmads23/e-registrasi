@extends('layouts.empty')

@section('title', 'Register')

@section('content')
<div class="login-box disable-margin-top">
  
  
  <div class="login-box-body">
    <p class="login-box-msg">Register</p>

    <form method="POST" action="{{ route('public-post-register') }}">
        {{ csrf_field() }}

      @if (session('register_failed'))
      <div class="callout callout-danger">
        <p><b>Register failed.</b> <br>
        @php $errors = session('register_failed'); @endphp
        @foreach ($errors->all() as $error)
          {{$error}}<br>
        @endforeach
      </p>
      </div>
      @endif

      @if (session('register_failed_text'))
      <div class="callout callout-danger">
        <p><b>Register failed.</b> <br>
        {{session('register_failed_text')}}
        </p>
      </div>
      @endif
      
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{ old('nama') }}" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group text-center">
        <label style='margin-right: 15px'>
          <input type="radio" name="jenis_kelamin" value="L"> Laki-laki
        </label>
        <label>
          <input type="radio" name="jenis_kelamin" value="P"> Perempuan
        </label>
      </div>

      <div class="form-group has-feedback">
        <select type="text" id="region" name="region" value="{{ old('region')}}" class="selectize-region" placeholder="Daerah" required>
            <option value="Provinsi Jawa Timur">Provinsi Jawa Timur</option>
            <option value="Kabupaten/Kota">Kabupaten/Kota</option>
            <option value="Lain-lain">Lain-lain</option>
        </select>
      </div>

      <div class="form-group has-feedback hidden" id="kabkot-zone">
        <select type="text" name="kabkot" value="{{ old('kabkot')}}" class="selectize-kab-kot" placeholder="Pilih Kabupaten/Kota" >
          @foreach ($kabkots as $kabkot)
            <option value="{{$kabkot->name}}">{{$kabkot->name}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group has-feedback hidden" id="opd-provinsi">
        <select type="text" name="opd" value="{{ old('opd')}}" class="selectize-opd" placeholder="Pilih OPD" >
          @foreach ($opds as $opd)
            <option value="{{$opd->opd}}">{{$opd->opd}}</option>
          @endforeach
        </select>
      </div>

      <div class="form-group has-feedback hidden" id="opd-other">
        <input type="text" class="form-control" placeholder="OPD" name="opd-text" value="{{ old('opd-text') }}">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="No HP" name="hp" value="{{ old('hp') }}" required>
      </div>

      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <div class="col-xs-12 text-center" style="margin-top: 10px;">
          Sudah punya akun?  <a href="{{route('public-login')}}"> Login</a>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('css')
<link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">
@stop
@section('js')
<script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>
<script>
$(function() {
  
  selectizeregion = $('.selectize-region').selectize({})[0].selectize;
  selectizeregion.setValue({{old('region')}});
  selectizekabkot = $('.selectize-kab-kot').selectize({})[0].selectize;
  selectizekabkot.setValue({{old('kabkot')}});
  selectizeopd = $('.selectize-opd').selectize({})[0].selectize;
  selectizeopd.setValue({{old('opd')}});

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $(this).find('.help-block').hide();
  });

  $('#region').change(function(event) {
    value = $(this).val();
    if(value == "Provinsi Jawa Timur"){
      $('#kabkot-zone').addClass('hidden');
      $('#kabkot-zone').find('select').prop('required',false);
      $('#kabkot-zone').find('select').prop('disabled',true);

      $('#opd-provinsi').removeClass('hidden');
      $('#opd-provinsi').find('select').prop('required',true);
      $('#opd-provinsi').find('select').prop('disabled',false);

      $('#opd-other').addClass('hidden');
      $('#opd-other').find('input').prop('required',false);
      $('#opd-other').find('input').prop('disabled',true);
    } else if(value == "Kabupaten/Kota"){
      $('#opd-provinsi').addClass('hidden');
      $('#opd-provinsi').find('select').prop('required',false);
      $('#opd-provinsi').find('select').prop('disabled',true);

      $('#opd-other').removeClass('hidden');
      $('#opd-other').find('input').prop('required',true);
      $('#opd-other').find('input').prop('disabled',false);

      $('#kabkot-zone').removeClass('hidden');
      $('#kabkot-zone').find('select').prop('required',true);
      $('#kabkot-zone').find('select').prop('disabled',false);
    } else {
      $('#kabkot-zone').addClass('hidden');
      $('#kabkot-zone').find('select').prop('required',false);
      $('#kabkot-zone').find('select').prop('disabled',true);
      $('#opd-provinsi').addClass('hidden');
      $('#opd-provinsi').find('input').prop('required',false);
      $('#opd-provinsi').find('input').prop('disabled',true);

      $('#opd-other').removeClass('hidden');
      $('#opd-other').find('input').prop('required',true);
      $('#opd-other').find('input').prop('disabled',false);
    }
  });
})
</script>
@endsection
