@extends('layouts.empty')

@section('title', 'Login')

@section('content')
<div class="login-box disable-margin-top">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Reset password</p>

    @if (session('password_different'))
    <div class="callout callout-danger">
      <p>Password tidak sama.</p>
    </div>
    @endif

    <form method="POST" action="{{ route('post-reset-password',['id' => $token]) }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Password Baru" name="password" value="{{ old('password') }}" required autofocus>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <label>Ketik Ulang Password</label>
        <input type="password" class="form-control" placeholder="Ketik Ulang Password" name="repassword" value="{{ old('repassword') }}" required autofocus>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
