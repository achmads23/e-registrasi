@extends('layouts.template.main')

@section('title', $kategori->nama)

@section('content')
<section id="klinik-center" class="section" style="background: linear-gradient(95deg, #5533ff 40%, #25ddf5 100%) !important;    padding: 130px 0px 20px 0px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-title" style="color:white">Klinik Center Online</h2>
				<nav aria-label="breadcrumb" class="br">
					<ol class="breadcrumb" style="background: transparent;">
						<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
						<li class="breadcrumb-item" aria-current="page">Klinik Center Online</li>
						<li class="breadcrumb-item" aria-current="page">Automisasi Jawaban</li>
						@foreach($parents as $key => $parent)
							@if($parent->id != $kategori->id)
								<li class="breadcrumb-item" aria-current="page"><a href="{{route('klinik-center.detail',['id'=> $parent->id, 'sub_id' => $parent->child[0]->id])}}">{{$parent->nama}}</a></li>
							@endif
						@endforeach
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="klinik-center" class="section" style="padding:60px 0;">
	<div class="container">
		<div class="row mb-5">
			<div class='col-lg-12 wysiwyg'>
				<h4><b>{{$kategori->pengantar_pertanyaan}}</b></h4>
				{!!$kategori->pengantar_jawaban!!} 
			</div>
		</div>
		<div class="row"  style="min-height: 300px;">
			<div class="col-md-4">
				<h5 class="mb-3">
					<span style="font-size: 15px;"> <i>Sub-Kategori</i></span> <br>
					@if($kategori->parent)
					<b><a href="{{route('klinik-center.detail',['id'=> $kategori->parent->id, 'sub_id' => $kategori->id])}}">{{$kategori->nama}}</a></b></b>
					@else
					<b>{{$kategori->nama}}</b></b>
					@endif
				</h5>
				<div id="accordion">
					@foreach($kategori->child as $key => $selevel)
					<div class="card mb-3">
						<div class="card-header" id="heading{{$key}}" data-toggle="collapse" data-target=".cCollapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}" style="cursor: pointer">
							<h5 class="mb-0">
								{{$selevel->nama}}
							</h5>
						</div>

						<div id="collapse{{$key}}" class="collapse cCollapse{{$key}} {{$sub_id != false && $selevel->id == $sub_id ? 'show' : ''}}" aria-labelledby="heading{{$key}}" data-parent="#accordion">
							<div class="card-body" style="color:black">
								@if(count($selevel->child) > 0)
								@foreach($selevel->child as $k => $child)
								<a href="{{route('klinik-center.detail',['id'=> $selevel->id])}}/{{$child->id}}">{{$k+1}}. {{$child->nama}}</a><br>
								@endforeach
								@else
								<i>Tidak punya sub-kategori</i>
								@endif
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-8 pembatas-kiri">
				@foreach($kategori->child as $k => $child)
				<div class="collapse cCollapse{{$k}} {{$sub_id != false && $child->id == $sub_id ? 'show' : ''}}" aria-labelledby="heading{{$k}}" data-parent=".pembatas-kiri">
					<div class="row">
						<div class="col-12  wysiwyg">
							<div class="row" style="margin-bottom: 20px;">
								<div class="col-12">
									<div class="feature-icon-detail" style="    float: left;">
					                  <i class="lni-{{$parents[0]->icon}}"></i>
					                </div>
				                	<h2 style="height: 100%;align-items: center;display: flex;">{{$child->nama}}</h2>
				                </div>
			            	</div>
			                <div>
								
								@if($child->gambar)
								<img src="{{asset($child->gambar)}}" class="mb-2" width="100%">
								@endif
								{!!$child->uraian!!}
			                </div>
						</div>
					</div>
					<br>
					<br>
					@if(count($child->child) > 0)
					<h5>{{$child->nama}} terdiri dari: </h5>
					<div class="row">
						<div class="col-12">
						@foreach($child->child as $k => $ch)
						<a href="{{route('klinik-center.detail',['id'=> $ch->id])}}/{{$ch->id}}">{{$k+1}}. {{$ch->nama}}</a><br>
						@endforeach
						</div>
					</div>
					@endif

					<div class="text-right">
						<span class="mr-2">
							<i class="fa fa-eye"></i> {{$kategori->view}}
						</span>
						<span class="mr-2">
							<a href="#!" class='thumbs like' onclick="like_dislike(1,{{$kategori->id}})">
							<i class="fa fa-thumbs-up"></i> <span class="counter">{{$kategori->like}}</span>
							</a>
						</span>
						<span>
							<a href="#!" class='thumbs dislike' onclick="like_dislike(0,{{$kategori->id}})">
								<i class="fa fa-thumbs-down"></i> <span class="counter">{{$kategori->dislike}}</span>
							</a>
						</span>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
</section>
@endsection


@section('js')
<script type="text/javascript">
	function like_dislike(status, id){
		let like = localStorage.getItem("status");
		if(like != undefined){
			like = JSON.parse(localStorage.getItem("status"));
		}
		let local_id = localStorage.getItem("id");

		let text_thumb = "";
		let text_key = -1;
		if(local_id != undefined){
			local_id = JSON.parse(localStorage.getItem("id"));
			for(var key in local_id) {
			    if(local_id[key] == id) {
					text_thumb = like[key];
					text_key = key;
					break;
			    }
			}
		}
    	$.ajax({
    		url: '{{route('klinik-center.like-dislike')}}',
    		type: 'POST',
    		data: {id: id, status: status, _token : '{{csrf_token()}}', prev: text_thumb},
    	})
    	.done(function(data) {
    		if(status == 1){
    			if(text_thumb == 'like'){
    				$('.thumbs').removeClass('selected');
    				like.splice(text_key, 1);
    				local_id.splice(text_key, 1);
    				localStorage.setItem("status", JSON.stringify(like));
    				localStorage.setItem("id",JSON.stringify(local_id));
    			} else {
    				var arr_id = [];
    				var len_id = -1;
    				if(local_id != undefined){
    					arr_id = local_id;
    					len_id = local_id.length;
    				}

    				var arr_like = [];
    				var len_like = -1;
    				if(like != undefined){
    					arr_like = like;
    					len_like = like.length;
	    				
	    				let flag = 1;
	    				for(var key in arr_id) {
						    if(arr_id[key] == id) {
						        arr_like[key] = "like";
	    						arr_id[key] = id;
	    						flag = 0;
	    						break;
						    }
						}

						if(flag){
							arr_like[len_like] = "like";
    						arr_id[len_id] = id;
						}
    				} else {
    					arr_like[len_like+1] = "like";
						arr_id[len_id+1] = id;
    				}

    				localStorage.setItem("status", JSON.stringify(arr_like));
    				localStorage.setItem("id",JSON.stringify(arr_id));
					check_like_dislike()
    			}
    		} else {
    			if(text_thumb == 'dislike'){
    				like.splice(text_key, 1);
    				local_id.splice(text_key, 1);
    				localStorage.setItem("status", JSON.stringify(like));
    				localStorage.setItem("id",JSON.stringify(local_id));
    			} else {
    				var arr_id = [];
    				var len_id = -1;
    				if(local_id != undefined){
    					arr_id = local_id;
    					len_id = local_id.length;
    				}

    				var arr_like = [];
    				var len_like = -1;
    				if(like != undefined){
    					arr_like = like;
    					len_like = like.length;
	    				
	    				let flag = 1;
	    				for(var key in arr_id) {
						    if(arr_id[key] == id) {
						        arr_like[key] = "dislike";
	    						arr_id[key] = id;
	    						flag = 0;
	    						break;
						    }
						}

						if(flag){
							arr_like[len_like] = "dislike";
    						arr_id[len_id] = id;
						}
    				} else {
    					arr_like[len_like+1] = "dislike";
						arr_id[len_id+1] = id;
    				}


    				localStorage.setItem("status", JSON.stringify(arr_like));
    				localStorage.setItem("id",JSON.stringify(arr_id));
    				check_like_dislike()
    			}
    		}
    		$('.like .counter').html(data.like)
    		$('.dislike .counter').html(data.dislike)
    		
    	})
    	.fail(function() {
    		console.log("error");
    	})
    	.always(function() {
    		console.log("complete");
    	});
    }

    function check_like_dislike(){
    	let like = localStorage.getItem("status");
		if(like != undefined){
			like = JSON.parse(localStorage.getItem("status"));
		}
		let local_id = localStorage.getItem("id");
		if(local_id != undefined){
			local_id = JSON.parse(localStorage.getItem("id"));
		}

		if(like != undefined){
			for(var key in local_id) {
			    if(local_id[key] == {{$kategori->id}}) {
			        $('.thumbs').removeClass('selected');
			    	if(like[key] == 'like'){
			    		$('.like').addClass('selected');
			    	} else if(like[key] == 'dislike'){
			    		$('.dislike').addClass('selected');
			    	}
			    }
			}
		}
    }

    $(document).ready(function() {
    	check_like_dislike()
    });
</script>
@endsection