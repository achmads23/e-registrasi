@extends('layouts.template.main')

@section('title','Hasil pencarian')

@section('content')
<section id="klinik-center" class="section" style="background: linear-gradient(95deg, #5533ff 40%, #25ddf5 100%) !important;    padding: 130px 0px 20px 0px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-title" style="color:white">Klinik Center Online</h2>
				<nav aria-label="breadcrumb" class="br">
					<ol class="breadcrumb" style="background: transparent;">
						<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Home</a></li>
						<li class="breadcrumb-item" aria-current="page">Klinik Center Online</li>
						<li class="breadcrumb-item" aria-current="page">Automisasi Jawaban</li>
						<li class="breadcrumb-item" aria-current="page">Pencarian</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="klinik-center" class="section" style="padding:60px 0;">
	<div class="container">
		<div class="row mb-5">
			<div class='col-lg-12 text-center'>
				<h4><b>Hasil Pencarian '{{$pencarian ?? ' '}}'</b></h4>
			</div>
		</div>
		@if(count($results) <= 0)
		<div class="row" style="min-height: 300px;">
			<div class="col-md-12 text-center">
				<i>Data tidak ditemukan</i>
			</div>
		</div>
		@else 
			<div class="row" style="min-height: 300px;">
			@foreach($results as $key => $result)
				@if($result->id_parent != 0)
				<div class="col-md-12 mb-4">
					<a href="{{route('klinik-center.detail',['id'=> $result->parent->id,'sub_id'=> $result->id])}}">
					<h4>{{$result->nama}}</h4>
					</a>
					@php
		                $string = strip_tags($result->uraian);
		                if (strlen($string) > 100) {

		                  $stringCut = substr($string, 0, 100);
		                  $endPoint = strrpos($stringCut, ' ');

		                  $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
		                  $string .= '....';
		                }
		                @endphp
		                <p>{{$string}}</p>
				</div>
				@endif
			@endforeach
			</div>
		@endif
		{{-- <div class="row">
			<div class="col-md-4">
				<h5 class="mb-3"><b>Sub-Kategori</b></h5>
				<div id="accordion">
					@foreach($kategori->child as $key => $selevel)
					<div class="card mb-3">
						<div class="card-header" id="heading{{$key}}" data-toggle="collapse" data-target=".cCollapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}" style="cursor: pointer">
							<h5 class="mb-0">
								{{$selevel->nama}}
							</h5>
						</div>

						<div id="collapse{{$key}}" class="collapse cCollapse{{$key}} {{$sub_id != false && $selevel->id == $sub_id ? 'show' : ''}}" aria-labelledby="heading{{$key}}" data-parent="#accordion">
							<div class="card-body" style="color:black">
								@if(count($selevel->child) > 0)
								@foreach($selevel->child as $k => $child)
								<a href="{{route('klinik-center.detail',['id'=> $selevel->id])}}/{{$child->id}}">{{$k+1}}. {{$child->nama}}</a><br>
								@endforeach
								@else
								<i>Tidak punya sub-kategori</i>
								@endif
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-8 pembatas-kiri">
				@foreach($kategori->child as $k => $child)
				<div class="collapse cCollapse{{$k}} {{$sub_id != false && $child->id == $sub_id ? 'show' : ''}}" aria-labelledby="heading{{$k}}" data-parent=".pembatas-kiri">
					<div class="row">
						<div class="col-12">
							<div class="feature-icon-detail" style="float:left">
			                  <i class="lni-{{$parents[0]->icon}}"></i>
			                </div>
			                <div>
								<h2>{{$child->nama}}</h2>
								<img src="{{asset($child->gambar)}}" class="mb-2" width="100%">
								{!!$child->uraian!!}
			                </div>
						</div>
					</div>
					<br>
					<br>
					<h5>{{$child->nama}} terdiri dari: </h5>
					<div class="row">
						<div class="col-12">
						@if(count($child->child) > 0)
						@foreach($child->child as $k => $ch)
						<a href="{{route('klinik-center.detail',['id'=> $ch->id])}}/{{$ch->id}}">{{$k+1}}. {{$ch->nama}}</a><br>
						@endforeach
						@else
						<i>-</i>
						@endif
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div> --}}
	</div>
</div>
</section>
@endsection