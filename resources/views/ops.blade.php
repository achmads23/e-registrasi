@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')
<div class="login-box disable-margin-top">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h4 class="text-center disable-margin">Ooops!</h4>
    <br>
    <div class="text-center">
      Halaman Tidak Ditemukan.
    </div>
  </div>
  <br>
  <div class="text-center" style="margin-bottom: 100px">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
    </div>
</div>
@endsection

@section('js')
<script>
$(function() {
  window.setTimeout(function(){
    window.location.href = '{{URL_SERVER}}';
  }, 5000);
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
