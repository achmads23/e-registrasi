@php use App\User; @endphp

@extends('layouts.main')

@section('title', 'Edit Kategori')

@section('title-content')
Automatisasi Jawaban | Edit Kategori
@endsection

@include('plugins.ckeditor')
@include('plugins.cropper')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Automatisasi Jawaban</li>
<li class="active">Edit Sub-Kategori</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/faq/sub-kategori')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/faq/create-sub-kategori')}}">Buat Sub-Kategori</a></li>
  <li class="active"><a href="#!">Edit</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class='no-style mb-0'>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <form class="form-horizontal" method="POST" action="{{URL::to('/admin/faq/sub-kategori/'.$kategori->id)}}">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama Sub-Kategori</label>
        <div class="col-sm-10">
          <input type="text" name="name" value="{{ old('name',$kategori->nama)}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 control-label">Gambar</label>
        <div class="col-sm-10">
          @if($kategori->gambar)
          {{ $kategori->gambar}}
          <div class="form-check" style="    display: inline-block;margin-left: 10px;color:red">
            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="hapus_image" value="1">
            <label class="form-check-label" for="exampleCheck1">Hapus</label>
          </div>
          <br>
          @endif
          <input type="hidden" name="image" id="cropped-file">
          <div class="selected-picture hidden">
            <span id="image-placeholder">
              <img src="" style='height:250px'>
            </span>

            <div style="margin-bottom:10px;text-align:center;" id="result">
            </div>

            <div style="text-align:center;" id="cropper-zone">
              <img id="image-cropper" src="" style='height:250px'>
              <button type="button" class="btn btn-primary crop-image" style="margin-top:10px;width:100%">
                Crop
              </button>
            </div>
          </div>
          <div class="select-picture" style="margin-top:10px;">
            <div class='input-file'>
              <input type="file" id="upload-gambar" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Uraian</label>
        <div class="col-sm-10">
          <textarea name="uraian" id="editor1" rows="20" cols="80">{{ old('uraian',$kategori->uraian)}}</textarea>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
  CKEDITOR.replace( 'editor1',{
    height: 400,
    extraPlugins: 'filebrowser',
    filebrowserUploadUrl: '{{URL::to('upload_image')}}',
    filebrowserUploadMethod: 'form'

  } );

</script>
<script type="text/javascript" src="{{asset('js/uploadfile.js')}}"></script>
@endsection
