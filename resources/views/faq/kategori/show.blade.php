@extends('layouts.main')

@section('title', 'Kategori | Show')

@section('title-content')
Kategori | Show
@endsection

@include('plugins.ckeditor')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Kategori</li>
<li class="active">Show</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/faq/kategori')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/faq/create')}}">Buat Kategori Baru</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>


<div class="row">
  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
        <div class="user">
          <div class="row">
            <div class="col-md-12">
              <div class="feature-icon" style="float:left">
                <i class="lni-{{$kategori->icon}}"></i>
              </div>
              <div>
                <i>Urutan {{$kategori->urutan}}<br></i>
                <h4 style="margin-top:0px;"><b>{{$kategori->nama}}</b></h4>
                {!!$kategori->uraian!!}
              </div>
            </div>
            
          </div>
        </div>
        <br>
        <div class="box-footer text-right">
          <form id="hapus" method="POST" action="{{URL::to('/admin/faq/'.$kategori->id)}}">
            {{ method_field('DELETE') }}
            {{ csrf_field()}}
            <button type="button" class="btn btn-danger hapus">Hapus</button>
            <a href="{{URL::to('/admin/faq/'.$kategori->id . '/edit')}}" class="btn btn-success">Ubah</a>
          </form>
        </div>
      </div>
      <!-- /.box-footer -->
    </div>
  </div>
</div>
<div class="detail-kategori">
  <div class="row">
    <div class='col-md-6 box-pengantaran-jawaban'>
      <div class="box box-success">
        <div class="box-header">    
          Pengantar Pertanyaan dan Jawaban
        </div>
        <div class="box-body">
          <form class="form-horizontal" action="#!">
            <input type="hidden" name="type" value="pertanyaan">
            <div class="form-group">
              <label class="col-sm-2 control-label">Pertanyaan</label>
              <div class="col-sm-10">
                <input type="text" name="k_pertanyaan" value="{{ old('k_pertanyaan',$kategori->pengantar_pertanyaan)}}" class="form-control" placeholder="Pertanyaan" required>
              </div>
            </div>   
            <div class="form-group">
              <label class="col-sm-2 control-label">Jawaban</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="k_jawaban" id="k_editor" rows="3" style="width:100%" placeholder="Jawaban" required>{{ old('k_jawaban',$kategori->pengantar_jawaban)}}</textarea>
              </div>
            </div>
            <div class="text-right">
              <button type="button" class="btn btn-primary submit-pertanyaan-jawaban" data-id="{{$kategori->id}}">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header">    
          Sub kategori
          <a href="#!" onclick="addSubKategori('{{$kategori->id}}','{{$kategori->id}}',this)"class="btn btn-primary btn-sm pull-right">Tambah</a>  
        </div>
        <div class="box-body">
          <table class="table table-stripped table-bordered">
            <thead>
              <tr>
                <th>Nama Sub-Kategori</th>
                <th>Urutan</th>
                <th class="text-center">Anak</th>
                <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
              </tr>
            </thead>
            <tbody>
            @foreach ($kategori->child as $key => $value)
            <tr>
              <td>{{$value->nama}}</td>
              <td width="150px">
                <form method="POST" action="{{URL::to('/admin/faq/ganti-urutan/'.$value->id)}}">
                  {{ csrf_field() }}
                  <div style="display: flex;">
                    <input type="number" name="urutan" class="form-control" value="{{$value->urutan}}">
                    <a href="#!" onclick="ajax_urutan('{{$value->id}}', this)" class='btn btn-sm btn-primary'><i class="fa fa-save"></i></a>
                  </div>
                </form>
              </td>
              <td class="text-center" id="child-subkategori{{$value->id}}">{{count($value->child)}}</td>
              <td nowrap>
                <a href="#!" onclick="detach('{{$kategori->id}}','{{$value->id}}','{{$value->nama}}',this)" class="btn btn-warning btn-sm"><i class="fa fa-unlink"></i></a>
                <a class="btn btn-info btn-sm" onclick="collapsed()" data-toggle="collapse" data-target="#element{{$value->id}}" aria-expanded="true" aria-controls="element{{$key}}"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  @foreach ($datas as $data)
  <div class="row">
    @php

    $temp = App\Model\FAQ\Kategori::where('id',$data->parent->id)->first();
    $class = '';
    while($temp->id_parent != 0) {
      $class .= ' parent'.$temp->id;
      $temp = App\Model\FAQ\Kategori::where('id',$temp->id_parent)->first();
    }
    @endphp
    <div class='col-lg-12 collapse anakanak {{$class}}' id="element{{$data->id}}">
      <div class="box box-success" style='border-color:orange !important'>
        <div class="box-header">  
          <b>Sub-Kategori {{$data->nama}}</b>
          <a href="#!" onclick="close_collapsed(this)" class="pull-right"><i class="fa fa-close"></i></a>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6 box-pengantaran-jawaban">
              <h5>Pengantar Pertanyaan dan Jawaban</h5>
              <hr>
              <form class="form-horizontal" method="POST" action="{{route('admin.faq.pertanyaan.create',['id' => $data->id])}}" id="add-pertanyaan">
                {{ csrf_field() }}
                <input type="hidden" name="id_kategori" value="{{$kategori->id}}">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Pertanyaan</label>
                  <div class="col-sm-10">
                    <input type="text" name="k_pertanyaan" value="{{ old('pertanyaan',$data->pengantar_pertanyaan)}}" class="form-control" placeholder="Pertanyaan" required>
                  </div>
                </div>   
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jawaban</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="k_jawaban" id="editor{{$data->id}}" rows="3" style="width:100%" placeholder="Jawaban" required>{{ old('jawaban',$data->pengantar_jawaban)}}</textarea>
                  </div>
                </div>
                <div class="text-right">
                  <button  type="button" class="btn btn-primary submit-pertanyaan-jawaban" data-id="{{$data->id}}">Simpan</button>
                </div>
              </form>
            </div>
            <div class="col-md-6">
              <h5>Sub kategori <a href="#!" onclick="addSubKategori('{{$kategori->id}}','{{$data->id}}',this)" class="btn btn-primary btn-sm pull-right">Tambah</a></h5>
              <hr>
              <table class="table table-stripped table-bordered">
                <thead>
                  <tr>
                    <th>Nama Sub-Kategori</th>
                    <th>Urutan</th>
                    <th class="text-center">Anak</th>
                    <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($data->child as $key => $value)
                <tr>
                  <td>{{$value->nama}}</td>
                  <td width="150px">
                    <form method="POST" action="{{URL::to('/admin/faq/ganti-urutan/'.$value->id)}}">
                      {{ csrf_field() }}
                      <input type="hidden" name="id_kategori" value="{{$kategori->id}}">
                      <div style="display: flex;">
                        <input type="number" name="urutan" class="form-control" value="{{$value->urutan}}">
                        <a href="#!" onclick="ajax_urutan('{{$value->id}}', this)" class='btn btn-sm btn-primary'><i class="fa fa-save"></i></a>
                      </div>
                    </form>
                  </td>
                  <td class="text-center" id="child-subkategori{{$value->id}}">{{count($value->child)}}</td>
                  <td nowrap>
                    <a href="#!" onclick="detach('{{$data->id}}','{{$value->id}}','{{$value->nama}}',this)" class="btn btn-warning btn-sm"><i class="fa fa-unlink"></i></a>
                    <a onclick="collapsed_setara({{$data->id}},{{$value->id}})" data-toggle="collapse" data-target="#element{{$value->id}}" aria-expanded="true" aria-controls="element{{$key}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
  <form id='detach' method="POST" action="{{route('admin.faq.detach-sub-kategori')}}">
    {{ csrf_field()}}
    <input type="hidden" name="id" value="{{$kategori->id}}">
    <input type="hidden" name="id_sub_kategori" value="">
    <input type="hidden" name="from" value="">
  </form>
</div>
@endsection

@section('modal')
@include('faq.modal-attach-sub-kategori')
@endsection

@section('js')
<script>

  $('.detail-kategori').on('click', '.submit-pertanyaan-jawaban', function(event) {
    id = $(this).data('id');
    parent = $(this).closest('.box-pengantaran-jawaban');
    $.ajax({
      url: '{{url('admin/faq')}}/' + id + '/ajax-pertanyaan',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
        pertanyaan: parent.find('input[name="k_pertanyaan"]').val(),
        jawaban: parent.find('textarea[name="k_jawaban"]').val()
      },
    })
    .done(function(data) {
      snackbar_show('Pertanyaan dan Jawaban berhasil disimpan');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });   
  function collapsed(id){
    $('.anakanak').collapse('hide');
    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
  }

  function collapsed_setara(parent_id,id){
    $('.parent'+parent_id).collapse('hide');
    $('#element' + id).addClass('parent'+parent_id);
    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
  }

  function close_collapsed(elem){
    $(elem).closest('.anakanak').collapse('hide');
  }

  function detach(kategori_id,id,nama,el){
    console.log(kategori_id);
    swal({
      title: "Apakah anda yakin melepaskan sub-kategori '"+nama+"'' ini?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Ya!",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
      url: '{{url('admin/faq')}}/' + id + '/ajax-unlink',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
      },
      })
      .done(function(data) {
        var $select = $('#id_sub_kategori').selectize();
        var control = $select[0].selectize;
        control.addOption({value:id,text:nama});

        $(el).closest('tr').remove();
        $('#element'+ id).remove();
        swal.close()
        snackbar_show('Detach Sub-Kategori berhasil');

        jumlah = parseInt($('#child-subkategori' + kategori_id).html())
        $('#child-subkategori' + kategori_id).html(jumlah - 1)
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
    });
  }

  var elemTable;
  function addSubKategori(redirect,id,el){
    elemTable = $(el).closest('.col-md-6').find('table');
    $modalAttach = $('#modal-attach-sub-kategori');
    $modalAttach.find('input#id-kategori').val(id);
    $modalAttach.find('input#from').val('kategori');
    $modalAttach.find('input#redirect').val(redirect);
    $modalAttach.modal('show');
  }

  function ajax_urutan(id,el){
    $.ajax({
      url: '{{url('admin/faq')}}/' + id + '/ajax-urutan',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
        urutan: $(el).closest('tr').find('input[name="urutan"]').val()
      },
      })
      .done(function(data) {
        snackbar_show('Urutan berhasil disimpan');
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
  }

  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah anda yakin kategori ini?",
        text: "Semua yang berbuhungan dengan kategori ini seperti pertanyaan ataupun sub-kategori akan terhapus juga",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
  
  @if (session('create_sub_kategori_success'))
  swal("Membuat sub kategori berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('create_pertanyaan_success'))
  swal("Membuat pertanyaan berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_pertanyaan_success'))
  swal("Hapus pertanyaan berhasil", "Data berhasil dihapus", "success");
  @endif

  @if (session('attach_success'))
  swal("Menambahkan sub-kategori berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('detach_success'))
  swal("Melepas sub-kategori berhasil", "Data berhasil dihapus", "success");
  @endif

  @if (session('pertanyaan_success'))
  swal("Menyimpan pertanyaan dan jawaban berhasil", "Data berhasil disimpan", "success");
  @endif

  @if (session('ganti_urutan_success'))
  swal("Merubah urutan berhasil", "Data berhasil disimpan", "success");
  @endif

  
  
</script>
@endsection
