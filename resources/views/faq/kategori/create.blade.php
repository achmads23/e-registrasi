@php use App\User; @endphp

@extends('layouts.main')

@section('title', $kategori ? 'Buat Sub-Kategori : ' . $kategori->nama:'Buat Kategori')

@section('title-content')
{{ $kategori ? 'Buat Sub-Kategori : ' . $kategori->nama:'Buat Kategori'}}
@endsection

@include('plugins.ckeditor')
@include('plugins.cropper')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Automatisasi Jawaban</li>
@if($kategori)
<li class="active">Buat Sub-Kategori: {{$kategori->nama}}</li>
@else 
<li class="active">Buat Kategori</li>
@endif
@endsection

@section('content')
<ul class="nav nav-pills">
  @if($kategori)
  <li class=""><a href="{{URL::to('/admin/faq/sub-kategori')}}">List</a></li>
  @else
  <li class=""><a href="{{URL::to('/admin/faq/kategori')}}">List</a></li>
  @endif

  @if($kategori)
  <li class=""><a href="{{route('admin.faq.show',['id'=>$kategori->id])}}">Detail</a></li>
  @endif
  <li class=""><a href="#!">{{ $kategori ? 'Buat Sub-Kategori : ' . $kategori->nama:'Buat Kategori'}}</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class='no-style mb-0'>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <form class="form-horizontal" method="POST">
      {{ csrf_field() }}
      @if($kategori)
      <div class="form-group">
        <label class="col-sm-2 control-label">Induk Kategori</label>
        <div class="col-sm-10 control-label">
          {{$kategori->nama}}
        </div>
      </div>
      @endif
      <div class="form-group">
        <label class="col-sm-2 control-label">Nama Kategori</label>
        <div class="col-sm-10">
          <input type="text" name="name" value="{{ old('name')}}" class="form-control" placeholder="Nama" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Urutan</label>
        <div class="col-sm-10">
          <input type="number" name="urutan" value="{{ old('urutan')}}" class="form-control" placeholder="Urutan" required>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Icon Kategori</label>
        <div class="col-sm-10">
          <input type="text" name="icon" value="{{ old('icon')}}" class="form-control" placeholder="nama-icon" required>
          <small class="form-text text-muted">Pilih icon <a href="https://lineicons.com/icons/">disini</a> (<a href="https://lineicons.com/icons/">https://lineicons.com/icons/</a>) dan tulis nama iconnya </small>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label">Uraian</label>
        <div class="col-sm-10">
          <textarea name="uraian" id="editor1" rows="20" cols="80">{{ old('uraian')}}</textarea>
        </div>
      </div>
      <div class="box-footer text-right">
        <a href="{{URL::previous()}}" class="btn btn-default">Batal</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
@endsection

@section('js')
<script>
  CKEDITOR.replace( 'editor1',{
    height: 400,
    extraPlugins: 'filebrowser',
    filebrowserUploadUrl: '{{URL::to('upload_image')}}',
    filebrowserUploadMethod: 'form'

  } );

</script>
<script type="text/javascript" src="{{asset('js/uploadfile.js')}}"></script>
@endsection
