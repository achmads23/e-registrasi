@extends('layouts.main')

@section('title', 'Kategori')
@include('plugins.datatable')
@section('title-content')
Kategori
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li class="">Automatisasi Jawaban</li>
<li class="active">Kategori</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/faq')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/faq/create')}}">Buat Kategori Baru</a></li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th width="10%">No</th>
          <th>Kategori</th>
          <th>Urutan</th>
          <th width="10%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($kategoris as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td><i class="lni-{{$d->icon}}"></i> {{$d->nama}}
            <br><a href="{{route('klinik-center.detail',['id'=> $d->id,'sub_id' => count($d->child) > 0 ? $d->child[0]->id: ''])}}">{{route('klinik-center.detail',['id'=> $d->id,'sub_id' => count($d->child) > 0 ? $d->child[0]->id: ''])}}</a>
          </td>
          <td>{{$d->urutan}}</td>
          <td class="text-center">
            <a href="{{URL::to('/admin/faq/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection