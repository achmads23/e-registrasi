@extends('layouts.main')

@section('title', 'Sub Kategori | Show')

@section('title-content')
Sub Kategori | Show
@endsection

@include('plugins.ckeditor')

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li>Sub Kategori</li>
<li class="active">Show</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class=""><a href="{{URL::to('/admin/faq/sub-kategori')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/faq/create-sub-kategori')}}">Buat Sub-Kategori</a></li>
  <li class="active"><a href="#">Show</a></li>
</ul>
<div class="box box-success">
  <div class="box-body">
    <div class="user">
      <div class="row">
        @if($kategori->gambar)
          <div class="col-md-3">
            <img src="{{asset($kategori->gambar)}}" width="100%">
          </div>
        @endif
        <div class="col-md-9 wysiwyg">
          <h4><b>{{$kategori->nama}}</b></h4>
          {!!$kategori->uraian!!}
        </div>
      </div>
    </div>
    <br>
    <div class="box-footer text-right">
      <form id="hapus" method="POST" action="{{URL::to('/admin/faq/'.$kategori->id)}}">
        {{ method_field('DELETE') }}
        {{ csrf_field()}}
        <button type="button" class="btn btn-danger hapus">Hapus</button>
        <a href="{{URL::to('/admin/faq/sub-kategori/'.$kategori->id . '/edit')}}" class="btn btn-success">Ubah</a>
      </form>
    </div>
  </div>
  <!-- /.box-footer -->
</div>
@endsection

@section('modal')
@include('faq.modal-attach-sub-kategori')
@endsection

@section('js')
<script>
  $(function () {
    $(".hapus").click(function(){
      swal({
        title: "Apakah anda yakin kategori ini?",
        text: "Semua yang berbuhungan dengan kategori ini seperti pertanyaan ataupun sub-kategori akan terhapus juga",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        $('form#hapus').submit();
      });
    });
  });

  $(function () {
    $(".delete-pertanyaan").click(function(){
      that = $(this);
      swal({
        title: "Apakah anda yakin menghapus pertanyaan ini?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Hapus!",
        closeOnConfirm: false
      },
      function(){
        that.closest('.row').find('form').submit();
      });
    });
  });

  @if (session('edit_success'))
  swal("Mengubah data berhasil", "Data berhasil disimpan", "success");
  @endif
  
  @if (session('create_sub_kategori_success'))
  swal("Membuat sub kategori berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('create_pertanyaan_success'))
  swal("Membuat pertanyaan berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_pertanyaan_success'))
  swal("Hapus pertanyaan berhasil", "Data berhasil dihapus", "success");
  @endif
</script>
@endsection
