@extends('layouts.main')

@section('title', 'Sub Kategori')
@include('plugins.datatable')
@section('title-content')
Sub Kategori
@endsection

@section('breadcrumb')
<li><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Sub Kategori</li>
@endsection

@section('content')
<ul class="nav nav-pills">
  <li class="active"><a href="{{URL::to('/admin/faq/sub-kategori')}}">List</a></li>
  <li class=""><a href="{{URL::to('/admin/faq/create-sub-kategori')}}">Buat Sub-Kategori</a></li>
</ul>
<div class="box box-success">
  <div class="box-body table-responsive">
    <table class="table table-striped DataTable">
      <thead>
        <tr class="judul-kolom">
          <th width="10%">No</th>
          <th width="60%">Sub-Kategori</th>
          <th width="20%">Induk</th>
          <th width="10%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($kategoris as $key => $d)
        <tr>
          <td>{{$key+1}}</td>
          <td>
            @if($d->id_parent != '-1')
            <i class="lni-check-mark-circle"></i> {{$d->nama}}
            <br><a href="{{route('klinik-center.detail',['id'=> $d->parent->id, 'sub_id'=> $d->id])}}">{{route('klinik-center.detail',['id'=> $d->parent->id, 'sub_id'=> $d->id])}}</a>
            @else
             {{$d->nama}}
            <br><i>Sub-kategori baru</i>
            @endif
          </td>
          <td>
            {{$d->id_parent == '-1' ? '-' : ($d->parent != null ? $d->parent->nama : '0')}}
          </td>
          <td class="text-center">
            <a href="{{URL::to('/admin/faq/sub-kategori/'.$d->id)}}" class="btn btn-sm btn-success">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('js')
<script>
  @if (session('create_success'))
  swal("Membuat data berhasil", "Data berhasil disimpan", "success");
  @endif
  @if (session('delete_success'))
  swal("Menghapus data berhasil", "Data berhasil dihapus", "success");
  @endif
  @if (session('delete_admin_error'))
  swal("Ooops..", "Administrator tidak dapat dihapus", "error");
  @endif
  
  function colorSet(e){
    e.style.color="#555";
  }

  $(function () {
    var table = $('.DataTable').DataTable( {
    } );
  });
</script>
@endsection