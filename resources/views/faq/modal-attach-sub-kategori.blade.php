<div class="modal fade" id="modal-attach-sub-kategori">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title">Tambahkan Sub Kategori</h5>
      </div>
      <form method="POST" action="{{route('admin.faq.attach-sub-kategori')}}">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id-kategori">
        <input type="hidden" name="redirect" id="redirect">
        <input type="hidden" name="from" id="from">
        <div class="modal-body">
           <div class="form-group">
            <label for="exampleInputEmail1">Sub Kategori</label>
            <select name="id_sub_kategori" id=id_sub_kategori required>
              <option selected value="" disabled>Pilih Sub Kategori</option>
              @foreach ($unrelated_sub_kategori as $unrelated)
                <option value="{{$unrelated->id}}">{{$unrelated->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Urutan</label>
            <input type="number" name="urutan" value="{{ old('urutan')}}" class="form-control" placeholder="Urutan" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary submit-sub-kategori">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#id_sub_kategori').selectize({
    create: false,
    sortField: 'text',
    placeholder: 'Pilih Sub Kategori'
  });

  $('.submit-sub-kategori').on('click', function(event) {
    $modal = $('#modal-attach-sub-kategori');
    label = $modal.find('select[name="id_sub_kategori"] option:selected').html()
    urutan =  $modal.find('input[name="urutan"]').val()
    id_sub_kategori =  $modal.find('select[name="id_sub_kategori"]').val()
    console.log(id_sub_kategori)
    console.log(elemTable)
    $.ajax({
      url: '{{url('admin/faq')}}/' + $modal.find('input[name="id"]').val() + '/ajax-link',
      type: 'POST',
      data: {
        _token: "{{ csrf_token() }}",
        urutan: urutan,
        id_sub_kategori: id_sub_kategori,
      },
      })
      .done(function(data) {
        snackbar_show('Sub-kategori berhasil ditambahkan');
        jumlah = parseInt($('#child-subkategori' + $modal.find('input[name="id"]').val()).html())
        $('#child-subkategori' + $modal.find('input[name="id"]').val()).html(jumlah + 1)
        html = `
                <tr>
                  <td>${label}</td>
                  <td width="150px">
                    <form>
                      <input type="hidden" name="id_kategori" value="${id_sub_kategori}">
                      <div style="display: flex;">
                        <input type="number" name="urutan" class="form-control" value="${urutan}">
                        <a href="#!" onclick="ajax_urutan('${urutan}', this)" class='btn btn-sm btn-primary'><i class="fa fa-save"></i></a>
                      </div>
                      </form>
                  </td>
                  <td class="text-center">0</td>
                  <td nowrap>
                    <a href="#!" onclick="detach(${$modal.find('input[name="id"]').val()},'${id_sub_kategori}','${label}',this)" class="btn btn-warning btn-sm"><i class="fa fa-unlink"></i></a>
                    <a onclick="collapsed_setara(${$modal.find('input[name="id"]').val()},${id_sub_kategori})" data-toggle="collapse" data-target="#element${id_sub_kategori}" aria-expanded="true" aria-controls="element${elemTable.find('tr').length}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                  </td>
                </tr>`;
        elemTable.find('tbody').append(html);
        html = `
        <div class="row">
          <div class='col-lg-12 collapse anakanak ${data.class}' id="element${id_sub_kategori}">
            <div class="box box-success" style='border-color:orange !important'>
              <div class="box-header">  
                <b>Sub-Kategori ${label}</b>
                <a href="#!" onclick="close_collapsed(this)" class="pull-right"><i class="fa fa-close"></i></a>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6 box-pengantaran-jawaban">
                    <h5>Pengantar Pertanyaan dan Jawaban</h5>
                    <hr>
                    <form class="form-horizontal" >
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Pertanyaan</label>
                        <div class="col-sm-10">
                          <input type="text" name="k_pertanyaan" value="" class="form-control" placeholder="Pertanyaan" required>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jawaban</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="k_jawaban" id="editor${id_sub_kategori}" rows="3" style="width:100%" placeholder="Jawaban" required></textarea>
                        </div>
                      </div>
                      <div class="text-right">
                        <button  type="button" class="btn btn-primary submit-pertanyaan-jawaban" data-id="${id_sub_kategori}">Simpan</button>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-6">
                    <h5>Sub kategori <a href="#!" onclick="addSubKategori(0,'${id_sub_kategori}',this)" class="btn btn-primary btn-sm pull-right">Tambah</a></h5>
                    <hr>
                    <table class="table table-stripped table-bordered">
                      <thead>
                        <tr>
                          <th>Nama Sub-Kategori</th>
                          <th>Urutan</th>
                          <th class="text-center">Anak</th>
                          <th width="5%" class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        `;
        $('.detail-kategori').append(html);
        $modal.modal('hide');

        var $select = $('#id_sub_kategori').selectize();
        var control = $select[0].selectize;
        var selectedValue = control.getValue()

        control.clear();
        control.removeOption( selectedValue )

        $modal.find('input[name="urutan"]').val('');
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
  });
</script>