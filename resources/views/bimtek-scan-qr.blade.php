@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')

<div class="container">
  <div class="box" style="border-top:none;padding:10px">
    <div class="box-body text-center">
      <div class='row'>
        <div class='col-md-6'>
          <h5 class="text-center disable-margin" style="margin-bottom:10px">Scan QR-Code Anda</h5>
          <video id="preview" width="40%"></video>
          <br>
          Tidak punya qr-code? Klik <a href="http://202.162.219.10/visitor/input_bimtek.php">disini</a> 
        </div>
        <div class='col-md-6'>
          <h5 class="text-center disable-margin" style="margin-bottom:10px">Masuk Menggunakan Password</h5>
          <br>
          @if(session()->get('token_login'))
          <p class="text-center text-danger italic" id="error-message">{{session()->get('token_login')}}</p>
          @endif
          <div class="col-md-6 col-md-offset-3">
            <form method="POST" action="{{route('pendaftaran.bimtek_checkin_token')}}">
                {{ csrf_field() }}              
              <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control keyboard-screen" name="password" placeholder="Password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
<iframe src="{{URL::to('/slider.html')}}"  width="100%" height="500px" frameBorder="0" style="-webkit-transform:scale(0.8);-moz-transform-scale(0.8);position:relative;top:-50px;z-index:-999"></iframe>
<div class="text-center" style="margin-bottom: 100px">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('css/jquery.ml-keyboard.css')}}">
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/instascan.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.ml-keyboard.min.js')}}"></script>
<script>
$(function() {
  $('input.keyboard-screen').mlKeyboard({
    layout: 'es_ES'
  });

  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
<script type="text/javascript">
  let opts = {
    // Whether to scan continuously for QR codes. If false, use scanner.scan() to manually scan.
    // If true, the scanner emits the "scan" event when a QR code is scanned. Default true.
    continuous: true,
    
    // The HTML element to use for the camera's video preview. Must be a <video> element.
    // When the camera is active, this element will have the "active" CSS class, otherwise,
    // it will have the "inactive" class. By default, an invisible element will be created to
    // host the video.
    video: document.getElementById('preview'),
    
    // Whether to horizontally mirror the video preview. This is helpful when trying to
    // scan a QR code with a user-facing camera. Default true.
    mirror: true,
    
    // Whether to include the scanned image data as part of the scan result. See the "scan" event
    // for image format details. Default false.
    captureImage: true,
    
    // Only applies to continuous mode. Whether to actively scan when the tab is not active.
    // When false, this reduces CPU usage when the tab is not active. Default true.
    backgroundScan: false,
    
    // Only applies to continuous mode. The period, in milliseconds, before the same QR code
    // will be recognized in succession. Default 5000 (5 seconds).
    refractoryPeriod: 5000,
    
    // Only applies to continuous mode. The period, in rendered frames, between scans. A lower scan period
    // increases CPU usage but makes scan response faster. Default 1 (i.e. analyze every frame).
    scanPeriod: 1
  };
  let scanner = new Instascan.Scanner(opts);
  scanner.addListener('scan', function (content) {
    console.log(scanner.content);
    console.log(scanner.image);
    $( ".loading" ).show();
    $.ajax({
      url: '{{route('ajax-cek-bimtek-qr-code')}}',
      type: 'POST',
      data: {_token:'{{ csrf_token() }}',username:content},
    })
    .done(function(data) {
      
      if(data.data){
        window.location = '{{URL::to("/bimtek/check-in?token=")}}' + data.data.token;
      } else {
      $( ".loading" ).hide();
        alert('data tidak ditemukan');
      }
    })
    .fail(function(error) {
      $( ".loading" ).hide();
      console.log("error");
    })
    .always(function() {
      $( ".loading" ).hide();
      console.log("complete");
    });
  });
  Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 0) {
      scanner.start(cameras[0]);
    } else {
      console.error('No cameras found.');
    }
  }).catch(function (e) {
    console.error(e);
  });
</script>

  @endsection
