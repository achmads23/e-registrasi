@extends('layouts.empty')

@section('title', 'Pendaftaran')

@section('content')
<div class="login-box disable-margin-top" style="width: 600px;">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h3 class="text-center disable-margin" style="color:red"><b>Selamat Datang di UPT PPK</b></h3>
    <br>
	@if($peserta->foto != '-')
  <div class="text-center">
    <img src="{{asset($peserta->foto)}}" width="50%">
  </div>
    <br>
    <br>
	@endif
    <table style="font-size:20px;margin:auto;">
      <tr>
        <td>Username</td>
        <td style="padding-left: 10px;">:</td>
        <td style="padding-left: 10px;">{{$peserta->username}}</td>
      </tr>
      <tr>
        <td>NIP</td>
        <td style="padding-left: 10px;">:</td>
        <td style="padding-left: 10px;">{{$peserta->pegawai->nip}}</td>
      </tr>
      <tr>
        <td>Nama</td>
        <td style="padding-left: 10px;">:</td>
        <td style="padding-left: 10px;">{{$peserta->pegawai->nama}}</td>
      </tr>
      <tr>
        <td>Satuan Kerja</td>
        <td style="padding-left: 10px;">:</td>
        <td style="padding-left: 10px;">{{ucwords(strtolower($peserta->pegawai->opd->opd))}}</td>
      </tr>
      <tr>
        <td>Acara</td>
        <td style="padding-left: 10px;">:</td>
        <td style="padding-left: 10px;">{{$peserta->acara->nama_acr}}</td>
      </tr>
    </table>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <a href="{{route('bimtek-scan')}}" class="btn btn-default btn-block btn-flat">
          <i class="fa fa-chevron-left"></i> Kembali
        </a>
      </div>
      <br>
      <!-- /.col -->
    </div>
  </div>
  <br>
  <div class="text-center" style="margin-bottom: 100px">
  <strong>Copyright &copy; {{date("Y")}} <a href="http://uptlpkd.bpkad.jatimprov.go.id/" target="_blank">UPT PPK Jawa Timur</a></strong>
    </div>
</div>
@endsection

@section('js')
<script>
$(function() {
	window.setTimeout(function(){
        // Move to a new location or you can do something else
        window.location.href = '{{URL_SERVER}}' + "bimtek-scan";

    }, 5000);
  $('.has-error').keypress(function(){
    $(this).removeClass('has-error');
    $('#error-message').remove();
    $(this).find('.help-block').hide();
  });
});
</script>
  @endsection
