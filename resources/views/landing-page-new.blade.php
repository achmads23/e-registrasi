@extends('layouts.template.main')

@section('title', 'Beranda')

@section('landing-page')
<div class="container">      
  <div class="row space-100">
    <div class="col-lg-6 col-md-12 col-xs-12">
      <div class="contents">
        <h2 class="head-title">Selamat Datang di Layanan UPT PPK</h2>
        <p>Unit Pelayanan Teknis Kerja Pengembangan Pengelolaan Keuangan (UPT PPK) adalah bagian dari Badan Pengelola Keuangan dan Aset Daerah Provinsi Jawa Timur</p>
        <div class="header-button">
          <a href="#klinik-center" class="btn btn-border-filled page-scroll">Mulai</a>
          <a href="#informasi" class="btn btn-border page-scroll">Informasi</a>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-12 col-xs-12 p-0">
      <div class="intro-img">
        <img src="{{asset('template/img/Landing.png')}}" alt="">
      </div>            
    </div>
  </div> 
</div>   
@endsection

@section('content')
<!-- Services Section Start -->
<section id="services" class="section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="team-text section-header text-center">  
          <div>   
            <h2 class="section-title">Layanan Kami</h2>
            <div class="desc-text">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>  
              <p>eiusmod tempor incididunt ut labore et dolore.</p>
            </div>
          </div> 
        </div>
      </div>
      <!-- Start Col -->
      <div class="col-lg-4 col-md-6 col-xs-12">
        <div class="services-item text-center" style="height:100%">
          <div class="icon">
            <img src="{{asset('images/bimtek.png')}}" width="50%">
          </div>
          <h4>Bimtek</h4>
          <p>Kegiatan pelatihan dan pengembangan pengetahuan serta kemampuan yang dapat digunakan untuk memecahkan masalah.</p>
        </div>
      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-4 col-md-6 col-xs-12">
        <div class="services-item text-center" style="height:100%">
          <div class="icon">
            <img src="{{asset('images/Klinik Center.png')}}" width="50%">
          </div>
          <h4>Klinik Center</h4>
          <p>Klinik Center Pengelolaan Keuangan Daerah berbentuk konsultasi bagi seluruh pemerintah daerah di seluruh.</p>
        </div>
      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-4 col-md-6 col-xs-12">
        <div class="services-item text-center" style="height:100%">
          <div class="icon">
            <img src="{{asset('images/Pendampingan.png')}}" width="50%">
          </div>
          <h4>Pendampingan</h4>
          <p>Salah satu Produk UPT-PPK, untuk menyelesaikan permasalahan tentang Pengelolaan Keuangan Daerah yang ada di organisasi.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Services Section End -->



<!-- Business Plan Section Start -->
<section id="business-plan">
  <div class="container">

    <div class="row">
      <!-- Start Col -->
      <div class="col-lg-6 col-md-12 pl-0 pt-70 pr-5">
        <div class="business-item-img">
          <img src="{{asset('template/img/bimtek-landing.png')}}" class="img-fluid" alt="">
        </div>
      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-6 col-md-12 pl-4">
        <div class="business-item-info">
          <h3>Bimbingan Teknis Pengembangan Pengelolaan Keuangan</h3>
          <p>Sebagaimana diketahui, pelatihan maupun Bimbingan Teknis (bimtek), merupakan kegiatan pelatihan dan pengembangan pengetahuan serta kemampuan yang dapat digunakan untuk memecahkan masalah yang dihadapi oleh setiap individu maupun institusi tertentu.Sehingga dengan mengikuti Bimtek diharapkan setiap individu maupun institusi tertentu, baik swasta maupun lembaga pemerintahan, dapat mengambil sebuah manfaat dengan  berorientasi  pada  kinerja.</p>
          <div class="text-center">
          <a class="btn btn-common mb-2" href="{{route('bimtek.grup-undangan')}}">Undangan</a>
          <a class="btn btn-common mb-2" href="{{route('bimtek.grup-umum')}}" style="background-color:blue;text-align: white;">Umum</a>
          <a class="btn btn-common mb-2" href="{{route('bimtek.acara.index')}}" style="background-color:black;text-align: white;">Jadwal</a>
          </div>
        </div>
      </div>
      <!-- End Col -->

    </div>
  </div>
</section>
<!-- Business Plan Section End -->



<!-- Cool Fetatures Section Start -->
<section id="klinik-center" class="section" style="padding-bottom: 0;
background-color: #e3fff1;
padding-top: 60px;">
<div class="container">
  <!-- Start Row -->
  <div class="row">
    <div class="col-lg-12">
      <div class="features-text section-header text-center">  
        <div>   
          <h2 class="section-title">Klinik Center Online</h2>
          <div class="desc-text">
            <p>Merupakan produk satu-satunya di Indonesia yang dimiliki UPT-PPK. Klinik Center Pengelolaan Keuangan Daerah berbentuk konsultasi bagi seluruh pemerintah daerah di seluruh Indonesia yang ingin menyelesaikan permasalahan keuangan yang dihadapi.</p>
          </div>
        </div> 
      </div>
    </div>

  </div>
</div>
</section>
<section class="section" style="padding-bottom: 0;
background-color: #ffcf00;
padding: 20px 0 !important;">
<div class="container">
  <!-- Start Row -->
  <div class="row">
    <div class="col-lg-12">
      <div class=" text-center">  
        <h5 class="mb-0"><b>OPTIMALISASI KINERJA KLINIK LAYANAN KONSULTASI PENATAUSAHAAN KEUANGAN BERBASIS TI PADA UPT-PENGEMBANGAN PENGELOLAAN KEUANGAN</b></h5>
      </div>
    </div>

  </div>
</div>
</section>

<section id="features" class="section" style="padding-top: 60px">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="features-text section-header text-center mb-0">  
          <h2 class="section-title" style="font-size: 30px;">Ada yang bisa kami bantu?</h2>
        </div>
      </div>
    </div>

    <div class="row mb-5">
      <div class="col-lg-12">
        <form class="form-inline" method="get" action="{{route('klinik-center.cari')}}">
          <div class="col-lg-6 offset-lg-3">
            <div class="form-group" style="display: flex;">
              <input type="text" name="pencarian" class="form-control text-center input-khusus" placeholder="" style="display: flex;flex-grow: 1;">
              <button class="btn btn-primary" style="min-height: 52px;margin-left: 10px;padding: 10px 20px;"><i class="fa fa-search" style="font-size:18px;"></i></button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <br>
    <div class="row mb-4">
      <div class="col-lg-12">
        <div class="features-text section-header text-center mb-0">  
          <h2 class="section-title" style="font-size: 25px;">Otomatisasi Jawaban Berdasarkan Kategori</h2>
        </div>
      </div>
    </div>
    <!-- End Row -->
    <!-- Start Row -->
    <div class="row featured-bg">
      <!-- Start Col -->
      @foreach ($kategoris as $key => $kategori)
      <div class="col-lg-6 col-md-6 col-xs-12 p-0">
        <a href="{{route('klinik-center.detail',['id'=> $kategori->id,'sub_id'=> count($kategori->child) > 0 ? $kategori->child[0]->id: ''])}}">
          <div class="feature-item featured-border2">
            <div class="row">
              <div class="col-3">
                <div class="feature-icon float-left">
                  <i class="lni-{{$kategori->icon}}"></i>
                </div>
                {{-- <img src="{{asset($kategori->gambar)}}" width="100px"> --}}
              </div>
              <div class="col-9">
                <h4 style="overflow:hidden;" class="mb-2">{{$kategori->nama}}</h4>

                @php
                $string = strip_tags($kategori->uraian);
                if (strlen($string) > 100) {

// truncate string
                  $stringCut = substr($string, 0, 100);
                  $endPoint = strrpos($stringCut, ' ');

//if the string doesn't contain any space then it will cut without word basis.
                  $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                  $string .= '....';
                }
                @endphp
                <p>{{$string}}</p>
              </div>
            </div>
          </div>
        </a>
      </div>
      @endforeach
    </div>
  </div>
  <!-- End Row -->
</div>
</section>
<!-- Cool Fetatures Section End --> 


<!-- Recent Showcase Section Start -->
<section id="showcase">
  <div class="container-fluid right-position">
    <!-- Start Row -->
    <div class="row gradient-bg">
      <div class="col-lg-12">
        <div class="showcase-text section-header text-center">  
          <div>   
            <h2 class="section-title">Cara Kerja Otomatisasi Jawaban</h2>
            <div class="desc-text">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do</p>  
              <p>eiusmod tempor incididunt ut labore et dolore.</p>
            </div>
          </div> 
        </div>
      </div>

    </div>
    <!-- End Row -->
    <!-- Start Row -->
    <div class="row justify-content-center showcase-area">
      <div class="col-lg-12 col-md-12 col-xs-12 pr-0">
        <div class="showcase-slider owl-carousel">
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/01.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/02.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/02.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/03.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/03.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/04.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/04.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/05.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/05.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/01.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/02.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/02.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/03.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/03.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/04.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/04.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/05.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/05.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/01.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/02.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/03.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/04.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/05.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/01.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/02.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/03.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/04.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="screenshot-thumb">
              <img src="{{asset('template/img/showcase/05.jpg')}}" class="img-fluid" alt="" />
              <div class="hover-content text-center">
                <div class="fancy-table">
                  <div class="table-cell">
                    <div class="single-text">
                      <p>Icon , Web</p>
                      <h5>Redesign Slack</h5>
                    </div>
                    <div class="zoom-icon">
                      <a class="lightbox" href="img/showcase/01.jpg"><i class="lni-zoom-in"></i></a>
                      <a href="#"><i class="lni-link"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>



        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
</section>
<!-- Recent Showcase Section End --> 

<!-- Client Testimoninals Section Start -->
<section id="testimonial" class="section">
  <div class="container right-position">
    <!-- Start Row -->
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="video-promo-content text-center">
        </div>
      </div>
    </div>
    <!-- End Row -->
    <!-- Start Row -->
    <div class="row justify-content-center testimonial-area">
      <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 pr-20 pl-20" style="overflow: hidden;padding-bottom: 60px">
        <div id="client-testimonial" class="text-center owl-carousel">
          <div class="item">
            <div class="testimonial-item">
              <div class="content-inner">
                <p class="description">Pelaksanaan Kegiatan yang ditujukan kepada OPD Pemerintah Provinsi Jawa Timur, Pemerintah Provinsi lain, Pemerintah Kabupaten/Kota di wilayah Jawa Timur, serta Institusi Pendidikan (universitas/Lembaga Pendidikan lainnya), pendampingan dapat melalui bimbingan teknis dan seminar/FGD</p>
                <div class="author-info">
                  <h5>UPT PPK</h5>
                </div>
              </div>
              <div class="client-thumb">
                <img src="{{asset('images/logo-jatim-kecil.png')}}" style="width: 50px">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Row -->
  </div>
</section>
<!-- Client Testimoninals Section End --> 


<!-- Team section Start -->
<section id="team" class="section" style="    background: #f9f9f9;">
  <div class="container">
    <!-- Start Row -->
    <div class="row">
      <div class="col-lg-12">
        <div class="team-text section-header text-center">  
          <div>   
            <h2 class="section-title">Team Pendamping</h2>
            <div class="desc-text">
              <p>Pendampingan yang dilakukan di UPT PPK dilaksanakan bersama</p>  
              <p>OPD di Lingkungan Pemerintah Provinsi Jawa Timur.</p>
            </div>
          </div> 
        </div>
      </div>

    </div>
    <!-- End Row -->
    <!-- Start Row -->
    <div class="row">
      <div id="team-pendamping" class="text-center owl-carousel">
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/1.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">INSPEKTORAT</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/2.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">DINAS SOSIAL</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/3.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">Bappeda</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/4.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">PUPR</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/5.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">DINAS PERTANIAN</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/6.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">RS HAJI</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/3.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">RS SOETOMO</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/2.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">BKD</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/1.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">BAPENDA</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/2.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">POL PP</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/3.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">BAKESBANG</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/4.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">DINAS ESDM</h5>
                <p>Pemprov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/7.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">BIRO KESOS</h5>
                <p>Setda Prov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">

          <div class="single-team" style="margin-right: 10px;">
            <div class="team-thumb">
              <img src="{{asset('images/tim-pendamping/8.png')}}" class="img-fluid" alt="">
            </div>

            <div class="team-details">
              <div class="team-social-icons">
                <ul class="social-list">
                  <li><a href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a href="#"><i class="lni-google-plus"></i></a></li>
                </ul>
              </div> 
              <div class="team-inner text-center">
                <h5 class="team-title">BIRO AP</h5>
                <p>Setda Prov. Jatim</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="contact" class="section">
  <!-- Container Starts -->
  <div class="container">
    <!-- Start Row -->
    <div class="row">
      <div class="col-lg-12">
        <div class="contact-text section-header text-center">  
          <div>   
            <h2 class="section-title">Cukup dengan Login</h2>
            <div class="desc-text">
              <p>Anda dapat mengakses Layanan Bimbingan Teknis di UPT PPK</p>
            </div>
          </div> 
        </div>
      </div>

    </div>
    <!-- End Row -->
    <!-- Start Row -->
    <div class="row">
      <!-- Start Col -->
      <div class="col-lg-6 col-md-12">
        <form id="contactForm" method="POST" action="{{ route('public-login') }}">
          {{ csrf_field() }}
          @if (session('login_failed'))
          <div class="callout callout-danger">
            <p><b>Login failed!</b> <br>Invalid username and/or password.</p>
          </div>
          @endif

          @if (session('register_success'))
          <div class="callout callout-success">
            <p><b>Register success!</b> <br>Please check your email to activate your account.</p>
          </div>
          @endif

          @if (session('reset_password_success'))
          <div class="callout callout-success">
            <p><b>Sukses</b> <br>Gunakan password baru anda untuk login.</p>
          </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                <span class="help-block text-red">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                <div class='text-right' style="margin-top: 10px;">
                  <a href="{{route('forget-password')}}"><i class="glyphicon glyphicon-lock"></i> Lupa password?</a>
                </div> 
              </div>
            </div>
            <div class="col-md-12">
              <div class="submit-button">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                <div class="clearfix"></div> 
              </div>
            </div>
            <div class="col-md-12 text-center" style="margin-top:10px">
              Belum punya akun?  <a href="{{route('public-register')}}"> Daftar</a>
            </div>
          </div>            
        </form>
      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-1">

      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-4 col-md-12">
        <div class="contact-img">
          <img src="{{asset('template/img/business-img.png')}}" class="img-fluid" alt="">
        </div>
      </div>
      <!-- End Col -->
      <!-- Start Col -->
      <div class="col-lg-1">
      </div>
      <!-- End Col -->

    </div>
    <!-- End Row -->
  </div>
</section>
<!-- Team section End -->
@endsection