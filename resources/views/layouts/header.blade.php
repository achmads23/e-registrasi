<header class="main-header">

  <!-- Logo -->
  <a href="{{route('home')}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">
      <img src="{{asset('images/logo-jatim.png')}}"  alt="User Image" width="40px">
    </span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg" style="font-size: 25px">
      <img src="{{asset('images/logo-jatim.png')}}" alt="User Image" width="40px"> Layanan
    </span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
      <a href="#!" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation </span>
      </a>
      {{-- <a class="logo header-title">
        <span style="font-size: 19px"></b></span>
      </a> --}}

    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#!" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{asset('images/user-01.png')}}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{Auth::user()->name}}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{asset('images/user-01.png')}}" class="img-circle" alt="User Image">

              <p>
                {{Auth::user()->name}}
                <small>Member since {{Auth::user()->created_at}}</small>
              </p>
            </li>
            
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
              </div>
              <div class="pull-right">
                <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>

  </nav>
</header>