<footer>
  <!-- Footer Area Start -->
  <section id="footer-Content">
    <div class="copyright">
      <div class="container">
        <!-- Star Row -->
        <div class="row">
          <div class="col-md-12">
            <div class="site-info text-center">
              <p><strong>Copyright &copy; {{date("Y")}} <a href="#" >UPT PPK - BPKAD Provinsi Jawa Timur</a>.</strong> All rights reserved.</p>
            </div>              

          </div>
          <!-- End Col -->
        </div>
        <!-- End Row -->
      </div>
    </div>
    <!-- Copyright End -->
  </section>
  <!-- Footer area End -->

</footer>