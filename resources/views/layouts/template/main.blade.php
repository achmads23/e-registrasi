<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Landing page, Template, Business, Service">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>Layanan | @yield('title')</title>
  <!--====== Favicon Icon ======-->
  <link rel="shortcut icon" type="image/png" href="{{asset('images/logo-jatim.ico')}}"/>
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/animate.css')}}">
  {{-- <link rel="stylesheet" href="{{asset('template/css/LineIcons.css')}}"> --}}
  <link rel="stylesheet" href="{{asset('css/LineIcons.min.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/owl.theme.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/nivo-lightbox.css')}}">
  <link rel="stylesheet" href="{{asset('template/css/main.css')}}">    
  <link rel="stylesheet" href="{{asset('template/css/responsive.css')}}">
  <link rel="stylesheet" href="{{asset('css/colors.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/sweetalert/dist/sweetalert.css')}}">
  @yield('css')
  <link href="{{asset('bower_components/selectize/dist/css/selectize.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{asset('css/custom-new.css')}}">
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')}}">
  <style type="text/css">
    .form-inline .selectize-control {
      margin-bottom: -5px;
    }

    /* Absolute Center Spinner */
    .loading {
      position: fixed;
      z-index: 999999;
      height: 2em;
      width: 2em;
      overflow: show;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }

    /* Transparent Overlay */
    .loading:before {
      content: '';
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0,0,0,0.3);
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: transparent;
      text-shadow: none;
      background-color: transparent;
      border: 0;
    }

    .loading:not(:required):after {
      content: '';
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 1500ms infinite linear;
      -moz-animation: spinner 1500ms infinite linear;
      -ms-animation: spinner 1500ms infinite linear;
      -o-animation: spinner 1500ms infinite linear;
      animation: spinner 1500ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
      box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @-moz-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @-o-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
  </style>
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min.js" type="text/javascript"></script>
</head>

<body>
  <div class="loading" style="display: none;"></div>
  <!-- Header Section Start -->
  <header id="home" class="hero-area">    
    <div class="overlay">
    </div>
    @include('layouts.template.header')  
    @yield('landing-page')          
  </header>
  <!-- Header Section End --> 

  @yield('content')

  <!-- Footer Section Start -->
  @include('layouts.template.footer')
  <!-- Footer Section End --> 


  <!-- Go To Top Link -->
  <a href="#" class="back-to-top">
    <i class="lni-chevron-up"></i>
  </a> 

  <!-- Preloader -->
  <div id="preloader">
    <div class="loader" id="loader-1"></div>
  </div>
  <!-- End Preloader -->

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="{{asset('template/js/jquery-min.js')}}"></script>
  <script src="{{asset('template/js/popper.min.js')}}"></script>
  <script src="{{asset('template/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('template/js/owl.carousel.js')}}"></script>      
  <script src="{{asset('template/js/jquery.nav.js')}}"></script>    
  <script src="{{asset('template/js/scrolling-nav.js')}}"></script>    
  <script src="{{asset('template/js/jquery.easing.min.js')}}"></script>     
  <script src="{{asset('template/js/nivo-lightbox.js')}}"></script>     
  <script src="{{asset('template/js/jquery.magnific-popup.min.js')}}"></script>     
  <script src="{{asset('template/js/main.js')}}"></script>

  <script src="{{asset('bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('bower_components/selectize/dist/js/standalone/selectize.js')}}"></script>

  <script>

    $( document ).ajaxStart(function() {
      $( ".loading" ).show();
    });
    $( document ).ajaxStop(function() {
      $( ".loading" ).hide();
    });
  </script>
  @yield('js-plugin')
  @yield('js')

  @include('plugins.datepicker')

  <script>
    $(function() {
      $('.has-error').keypress(function(){
        $(this).removeClass('has-error');
        $('#error-message').remove();
        $(this).find('.help-block').hide();
      });
    });
  </script>

</body>
</html>