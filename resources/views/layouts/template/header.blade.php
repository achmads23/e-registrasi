<nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar menu-bg-on-top">
    <div class="container">
      <a href="{{url('/')}}" class="navbar-brand"><img src="{{asset('template/img/logo.png')}}" alt="" style="width: 180px;"></a>       
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <i class="lni-menu"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto w-100 justify-content-end">
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}#services">Layanan</a>
          </li>  
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}#business-plan">Bimtek</a>
          </li>                            
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}#klinik-center">Klinik Center</a>
          </li>       
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}#testimonial">Pendampingan</a>
          </li>     
          <li class="nav-item">
            <a class="nav-link page-scroll" href="https://uptppk.bpkad.jatimprov.go.id/" target="_blank">Informasi</a>
          </li> 
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/')}}#contact"><i class="fa fa-sign-in"></i></a>
          </li> 
          <li class="nav-item">
            <a class="btn btn-singin" href="https://play.google.com/store/apps/details?id=com.layananbpkad" target="_blank">Download</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>