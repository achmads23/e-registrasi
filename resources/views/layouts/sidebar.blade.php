@php
$route = Route::getCurrentRoute()->uri();
$route = explode('/',$route);
@endphp
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('images/user-01.png')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
        <a href="#!"><i class="fa fa-circle text-success"></i>Online</a>
      </div>
    </div>    
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">NAVIGASI UTAMA</li>
      <li class="{{ !isset($route[1]) ? 'active ' : ''}}">
        <a href="{{URL::to('/admin')}}">
          <i class="side-icon fa fa-dashboard"></i><span>Beranda</span>
        </a>
      </li>
      <li class="header">Automatisasi Jawaban</li>
      <li class="">        
        <a href="{{URL::to('/admin/faq/kategori')}}">
          <i class="side-icon fa fa-question-circle"></i><span>Kategori</span>
        </a>
      </li>
      <li class="">        
        <a href="{{URL::to('/admin/faq/sub-kategori')}}">
          <i class="side-icon fa fa-question-circle"></i><span>Sub Kategori</span>
        </a>
      </li>
      <li class="header"></li>
      <li class="{{ isset($route[1]) && $route[1] == 'acara' ? 'active ' : ''}}">
        <a href="{{URL::to('/admin/acara')}}">
          <i class="side-icon fa fa-file-o"></i><span>Acara</span>
        </a>
      </li>
      @if(Auth::user()->role == ADMIN)
        <li class="header">Aplikasi Web</li>
        <li class="{{ isset($route[1]) && $route[1] == 'konfirmasi-grup-umum' ? 'active ' : ''}}">
          <a href="{{route('admin.bimtek.grup-umum')}}">
            <i class="side-icon fa fa-users"></i><span>Konfirmasi Umum</span>
          </a>
        </li>
        <li class="{{ isset($route[1]) && $route[1] == 'konfirmasi-grup-undangan' ? 'active ' : ''}}">
          <a href="{{route('admin.bimtek.grup-undangan')}}">
            <i class="side-icon fa fa-users"></i><span>Konfirmasi Undangan</span>
          </a>
        </li>
        
        <li class="header">Android</li>
        <li class="{{ isset($route[1]) && $route[1] == 'konfirmasi-opd' ? 'active ' : ''}}">
          <a href="{{URL::to('/admin/konfirmasi-opd')}}">
            <i class="side-icon fa fa-user"></i><span>Konfirmasi Perubahan OPD</span>
          </a>
        </li>

        <li class="{{ isset($route[1]) && $route[1] == 'konfirmasi-umum' ? 'active ' : ''}}">
          <a href="{{route('admin.bimtek.umum')}}">
            <i class="side-icon fa fa-user"></i><span>Konfirmasi Umum</span>
          </a>
        </li>
      @endif
      <li class="header">eClinic</li>
      <li class="{{ isset($route[1]) && $route[1] == 'ahli' ? 'active ' : ''}}">
        <a href="{{URL::to('/admin/e-clinic/ahli')}}">
          <i class="side-icon fa fa-user-o"></i><span>Ahli</span>
        </a>
      </li>
      <li class="{{ isset($route[1]) && $route[1] == 'laporan' ? 'active ' : ''}}">
        <a href="{{URL::to('/admin/e-clinic/laporan')}}">
          <i class="side-icon fa fa-file-o"></i><span>Laporan</span>
        </a>
      </li>

      @if(Auth::user()->role == ADMIN)
        <li class="header">Master Data</li>
        <li class="{{ isset($route[1]) && $route[1] == 'kelola' ? 'active ' : ''}} treeview">
          <a href="#">
            <i class="side-icon fa fa-database"></i><span>Kelola Data</span>
            <span class="pull-right-container">
              <i class="side-icon fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{isset($route[2]) && $route[2] == 'user' ? ' active' : ''}}">
              <a href="{{URL::to('/admin/kelola/user')}}"><i class="side-icon fa fa-circle-o"></i>User</a>
            </li>
            <li class="{{isset($route[2]) && $route[2] == 'pegawai' ? ' active' : ''}}">
              <a href="{{URL::to('/admin/kelola/pegawai')}}"><i class="side-icon fa fa-circle-o"></i>Pegawai</a>
            </li>
          </ul>
        </li>
      @endif

      
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
