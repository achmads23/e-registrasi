var xmlHttp;
function srvTime(){
	var req = new XMLHttpRequest();
	req.open('GET', document.location, false);
	req.send(null);
	var headers = req.getResponseHeader("Date");
	var serverTimeMillisGMT = Date.parse(new Date(headers).toUTCString());
    var localMillisUTC = Date.parse(new Date().toUTCString());
    
	var headers = headers.substring(headers.length - 29);
	var date = new Date(headers);
	return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}

function display_c(){
	var refresh=1000; // Refresh rate in milli seconds
	mytime=setTimeout('display_ct()',refresh)
}

function display_ct() {
	var st = srvTime();
	var localTime = new Date();
	document.getElementById('time-clock').value  = st;
	tt=display_c();
}