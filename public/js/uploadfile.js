var image = $('#image-cropper');
image.cropper({
  // aspectRatio: 16/9,
  crop: function (event) {
      var data = event.detail;
      var cropper = this.cropper;
      var imageData = cropper.getImageData();
      var previewAspectRatio = data.width / data.height;
  },
});

  $("#upload-gambar").change(function() {
    readURL(this);
  });

  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onloadend  = function(e) {
      var string = e.target.result;
      checkFile = string.includes('data:image');
      if(checkFile){
        image.cropper('replace', e.target.result);
        $('#image-placeholder').addClass('hidden');
        $('#cropper-zone').removeClass('hidden');
        $('.selected-picture').removeClass('hidden');
      } else {
        var element = '<div class="alert alert-danger"><ul class="no-style"><li>File yang dipilih harus gambar</li></ul></div>';
        $( ".box-body" ).prepend( element);
        $('input[type="file"]').val('');
      }
    }

    reader.readAsDataURL(input.files[0]);
  }
}


  $('.crop-image').on('click', function(event) {
    var result = document.getElementById('result');
      result.innerHTML = '';
      dataURL = image.cropper('getCroppedCanvas').toDataURL('image/jpeg');
      $('#image-placeholder img').attr('src', dataURL);
      $('#image-placeholder').removeClass('hidden');
      $('#cropper-zone').addClass('hidden');
      $('.selected-picture').removeClass('hidden');

      $('#cropped-file').val(dataURL);
  });