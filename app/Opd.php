<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Opd extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function pegawais()
    {
        return $this->hasMany('App\Pegawai', 'id_opd', 'kode_satker');
    }
}
