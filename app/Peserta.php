<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
    }

    public function opd()
    {
        return $this->belongsTo('App\Opd', 'id_opd', 'id');
    }

    public function acara()
    {
        return $this->belongsTo('App\Acara', 'id_acara', 'id_acr');
    }
}
