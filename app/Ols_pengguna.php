<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Ols_pengguna extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ols_pengguna';
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
