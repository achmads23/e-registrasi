<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const USER = 0;
    const ADMIN = 1;
    const PEGAWAI = 2;
    const AHLI = 3;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ahli(){
        return $this->hasOne('App\Model\eClinic\Ahlis', 'id_user','id');
    }

    public function detail()
    {
        return $this->hasOne('App\DetailUser', 'id_user','id');
    }
}
