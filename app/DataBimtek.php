<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class DataBimtek extends Model
{
    protected $table = 'data_bimtek';
    protected $primaryKey = 'id_bimtek';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
