<?php

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

function notification($title,$body,$data,$token_firebase)
{
    $optionBuilder = new OptionsBuilder();
    $optionBuilder->setTimeToLive(60*20);

    $notificationBuilder = new PayloadNotificationBuilder($title);
    $notificationBuilder->setBody($body)
                        ->setSound('default');

    $dataBuilder = new PayloadDataBuilder();
    $dataBuilder->addData($data);

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $data = $dataBuilder->build();

    $token = $token_firebase;

    $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    return $downstreamResponse;
}

function generateRandomString($length = 10,$tipe='semua') {
    if($tipe == 'semua'){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else if($tipe == 'huruf_besar'){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    } else if($tipe == 'huruf_kecil'){
        $characters = 'abcdefghijklmnopqrstuvwxyz';
    } else if($tipe == 'angka'){
        $characters = '0123456789';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function date_view($date, $format = 'd-m-Y')
{
    $time = strtotime($date);
    if (!$time) {
        return '';
    }
    return date($format, $time);
}

/**
 * Ngubah date dari format view ke database
 * 31-1-2017 jadi 2017-1-31
 * kalo lagi iseng, coba buka ini.
 * http://php.net/manual/en/datetime.createfromformat.php
 */
function date_database($date, $format = 'd-m-Y')
{
    try {
        return Carbon::createFromFormat($format, $date)->toDateString();
    } catch (Exception $e) {
        return null;
    }
}

/**
 * Ngubah time dari format database ke yang mudah dibaca
 * 2017-1-31 06:35:12 jadi 31-1-2017 06:35
 */
function time_view($time, $format = 'H:i')
{
    $time = strtotime($time);
    if (!$time) {
        return '';
    }
    return date($format, $time);
}

/**
 * Ngubah date dari format database ke yang mudah dibaca
 * 2017-1-31 06:35:12 jadi 31-1-2017 06:35
 */
function datetime_view($date, $format = 'd-m-Y H:i')
{
    $time = strtotime($date);
    if (!$time) {
        return '';
    }
    return date($format, $time);
}

/**
 * Ngubah date dari format view ke database
 * 31-1-2017 jadi 2017-1-31
 * kalo lagi iseng, coba buka ini.
 * http://php.net/manual/en/datetime.createfromformat.php
 */
function datetime_database($date, $format = 'd-m-Y H:i')
{
    try {
        return Carbon::createFromFormat($format, $date)->toDateTimeString();
    } catch (Exception $e) {
        return null;
    }
}

/**
 * Cari selisih hari
 *
 * @param date1
 * @param date2
 * @param apakah nilainya arus absolute (pasti positif)
 * @return difference in day
 */
function day_diff($date1, $date2, $absolute = true)
{
    $diff = strtotime($date1) - strtotime($date2);
    if ($absolute) {
        $diff = abs($diff);
    }
    return round($diff / 3600 / 24);
}

/**
 * Ngubah angka format uang ke integer normal
 * 12,500,000 jadi 12500000
 */
function unaccounting($number, $always_int = true)
{
    if ($always_int) {
        return intval(str_replace(',', '', $number));
    }
    return str_replace(',', '', $number);
}

/**
 * Change [1, 2, 3], 'num'
 * to [['num' => 1], ['num' => 2], ['num' => 3]]
 */
function array_set_key($array, $key)
{
    $ans = [];
    foreach ($array as $val) {
        array_push($ans, [$key => $val]);
    }
    return $ans;
}

function terbilang_recursive($x)
{
    $abil = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas'];
    if ($x < 12)
        return ' ' . $abil[$x];
    elseif ($x < 20)
        return terbilang_recursive($x - 10) . 'belas';
    elseif ($x < 100)
        return terbilang_recursive($x / 10) . ' puluh' . terbilang_recursive($x % 10);
    elseif ($x < 200)
        return ' seratus' . terbilang_recursive($x - 100);
    elseif ($x < 1000)
        return terbilang_recursive($x / 100) . ' ratus' . terbilang_recursive($x % 100);
    elseif ($x < 2000)
        return ' seribu' . terbilang_recursive($x - 1000);
    elseif ($x < 1000000)
        return terbilang_recursive($x / 1000) . ' ribu' . terbilang_recursive($x % 1000);
    elseif ($x < 1000000000)
        return terbilang_recursive($x / 1000000) . ' juta' . terbilang_recursive($x % 1000000);
}

/**
 * Ngubah angka jadi terbilang
 */
function terbilang($x)
{
    $x = intval($x);
    return $x ? trim(terbilang_recursive($x)) : 'nol';
}

/**
 * Ngubah angka jadi romawi
 */
function romawi($n) {
    $n = intval($n);
    $result = "";
    $iromawi = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
            20=>"XX", 30=>"XXX", 40=>"XL", 50=>"L", 60=>"LX",
            70=>"LXX", 80=>"LXXX", 90=>"XC", 100=>"C", 200=>"CC",
            300=>"CCC", 400=>"CD", 500=>"D", 600=>"DC",
            700=>"DCC", 800=>"DCCC",
            900=>"CM", 1000=>"M",
            2000=>"MM", 3000=>"MMM"
        ];
    if(array_key_exists($n, $iromawi)) {
        $result = $iromawi[$n];
    } else if ($n >= 11 && $n <= 99) {
        $i = $n % 10;
        $result = $iromawi[$n-$i] . romawi($n % 10);
    } else if ($n >= 101 && $n <= 999) {
        $i = $n % 100;
        $result = $iromawi[$n-$i] . romawi($n % 100);
    } else {
        $i = $n % 1000;
        $result = $iromawi[$n-$i] . romawi($n % 1000);
    }
    return $result;
}


?>

