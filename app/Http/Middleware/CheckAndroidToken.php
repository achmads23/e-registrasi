<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use DB;
class CheckAndroidToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null($request->bearerToken())){
            $user = User::where('token', '=', $request->bearerToken())->get();
            if(count($user) > 0) {
                return $next(request());
            }
        }
        return response()->json(['success' => false,'error' => "401"], 500);
    }
}
