<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use URL;
class CheckPublic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role == User::USER) {
            return $next($request);
        } else {
            Auth::logout();
            return redirect(URL::to('/#contact'));
        }
    }
}
