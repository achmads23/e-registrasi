<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pegawai;
use App\Opd;
use App\Peserta;
use App\Gedung;
use Carbon\Carbon;
use Mail;
use Auth;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pegawai = Pegawai::with('opd');
        
        if($request->input()){
            if($request->nip){
                $pegawai->whereRaw("REPLACE(`nip`, ' ', '') LIKE ? ", '%'.str_replace(' ', '', $request->nip).'%');
            }
            if($request->nama){
                $pegawai->where('nama','LIKE', '%'.$request->nama.'%');
            }
            if($request->jk){
                $pegawai->where('jk','LIKE', '%'.$request->jk.'%');
            }
            if($request->golongan){
                $ruang = substr($request->golongan, -1);
                if(in_array($ruang, ['A','B','C','D','E'])){
                    $golongan = substr_replace($request->golongan, "", -1);

                    $pegawai->where('ruang',$ruang);
                    $pegawai->where('gol','LIKE', '%'.$golongan.'%');
                } else {
                    $pegawai->where('gol','LIKE', '%'.$golongan.'%');        
                }
            }

            if($request->opd){
                $pegawai->whereHas('opd', function($query) use ($request) {
                    $query->where('opd','LIKE', '%'.$request->opd.'%');
                });
            }
        }
         $data['pegawais'] = $pegawai->paginate(20);
        $data['inputs'] = $request->input();
        return view('kelola.pegawai.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['opds'] = Opd::get();
        return view('kelola.pegawai.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nip = $request->input('nip');
        $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $nip))->with('opd')->first();
        if(!$pegawai){
            $pegawai = new Pegawai;
            $pegawai->nama = $request->input('nama');
            $pegawai->nip = $request->input('nip');
            $pegawai->jk = $request->input('jk');
            $pegawai->gol = $request->input('gol');
            $pegawai->ruang = $request->input('ruang');
            $pegawai->nama_satker = $request->input('satker');
            $opd = Opd::where('id',$request->input('opd'))->first();
            $pegawai->kode_satker = $opd->id_opd;
            $pegawai->created_by = Auth::user()->id;
            $pegawai->save();
            return redirect('/admin/kelola/pegawai')->with('create_success',true);
        } else {
            return redirect()->back()->with('nip_error', 'NIP sudah terdaftar.')->withInput($request->input());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pegawai'] = Pegawai::with('opd')->findOrFail($id);
        return view('kelola.pegawai.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pegawai'] = Pegawai::with('opd')->findOrFail($id);
        $data['opds'] = Opd::get();
        return view('kelola.pegawai.edit', $data);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pegawai = Pegawai::with('opd')->findOrFail($id);
        if($pegawai->nip != $request->input('nip')){
            $nip_exist = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $request->input('nip')))->with('opd')->first();
            if($nip_exist){
                return redirect()->back()->with('nip_error', 'NIP sudah terdaftar.')->withInput($request->input());
            }
        }
        
        $pegawai->nama = $request->input('nama');
        $pegawai->nip = $request->input('nip');
        $pegawai->jk = $request->input('jk');
        $pegawai->gol = $request->input('gol');
        $pegawai->ruang = $request->input('ruang');
        $pegawai->nama_satker = $request->input('satker');
        $opd = Opd::where('id',$request->input('opd'))->first();
        $pegawai->kode_satker = $opd->id_opd;
        $pegawai->updated_by = Auth::user()->id;
        $pegawai->save();
        return redirect('/admin/kelola/pegawai/'.$id)->with('edit_success',true);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::with('opd')->findOrFail($id);
        $peserta = Peserta::where('id_pegawai',$id)->get();
        if(count($peserta) > 0){
            $peserta->delete();
        }
        $pegawai->delete();
        return redirect('/admin/kelola/pegawai')->with('delete_success',true);    
        
    }
}
