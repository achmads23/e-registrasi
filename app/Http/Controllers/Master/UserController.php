<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Gedung;
use Carbon\Carbon;
use Mail;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['user'] = User::where('role','!=',AHLI)->select('*')->get();
        return view('kelola.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        return view('kelola.user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email_exist = User::where('email',$request->input('email'))->get();
        if(count($email_exist) == 0){
            $user = new User;
            $user->name = $request->input('name');
            $user->password = bcrypt($request->input('password'));
            $user->email = $request->input('email');
            $now = Carbon::now();
            $user->role = $request->input('role');
            $user->is_verified = $request->input('status');
            $user->created_by = Auth::user()->id;
            $user->save();
            return redirect('/admin/kelola/user')->with('create_success',true);
        } else {
            return redirect()->back()->with('email_error', 'Email sudah terdaftar.')->withInput($request->input());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['user'] = User::findOrFail($id);
        return view('kelola.user.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::findOrFail($id);
        return view('kelola.user.edit', $data);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if($user->email != $request->input('email')){
            $email_exist = User::where('email',$request->input('email'))->get();
            if(count($email_exist) > 0){
                return redirect()->back()->with('email_error', 'Email sudah terdaftar.')->withInput($request->input());
            }
        }
        
        $user->name = $request->input('name');
        if(!is_null($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->is_verified = $request->input('status');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->updated_by = Auth::user()->id;
        $user->save();
        return redirect('/admin/kelola/user/'.$id)->with('edit_success',true);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($id != 1){
            $user->delete();
            return redirect('/admin/kelola/user')->with('delete_success',true);    
        } else {
            return redirect('/admin/kelola/user')->with('delete_admin_error',true);    
        }
        
    }
}
