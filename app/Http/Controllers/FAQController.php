<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Validator;
use App\Model\FAQ\Kategori;
use App\Model\FAQ\Pertanyaan;

class FAQController extends Controller
{

    public $array_list = [];

    public function kategori()
    {
        $data['kategoris'] = Kategori::with(['parent','child','pertanyaan'])->where('id_parent',0)->orderBy('urutan','ASC')->get();
        return view('faq.kategori.index', $data);
    }

    public function sub_kategori()
    {
        $data['kategoris'] = Kategori::with(['parent','child','pertanyaan'])->where('id_parent','!=', 0)->orderBy('urutan','ASC')->get();
        return view('faq.index', $data);
    }

    public function create($id = 0)
    {
    	$data['kategori'] = false;
    	if($id != 0){
    		$data['kategori'] = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
    	}
        return view('faq.kategori.create',$data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make( $request->input(), [
            'name' => ['required', 'string'],
            'uraian' => ['required', 'string'],
            'urutan' => ['required', 'numeric'],
            'icon' => ['required'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $kategori = new Kategori;
        $kategori->nama = $request->name;
        $kategori->uraian = $request->uraian;
        $kategori->urutan = $request->urutan;
        $kategori->icon = $request->icon;

        $kategori->created_by = Auth::user()->id;
        $kategori->save();

        return redirect()->route('admin.faq')->with('create_success',true);
    }

    function get_child($id){
        $kategori = Kategori::with(['child','pertanyaan','parent'])->findOrFail($id);
        if($kategori->id_parent != 0){
            array_push($this->array_list,$kategori);
        }
        if($kategori->child != null || count($kategori->child) > 0){
            foreach ($kategori->child as $key => $child) {
                $this->get_child($child->id);
            }
        }
        
        return true;
    }

    public function show($id)
    {

    	$data['kategori'] = Kategori::with(['parent',"child" => function($q){
                $q->orderBy('urutan','ASC');
            },'pertanyaan'])->where('id',$id)->first();
        $data['unrelated_sub_kategori'] = Kategori::where('id_parent',-1)->get();

        $this->array_list = [];
        $this->get_child($id);
        $data['datas'] = $this->array_list;
        return view('faq.kategori.show', $data);	
    }

    public function update(Request $request,$id)
    {
        $kategori = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();

        if($request->type == 'pertanyaan') {
            $validator = Validator::make( $request->input(), [
                'k_pertanyaan' => ['required', 'string'],
                'k_jawaban' => ['required', 'string'],
            ],[
                'required' => ':attribute tidak boleh kosong',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $kategori->pengantar_pertanyaan = $request->k_pertanyaan;
            $kategori->pengantar_jawaban = $request->k_jawaban;

            $kategori->updated_by = Auth::user()->id;
            $kategori->save();

            return redirect()->route('admin.faq.show',['id' => $kategori->id])->with('pertanyaan_success',true);
        } else {
            $validator = Validator::make( $request->input(), [
                'name' => ['required', 'string'],
                'uraian' => ['required', 'string'],
                'urutan' => ['required', 'numeric'],
            ],[
                'required' => ':attribute tidak boleh kosong',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $kategori->nama = $request->name;
            $kategori->uraian = $request->uraian;
            $kategori->icon = $request->icon;
            $kategori->id_parent = $request->induk ?? 0 ;
            $kategori->urutan = $request->urutan;

            $kategori->updated_by = Auth::user()->id;
            $kategori->save();

            return redirect()->route('admin.faq.show',['id' => $kategori->id])->with('edit_success',true);
        }
    }

    public function ganti_urutan(Request $request,$id){
        $kategori = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
        $validator = Validator::make( $request->input(), [
            'urutan' => ['required', 'numeric'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);
        $kategori->urutan = $request->urutan;
        $kategori->save();
        return redirect()->route('admin.faq.show',['id' => $request->id_kategori])->with('ganti_urutan_success',true);
    }

    public function pertanyaan_store(Request $request, $id)
    {
        $validator = Validator::make( $request->input(), [
            'pertanyaan' => ['required', 'string'],
            'jawaban' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $kategori = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
        $kategori->pengantar_pertanyaan = $request->pertanyaan;
        $kategori->pengantar_jawaban = $request->jawaban;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

        return redirect()->route('admin.faq.show',['id' => $request->id_kategori])->with('create_pertanyaan_success',true);
    }

    public function pertanyaan_destroy($id,$id_pertanyaan){
    	$pertanyaan = Pertanyaan::findOrFail($id_pertanyaan);
    	$pertanyaan->delete();
    	return redirect()->route('admin.faq.show',['id' => $id])->with('delete_pertanyaan_success',true);
    }

    function delete_rekursif($id){
    	$kategori = Kategori::with(['child','pertanyaan'])->findOrFail($id);
    	if($kategori->child != null || count($kategori->child) > 0){
    		foreach ($kategori->child as $key => $child) {
    			$this->delete_rekursif($child->id);
    		}
    	}

    	if($kategori->pertanyaan != null){
    		foreach ($kategori->pertanyaan as $key => $value) {
    			$value->delete();
    		}
    	}

    	$kategori->delete();
    	return true;
    }
    public function destroy($id){
        $kategori = Kategori::with(['child','pertanyaan'])->findOrFail($id);
    	$this->delete_rekursif($id);

        if($kategori->id_parent != 0){
    	   return redirect()->route('admin.faq.sub-kategori')->with('delete_success',true);
        } else {
            return redirect()->route('admin.faq')->with('delete_success',true);
        }
    }

    //===========================================================================//
    // SUB KATEGORI

    public function create_sub_kategori()
    {
        $data['kategori'] = Kategori::with(['parent','child','pertanyaan'])->get();
        return view('faq.create',$data);
    }

    public function store_sub_kategori(Request $request)
    {
        $validator = Validator::make( $request->input(), [
            'name' => ['required', 'string'],
            'uraian' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $kategori = new Kategori;
        $kategori->nama = $request->name;
        $kategori->uraian = $request->uraian;
        $kategori->icon = $request->icon;
        $kategori->id_parent = -1;

        if($request->image){
            $now = Carbon::now();
            $path = 'storage/faq/'  . $now->timestamp .  str_random(5) . '.png';
            $data = explode(',', $request->image);
            $content = base64_decode($data[1]);
            file_put_contents(public_path($path),$content);
            $path = str_replace('public', 'storage', $path);
            $kategori->gambar = $path;
        }

        $kategori->created_by = Auth::user()->id;
        $kategori->save();
        return redirect()->route('admin.faq.show_sub_kategori',['id' => $kategori->id])->with('create_sub_kategori_success',true);
    }

    public function show_sub_kategori($id)
    {
        $data['kategori'] = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
        $data['unrelated_sub_kategori'] = Kategori::where('id_parent',-1)->get();
        return view('faq.show', $data);    
    }

    public function edit($id)
    {
        $data['kategori'] = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
        return view('faq.kategori.edit', $data);    
    }

    public function edit_sub_kategori($id)
    {
        $data['kategori'] = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();

        $data['kategoris'] = Kategori::with(['parent','child','pertanyaan'])->get();
        return view('faq.edit', $data);    
    }

    public function update_sub_kategori(Request $request,$id)
    {
        $validator = Validator::make( $request->input(), [
            'name' => ['required', 'string'],
            'uraian' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        

        $kategori = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();
        $kategori->nama = $request->name;
        $kategori->uraian = $request->uraian;
        
        if($request->image){
            $now = Carbon::now();

            $path = 'storage/faq/'  . $now->timestamp .  str_random(5) . '.png';
            $data = explode(',', $request->image);
            $content = base64_decode($data[1]);
            file_put_contents(public_path($path),$content);
            $path = str_replace('public', 'storage', $path);
            $kategori->gambar = $path;
        }

        if($request->hapus_image){
            $kategori->gambar = null;
        }
        
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();
        return redirect()->route('admin.faq.show_sub_kategori',['id' => $kategori->id])->with('edit_success',true);
    }

    public function attach_sub_kategori(Request $request){
        $validator = Validator::make( $request->input(), [
            'id' => ['required', 'string'],
            'urutan' => ['required', 'numeric'],
            'id_sub_kategori' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $kategori = Kategori::where('id',$request->id_sub_kategori)->first();
        $kategori->id_parent = $request->id;
        $kategori->urutan = $request->urutan;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

        return redirect()->route('admin.faq.show',['id' => $request->redirect])->with('attach_success',true);
    }

    public function detach_sub_kategori(Request $request){
        $validator = Validator::make( $request->input(), [
            'id' => ['required', 'string'],
            'id_sub_kategori' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $kategori = Kategori::where('id',$request->id_sub_kategori)->first();
        $kategori->id_parent = -1;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

        return redirect()->route('admin.faq.show',['id' => $request->id])->with('detach_success',true);
    }

    public function ajax_pertanyaan(Request $request, $id){
        $kategori = Kategori::with(['parent','child','pertanyaan'])->where('id',$id)->first();

        $validator = Validator::make( $request->input(), [
            'pertanyaan' => ['required', 'string'],
            'jawaban' => ['required', 'string'],
        ],[
            'required' => ':attribute tidak boleh kosong',
        ]);

        if ($validator->fails()) {
            return response()->json(['code' => 200, 'data' => $validator], 200);
        }

        $kategori->pengantar_pertanyaan = $request->pertanyaan;
        $kategori->pengantar_jawaban = $request->jawaban;

        $kategori->updated_by = Auth::user()->id;
        $kategori->save();
        return response()->json(['code' => 200, 'data' => 200], 200);
    }

    public function ajax_unlink(Request $request, $id){
        $kategori = Kategori::where('id',$id)->first();
        $kategori->id_parent = -1;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

        return response()->json(['code' => 200, 'data' => 200], 200);
    }


    public function ajax_urutan(Request $request, $id){
        $kategori = Kategori::where('id',$id)->first();
        $kategori->urutan = $request->urutan;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

        return response()->json(['code' => 200, 'data' => 200], 200);
    }

    public function ajax_link(Request $request, $id){
        $kategori = Kategori::where('id',$request->id_sub_kategori)->first();
        $kategori->id_parent = $id;
        $kategori->urutan = $request->urutan;
        $kategori->updated_by = Auth::user()->id;
        $kategori->save();

         $temp = Kategori::where('id',$kategori->parent->id)->first();
          $class = '';
          while($temp->id_parent != 0) {
            $class .= ' parent'.$temp->id;
            $temp = Kategori::where('id',$temp->id_parent)->first();
          }
        return response()->json(['code' => 200, 'data' => $class], 200);
    }
}
