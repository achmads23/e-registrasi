<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Setting;
use App\Acara;
use App\Peserta;
use Carbon\Carbon;
use App\Ols_pengguna;
use App\DataBimtek;

class PublicController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function pindah_data(){
        $pesertas = Peserta::where('sesuai_spt',1)->get();
        foreach ($pesertas as $key => $peserta) {
            $acara = $peserta->acara;
            $now = Carbon::now();

            $data_bimtek = new DataBimtek;
            $data_bimtek->nama_bimtek = $peserta->nama;
            $data_bimtek->nip_bimtek = $peserta->nip;
            $data_bimtek->asal_bimtek = $peserta->asal_instansi;
            $data_bimtek->photo_bimtek = "gb_bimtek/" . $peserta->foto;
            $data_bimtek->date_bimtek = $now->toDateString();
            $data_bimtek->time_bimtek = $now->toTimeString();
            $data_bimtek->tt_bimtek = "-";
            $data_bimtek->acara_bimtek = $acara->nama_acr;
            $data_bimtek->user_bimtek = $peserta->username;
            $data_bimtek->pass_bimtek = $peserta->password;
            $data_bimtek->save();

            $ols = new Ols_pengguna;
            $ols->idkelompok = $acara->id_acr;
            $ols->kantor = $peserta->asal_instansi;
            $ols->nama = $peserta->nama;
            $ols->nip = $peserta->nip;
            $ols->pass = md5($peserta->password);
            $ols->tgl = $now;
            $ols->user = $peserta->username;
            $ols->jabatan = $peserta->jabatan;
            $ols->golongan = $peserta->golongan;
            $ols->useless = md5($peserta->password);
            $ols->tusi = '-';
            $ols->save();
        }
        return 'sukses';
    }
    public function monitor()
    {
        $monitor = Setting::where('label','monitor')
        ->first();
        if($monitor){
            if($monitor->value != 0){
                $acara = Acara::with(['peserta' => function($query){
                    $query->orderBy('updated_at', 'DESC');
                }
                    ,'peserta.opd','peserta.pegawai'])->where('id_acr',$monitor->value)->first();
                $data['acara'] = $acara;
                $data['acara']->opds = $data['acara']->opds();
                if($acara){
                    return view('monitor', $data);
                }
            } else {
            $data['acara'] = false;
                return view('monitor', $data);
            }
        }
        
    }

    public function reminder()
    {
        $now = Carbon::now();
        $now = $now->subMinutes(3600);

        $Responders = Responders::where('created_at', '<', $now)->where('answer', NULL)->get();
        foreach ($Responders as $key => $Responder) {
            if($Responder->user->token_firebase != ''){
                $token = $Responder->user->token_firebase;
                $title = 'Pertanyaan Belum Dijawab';
                $body = 'Pertanyaan ' . strtolower($Responder->thread->judul) . ' belum dijawab, jawab pertanyaan sekarang.';
                $dat =['jenis_notif' => 200];

                $res =  notification($title,$body,$dat,$token);
            }
        }
        return true;
    }
}
