<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
