<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use Carbon\Carbon;
use Hash;

class AjaxController extends Controller
{
	public function cek_code(Request $request){
		$code = Acara::where('code',$request->input('code'))->get();
        if(count($code) == 0){
            return response()->json(['code' => 200, 'data' => true], 200);
        } else {
            return response()->json(['code' => 200, 'data' => false], 200);
        }
	}

	public function cek_peserta(Request $request){
		$pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", $request->input('nip'))->with('opd')->first();

        if($pegawai){
            if($request->input('code') != 'false'){
                $acara = Acara::where('code',$request->input('code'))->first();
            } else {
                $acara = Acara::where('id_acr',$request->input('id'))->first();
            }

            $peserta = Peserta::where('id_pegawai', $pegawai->id)->where('id_acara', $acara->id_acr)->first();

            if(!$peserta) {
                if($acara->id_opds){
                    $id_opds = explode(';', $acara->id_opds);
                    $flag = 0;
                    foreach ($id_opds as $key => $value) {
                        if($value == $pegawai->opd->id){
                            $flag = 1;
                        }
                    }

                    if($flag == 1){
                        $data['pegawai'] = $pegawai;
                        $data['is_peserta'] = false;

                        if($pegawai){
                            return response()->json(['code' => 200, 'data' => $data], 200);
                        } else {
                            return response()->json(['code' => 200, 'data' => false], 200);
                        }
                    } else {
                        $data['opd'] = $pegawai->opd->opd;
                        return response()->json(['code' => 200, 'data' => $data], 200);
                    }
                } else {
                    $data['pegawai'] = $pegawai;
                    return response()->json(['code' => 200, 'data' => $data], 200);
                }
            } else {
                $data['peserta'] = $peserta;
                $data['is_peserta'] = true;
                return response()->json(['code' => 200, 'data' => $data], 200);
            }
        } else {
            return response()->json(['code' => 200, 'data' => false], 200);
        }
	}

    public function cek_bimtek_qr_code(Request $request){
        $username = $request->input('username');
        $peserta = Peserta::where('username', $request->input('username'))->first();
        $token = Hash::make($peserta->username . $peserta->password);
        $peserta->token_check_in = $token;
        $peserta->check_in = Carbon::now();
        $data['token'] = $token;
        $data['check_in'] = $peserta->check_in;
        if($peserta->save()){
            return response()->json(['code' => 200, 'data' => $data], 200);
        } else {
            return response()->json(['code' => 200, 'data' => false], 200);
        }
    }
}