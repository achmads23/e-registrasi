<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\FAQ\Kategori;
use App\Model\FAQ\Pertanyaan;
use Validator;
use Searchy;
class KlinikCenterController extends Controller
{

    function get_parent($id)
    {
        $array = [];
        $kategori = Kategori::with(['parent'])->findOrFail($id);
        if($kategori->parent != null){
            $arr = $this->get_parent($kategori->parent->id);
            $arr[] = $kategori;
            $array = $arr;
        } else {
            $array[] = $kategori;    
        }
        
        return $array;
    }

    public function detail($id, $sub_id = false)
    {
        $kategori = Kategori::with(['parent',"child" => function($q){
                $q->orderBy('urutan','ASC');
            },'child.pertanyaan','pertanyaan'])->where('id',$id)->first();

        $kategori->view = $kategori->view + 1;
        $kategori->save();
        $data['parents'] = $this->get_parent($id);
        $data['kategori'] = $kategori;
        $data['sub_id'] = $sub_id;
        return view('frontend.klinik-center.detail', $data); 
    }

    public function search(Request $request){
        $string = $request->pencarian;

        $searchValues = preg_split('/\s+/', $string, -1, PREG_SPLIT_NO_EMPTY); 

        $query = Kategori::whereHas('parent');
        $query->selectRaw("*, MATCH(`nama`) AGAINST(?) as relevance_score", $searchValues);
        $query->whereRaw("MATCH(`nama`) AGAINST(? IN BOOLEAN MODE)", $searchValues);
        $result = $query->orderByDesc('relevance_score')->get();

        $kategori = Kategori::where(function ($q) use ($searchValues) {
          foreach ($searchValues as $value) {
            $q->orWhere('nama', 'like', "%{$value}%");
            $q->orWhere('uraian', 'like', "%{$value}%");
          }
        })->whereHas('parent')->get();

        $merged = $result->merge($kategori);
        
        $data['pencarian'] = $string;
        $data['results'] = $merged->unique('id');


        return view('frontend.klinik-center.hasil-pencarian', $data); 
    }

    public function like_dislike(Request $request){
        $validator = Validator::make($request->all(), [
            'status'=> 'required|integer',
            'id'=> 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

        $kategori = Kategori::with(['parent',"child" => function($q){
                $q->orderBy('urutan','ASC');
            },'child.pertanyaan','pertanyaan'])->where('id',$request->id)->first();

        if($request->prev == 'dislike' && $request->status == 0){
            $kategori->dislike = $kategori->dislike - 1;
        } else if($request->prev == 'like' && $request->status == 1){
            $kategori->like = $kategori->like - 1;
        } else  {
            if($request->prev == 'like'){
                $kategori->like = $kategori->like - 1;
            } else if($request->prev == 'dislike'){
                $kategori->dislike = $kategori->dislike - 1;
            }


            if ($request->status == 1) {
                $kategori->like = $kategori->like + 1;
            } else {
                $kategori->dislike = $kategori->dislike + 1;
            }
        }

        if($kategori->like < 0){
            $kategori->like = 0;
        }

        if($kategori->dislike < 0){
            $kategori->dislike = 0;
        }

        $kategori->save();
        return response()->json($kategori, 200);
    }
}
