<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\DetailUser;
use App\Opd;
use App\Model\Wilayah\KabupatenKota;
use Validator, DB, Hash, Mail;
use App\Model\eClinic\User_verification;
use URL;
class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     */
    protected function get_verification_code()
    {
        $verification_code = str_random(30);

        while (true){
            $cek_vercode = User_verification::where('token',$verification_code)->first();
            if ($cek_vercode){
                $verification_code = str_random(30);                
            } else {
                return $verification_code;
            }
        }
    }

    protected function cek_email($email)
    {
        $cek_email = User::where('email',$email)->first();
        if($cek_email){
            return true;
        } else {
            return false;
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('public-login');
    }

    public function login(Request $request)
    {
        if(Auth::user()){
            return redirect()->route('bimtek');
        }
        return redirect(URL::to('/#contact'));
    }

    public function forget_password(Request $request)
    {
        return view('frontend.auth.forget-password');   
    }

    public function post_forget_password(Request $request)
    {
        $email = $request->email;

        $user = User::where('email',$email)->first();
        if($user){
            $user_verification = new User_verification;
            $user_verification->user_id = $user->id;
            $user_verification->token = $this->get_verification_code();
            $user_verification->save();

            $name =  $user->name;
            $subject = "[UPT PPK - BPKAD Provinsi Jawa Timur] Lupa Password.";
            Mail::send('email.lupa-password', ['name' => $name,'jenis_kelamin' => $user->jenis_kelamin, 'verification_code' => $user_verification->token],
                function($mail) use ($email, $name, $subject){
                    $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                    $mail->to($email, $name);
                    $mail->subject($subject);
                });

            return redirect()->back()->with('forget_password_success',true);   
        } else {
            return redirect()->back()->with('user_not_found',true);   
        }
    }

    public function reset_password(Request $request,$token)
    {
        $data['token'] = $token;
        return view('frontend.auth.reset-password',$data);   
    }

    public function post_reset_password(Request $request,$token)
    {
        $user_verification = User_verification::where('token',$token)->first();

        if($user_verification){
            $password = $request->password;
            $repassword = $request->repassword;
            
            if($password != $repassword){
                return redirect()->back()->with('password_different',true); 
            }

            $user = User::where('id', $user_verification->user_id)->first();
            $user->password = Hash::make($password);
            $user->save();

            return redirect(URL::to('/#contact'))->with('reset_password_success',true);
        } else {
            return redirect(URL::to('/#contact'));
        }
    }

    public function register(Request $request)
    {
        $data['kabkots'] = KabupatenKota::where('province_id',35)->get();
        $data['opds'] = Opd::get();
        return view('frontend.auth.register', $data);
    }

    public function post_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

        $user = User::where('email', $request->email)->first();
        if($user){
            if(Hash::check($request->password, $user->password)){
                Auth::login($user);
                return redirect()->route('bimtek');
            }
        }
        return redirect(URL::to('/#contact'))->with('login_failed',true);
    }

    public function post_register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'nama' => 'required',
            'password'=> 'required'
        ],[
           'email.unique' => 'Email sudah terdaftar!'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('register_failed',$validator->messages());
        }

        $name = $request->nama;
        $email = $request->email;
        $password = $request->password;
        
        if(!$this->cek_email($email)){
            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->name = $name;
            $user->password = Hash::make($password);
        } else {
            return redirect()->back()->with('register_failed_text','Email sudah terdaftar!');
        }

        $user_verification = new User_verification;
        $user_verification->token = $this->get_verification_code();
        

        $subject = "[UPT PPK - BPKAD Provinsi Jawa Timur] Verifikasi email.";
        Mail::send('email.verify', ['name' => $name, 'verification_code' => $user_verification->token],
            function($mail) use ($email, $name, $subject){
                $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                $mail->to($email, $name);
                $mail->subject($subject);
            });
        $data['message'] = 'Terimakasih, Buka email anda untuk melengkapi pendaftaran.';

        $user->save();
        $user_verification->user_id = $user->id;
        $user_verification->save();

        $detail_user = new DetailUser;
        $detail_user->id_user = $user->id;
        $detail_user->area = $request->region;
        $detail_user->jenis_kelamin = $request->jenis_kelamin;
        $detail_user->hp = $request->hp;
        if($request->region != "Provinsi Jawa Timur"){
            $detail_user->region= $request->kabkot;
            $detail_user->opd= $request->input('opd-text');
        } else {
            $detail_user->region = $request->region;
            $detail_user->opd= $request->input('opd');
        }
        
        $detail_user->created_by = $user->id;
        $detail_user->save();

        return redirect(URL::to('/#contact'))->with('register_success',true);
    }
}
