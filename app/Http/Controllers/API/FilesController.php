<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Files;
use Validator;
use App\User;
class FilesController extends Controller
{
	public function upload(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $validator = Validator::make($request->all(), [
            'file'=> 'file',
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

        $location = 'public/files';

        $path = $request->file->store($location);
        $path = str_replace('public', 'storage', $path);

        $file = new Files;
        $file->jenis = 'file';
        $file->file = $path;
        $file->create_by = $user->id;
        $file->save();

	    $data['success'] = true;
        $data['data']['file'] = $file;
    	return response()->json($data,200);
    }

    public function show($id)
    {
    	$file = Files::where('id',$id)->get();
    	$data['success'] = true;
        $data['data']['file'] = $file;
    	return response()->json($data,200);
    }
}