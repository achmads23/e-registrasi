<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;


class FirebaseController extends Controller
{
	public function set(Request $request){
		$users = User::where('token_firebase', 'LIKE', request('token_firebase'))->get();
        foreach ($users as $key => $user) {
            $user->token_firebase = '';
            $user->save();
        }

        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $user->token_firebase = request('token_firebase');
        $user->save();

        $data['success'] = true;
    	return response()->json($data,200);
	}

	public function get(Request $request){
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();

        $data['success'] = true;
        $data['data']['token_firebase'] = $user->token_firebase;
    	return response()->json($data,200);
	}
}