<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\eClinic\User_verification;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Response;
use Auth;
use Carbon\Carbon;

class AuthController extends Controller
{
    protected function get_verification_code()
    {
        $verification_code = str_random(30);

        while (true){
            $cek_vercode = User_verification::where('token',$verification_code)->first();
            if ($cek_vercode){
                $verification_code = str_random(30);                
            } else {
                return $verification_code;
            }
        }
    }

    protected function cek_email($email)
    {
        $cek_email = User::where('email',$email)->first();
        if($cek_email){
            return true;
        } else {
            return false;
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required'
        ],[
           'email.unique' => 'Email sudah terdaftar!',
           'email.email' => 'Gunakan email yang valid!'
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()],500);
        }

        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        
        if(!$this->cek_email($email)){
            $user = new User;
            $user->name = $name;
            $user->email = $email;
            $user->name = $name;
            $user->password = Hash::make($password);
        } else {
            return response()->json(['success'=> false, 'error'=> 'Email sudah terdaftar!'],500);   
        }

        $user_verification = new User_verification;
        $user_verification->token = $this->get_verification_code();
        

        $subject = "[e-clinic UPT PPK - BPKAD Provinsi Jawa Timur] Verifikasi email.";
        Mail::send('email.verify', ['name' => $name, 'verification_code' => $user_verification->token],
            function($mail) use ($email, $name, $subject){
                $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                $mail->to($email, $name);
                $mail->subject($subject);
            });
        $data['message'] = 'Terimakasih, Buka email anda untuk melengkapi pendaftaran.';

        $user->save();
        $user_verification->user_id = $user->id;
        $user_verification->save();

        return response()->json(['success'=> true, 'data'=> $data ],200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

        $user = User::where('email', 'LIKE', request('email'))->first();
        
        if($user){
            if(Hash::check(request('password'),$user->password)){
                $now = Carbon::now();
                $token = Hash::make(request('email') . $now->timestamp . str_random(20));
                $user->token = $token;
                $user->save();

                $data['success'] = true;
                $data['data']['user'] = $user;

                return response()->json($data,200);
            }
        }

        return response()->json(['success' => false, 'error' => 'invalid_credentials'], 401);
    }

    public function logout(Request $request) {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $user->token = '';
        $user->token_firebase = '';
        $user->save();
        return response()->json(['success' => true,'msg' => API_SUKSES_MSG], API_SUKSES);
    }

    public function recover(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "Email tidak ditemukan.";
            return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 401);
        }
        try {
            $password = str_random(6);
            $user->password = Hash::make($password);
            $user->save();
            $subject = "Reset password success.";
            Mail::send('email.reset-password', ['name' => $user->name,'jenis_kelamin' => $user->jenis_kelamin, 'password' => $password],
                function($mail) use ($user, $subject){
                    $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                    $mail->to($user->email, $user->name);
                    $mail->subject($subject);
                });
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json([
            'success' => true, 'data' => ['message'=> 'Password telah di reset. cek email anda segera!']
        ], 500);
    }
}