<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Acara;
use App\Peserta;
use App\Opd;
use Carbon\Carbon;
use App\Setting;
use PDF;
use App\DataBimtek;
use App\Ols_pengguna;
class AcaraController extends Controller
{
	public function index(Request $request)
    {
        $data['acara'] = Acara::orderBy('created_at','DESC')->get();
        $data['monitor'] = Setting::where('label','monitor')->first();
        return view('acara.index', $data);
    }

    public function create()
    {
    	$data = [];
        $data['opds'] = Opd::get();
        return view('acara.create',$data);
    }

    public function cetak_daftar_hadir(Request $request,$id)
    {
        $acara = Acara::with(['peserta' => function($query){
                    $query->where('status', 1);
                }])->where('id_acr',$id)->first();
        $data['acara'] = $acara;
        $data['acara']->opds = $data['acara']->opds();
        $tgl_acr = Carbon::createFromFormat('Y-m-d', $acara->tgl_acr);
        $day = $tgl_acr->dayOfWeek;
        if($day == 1) $day = 'Senin'; 
        elseif($day == 2) $day = 'Selasa'; 
        elseif($day == 3) $day = 'Rabu'; 
        elseif($day == 4) $day = 'Kamis'; 
        elseif($day == 5) $day = 'Jumat'; 
        elseif($day == 6) $day = 'Sabtu'; 
        elseif($day == 7) $day = 'Minggu'; 
        
        $data['date'] = $tgl_acr->day;
        $data['year'] = $tgl_acr->year;
        $month = $tgl_acr->month;
        if($month == 1)
        $month = 'Januari';
        else if($month == 2)
        $month = 'Februari';
        else if($month == 3)
        $month = 'Maret';
        else if($month == 4)
        $month = 'April';
        else if($month == 5)
        $month = 'Mei';
        else if($month == 6)
        $month = 'Juni';
        else if($month == 7)
        $month = 'Juli';
        else if($month == 8)
        $month = 'Agustus';
        else if($month == 9)
        $month = 'September';
        else if($month == 10)
        $month = 'Oktober';
        else if($month == 11)
        $month = 'November';
        else if($month == 12)
        $month = 'Desember';
        $data['month'] = $month;
        $data['day'] = $day;

        $pdf = PDF::loadView('pdf.daftar-hadir', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
        // return $pdf->stream();
        return $pdf->download('daftar-hadir-'. str_replace(' ','-',$acara->nama_acr) .'.pdf');
    }

    public function cetak_undangan_opd(Request $request,$id)
    {
        $now = Carbon::createFromFormat('d-m-Y', $request->tgl_surat);
        $acara = Acara::with(['peserta' => function($query){
                    $query->where('status', 1);
                }])->where('id_acr',$id)->first();
        $data['acara'] = $acara;
        $data['date'] = $now->day;
        $data['year'] = $now->year;
        $month = $now->month;
        if($month == 1)
        $month = 'Januari';
        else if($month == 2)
        $month = 'Februari';
        else if($month == 3)
        $month = 'Maret';
        else if($month == 4)
        $month = 'April';
        else if($month == 5)
        $month = 'Mei';
        else if($month == 6)
        $month = 'Juni';
        else if($month == 7)
        $month = 'Juli';
        else if($month == 8)
        $month = 'Agustus';
        else if($month == 9)
        $month = 'September';
        else if($month == 10)
        $month = 'Oktober';
        else if($month == 11)
        $month = 'November';
        else if($month == 12)
        $month = 'Desember';
        $data['month'] = $month;
        $data['acara']->opds = $data['acara']->opds();

        $tgl_acr = Carbon::createFromFormat('Y-m-d', $acara->tgl_acr);
        $day = $tgl_acr->dayOfWeek;
        if($day == 1) $day = 'Senin'; 
        elseif($day == 2) $day = 'Selasa'; 
        elseif($day == 3) $day = 'Rabu'; 
        elseif($day == 4) $day = 'Kamis'; 
        elseif($day == 5) $day = 'Jumat'; 
        elseif($day == 6) $day = 'Sabtu'; 
        elseif($day == 7) $day = 'Minggu'; 
        $data['day'] = $day;

        $month = $tgl_acr->month;
        if($month == 1)
        $month = 'Januari';
        else if($month == 2)
        $month = 'Februari';
        else if($month == 3)
        $month = 'Maret';
        else if($month == 4)
        $month = 'April';
        else if($month == 5)
        $month = 'Mei';
        else if($month == 6)
        $month = 'Juni';
        else if($month == 7)
        $month = 'Juli';
        else if($month == 8)
        $month = 'Agustus';
        else if($month == 9)
        $month = 'September';
        else if($month == 10)
        $month = 'Oktober';
        else if($month == 11)
        $month = 'November';
        else if($month == 12)
        $month = 'Desember';
        $data['bulan'] = $month;
        $data['tgl'] = $tgl_acr->day;
        $data['tahun'] = $tgl_acr->year;
        $data['nomor_surat'] = $request->nomor_surat;
        $data['ttd'] = $request->ttd;
        $data['kepada'] = $request->kepada;
        $pdf = PDF::loadView('pdf.undangan-opd', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
        return $pdf->stream('undangan-opd-'. str_replace(' ','-',$acara->nama_acr) .'.pdf');
    }

    public function cetak_undangan_umum(Request $request,$id)
    {
        $now = Carbon::createFromFormat('d-m-Y', $request->tgl_surat);
        $acara = Acara::with(['peserta' => function($query){
                    $query->where('status', 1);
                }])->where('id_acr',$id)->first();
        $data['acara'] = $acara;
        $data['date'] = $now->day;
        $data['year'] = $now->year;
        $month = $now->month;
        if($month == 1)
        $month = 'Januari';
        else if($month == 2)
        $month = 'Februari';
        else if($month == 3)
        $month = 'Maret';
        else if($month == 4)
        $month = 'April';
        else if($month == 5)
        $month = 'Mei';
        else if($month == 6)
        $month = 'Juni';
        else if($month == 7)
        $month = 'Juli';
        else if($month == 8)
        $month = 'Agustus';
        else if($month == 9)
        $month = 'September';
        else if($month == 10)
        $month = 'Oktober';
        else if($month == 11)
        $month = 'November';
        else if($month == 12)
        $month = 'Desember';
        $data['month'] = $month;
        $data['acara']->opds = $data['acara']->opds();
        $data['nomor_surat'] = $request->nomor_surat;
        $data['ttd'] = $request->ttd;
        $data['jumlah'] = $request->jumlah;
        $data['kepada'] = $request->kepada;
        $pdf = PDF::loadView('pdf.undangan-umum', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
        return $pdf->download('undangan-umum-'. str_replace(' ','-',$acara->nama_acr) .'.pdf');
    }

    public function cetak_konfirmasi_peserta(Request $request,$id)
    {
        $acara = Acara::with(['peserta' => function($query){
                    $query->where('status', 1);
                }])->where('id_acr',$id)->first();
        $data['acara'] = $acara;
        $data['acara']->opds = $data['acara']->opds();
        $tgl_acr = Carbon::createFromFormat('Y-m-d', $acara->tgl_acr);
        $day = $tgl_acr->dayOfWeek;
        if($day == 1) $day = 'Senin'; 
        elseif($day == 2) $day = 'Selasa'; 
        elseif($day == 3) $day = 'Rabu'; 
        elseif($day == 4) $day = 'Kamis'; 
        elseif($day == 5) $day = 'Jumat'; 
        elseif($day == 6) $day = 'Sabtu'; 
        elseif($day == 7) $day = 'Minggu'; 
        
        $data['date'] = $tgl_acr->day;
        $data['year'] = $tgl_acr->year;
        $month = $tgl_acr->month;
        if($month == 1)
        $month = 'Januari';
        else if($month == 2)
        $month = 'Februari';
        else if($month == 3)
        $month = 'Maret';
        else if($month == 4)
        $month = 'April';
        else if($month == 5)
        $month = 'Mei';
        else if($month == 6)
        $month = 'Juni';
        else if($month == 7)
        $month = 'Juli';
        else if($month == 8)
        $month = 'Agustus';
        else if($month == 9)
        $month = 'September';
        else if($month == 10)
        $month = 'Oktober';
        else if($month == 11)
        $month = 'November';
        else if($month == 12)
        $month = 'Desember';
        $data['month'] = $month;
        $data['day'] = $day;

        $pdf = PDF::loadView('pdf.konfirmasi-peserta', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
        // return $pdf->stream();
        return $pdf->download('konfirmasi-peserta-'. str_replace(' ','-',$acara->nama_acr) .'.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $acara = new Acara;
        
        $acara->nama_acr = $request->input('nama_acr');

        $acara->tempat_acr = $request->input('tempat_acr');
        $acara->tgl_acr = Carbon::createFromFormat('d-m-Y', $request->input('tgl_acr'));
        $acara->akhir_tgl_acr = Carbon::createFromFormat('d-m-Y', $request->input('akhir_tgl_acr'));
        $code = generateRandomString(2,'huruf_besar') . generateRandomString(2,'angka');
        
        $cek_code = Acara::where('code',$code)->get();
        while(count($cek_code) != 0){
            $code = generateRandomString(2,'huruf_besar') . generateRandomString(2,'angka');
            $cek_code = Acara::where('code',$code)->get();
        }
        $acara->code = $code;
        $acara->deskripsi = $request->deskripsi;

        $acara->id_opds = '';
        if($request->input('opd'))
        foreach ($request->input('opd') as $key => $value) {
            $acara->id_opds .= $value.';';
        }

        $acara->pukul = $request->pukul;
        $acara->bagi = $request->bagi;
        $acara->lingkungan = $request->lingkungan;

        $acara->tanggal_awal = Carbon::createFromFormat('d-m-Y', $request->input('tanggal_awal'));
        $acara->tanggal_akhir = Carbon::createFromFormat('d-m-Y', $request->input('tanggal_akhir'));

        $acara->tampil_awal = Carbon::createFromFormat('d-m-Y', $request->input('tampil_awal'));
        $acara->tampil_akhir = Carbon::createFromFormat('d-m-Y', $request->input('tampil_akhir'));

        $acara->kuota_undangan = $request->input('kuota_undangan');
        $acara->kuota_umum = $request->input('kuota_umum');
        $acara->created_by = Auth::user()->id;
        $acara->save();
        return redirect('/admin/acara')->with('create_success',true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $acara = Acara::where('id_acr',$id)->with(['peserta' => function($query){
                    $query->where('status', 1);
                }])->first();
        
        $data['terdaftar'] = Peserta::where('id_acara',$id)->where('status',1)->count();
        $data['check_in'] = Peserta::where('id_acara',$id)->where('check_in','!=' , NULL)->count();
        $data['sesuai_spt'] = Peserta::where('id_acara',$id)->where('sesuai_spt','1')->count();
        $data['tidak_sesuai_spt'] = Peserta::where('id_acara',$id)->where('sesuai_spt','-1')->count();

        $data['inap'] = Peserta::where('id_acara',$id)->where('inap',1)->where('status',1)->count();
        
        $data['acara'] = $acara;
        $data['acara']->opds = $data['acara']->opds();
        return view('acara.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['acara'] = Acara::where('id_acr',$id)->first();
        $data['opds'] = Opd::get();
        return view('acara.edit', $data);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $acara = Acara::findOrFail($id);

        $acara->id_opds = '';
        if($request->input('opd'))
        foreach ($request->input('opd') as $key => $value) {
            $acara->id_opds .= $value.';';
        }

        $acara->nama_acr = $request->input('nama_acr');
        $acara->tempat_acr = $request->input('tempat_acr');
        $acara->tgl_acr = Carbon::createFromFormat('d-m-Y', $request->input('tgl_acr'));
        $acara->akhir_tgl_acr = Carbon::createFromFormat('d-m-Y', $request->input('akhir_tgl_acr'));
        $acara->pukul = $request->pukul;
        
        $acara->kuota_undangan = $request->input('kuota_undangan');
        $acara->kuota_umum = $request->input('kuota_umum');
        $acara->bagi = $request->bagi;
        $acara->lingkungan = $request->lingkungan;

        $acara->tanggal_awal = Carbon::createFromFormat('d-m-Y', $request->input('tanggal_awal'));
        $acara->tanggal_akhir = Carbon::createFromFormat('d-m-Y', $request->input('tanggal_akhir'));

        $acara->tampil_awal = Carbon::createFromFormat('d-m-Y', $request->input('tampil_awal'));
        $acara->tampil_akhir = Carbon::createFromFormat('d-m-Y', $request->input('tampil_akhir'));

        $acara->updated_by = Auth::user()->id;
        $acara->save();
        return redirect('/admin/acara/'.$acara->id_acr)->with('edit_success',true);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acara = Acara::findOrFail($id);
        foreach ($acara->peserta as $key => $peserta) {
            $peserta->delete();
        }
        $acara->delete();
        return redirect('/admin/acara')->with('delete_success',true);  
    }

    public function cek_spt(Request $request)
    {
        $data['id_peserta'] = $request->input('id_peserta');
        $data['value'] = $request->input('value');
        $peserta = Peserta::where('id',$data['id_peserta'])->first();
        $peserta->sesuai_spt = $data['value'];
        if($data['value'] == 1){
            $acara = $peserta->acara;
            $now = Carbon::now();

            $data_bimtek = new DataBimtek;
            $data_bimtek->nama_bimtek = $peserta->nama;
            $data_bimtek->nip_bimtek = $peserta->nip;
            $data_bimtek->asal_bimtek = $peserta->asal_instansi;
            $data_bimtek->photo_bimtek = "gb_bimtek/" . $peserta->foto;
            $data_bimtek->date_bimtek = $now->toDateString();
            $data_bimtek->time_bimtek = $now->toTimeString();
            $data_bimtek->tt_bimtek = "-";
            $data_bimtek->acara_bimtek = $acara->nama_acr;
            $data_bimtek->user_bimtek = $peserta->username;
            $data_bimtek->pass_bimtek = $peserta->password;
            $data_bimtek->save();

            $ols = new Ols_pengguna;
            $ols->idkelompok = $acara->id_acr;
            $ols->kantor = $peserta->asal_instansi;
            $ols->nama = $peserta->nama;
            $ols->nip = $peserta->nip;
            $ols->pass = md5($peserta->password);
            $ols->tgl = $now;
            $ols->user = $peserta->username;
            $ols->jabatan = '-';
            $ols->golongan = $peserta->golongan;
            $ols->useless = md5($peserta->password);
            $ols->tusi = '-';
            $ols->save();
        }

        if($peserta->save()){
            return response()->json(['code' => 200,
                'data' => $peserta
            ], 200);
        } else {
            return response()->json(['code' => 500,
                'message' => 'Internal Server Error!'
            ], 500);
        }
    }

    public function force_check_in(Request $request)
    {
        $data['id_peserta'] = $request->input('id_peserta');
        $peserta = Peserta::where('id',$data['id_peserta'])->first();
        $now = Carbon::now();
        $peserta->check_in = $now;
        if($peserta->save()){
            return response()->json(['code' => 200,
                'data' => $now->format('d-m-Y H:i')
            ], 200);
        } else {
            return response()->json(['code' => 500,
                'message' => 'Internal Server Error!'
            ], 500);
        }
    }

    public function notify(Request $request)
    {
        $data['id_peserta'] = $request->input('id_peserta');
        $peserta = Peserta::where('id',$data['id_peserta'])->first();
        $now = Carbon::now();
        if($peserta->notif)
            $peserta->notif = 0;
        else 
            $peserta->notif = 1;

        if($peserta->save()){
            return response()->json(['code' => 200,'data' => $peserta->notif], 200);
        } else {
            return response()->json(['code' => 500,
                'message' => 'Internal Server Error!'
            ], 500);
        }
    }

    public function set_monitor(Request $request,$id)
    {
        $setting = Setting::where('label','monitor')->first();
        $setting->value = $id;
        $setting->save();
        return redirect('/admin/acara/'. $id)->with('monitor_success',true);  
    }

}
