<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Peserta;
use Mail;
class KonfirmasiPerubahanOpdController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pesertas'] = Peserta::where('status',0)->where('jenis','undangan')->get();
        $data['pesertas_ditolak'] = Peserta::where('status',-1)->get();
        return view('konfirmasi-perubahan-opd.index',$data);
    }

    public function detail($id)
    {
        $data['peserta'] = Peserta::findOrFail($id);
        return view('konfirmasi-perubahan-opd.show',$data);
    }

    public function tolak($id)
    {
        $peserta = Peserta::with('acara')->findOrFail($id);
        $peserta->status = -1;
        $peserta->save();

        $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Konfirmasi Perubahan OPD.";
        $name = $peserta->nama;
        $email = $peserta->email;
        if($email){
            Mail::send('email.konfirmasi-opd-tolak', ['name' => $peserta->nama],
                function($mail) use ($email, $name, $subject){
                    $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                    $mail->to($email, $name);
                    $mail->subject($subject);
            });
        }

        return redirect('/admin/konfirmasi-opd')->with('tolak_success',true);
    }

    public function setuju($id)
    {
        $peserta = Peserta::with('acara')->findOrFail($id);
        $peserta->status = 1;
        $peserta->save();

        $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Konfirmasi Perubahan OPD.";
        $name = $peserta->nama;
        $email = $peserta->email;
        if($email){
            Mail::send('email.selamat-peserta', ['name' => $peserta->nama,'jenis_kelamin' => $peserta->jenis_kelamin, 'nama_acr' => $peserta->acara->nama_acr,'tgl_acr' => $peserta->acara->tgl_acr,'tempat_acr' => $peserta->acara->tempat_acr, 'username' => $peserta->username,'password' => $peserta->password],
                function($mail) use ($email, $name, $subject){
                    $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                    $mail->to($email, $name);
                    $mail->subject($subject);
            });
        }
        
        return redirect('/admin/konfirmasi-opd')->with('setuju_success',true);
    }
}
