<?php

namespace App\Http\Controllers\Bimtek;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
class UmumController extends Controller
{
	public function index(Request $request)
    {
        $now = Carbon::now();
        $data['acaras'] = Acara::whereDate('tampil_awal','<=',$now->format('Y-m-d'))->whereDate('tampil_akhir','>=',$now->format('Y-m-d'))->get();
        return view('bimtek.umum.index', $data);
    }

    public function post_index(Request $request)
    {
        $id_acara = $request->acara;
        $nip = $request->nip;
        $code = $request->code;
        $jabatan = $request->jabatan;
        $email = $request->email;
        $jenis_kelamin = $request->jenis_kelamin;
        $hp = $request->hp;
        $nama = $request->nama;
        $asal_instansi = $request->asal_instansi;
        $golongan = $request->golongan;
        $foto = $request->foto;

        $acara = Acara::with(['peserta' => function($query){
                    $query->where('jenis', 'umum')->where(function($q) {
                         $q->where('status', 0)->orWhere('status',1);
                     });
                }])->where('id_acr',$id_acara)->first();
        $hitungpeserta = Peserta::where(function($query){
                 $query->where('status', 1);
                 $query->orWhere('status', 0);
             })->where(function($query){
                 $query->where('jenis', 'umum');
                 $query->orWhere('jenis', 'grup-umum');
             })->get();
        if($hitungpeserta->count() < $acara->kuota_umum){
            $peserta = Peserta::where('id_acara',$id_acara)->where('id_pegawai',0)->where('jenis','umum')->whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $nip))->first();
            if(!$peserta){
                $peserta = new Peserta;
                $peserta->id_acara = $acara->id_acr;
                $peserta->id_pegawai = 0;                
                $peserta->jenis = 'umum';                
                $peserta->id_opd = 0; 
                $peserta->nip = $nip; 
                $peserta->nama = $nama;
                $peserta->asal_instansi = $asal_instansi;
                $peserta->golongan = $golongan;
                $peserta->jenis_kelamin = $jenis_kelamin;
                if($jabatan != 'Lain-lain'){
                    $peserta->jabatan = $jabatan;
                } else {
                    $peserta->jabatan = $request->jabatan_text;
                }
                $peserta->email = $email;
                $peserta->no_hp = $hp;
                $peserta->inap = $request->inap;
                $peserta->status = 0;
                $peserta->username = $this->username($acara->code,$peserta->id_acara);
                $peserta->qrcode = $this->qrcode($peserta->username);

                if ($request->hasFile('foto')) {
                    $path = $request->foto->store('public/foto');
                    $peserta->foto = str_replace('public', 'storage', $path);
                } else {
                    $peserta->foto = 'user.png';
                }

                $peserta->password = generateRandomString(6);
                while (true){
                    $cek_peserta = Peserta::where('password',$peserta->password)->first();
                    if ($cek_peserta){
                        $peserta->password = generateRandomString(6);
                    } else {
                        break;
                    }
                }
                
                $peserta->save();
                $now = Carbon::now();
                $data['title'] = 'Pendaftaran Peserta Umum Sukses';
                $data['message'] = 'Data anda akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';
                return view('bimtek.umum.info',$data);
            }else {
                $data['peserta'] = $peserta;
                return view('bimtek.umum.registrasi-sukses', $data);
            }
        } else {
            return redirect()->route('bimtek.umum')->with('kuota_habis', true);
        }
    }

    public function show(Request $request,$id)
    {
        $data['acara'] = Acara::where('id_acr',$id)->first();
        $data['acara']->opds = $data['acara']->opds();
        return view('bimtek.acara.show', $data);
    }

    protected function username($code,$id_acara){
        $jumlah_peserta = Peserta::where('id_acara',$id_acara)->count();
        
        $jumlah_peserta = $jumlah_peserta + 1;
        $jumlah_peserta = sprintf('%03d', $jumlah_peserta);

        return $code . $jumlah_peserta;
    }

    protected function qrcode($jumlah_peserta){
        $file = Hash::make($jumlah_peserta);
        $file = preg_replace('~[^a-zA-Z0-9]+~', '', $file);
        $pf = '/qrcode/'.$file.'.png';
        $db_pf = 'storage/qrcode/'.$file.'.png';
        $ifp = fopen(storage_path('app/public') .$pf, 'wb' ); 
        fwrite( $ifp, base64_decode(DNS2D::getBarcodePNG($jumlah_peserta, "QRCODE",64,64)));
        fclose( $ifp ); 
        return $db_pf;
    }
}