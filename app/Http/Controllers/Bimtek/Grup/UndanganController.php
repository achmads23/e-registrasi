<?php

namespace App\Http\Controllers\Bimtek\Grup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
class UndanganController extends Controller
{
	public function index(Request $request)
    {
    	return view('bimtek.grup.undangan.index');
    }

    public function detail(Request $request)
    {
        $now = Carbon::now();
        $code = Acara::where('code',$request->input('token'))->get();
        if(count($code) > 0){
            $tanggal_awal = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_awal)->setTime(0, 0, 0);
            $tanggal_akhir = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_akhir)->setTime(23, 59, 59);;
            if($now >= $tanggal_awal && $now <= $tanggal_akhir){
                $data['acara'] = $code[0];
                $data['acara']->opds = $data['acara']->opds();
                return view('bimtek.grup.undangan.detail', $data);
            } else if($now < $tanggal_awal){
                return redirect()->back()->with('token_error', 'Pendaftaran Belum Dibuka.')->withInput($request->input());    
            } else if($now > $tanggal_akhir){
                return redirect()->back()->with('token_error', 'Pendaftaran Sudah Ditutup.')->withInput($request->input());    
            }
        } else {
            return redirect()->back()->with('token_error', 'Token Tidak Ditemukan.')->withInput($request->input());
        }
    }

    public function register(Request $request)
    {
    	$code = $request->code;

    	$id_acara = $request->acara;
    	$nama = $request->nama;
    	$nip = $request->nip;
    	$golongan = $request->golongan;
    	$jenis_kelamin = $request->jenis_kelamin;
    	$jabatan_text = $request->jabatan_text;
    	$email = $request->email;
    	$hp = $request->hp;
    	$inap = $request->inap;
    	$jabatans = $request->jabatan;
    	$foto = $request->foto;		


    	if($request->jumlah_rombongan == 0){
    		return redirect()->back()->with('message_success','Jumlah rombongan minimal 1');
    	}

		if($jabatans && $nama && $nip && $golongan && $email && $hp && $inap && $jenis_kelamin){
			$jabatans = array_filter($jabatans, function($value) { return $value !== null && $value !== ''; });
			$nama = array_filter($nama, function($value) { return $value !== null && $value !== ''; });
			$nip = array_filter($nip, function($value) { return $value !== null && $value !== ''; });
			$golongan = array_filter($golongan, function($value) { return $value !== null && $value !== ''; });
			$jenis_kelamin = array_filter($jenis_kelamin, function($value) { return $value !== null && $value !== ''; });
			$email = array_filter($email, function($value) { return $value !== null && $value !== ''; });
			$hp = array_filter($hp, function($value) { return $value !== null && $value !== ''; });
			$inap = array_filter($inap, function($value) { return $value !== null && $value !== ''; });
			
			if(count($jabatans) < $request->jumlah_rombongan || 
				count($nama) < $request->jumlah_rombongan  || 
				count($nip) < $request->jumlah_rombongan  || 
				count($golongan) < $request->jumlah_rombongan  || 
				count($jenis_kelamin) < $request->jumlah_rombongan  || 
				count($email) < $request->jumlah_rombongan  || 
				count($hp) < $request->jumlah_rombongan  || 
				count($inap) < $request->jumlah_rombongan){
				return redirect()->back()->with('message_success','Data tidak lengkap, semua data wajib diisi!');
			}
		} else {
			return redirect()->back()->with('message_success','Data tidak lengkap, semua data wajib diisi!');
		}




    	// if($request->jumlah_rombongan == $jabatans){

    	// }
    	$acara = Acara::where('code',$code)->first();

        $hitungpeserta = Peserta::where(function($query){
                 $query->where('status', 1);
                 $query->orWhere('status', 0);
             })->where(function($query){
                 $query->where('jenis', 'undangan');
                 $query->orWhere('jenis', 'grup-undangan');
             })->get();

        if($hitungpeserta->count() + $request->jumlah_rombongan < $acara->kuota_undangan){
        	$flag = 0;
        	foreach ($jabatans as $key => $jabatan) {
        		$peserta = Peserta::where('id_acara',$acara->id_acr)->whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $nip[$key + 1]))->first();
	            if(!$peserta){
	                $peserta = new Peserta;
	                $peserta->id_acara = $acara->id_acr;
	                $peserta->id_pegawai = 0;                
	                $peserta->jenis = 'grup-undangan';                
	                $peserta->id_opd = 0; 
	                $peserta->nip = $nip[$key+1];
	                $peserta->nama = $nama[$key+1];
	                $peserta->asal_instansi = Auth::user()->detail->opd;
	                $peserta->golongan = $golongan[$key];;
	                $peserta->jenis_kelamin = $jenis_kelamin[$key];;
	                if($jabatan != 'Lain-lain'){
	                    $peserta->jabatan = $jabatan;
	                } else {
	                    $peserta->jabatan = $jabatan_text[$key+1];
	                }
	                $peserta->email = $email[$key+1];;
	                $peserta->no_hp = $hp[$key+1];;
	                $peserta->inap = $inap[$key+1];;
	                $peserta->status = 0;
	                $peserta->created_by = Auth::user()->id;
	                $peserta->username = $this->username($acara->code,$peserta->id_acara);
	                $peserta->qrcode = $this->qrcode($peserta->username);

	                if ($request->hasFile('foto')) {
	                    $path = $request->foto[$key+1]->store('public/foto');
	                    $peserta->foto = str_replace('public', 'storage', $path);
	                } else {
	                    $peserta->foto = 'user.png';
	                }

	                $peserta->password = generateRandomString(6);
	                while (true){
	                    $cek_peserta = Peserta::where('password',$peserta->password)->first();
	                    if ($cek_peserta){
	                        $peserta->password = generateRandomString(6);
	                    } else {
	                        break;
	                    }
	                }
	                
	                $peserta->save();
	                $now = Carbon::now();
	                $flag++;
	            }else {
	                $data['nips'][] = $nip[$key + 1];
	            }	
        	}

        	if($flag == count($jabatans)){
	        	$data['title'] = 'Pendaftaran Sukses';
	            $data['message'] = 'Peserta akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';
	            return view('bimtek.grup.undangan.info',$data);
	        } else {
	        	if($flag == 0){
	        		$data['title'] = 'Peserta sudah terdaftar';
	            	$data['message'] = 'Peserta telah terdaftar sebagai peserta, Informasi lebih lanjut hubungi admin. Terima Kasih.';
	            	return view('bimtek.grup.undangan.info',$data);
	        		
	        	} else {
	        		$data['title'] = 'Pendaftaran Sukses';
	        		$data['message'] = 'NIP ';
	        		foreach ($data['nips'] as $key => $nip) {
	        			$data['message'] = $data['message'] . $nip;
	        		}
	        		$data['message'] .= ' Sudah terdaftar sebagai peserta. ';
	            	$data['message'] .= 'Peserta akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';	            	
	            return view('bimtek.grup.undangan.info',$data);
	        	}
	        }
        } else {
            return redirect()->route('bimtek')->with('kuota_habis', true);
        }
    }

    protected function username($code,$id_acara){
        $jumlah_peserta = Peserta::where('id_acara',$id_acara)->count();
        
        $jumlah_peserta = $jumlah_peserta + 1;
        $jumlah_peserta = sprintf('%03d', $jumlah_peserta);

        return $code . $jumlah_peserta;
    }

    protected function qrcode($jumlah_peserta){
        $file = Hash::make($jumlah_peserta);
        $file = preg_replace('~[^a-zA-Z0-9]+~', '', $file);
        $pf = '/qrcode/'.$file.'.png';
        $db_pf = 'storage/qrcode/'.$file.'.png';
        $ifp = fopen(storage_path('app/public') .$pf, 'wb' ); 
        fwrite( $ifp, base64_decode(DNS2D::getBarcodePNG($jumlah_peserta, "QRCODE",64,64)));
        fclose( $ifp ); 
        return $db_pf;
    }
}