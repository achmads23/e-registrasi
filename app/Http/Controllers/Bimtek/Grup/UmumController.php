<?php

namespace App\Http\Controllers\Bimtek\Grup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
class UmumController extends Controller
{
	public function index(Request $request)
    {
    	$now = Carbon::now();
        $data['acaras'] = Acara::whereDate('tampil_awal','<=',$now->format('Y-m-d'))->whereDate('tampil_akhir','>=',$now->format('Y-m-d'))->get();
    	$data['opds'] = Opd::get();
    	return view('bimtek.grup.umum.index',$data);
    }

    public function register(Request $request)
    {
    	$id_acara = $request->acara;
    	$nama = $request->nama;
    	$nip = $request->nip;
    	$golongan = $request->golongan;
    	$jabatan_text = $request->jabatan_text;
    	$email = $request->email;
    	$jenis_kelamin = $request->jenis_kelamin;
    	$hp = $request->hp;
    	$inap = $request->inap;
    	$jabatans = $request->jabatan;
    	$foto = $request->foto;	

    	if($request->jumlah_rombongan == 0){
    		return redirect()->back()->with('message_success','Jumlah rombongan minimal 1')->withInput();
    	}

		if($id_acara && $jabatans && $nama && $nip && $golongan && $email && $hp && $inap && $jenis_kelamin){
			$jabatans = array_filter($jabatans, function($value) { return $value !== null && $value !== ''; });
			$nama = array_filter($nama, function($value) { return $value !== null && $value !== ''; });
			$nip = array_filter($nip, function($value) { return $value !== null && $value !== ''; });
			$golongan = array_filter($golongan, function($value) { return $value !== null && $value !== ''; });
			$jenis_kelamin = array_filter($jenis_kelamin, function($value) { return $value !== null && $value !== ''; });
			$email = array_filter($email, function($value) { return $value !== null && $value !== ''; });
			$hp = array_filter($hp, function($value) { return $value !== null && $value !== ''; });
			$inap = array_filter($inap, function($value) { return $value !== null && $value !== ''; });
			
			if(count($jabatans) < $request->jumlah_rombongan || 
				count($nama) < $request->jumlah_rombongan  || 
				count($nip) < $request->jumlah_rombongan  || 
				count($golongan) < $request->jumlah_rombongan  || 
				count($jenis_kelamin) < $request->jumlah_rombongan  || 
				count($email) < $request->jumlah_rombongan  || 
				count($hp) < $request->jumlah_rombongan  || 
				count($inap) < $request->jumlah_rombongan){
				return redirect()->back()->with('message_success','Data tidak lengkap, semua data wajib diisi!')->withInput();
			}
		} else {
			return redirect()->back()->with('message_success','Data tidak lengkap, semua data wajib diisi!')->withInput();
		}

    	$acara = Acara::where('id_acr',$id_acara)->first();

        $hitungpeserta = Peserta::where(function($query){
                 $query->where('status', 1);
                 $query->orWhere('status', 0);
             })->where(function($query){
                 $query->where('jenis', 'umum');
                 $query->orWhere('jenis', 'grup-umum');
             })->get();

        if($hitungpeserta->count() + $request->jumlah_rombongan < $acara->kuota_umum){
        	$flag = 0;
        	foreach ($jabatans as $key => $jabatan) {
        		$peserta = Peserta::where('id_acara',$id_acara)->whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $nip[$key + 1]))->first();
	            if(!$peserta){
	                $peserta = new Peserta;
	                $peserta->id_acara = $acara->id_acr;
	                $peserta->id_pegawai = 0;                
	                $peserta->jenis = 'grup-umum';                
	                $peserta->id_opd = 0; 
	                $peserta->nip = $nip[$key+1];
	                $peserta->nama = $nama[$key+1];
	                $peserta->asal_instansi = Auth::user()->detail->opd;
	                $peserta->golongan = $golongan[$key];;
	                $peserta->jenis_kelamin = $jenis_kelamin[$key];;
	                if($jabatan != 'Lain-lain'){
	                    $peserta->jabatan = $jabatan;
	                } else {
	                    $peserta->jabatan = $jabatan_text[$key+1];
	                }
	                $peserta->email = $email[$key+1];;
	                $peserta->no_hp = $hp[$key+1];;
	                $peserta->inap = $inap[$key+1];;
	                $peserta->status = 0;
	                $peserta->created_by = Auth::user()->id;
	                $peserta->username = $this->username($acara->code,$peserta->id_acara);
	                $peserta->qrcode = $this->qrcode($peserta->username);

	                if ($request->hasFile('foto')) {
	                    $path = $request->foto[$key+1]->store('public/foto');
	                    $peserta->foto = str_replace('public', 'storage', $path);
	                } else {
	                    $peserta->foto = 'user.png';
	                }

	                $peserta->password = generateRandomString(6);
	                while (true){
	                    $cek_peserta = Peserta::where('password',$peserta->password)->first();
	                    if ($cek_peserta){
	                        $peserta->password = generateRandomString(6);
	                    } else {
	                        break;
	                    }
	                }
	                
	                $peserta->save();
	                $now = Carbon::now();
	                $flag++;
	            }else {
	                $data['nips'][] = $nip[$key + 1];
	            }	
        	}

        	if($flag == count($jabatans)){
	        	$data['title'] = 'Pendaftaran Sukses';
	            $data['message'] = 'Peserta akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';
	            return view('bimtek.grup.umum.info',$data);
	        } else {
	        	if($flag == 0){
	        		$data['title'] = 'Peserta sudah terdaftar';
	            	$data['message'] = 'Peserta telah terdaftar sebagai peserta, Informasi lebih lanjut hubungi admin. Terima Kasih.';
	            	return view('bimtek.grup.umum.info',$data);
	        		
	        	} else {
	        		$data['title'] = 'Pendaftaran Sukses';
	        		$data['message'] = 'NIP ';
	        		foreach ($data['nips'] as $key => $nip) {
	        			$data['message'] = $data['message'] . $nip;
	        		}
	        		$data['message'] .= ' Sudah terdaftar sebagai peserta. ';
	            	$data['message'] .= 'Peserta akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';	            	
	            return view('bimtek.grup.umum.info',$data);
	        	}
	        }
        } else {
            return redirect()->route('bimtek')->with('kuota_habis', true);
        }
    }

    protected function username($code,$id_acara){
        $peserta = Peserta::where('id_acara',$id_acara)->orderBy('username','DESC')->first();
        $last_username = $peserta->username;
        $last_number = str_replace($code, '', $last_username);
        
        $jumlah_peserta = (int)$last_number + 1;
        $jumlah_peserta = sprintf('%03d', $jumlah_peserta);
        return $code . $jumlah_peserta;
    }

    protected function qrcode($jumlah_peserta){
        $file = Hash::make($jumlah_peserta);
        $file = preg_replace('~[^a-zA-Z0-9]+~', '', $file);
        $pf = '/qrcode/'.$file.'.png';
        $db_pf = 'storage/qrcode/'.$file.'.png';
        $ifp = fopen(storage_path('app/public') .$pf, 'wb' ); 
        fwrite( $ifp, base64_decode(DNS2D::getBarcodePNG($jumlah_peserta, "QRCODE",64,64)));
        fclose( $ifp ); 
        return $db_pf;
    }
}