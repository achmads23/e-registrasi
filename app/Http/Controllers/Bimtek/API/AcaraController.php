<?php

namespace App\Http\Controllers\Bimtek\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use Carbon\Carbon;
use Hash;

use DNS2D;
use PDF;

class AcaraController extends Controller
{
    public function list(Request $request){
        $now = Carbon::now();
        $acaras = Acara::whereDate('tampil_awal','<=',$now->format('Y-m-d'))->whereDate('tampil_akhir','>=',$now->format('Y-m-d'))->get();
        return response()->json(['success' => true, 'data' => $acaras], 200);
    }

    public function createUmum(Request $request){
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();

        $acara = Acara::where('code',$request->token)->first();

        if($acara->peserta->count() < $acara->kuota_umum){

            if($acara){
                $peserta = Peserta::where('id_acara',$acara->id_acr)->whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $request->nip))->first();

                if(!$peserta){
                    $peserta = new Peserta;
                    $peserta->nip = $request->nip;
                    $peserta->id_acara = $acara->id_acr;
                    $peserta->id_pegawai = 0;
                    $peserta->id_opd = 0;
                    
                    if($request->jabatan != 'Lain-lain'){
                        $peserta->jabatan = $request->jabatan;
                    } else {
                        $peserta->jabatan = $request->jabatanText;
                    }

                    $peserta->email = $request->email;
                    $peserta->no_hp = $request->nohp;
                    $peserta->jenis_kelamin = $request->jenis_kelamin;

                    if($request->id_kartu_identitas){
                        $peserta->foto = $request->id_kartu_identitas;
                    } else {
                        $peserta->foto = 'user.png';
                    }
                    
                    $peserta->inap = $request->inap;
                    $peserta->username = $this->username($request->token,$peserta->id_acara);
                    $peserta->qrcode = $this->qrcode($peserta->username);
                    
                    $peserta->password = generateRandomString(6);
                    while (true){
                        $cek_peserta = Peserta::where('password',$peserta->password)->first();
                        if ($cek_peserta){
                            $peserta->password = generateRandomString(6);
                        } else {
                            break;
                        }
                    }

                    $peserta->created_by = $user->id;
                    $peserta->jenis = $request->jenis;
                    
                    $peserta->status = 0;
                    $peserta->nama = $request->nama;
                    $peserta->asal_instansi = $request->asal_instansi;
                    $peserta->golongan = $request->golongan;

                    $now = Carbon::now();

                    $peserta->save();
                }

                return response()->json(['success' => true, 'data' => $peserta], 200);    
            } else {
                return response()->json(['success' => false, 'error' => 'Data acara tidak ditemukan'], 200);    
            }
        } else {
            return response()->json(['success' => false, 'error' => 'Maaf, kuota sudah habis'], 200);
        }
    }
    public function createUndangan(Request $request){
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();

        $acara = Acara::where('code',$request->token)->first();

        if($acara->peserta->count() < $acara->kuota_undangan){

            $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $request->nip))->with('opd')->first();

            if($acara && $pegawai){
                $peserta = Peserta::where('id_acara',$acara->id_acr)->where('id_pegawai',$pegawai->id)->first();

                if(!$peserta){
                    $peserta = new Peserta;
                    $peserta->nip = $request->nip;
                    $peserta->id_acara = $acara->id_acr;
                    $peserta->id_pegawai = $pegawai->id;
                    $peserta->id_opd = $pegawai->opd->id;
                    
                    if($request->jabatan != 'Lain-lain'){
                        $peserta->jabatan = $request->jabatan;
                    } else {
                        $peserta->jabatan = $request->jabatanText;
                    }

                    $peserta->email = $request->email;
                    $peserta->no_hp = $request->nohp;
                    $peserta->jenis_kelamin = $request->jenis_kelamin;

                    if($request->id_kartu_identitas){
                        $peserta->foto = $request->id_kartu_identitas;
                    } else {
                        $peserta->foto = 'user.png';
                    }
                    
                    $peserta->inap = $request->inap;
                    $peserta->username = $this->username($request->token,$peserta->id_acara);
                    $peserta->qrcode = $this->qrcode($peserta->username);
                    
                    //check password exist
                    $peserta->password = generateRandomString(6);
                    while (true){
                        $cek_peserta = Peserta::where('password',$peserta->password)->first();
                        if ($cek_peserta){
                            $peserta->password = generateRandomString(6);
                        } else {
                            break;
                        }
                    }

                    $peserta->created_by = $user->id;
                    $peserta->jenis = $request->jenis;

                    if($request->opdBaru){
                        $peserta->file = $request->id_sk;
                        $peserta->id_opd = $request->opdBaru; 

                        $peserta->status = 0;
                        $peserta->nama = $pegawai->nama;
                        $peserta->asal_instansi = $pegawai->opd->opd;
                        $peserta->golongan = $pegawai->gol;
                    } else {
                        $peserta->status = 1;
                        $peserta->nama = $request->nama;
                        $peserta->asal_instansi = $request->asal_instansi;
                        $peserta->golongan = $request->golongan;
                    }

                    $now = Carbon::now();

                    $peserta->save();
                }

                return response()->json(['success' => true, 'data' => $peserta], 200);    
            } else {
                return response()->json(['success' => false, 'error' => 'Data pegawai tidak ditemukan'], 200);    
            }
        } else {
            return response()->json(['success' => false, 'error' => 'Maaf, kuota sudah habis'], 200);
        }
    }

    public function cek_nip(Request $request){
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();   
        $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $request->nip))->with('opd')->first();

        $acara = Acara::where('code',$request->input('code'))->first();

        $peserta = Peserta::where('id_pegawai', $pegawai->id)->where('id_acara', $acara->id_acr)->first();

        if(!$peserta) {
            if($request->jenis == 'undangan'){
                if($acara->id_opds){
                    $id_opds = explode(';', $acara->id_opds);
                    array_pop($id_opds);
                    $flag = 0;
                    foreach ($id_opds as $key => $value) {
                        if($value == $pegawai->opd->id){
                            $flag = 1;
                        }
                    }

                    if($flag == 1 || count($id_opds) == 0){
                        $data['pegawai'] = $pegawai;
                        $data['is_peserta'] = false;

                        if($pegawai){
                            return response()->json(['success' => true, 'data' => $data], 200);
                        } else {
                            return response()->json(['success' => false, 'data' => false], 200);
                        }
                    } else {
                        $data['opd'] = $pegawai->opd->opd;
                        return response()->json(['success' => false, 'data' => $data], 200);
                    }
                } else {
                    $data['pegawai'] = $pegawai;
                    return response()->json(['success' => true, 'data' => $data], 200);
                }
            } else {
                $data['pegawai'] = $pegawai;
                return response()->json(['success' => true, 'data' => $data], 200);
            }
        } else {
            $data['peserta'] = $peserta;
            $data['is_peserta'] = true;
            return response()->json(['success' => false, 'data' => $data], 200);
        }
    }
	public function cek_code(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $code = Acara::where('code',$request->input('code'))->get();
        if(count($code) > 0){
        	$now = Carbon::now();
            $tanggal_awal = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_awal)->setTime(0, 0, 0);
            $tanggal_akhir = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_akhir)->setTime(23, 59, 59);;
            if($now >= $tanggal_awal && $now <= $tanggal_akhir){
                $data['acara'] = $code[0];
                $data['acara']->opds = $data['acara']->opds();
                return response()->json(['success' => true],200);
            } else if($now < $tanggal_awal){
            	return response()->json(['success' => false,'data' => 'Pendaftaran belum dibuka.'], 200);
            } else if($now > $tanggal_akhir){
            	return response()->json(['success' => false,'data' => 'Pendaftaran sudah ditutup'], 200);
            }
        } else {
            return response()->json(['success' => false,'data' => 'Kode login tidak ditemukan'], 200);
        }
    }

    public function get_by_code(Request $request,$code)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $acara = Acara::where('code',$code)->first();
        if($acara){
            return response()->json(['success' => true, 'data' => ['acara' => $acara]], 200);
        } else {
            return response()->json(['success' => false], 200);
        }
    }

    public function show(Request $request, $id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $acara = Acara::where('id_acr',$id)->first();
        if($acara){
            return response()->json(['success' => true, 'data' => ['acara' => $acara]], 200);
        } else {
            return response()->json(['success' => false], 200);
        }
    }

    protected function username($code,$id_acara){
        $jumlah_peserta = Peserta::where('id_acara',$id_acara)->count();
        
        $jumlah_peserta = $jumlah_peserta + 1;
        $jumlah_peserta = sprintf('%03d', $jumlah_peserta);

        return $code . $jumlah_peserta;
    }

    protected function qrcode($jumlah_peserta){
        $file = Hash::make($jumlah_peserta);
        $file = preg_replace('~[^a-zA-Z0-9]+~', '', $file);
        $pf = '/qrcode/'.$file.'.png';
        $db_pf = 'storage/qrcode/'.$file.'.png';
        $ifp = fopen(storage_path('app/public') .$pf, 'wb' ); 
        fwrite( $ifp, base64_decode(DNS2D::getBarcodePNG($jumlah_peserta, "QRCODE",64,64)));
        fclose( $ifp ); 
        return $db_pf;
    }
}