<?php

namespace App\Http\Controllers\Bimtek\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Peserta;

use App\Acara;

class UmumController extends Controller
{
    public function index(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $pesertas = Peserta::with(['user','pegawai','acara'])->where('jenis','umum')->where('created_by',$user->id)->get();
        $data['success'] = true;
        $data['data']['pesertas'] = $pesertas;
        return response()->json($data,200);
    }

    public function detail(Request $request, $id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $peserta = Peserta::with(['user','pegawai','acara'])->where('jenis','umum')->where('created_by',$user->id)->where('id',$id)->first();
        $data['success'] = true;
        $data['data']['peserta'] = $peserta;
        return response()->json($data,200);
    }

    public function find(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $pesertas = Peserta::with(['user','pegawai','acara'])->whereHas('acara', function ($query) use ($request) {
                $query->where('nama_acr','LIKE','%'.$request->text.'%');
            })->where('created_by',$user->id)->where('jenis','umum')->get();
        $data['success'] = true;
        $data['data']['pesertas'] = $pesertas;
        return response()->json($data,200);
    }
}
