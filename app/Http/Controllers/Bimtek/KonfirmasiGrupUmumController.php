<?php

namespace App\Http\Controllers\Bimtek;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
use Mail;
use DB;
use URL;
class KonfirmasiGrupUmumController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pesertas'] = Peserta::select('*',DB::raw('count(*) as jumlah'))->with('user')->where('status',0)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->groupBy('id_acara','created_by')->get();
        $data['pesertas_ditolak'] = Peserta::select('*',DB::raw('count(*) as jumlah'))->with('user')->where('status',-1)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->groupBy('id_acara','created_by')->get();

        $data['pesertas_diterima'] = Peserta::select('*',DB::raw('count(*) as jumlah'))->with('user')->where('status',1)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->groupBy('id_acara','created_by')->get();
        return view('bimtek.grup.umum.konfirmasi',$data);
    }

    public function show($id,$id_acara)
    {
        $now = Carbon::now();
        $data['acaras'] = Acara::whereDate('tampil_awal','<=',$now)->whereDate('tampil_akhir','>=',$now)->get();
        $data['peserta'] = Peserta::with('user','acara')->where('status',0)->where('created_by',$id)->where('id_acara',$id_acara)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->get();        
        if(count($data['peserta']) > 0){
            return view('bimtek.grup.umum.konfirmasi-show',$data);
        } else {
            return redirect()->route('admin.bimtek.grup-umum');
        }
    }

    public function detail($id,$id_acara)
    {
        $now = Carbon::now();
        $data['acaras'] = Acara::whereDate('tampil_awal','<=',$now)->whereDate('tampil_akhir','>=',$now)->get();
        $data['peserta'] = Peserta::with('user','acara')->where('created_by',$id)->where('id_acara',$id_acara)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->get();

        return view('bimtek.grup.umum.konfirmasi-detail',$data);
    }

    public function konfirmasi(Request $request, $id,$id_acara)
    {
        $result = $request->result;
        if($result == 1){
            $pesertas = Peserta::with('user','acara')->where('status',0)->where('created_by',$id)->where('id_acara',$id_acara)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->get();

            if(count($pesertas) > 0){
                foreach ($pesertas as $key => $peserta) {
                    $peserta->status = 1;
                    $peserta->save();

                    $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Peserta.";

                    $name = $peserta->nama;
                    $email = $peserta->email;
                    if($email){
                        Mail::send('email.selamat-peserta', ['name' => $peserta->nama,'jenis_kelamin' => $peserta->jenis_kelamin, 'nama_acr' => $peserta->acara->nama_acr,'tgl_acr' => $peserta->acara->tgl_acr,'tempat_acr' => $peserta->acara->tempat_acr, 'username' => $peserta->username,'password' => $peserta->password],
                            function($mail) use ($email, $name, $subject){
                                $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                                $mail->to($email, $name);
                                $mail->subject($subject);
                        });
                    }
                }

                $now = Carbon::createFromFormat('d-m-Y', $request->tgl_surat);
                $acara = Acara::where('id_acr',$pesertas[0]->acara->id_acr)->first();
                $data['acara'] = $acara;
                $data['pesertas'] = $pesertas;
                $data['date'] = $now->day;
                $data['year'] = $now->year;
                $month = $now->month;
                if($month == 1)
                $month = 'Januari';
                else if($month == 2)
                $month = 'Februari';
                else if($month == 3)
                $month = 'Maret';
                else if($month == 4)
                $month = 'April';
                else if($month == 5)
                $month = 'Mei';
                else if($month == 6)
                $month = 'Juni';
                else if($month == 7)
                $month = 'Juli';
                else if($month == 8)
                $month = 'Agustus';
                else if($month == 9)
                $month = 'September';
                else if($month == 10)
                $month = 'Oktober';
                else if($month == 11)
                $month = 'November';
                else if($month == 12)
                $month = 'Desember';
                $data['month'] = $month;
                $data['acara']->opds = $data['acara']->opds();

                $tgl_acr = Carbon::createFromFormat('Y-m-d', $acara->tgl_acr);
                $day = $tgl_acr->dayOfWeek;
                if($day == 1) $day = 'Senin'; 
                elseif($day == 2) $day = 'Selasa'; 
                elseif($day == 3) $day = 'Rabu'; 
                elseif($day == 4) $day = 'Kamis'; 
                elseif($day == 5) $day = 'Jumat'; 
                elseif($day == 6) $day = 'Sabtu'; 
                elseif($day == 7) $day = 'Minggu'; 
                $data['day'] = $day;

                $month = $tgl_acr->month;
                if($month == 1)
                $month = 'Januari';
                else if($month == 2)
                $month = 'Februari';
                else if($month == 3)
                $month = 'Maret';
                else if($month == 4)
                $month = 'April';
                else if($month == 5)
                $month = 'Mei';
                else if($month == 6)
                $month = 'Juni';
                else if($month == 7)
                $month = 'Juli';
                else if($month == 8)
                $month = 'Agustus';
                else if($month == 9)
                $month = 'September';
                else if($month == 10)
                $month = 'Oktober';
                else if($month == 11)
                $month = 'November';
                else if($month == 12)
                $month = 'Desember';
                $data['bulan'] = $month;
                $data['tgl'] = $tgl_acr->day;
                $data['tahun'] = $tgl_acr->year;
                $data['nomor_surat'] = $request->nomor_surat;
                $data['ttd'] = $request->ttd;
                $pdf = PDF::loadView('pdf.balasan-grup', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
                $filename = 'surat-balasan-' . str_replace(' ', '-', $acara->nama_acr) . '.pdf';
                
                \Storage::put('public/'.$filename, $pdf->output());

                $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Peserta.";

                $name = $pesertas[0]->user->name;
                $email = $pesertas[0]->user->email;
                if($email){
                    Mail::send('email.konfirmasi-grup-terima', ['name' => $pesertas[0]->user->name, 'nama_acr' => $pesertas[0]->acara->nama_acr,'tgl_acr' => $pesertas[0]->acara->tgl_acr,'tempat_acr' => $pesertas[0]->acara->tempat_acr],
                        function($mail) use ($email, $name, $subject,$filename){
                            $mail->attach(URL::to('storage/'.$filename));
                            $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                            $mail->to($email, $name);
                            $mail->subject($subject);
                    });
                }
            }
            return redirect('/admin/konfirmasi-grup-umum')->with(['setuju_success' => true, 'filename' => 'storage/' . $filename]);
            
        } else {
            $pesertas = Peserta::with('user','acara')->where('status',0)->where('created_by',$id)->where(function($query){
                 $query->where('jenis', 'grup-umum');
             })->get();

            if(count($pesertas) > 0){
                foreach ($pesertas as $key => $peserta) {
                    $peserta->status = -1;
                    $peserta->save();
                }

                $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Konfirmasi Pendaftaran Grup";
                $name = $pesertas[0]->user->nama;
                $email = $pesertas[0]->user->email;
                if($email){
                    Mail::send('email.konfirmasi-grup-tolak', ['name' => $pesertas[0]->nama, 'nama_acr' => $pesertas[0]->acara->nama_acr],
                        function($mail) use ($email, $name, $subject){
                            $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                            $mail->to($email, $name);
                            $mail->subject($subject);
                    });
                }
            }

            return redirect('/admin/konfirmasi-grup-umum')->with('tolak_success',true); 
        }
    }
}
