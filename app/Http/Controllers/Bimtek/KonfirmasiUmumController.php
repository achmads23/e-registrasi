<?php

namespace App\Http\Controllers\Bimtek;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
use Mail;
use URL;
class KonfirmasiUmumController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pesertas'] = Peserta::where('status',0)->where('jenis','umum')->get();
        $data['pesertas_ditolak'] = Peserta::where('status',-1)->get();
        return view('bimtek.umum.konfirmasi',$data);
    }

    public function show($id)
    {
        $now = Carbon::now();
        $data['acaras'] = Acara::whereDate('tampil_awal','<=',$now->format('Y-m-d'))->whereDate('tampil_akhir','>=',$now->format('Y-m-d'))->get();
        $data['peserta'] = Peserta::findOrFail($id);
        return view('bimtek.umum.konfirmasi-show',$data);
    }

    public function konfirmasi(Request $request, $id)
    {
        $result = $request->result;
        $peserta = Peserta::with('acara')->findOrFail($id);
        if($result == 1){

            $now = Carbon::createFromFormat('d-m-Y', $request->tgl_surat);
            $acara = Acara::with(['peserta' => function($query) use ($id) {
                        $query->where('id', $id);
                    }])->where('id_acr',$peserta->acara->id_acr)->first();
            $data['acara'] = $acara;
            $data['peserta'] = $peserta;
            $data['date'] = $now->day;
            $data['year'] = $now->year;
            $month = $now->month;
            if($month == 1)
            $month = 'Januari';
            else if($month == 2)
            $month = 'Februari';
            else if($month == 3)
            $month = 'Maret';
            else if($month == 4)
            $month = 'April';
            else if($month == 5)
            $month = 'Mei';
            else if($month == 6)
            $month = 'Juni';
            else if($month == 7)
            $month = 'Juli';
            else if($month == 8)
            $month = 'Agustus';
            else if($month == 9)
            $month = 'September';
            else if($month == 10)
            $month = 'Oktober';
            else if($month == 11)
            $month = 'November';
            else if($month == 12)
            $month = 'Desember';
            $data['month'] = $month;
            $data['acara']->opds = $data['acara']->opds();

            $tgl_acr = Carbon::createFromFormat('Y-m-d', $acara->tgl_acr);
            $day = $tgl_acr->dayOfWeek;
            if($day == 1) $day = 'Senin'; 
            elseif($day == 2) $day = 'Selasa'; 
            elseif($day == 3) $day = 'Rabu'; 
            elseif($day == 4) $day = 'Kamis'; 
            elseif($day == 5) $day = 'Jumat'; 
            elseif($day == 6) $day = 'Sabtu'; 
            elseif($day == 7) $day = 'Minggu'; 
            $data['day'] = $day;

            $month = $tgl_acr->month;
            if($month == 1)
            $month = 'Januari';
            else if($month == 2)
            $month = 'Februari';
            else if($month == 3)
            $month = 'Maret';
            else if($month == 4)
            $month = 'April';
            else if($month == 5)
            $month = 'Mei';
            else if($month == 6)
            $month = 'Juni';
            else if($month == 7)
            $month = 'Juli';
            else if($month == 8)
            $month = 'Agustus';
            else if($month == 9)
            $month = 'September';
            else if($month == 10)
            $month = 'Oktober';
            else if($month == 11)
            $month = 'November';
            else if($month == 12)
            $month = 'Desember';
            $data['bulan'] = $month;
            $data['tgl'] = $tgl_acr->day;
            $data['tahun'] = $tgl_acr->year;
            $data['nomor_surat'] = $request->nomor_surat;
            $data['ttd'] = $request->ttd;
            $pdf = PDF::loadView('pdf.balasan-umum', $data)->setPaper('f4','portrait');
            $filename = 'balasan-umum-' . $peserta->username . '.pdf';
            \Storage::put('public/'.$filename, $pdf->output());

            $peserta->status = 1;
            if($request->acara){
                $peserta->id_acara  = $request->ganti_acara;
            }
            $peserta->save();

            $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Konfirmasi Pendaftaran Peserta.";

            $name = $peserta->nama;
            $email = $peserta->email;
            if($email){
                Mail::send('email.selamat-peserta', ['name' => $peserta->nama,'jenis_kelamin' => $peserta->jenis_kelamin, 'nama_acr' => $peserta->acara->nama_acr,'tgl_acr' => $peserta->acara->tgl_acr,'tempat_acr' => $peserta->acara->tempat_acr, 'username' => $peserta->username,'password' => $peserta->password],
                    function($mail) use ($email, $name, $subject,$filename){
                        $mail->attach(URL::to('storage/'.$filename));
                        $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                        $mail->to($email, $name);
                        $mail->subject($subject);
                });
            }

            return redirect('/admin/konfirmasi-umum')->with(['setuju_success' => true, 'filename' => 'storage/' . $filename]);
        } else {
            $peserta->status = -1;
            $peserta->save();

            $subject = "[Bimbingan Teknis UPT PPK - BPKAD Provinsi Jawa Timur] Konfirmasi Pendaftaran Peserta";
            $name = $peserta->nama;
            $email = $peserta->email;
            if($email){
                Mail::send('email.konfirmasi-umum-tolak', ['name' => $peserta->nama, 'nama_acr' => $peserta->acara->nama_acr],
                    function($mail) use ($email, $name, $subject){
                        $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                        $mail->to($email, $name);
                        $mail->subject($subject);
                });
            }

            return redirect('/admin/konfirmasi-umum')->with('tolak_success',true);
        }
    }
}
