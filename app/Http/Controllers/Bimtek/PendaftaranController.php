<?php

namespace App\Http\Controllers\Bimtek;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Acara;
use App\Pegawai;
use App\Peserta;
use App\DataBimtek;
use App\Opd;
use App\Ols_pengguna;
use Carbon\Carbon;
use Hash;
use DNS2D;
use PDF;
class PendaftaranController extends Controller
{
    /**
     * Handle a login request to the application.
     */
    public function post_index(Request $request)
    {
        $now = Carbon::now();
        $code = Acara::where('code',$request->input('token'))->get();
        if(count($code) > 0){
            $tanggal_awal = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_awal)->setTime(0, 0, 0);
            $tanggal_akhir = Carbon::createFromFormat('Y-m-d H:i:s', $code[0]->tanggal_akhir)->setTime(23, 59, 59);;
            if($now >= $tanggal_awal && $now <= $tanggal_akhir){
                $data['acara'] = $code[0];
                $data['acara']->opds = $data['acara']->opds();
                return view('pendaftaran.registrasi', $data);
            } else if($now < $tanggal_awal){
                return redirect()->back()->with('token_error', 'Pendaftaran Belum Dibuka.')->withInput($request->input());    
            } else if($now > $tanggal_akhir){
                return redirect()->back()->with('token_error', 'Pendaftaran Sudah Ditutup.')->withInput($request->input());    
            }
        } else {
            return redirect()->back()->with('token_error', 'Token Tidak Ditemukan.')->withInput($request->input());
        }
    }

    public function post_register(Request $request)
    {
        $nip = $request->nip;
        $code = $request->code;
        $jabatan = $request->jabatan;
        $email = $request->email;
        $hp = $request->hp;
        $nama = $request->nama;
        $jenis_kelamin = $request->jenis_kelamin;
        $asal_instansi = $request->asal_instansi;
        $golongan = $request->golongan;

        $acara = Acara::where('code',$request->input('code'))->first();
        $hitungpeserta = Peserta::where(function($query){
                 $query->where('status', 1);
                 $query->orWhere('status', 0);
             })->where(function($query){
                 $query->where('jenis', 'undangan');
                 $query->orWhere('jenis', 'grup-undangan');
             })->get();
        if($hitungpeserta->count() < $acara->kuota_undangan){
            $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", str_replace(' ', '', $nip))->with('opd')->first();
            if($acara){
                if($pegawai){
                    $peserta = Peserta::where('id_acara',$acara->id_acr)->where('id_pegawai',$pegawai->id)->first();
                    if(!$peserta){
                        $foto = $request->input('foto');                            

                        $peserta = new Peserta;
                        $peserta->id_acara = $acara->id_acr;
                        $peserta->id_pegawai = $pegawai->id;                
                        $peserta->id_opd = $pegawai->opd->id; 
                        $peserta->nama = $nama;
                        $peserta->jenis_kelamin = $jenis_kelamin;
                        $peserta->asal_instansi = $asal_instansi;
                        $peserta->golongan = $golongan;
                        if($jabatan != 'Lain-lain'){
                            $peserta->jabatan = $jabatan;
                        } else {
                            $peserta->jabatan = $request->jabatan_text;
                        }
                        $peserta->email = $email;
                        $peserta->no_hp = $hp;
                        $peserta->status = 1;
                        $peserta->inap = $request->inap;
                        $peserta->username = $this->username($code,$peserta->id_acara);
                        $peserta->qrcode = $this->qrcode($peserta->username);

                        if ($request->hasFile('foto')) {
                            $path = $request->foto->store('public/foto');
                            $peserta->foto = str_replace('public', 'storage', $path);
                        } else {
                            $peserta->foto = 'user.png';
                        }

                        //check password exist
                        $peserta->password = generateRandomString(6);
                        while (true){
                            $cek_peserta = Peserta::where('password',$peserta->password)->first();
                            if ($cek_peserta){
                                $peserta->password = generateRandomString(6);
                            } else {
                                break;
                            }
                        }
                        
                        $peserta->save();
                    }
                    $data['peserta'] = $peserta;
                    return view('pendaftaran.registrasi-sukses', $data);
                } else {
                    $peserta = Peserta::where('id_acara',$acara->id_acr)->where('nip',$nip)->first();
                    if(!$peserta){
                        $foto = $request->input('foto');                            
                        $peserta = new Peserta;
                        $peserta->id_acara = $acara->id_acr;
                        $peserta->id_pegawai = 0;                
                        $peserta->jenis = 'umum';  
                        $peserta->id_opd = 0; 
                        $peserta->nip = $nip;
                        $peserta->nama = $nama;
                        $peserta->jenis_kelamin = $jenis_kelamin;
                        $peserta->asal_instansi = $asal_instansi;
                        $peserta->golongan = $golongan;
                        if($jabatan != 'Lain-lain'){
                            $peserta->jabatan = $jabatan;
                        } else {
                            $peserta->jabatan = $request->jabatan_text;
                        }
                        $peserta->email = $email;
                        $peserta->no_hp = $hp;
                        $peserta->status = 1;
                        $peserta->inap = $request->inap;
                        $peserta->username = $this->username($code,$peserta->id_acara);
                        $peserta->qrcode = $this->qrcode($peserta->username);

                        if ($request->hasFile('foto')) {
                            $path = $request->foto->store('public/foto');
                            $peserta->foto = str_replace('public', 'storage', $path);
                        } else {
                            $peserta->foto = 'user.png';
                        }

                        //check password exist
                        $peserta->password = generateRandomString(6);
                        while (true){
                            $cek_peserta = Peserta::where('password',$peserta->password)->first();
                            if ($cek_peserta){
                                $peserta->password = generateRandomString(6);
                            } else {
                                break;
                            }
                        }
                        
                        $peserta->save();
                    }
                    $data['peserta'] = $peserta;
                    return view('pendaftaran.registrasi-sukses', $data);
                }
            } else {
                return redirect()->route('bimtek.undangan')->with('acara_tidak_ditemukan', true); 
            }
        } else {
            return redirect()->route('bimtek.undangan')->with('kuota_habis', true);
        }
    }

    public function peserta(Request $request,$id)
    {
        $peserta = Peserta::where('id',$id)->first();
        $data['peserta'] = $peserta;
        return view('pendaftaran.registrasi-sukses', $data);
    }

    public function download(Request $request)
    {
        $peserta = Peserta::where('id',$request->input('id'))->first();
        $data['peserta'] = $peserta;
        $pdf = PDF::loadView('pdf.bukti-peserta', $data)->setPaper('f4','portrait');
        return $pdf->download('bukti-peserta.pdf');
    }

    public function bimtek_checkin(Request $request){
        $token = $request->input('token');
        $peserta = Peserta::where('token_check_in',$token)->first();
        $data['peserta'] = $peserta;
        return view('bimtek-scan-sukses', $data);
    }

    public function bimtek_checkin_token(Request $request){
        $peserta = Peserta::where('password', $request->input('password'))->first();
        if($peserta) {
            $token = Hash::make($peserta->username . $peserta->password);
            $peserta->token_check_in = $token;
            $peserta->check_in = Carbon::now();
            $peserta->save();
            $data['peserta'] = $peserta;
            return view('bimtek-scan-sukses', $data);
        } else {
            return redirect()->back()->with('token_login', 'Token username dan password salah')->withInput($request->input());
        }
    }

    public function konfirmasi_perubahan_opd(Request $request){
        $pegawai = false;
        $acara = false;
        if($request->input('nip')){
            $nip = $request->input('nip');
            $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", $request->input('nip'))->with('opd')->first();
        }
        if($request->input('code')){
            $acara = Acara::where('code',$request->input('code'))->first();
        }
        if($acara && $pegawai){
            $data['pegawai'] = $pegawai;
            $data['opds'] = Opd::get();
            $data['acara'] = $acara;
            return view('pendaftaran.konfirmasi-perubahan-opd', $data);
        } 

        $data['title'] = 'Ooooooops!';
        $data['message'] = 'Data Tidak Ditemukan';
        return view('pendaftaran.info',$data);
    }

    public function post_konfirmasi_perubahan_opd(Request $request){
        $acara = Acara::where('code',$request->input('code'))->first();
        $pegawai = Pegawai::whereRaw("REPLACE(`nip`, ' ', '') = ? ", $request->input('nip'))->with('opd')->first();

        $peserta = Peserta::where('id_pegawai', $pegawai->id)->where('id_acara', $acara->id_acr)->first();
        if(!$peserta){
            $peserta = new Peserta;
            $peserta->id_acara = $acara->id_acr;
            $peserta->id_pegawai = $pegawai->id;                
            $peserta->id_opd = $request->opd; 
            $peserta->nama = $pegawai->nama;
            $peserta->asal_instansi = $pegawai->opd->opd;
            $peserta->golongan = $pegawai->gol;
            if($request->jabatan != 'Lain-lain'){
                $peserta->jabatan = $request->jabatan;
            } else {
                $peserta->jabatan = $request->jabatan_text;
            }
            $peserta->email = $request->email;
            $peserta->no_hp = $request->hp;
            $peserta->status = 0;
            $peserta->username = $this->username($request->input('code'),$acara->id_acr);
            $peserta->qrcode = $this->qrcode($peserta->username);

            if ($request->hasFile('foto')) {
                $path = $request->foto->store('public/foto');
                $peserta->foto = str_replace('public', 'storage', $path);
            } else {
                $peserta->foto = 'user.png';
            }

            if ($request->hasFile('filesk')) {
                $path = $request->filesk->store('public/file');
                $peserta->file = str_replace('public', 'storage', $path);
            }

            $peserta->password = generateRandomString(6);
            while (true){
                $cek_peserta = Peserta::where('password',$peserta->password)->first();
                if ($cek_peserta){
                    $peserta->password = generateRandomString(6);
                } else {
                    break;
                }
            }
            $peserta->inap = $request->inap;
            
            $peserta->save();
            if($peserta->save()){
                $data['title'] = 'Konfirmasi Perubahan OPD Sukses';
                $data['message'] = 'Data baru anda akan diverifikasi terlebih dahulu, Informasi lebih lanjut akan diberitahukan secepatnya. Terima Kasih.';
                return view('pendaftaran.info',$data);
            } else {
                echo 'Ooops';
            }
        } else {
            if(!$peserta->status){
                $data['title'] = 'NIP sudah terdaftar';
                $data['message'] = 'NIP anda sudah terdaftar, dan masih dalam verifikasi. Harap menunggu sampai proses verifikasi selesai. Terima Kasih.';
                return view('pendaftaran.info',$data);
            } else {
                $data['title'] = 'NIP sudah terdaftar';
                $data['message'] = 'NIP anda sudah terdaftar sebagai peserta. Terima Kasih.';
                return view('pendaftaran.info',$data);
            }
        }
    }


    protected function username($code,$id_acara){
        $jumlah_peserta = Peserta::where('id_acara',$id_acara)->count();
        
        $jumlah_peserta = $jumlah_peserta + 1;
        $jumlah_peserta = sprintf('%03d', $jumlah_peserta);

        return $code . $jumlah_peserta;
    }

    protected function qrcode($jumlah_peserta){
        $file = Hash::make($jumlah_peserta);
        $file = preg_replace('~[^a-zA-Z0-9]+~', '', $file);
        $pf = '/qrcode/'.$file.'.png';
        $db_pf = 'storage/qrcode/'.$file.'.png';
        $ifp = fopen(storage_path('app/public') .$pf, 'wb' ); 
        fwrite( $ifp, base64_decode(DNS2D::getBarcodePNG($jumlah_peserta, "QRCODE",64,64)));
        fclose( $ifp ); 
        return $db_pf;
    }
}
