<?php

namespace App\Http\Controllers\eClinic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\eClinic\User_verification;

class IndexController extends Controller
{
    public function verifyUser($verification_code)
    {
        $check = User_verification::where('token',$verification_code)->first();

        if(!is_null($check)){
            $user = User::find($check->user_id);
            if($user->is_verified == 1){
                return 'Akun telah di aktivasi..';
            }

            $user->is_verified = 1;
            $user->save();
            User_verification::where('token',$verification_code)->delete();
            return 'Aktivasi akun berhasil.';
        }
        
        return 'Aktivasi akun berhasil.';
    }
}
