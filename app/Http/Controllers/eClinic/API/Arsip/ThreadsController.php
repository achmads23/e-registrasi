<?php

namespace App\Http\Controllers\eClinic\API\Arsip;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Threads;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Responders;
use App\User;
use Carbon\Carbon;

use Validator;

class ThreadsController extends Controller
{
	protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

	public function index_by_category(Request $request, $id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $threads = Threads::with('user')->where('status',2)->where('publish',1)->where('category_id', $id)->get();
        foreach ($threads as $key => $thread) {
            $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
        }
	    $data['success'] = true;
        $data['data']['threads'] = $threads;
    	return response()->json($data,200);
    }

    public function find(Request $request)
    {
        $threads = Threads::with('user')->where('status',2)->where('publish',1);
        $threads = $threads->where('judul','LIKE','%'.$request->text.'%');
        $threads = $threads->get();
        foreach ($threads as $key => $thread) {
            $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
        }
        $data['success'] = true;
        $data['data']['threads'] = $threads;
        return response()->json($data,200);
    }

    public function show(Request $request,$id)
    {
    	$thread = Threads::where('id',$id)->with(['user'])->where('status',2);
        $thread = $thread->first();
    	if($thread) {
            $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            if($thread->answered_at){
                $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
            }
            if($thread->updated_at)
                $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
			$data['success'] = true;
	        $data['data']['thread'] = $thread;
            $data['data']['thread']['categories'] = $this->parents_category($thread->category_id);
            $data['data']['thread']['question_files'] = $thread->question_files();
            $data['data']['thread']['answer_files'] = $thread->answer_files();
	    	return response()->json($data,200);
    	} else {
    		$data['success'] = false;
	        $data['error'] = 'Data tidak ditemukan';
	    	return response()->json($data,200);
    	}
    }
}