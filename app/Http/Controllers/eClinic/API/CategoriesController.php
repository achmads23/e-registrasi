<?php

namespace App\Http\Controllers\eClinic\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Categories;

class CategoriesController extends Controller
{
    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

	protected function get_childs($category){
		$categories = Categories::where('parent_id',$category->id)->get();
		if(count($categories) > 0){
			foreach ($categories as $key => $category) {
	    		$categories[$key]['childs'] = $this->get_childs($category);
	    	}
		}
		return $categories;
	}

	public function index()
    {
    	$categories = Categories::where('parent_id',0)->get();
    	foreach ($categories as $key => $category) {
	    	$categories[$key]['childs'] = $this->get_childs($category);
	    }
	    $data['success'] = true;
        $data['data']['categories'] = $categories;
    	return response()->json($data,200);
    }

    public function show($id)
    {
    	$category = Categories::where('id',$id)->first();
        $category['childs'] = $this->get_childs($category);
    	$data['success'] = true;
        $data['data']['category'] = $category;
        $data['data']['category']['parents'] = $this->parents_category($category->id);
    	return response()->json($data,200);
    }
}