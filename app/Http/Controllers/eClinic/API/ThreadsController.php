<?php

namespace App\Http\Controllers\eClinic\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Threads;
use App\Model\eClinic\Categories;
use App\User;
use App\Model\eClinic\Ahlis;
use App\Model\eClinic\Responders;
use Carbon\Carbon;
use Validator;

class ThreadsController extends Controller
{
    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

	public function index(Request $request)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
    	$threads = Threads::with('user')->where('created_by',$user->id)->get();
        foreach ($threads as $key => $thread) {
            $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
        }
	    $data['success'] = true;
        $data['data']['threads'] = $threads;
    	return response()->json($data,200);
    }

    public function find(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $threads = Threads::with('user')->where('created_by',$user->id);
        $threads = $threads->where('judul','LIKE','%'.$request->text.'%');
        $threads = $threads->get();
        foreach ($threads as $key => $thread) {
            $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
        }
        $data['success'] = true;
        $data['data']['threads'] = $threads;
        return response()->json($data,200);
    }

    public function create(Request $request){
    	$validator = Validator::make($request->all(), [
            'category_id'=> 'required|integer',
            'question'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();	
        $category = Categories::where('id',$request->category_id)->first();


    	$thread = new Threads;
    	$thread->created_by = $user->id;
    	$thread->judul = $category->name;
        $thread->question = $request->question;
        $thread->file_question = $request->file;
    	$thread->status = 0;
    	$thread->category_id = $request->category_id;
    	$thread->created_by = $user->id;

        $thread->save();

        $ahlis = Ahlis::whereHas('ahli', function($query){
            $query->where('is_verified',1);
        })->with('ahli','category')->get();

        foreach ($ahlis as $key => $ahli) {
            $categories = explode(';', $ahli->id_category);
            if(in_array($request->category_id, $categories))
            {
                $responders = new Responders;
                $responders->thread_id = $thread->id;
                $responders->responder_id = $ahli->id_user;
                $responders->created_by = 0;
                $responders->save();
        
                if($ahli->ahli->token_firebase != ''){
                    $token = $ahli->ahli->token_firebase;
                    $title = 'Pertanyaan Baru';
                    $body = 'Pertanyaan ' . $category->name . ' baru, jawab pertanyaan sekarang.';
                    $dat =['jenis_notif' => 200];

                    $res =  notification($title,$body,$dat,$token);
                }
            }
        }
        $data['success'] = true;

        $data['success'] = true;
       	$data['data']['thread'] = $thread;
    	return response()->json($data,200);
    }

    public function show(Request $request,$id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
    	$thread = Threads::where('id',$id)->with('user')->where('created_by',$user->id);
        $thread = $thread->first();
    	if($thread) {
			$data['success'] = true;
            $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            if($thread->answered_at){
                $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
            }
            if($thread->updated_at)
                $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
	        $data['data']['thread'] = $thread;
            $data['data']['thread']['categories'] = $this->parents_category($thread->category_id);
            $data['data']['thread']['question_files'] = $thread->question_files();
            $data['data']['thread']['answer_files'] = $thread->answer_files();
	    	return response()->json($data,200);
    	} else {
    		$data['success'] = false;
	        $data['error'] = 'Data tidak ditemukan';
	    	return response()->json($data,200);
    	}
    }

    public function rating(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'rating' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $thread = Threads::where('id',$id)->with('user')->where('created_by',$user->id);
        $thread = $thread->first();
        if($thread) {
            $thread->rating = $request->rating;
            $thread->keterangan = $request->keterangan;
            $thread->updated_by = $user->id;
            try{
                $thread->save();
                $data['success'] = true;
                $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
                if($thread->answered_at){
                    $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
                }
                if($thread->updated_at)
                    $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
                $data['data']['thread'] = $thread;
                $data['data']['thread']['categories'] = $this->parents_category($thread->category_id);
                $data['data']['thread']['question_files'] = $thread->question_files();
                $data['data']['thread']['answer_files'] = $thread->answer_files();
                return response()->json($data,200);
            }
            catch(\Exception $e){
                $data['success'] = false;
                $data['error'] = $e->getMessage();
                return response()->json($data,200);
            }
        } else {
            $data['success'] = false;
            $data['error'] = 'Data tidak ditemukan';
            return response()->json($data,200);
        }
    }

    public function update(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
            'judul' => 'required',
            'question' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 500);
        }

    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $thread = Threads::where('id',$id)->with('user')->where('created_by',$user->id);
        $thread = $thread->first();
    	if($thread) {
    		$thread->judul = $request->judul;
            $thread->question = $request->question;
            $thread->updated_by = $user->id;
			try{
		        $thread->save();
                $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
                if($thread->answered_at){
                    $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
                }
                if($thread->updated_at)
                    $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
		        $data['success'] = true;
		       	$data['data']['thread'] = $thread;
		    	return response()->json($data,200);
		    }
		    catch(\Exception $e){
		       	$data['success'] = false;
		        $data['error'] = $e->getMessage();
		    	return response()->json($data,200);
		    }
    	} else {
    		$data['success'] = false;
	        $data['error'] = 'Data tidak ditemukan';
	    	return response()->json($data,200);
    	}
    }

    public function delete(Request $request,$id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        $thread = Threads::where('id',$id)->where('created_by',$user->id);
        $thread = $thread->first();
    	if($thread) {
    		$thread->delete();
			$data['success'] = true;
	    	return response()->json($data,200);
    	} else {
    		$data['success'] = false;
	        $data['error'] = 'Data tidak ditemukan';
	    	return response()->json($data,200);
    	}
    }
}
