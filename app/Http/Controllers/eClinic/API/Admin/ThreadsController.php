<?php

namespace App\Http\Controllers\eClinic\API\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Threads;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Responders;
use App\User;
use Carbon\Carbon;


use Validator;

class ThreadsController extends Controller {
    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

	public function index(Request $request)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
        	$threads = Threads::with('user','responders')->where('status','>=',0)->where('status','<=',1)->get();
            foreach ($threads as $key => $thread) {
                $is_answered = 0;
                if(count($thread->responders) > 0){
                    foreach ($thread->responders as $k => $responder) {
                        if($responder->answer != NULL){
                            $is_answered = 1;
                        }
                    }
                }
                $threads[$key]->is_answered = $is_answered;
                $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            }
    	    $data['success'] = true;
            $data['data']['threads'] = $threads;
        	return response()->json($data,200);
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function find(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
            $threads = Threads::with('user');
            $threads = $threads->where('judul','LIKE','%'.$request->text.'%');
            $threads = $threads->get();
            foreach ($threads as $key => $thread) {
                $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            }
            $data['success'] = true;
            $data['data']['threads'] = $threads;
            return response()->json($data,200);
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function show(Request $request,$id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
        	$thread = Threads::where('id',$id)->with(['user','responders','responders.responden']);
            $thread = $thread->first();
        	if($thread) {
    			$data['success'] = true;
                $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
                if($thread->answered_at){
                    $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
                }
                if($thread->updated_at)
                    $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
    	        $data['data']['thread'] = $thread;
                $data['data']['thread']['categories'] = $this->parents_category($thread->category_id);
                $data['data']['thread']['question_files'] = $thread->question_files();
                $data['data']['thread']['answer_files'] = $thread->answer_files();
    	    	return response()->json($data,200);
        	} else {
        		$data['success'] = false;
    	        $data['error'] = 'Data tidak ditemukan';
    	    	return response()->json($data,200);
        	}
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function delete(Request $request,$id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
            $thread = Threads::where('id',$id);
            $thread = $thread->first();
        	if($thread) {
        		$thread->delete();
    			$data['success'] = true;
    	    	return response()->json($data,200);
        	} else {
        		$data['success'] = false;
    	        $data['error'] = 'Data tidak ditemukan';
    	    	return response()->json($data,200);
        	}
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function assign(Request $request,$id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
            $thread = Threads::where('id',$id);
            $thread = $thread->first();
            if($thread && $thread->status < 2) {
                $responder = Responders::where('thread_id',$id)->where('responder_id', $request->id_user)->get();
                if(count($responder) > 0){
                    $data['success'] = false;
                    $data['error'] = 'Responden sudah ditambahkan';
                    return response()->json($data,200);       
                } else {
                    $responders = new Responders;
                    $responders->thread_id = $id;
                    $responders->responder_id = $request->id_user;
                    $responders->created_by = $user->id;
                    $responders->save();

                    if($responders->responden->token_firebase != ''){
                        $token = $responders->responden->token_firebase;
                        $title = 'Pertanyaan Baru';
                        $body = 'Pertanyaan ' . strtolower($thread->judul) . ' baru, jawab pertanyaan sekarang.';
                        $dat =['jenis_notif' => 200];

                        $res =  notification($title,$body,$dat,$token);
                    }
                }

                $thread->status = 1;
                $thread->updated_by = $user->id;
                $thread->save();

                $data['success'] = true;
                return response()->json($data,200);
            } else {
                $data['success'] = false;
                $data['error'] = 'Data tidak ditemukan';
                return response()->json($data,200);
            }
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function delete_responder(Request $request,$thread_id, $responder_id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
            $thread = Threads::where('id',$thread_id);
            $thread = $thread->first();
            if($thread && $thread->status < 2) {
                $responder = Responders::where('thread_id',$thread_id)->where('id', $responder_id)->first();
                if($responder){
                    if($responder->answer == null){
                        $responder->delete();
                        $data['success'] = true;
                        return response()->json($data,200);  
                    } else {
                        $data['success'] = false;
                        $data['error'] = 'Responder tidak dapat dihapus';
                        return response()->json($data,200);
                    }     
                } else {
                    $data['success'] = false;
                    $data['error'] = 'Data tidak ditemukan';
                    return response()->json($data,200);
                }
            } else {
                $data['success'] = false;
                $data['error'] = 'Data tidak ditemukan';
                return response()->json($data,200);
            }
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function conclusion(Request $request,$id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::ADMIN){
            $thread = Threads::where('id',$id);
            $thread = $thread->first();
            if($thread){
                if($thread->status < 2) {
                    $thread->answer = $request->answer;
                    $thread->publish = $request->publish;
                    $thread->status = 2;
                    $thread->updated_by = $user->id;
                    $thread->answered_at = Carbon::now();
                    $thread->save();

                    if($thread->user->token_firebase != ''){
                        $token = $thread->user->token_firebase;
                        $title = 'Pertanyaan Sudah Terjawab';
                        $body = 'Pertanyaan ' . strtolower($thread->judul) . ' sudah terjawab.';
                        $dat =['jenis_notif' => 200];

                        $res =  notification($title,$body,$dat,$token);
                    }

                    $data['success'] = true;
                    return response()->json($data,200);
                } else {
                    $data['success'] = false;
                    $data['error'] = 'Jawaban sudah disimpulkan';
                    return response()->json($data,200);    
                }
            } else {
                $data['success'] = false;
                $data['error'] = 'Data tidak ditemukan';
                return response()->json($data,200);
            }
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }
}