<?php

namespace App\Http\Controllers\eClinic\API\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Ahlis;
use App\User;

class AhliController extends Controller
{
    public function category($id)
    {
    	$ahlis = Ahlis::whereHas('ahli', function($query){
			$query->where('is_verified',1);
		})->with('ahli','category')->get();

		foreach ($ahlis as $key => $ahli) {
			$categories = explode(';', $ahli->id_category);
			if(!in_array($id, $categories))
			{
				unset($ahlis[$key]);
			}
		}
    	$data['success'] = true;
        $data['data']['ahlis'] = $ahlis;
    	return response()->json($data,200);
    }
}