<?php

namespace App\Http\Controllers\eClinic\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\eClinic\User_verification;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Response;

class UserController extends Controller
{
	public function index(Request $request)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
    	$data['success'] = true;
        $data['data']['user'] = $user;
    	return response()->json($data,200);
    }
}