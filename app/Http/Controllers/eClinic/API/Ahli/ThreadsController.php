<?php

namespace App\Http\Controllers\eClinic\API\Ahli;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\eClinic\Threads;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Responders;
use App\User;
use Carbon\Carbon;

use Validator;

class ThreadsController extends Controller
{
    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

	public function index(Request $request)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::AHLI){
            $threads = Threads::with(['user','responders','responders.responden'])->whereHas('responders', function ($query) use ($user) {
                    $query->where('responder_id',$user->id)->where('answer',NULL);
                })->get();
            foreach ($threads as $key => $thread) {
                $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            }
    	    $data['success'] = true;
            $data['data']['threads'] = $threads;
        	return response()->json($data,200);
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function show(Request $request,$id)
    {
    	$user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::AHLI){
            $thread = Threads::where('id',$id)->with('user');
            $thread = $thread->first();
        	if($thread) {
    			$data['success'] = true;
                $thread->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
                if($thread->answered_at){
                    $thread->answered_at_formatted = Carbon::createFromFormat('Y-m-d H:i:s',$thread->answered_at)->format('d-m-Y H:i:s');
                }
                if($thread->updated_at)
                    $thread->updated_at_formatted = $thread->updated_at->format('d-m-Y H:i:s');
                
    	        $data['data']['thread'] = $thread;
                $data['data']['thread']['categories'] = $this->parents_category($thread->category_id);
                $data['data']['thread']['question_files'] = $thread->question_files();
                $data['data']['thread']['answer_files'] = $thread->answer_files();
    	    	return response()->json($data,200);
        	} else {
        		$data['success'] = false;
    	        $data['error'] = 'Data tidak ditemukan';
    	    	return response()->json($data,200);
        	}
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function answer(Request $request, $thread_id)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::AHLI){

            $respon = Responders::where('thread_id',$thread_id)->where('responder_id',$user->id)->first();
            $respon->answer = $request->answer;
            $respon->answered_at = Carbon::now();
            $respon->save();

            $users= User::where('role', User::ADMIN)->get();
            foreach ($users as $key => $user) {
                if($user->token_firebase != ''){
                    $token = $user->token_firebase;
                    $title = 'Pertanyaan Terjawab';
                    $body = 'Pertanyaan ' . strtolower($respon->thread->judul) . ' sudah terjawab.';
                    $dat =['jenis_notif' => 200];

                    $res =  notification($title,$body,$dat,$token);   
                }
            }
            
            $data['success'] = true;
            return response()->json($data,200);
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }

    public function find(Request $request)
    {
        $user = User::where('token', 'LIKE', $request->bearerToken())->first();
        if($user->role == User::AHLI){
            $threads = Responders::with('thread','thread.user')->whereHas('thread', function ($query) {
                    $query->where('judul','LIKE','%'.$request->text.'%');
                })->where('answer',NULL)->where('responder_id',$user->id)->get();
            foreach ($threads as $key => $thread) {
                $threads[$key]->created_at_formatted = $thread->created_at->format('d-m-Y H:i:s');
            }
            $data['success'] = true;
            $data['data']['threads'] = $threads;
            return response()->json($data,200);
        } else {
            $data['success'] = false;
            $data['error'] = 'Forbidden Access';
            return response()->json($data,200);
        }
    }
}