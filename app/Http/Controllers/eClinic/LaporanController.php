<?php

namespace App\Http\Controllers\eClinic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\DetailUser;
use Carbon\Carbon;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Ahlis;
use App\Model\eClinic\Responders;
use App\Model\eClinic\Threads;
use Validator, DB, Hash, Mail;
use PDF;
class LaporanController extends Controller{

	public function index(Request $request){
		$data['ahlis'] = User::where('role',User::AHLI)->get();
		return view('e-clinic.laporan.index', $data);
	}

	public function download(Request $request){
		$month = $request->bulan;
		$year = $request->tahun;
		$ahli = $request->ahli;
		$data['user'] = User::with('ahli','detail')->where('id',$ahli)->first();
		$data['responders'] = Responders::with('thread','responden')->where('responder_id', $ahli)->where('answer','!=',NULL)->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->get();

		$data['from'] = Responders::with('thread','responden')->where('responder_id', $ahli)->where('answer','!=',NULL)->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->count();

		$data['max'] = Responders::with('thread','responden')->where('responder_id', $ahli)->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->count();
		if($month == 1)
                $month = 'Januari';
                else if($month == 2)
                $month = 'Februari';
                else if($month == 3)
                $month = 'Maret';
                else if($month == 4)
                $month = 'April';
                else if($month == 5)
                $month = 'Mei';
                else if($month == 6)
                $month = 'Juni';
                else if($month == 7)
                $month = 'Juli';
                else if($month == 8)
                $month = 'Agustus';
                else if($month == 9)
                $month = 'September';
                else if($month == 10)
                $month = 'Oktober';
                else if($month == 11)
                $month = 'November';
                else if($month == 12)
                $month = 'Desember';
                $data['month'] = $month;
                $data['year'] = $year;

		$pdf = PDF::loadView('pdf.laporan-eclinic', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
		return $pdf->stream();
	}

        public function rekap(Request $request){
                $month = $request->bulan;
                $year = $request->tahun;
                $data['users'] = User::with('ahli','detail')->where('role',User::AHLI)->get();
                $data['from'] = 0;
                $data['max'] = 0;
                foreach ($data['users'] as $key => $user) {
                        $data['users'][$key]['from'] = Responders::with('thread','responden')->where('responder_id', $user->id)->where('answer','!=',NULL)->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->count();
                        $data['from'] += $data['users'][$key]['from'];

                        $data['users'][$key]['max'] = Responders::with('thread','responden')->where('responder_id', $user->id)->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->count();
                        $data['max'] += $data['users'][$key]['max'];
                }
                
                if($month == 1)
                $month = 'Januari';
                else if($month == 2)
                $month = 'Februari';
                else if($month == 3)
                $month = 'Maret';
                else if($month == 4)
                $month = 'April';
                else if($month == 5)
                $month = 'Mei';
                else if($month == 6)
                $month = 'Juni';
                else if($month == 7)
                $month = 'Juli';
                else if($month == 8)
                $month = 'Agustus';
                else if($month == 9)
                $month = 'September';
                else if($month == 10)
                $month = 'Oktober';
                else if($month == 11)
                $month = 'November';
                else if($month == 12)
                $month = 'Desember';
                $data['month'] = $month;
                $data['year'] = $year;

                $pdf = PDF::loadView('pdf.rekap-eclinic', $data)->setPaper(array(0,0,609.4488,935.433),'portrait');
                return $pdf->stream();
        }
}