<?php

namespace App\Http\Controllers\eClinic;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\DetailUser;
use Carbon\Carbon;
use App\Model\eClinic\Categories;
use App\Model\eClinic\Ahlis;
use App\Model\eClinic\User_verification;
use Validator, DB, Hash, Mail;

class AhliController extends Controller{

    public $category = '';

    protected function get_verification_code()
    {
        $verification_code = str_random(30);

        while (true){
            $cek_vercode = User_verification::where('token',$verification_code)->first();
            if ($cek_vercode){
                $verification_code = str_random(30);                
            } else {
                return $verification_code;
            }
        }
    }

    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        $categories_stack = [];
        while($categories->parent_id != 0){
            array_push($categories_stack,$categories->name);
            $categories = Categories::where('id',$categories->parent_id)->first();
        }
        array_push($categories_stack,$categories->name);
        return $categories_stack;
    }

    protected function get_childs($category,$default_value = false){
        $categories = Categories::where('parent_id',$category->id)->get();
        if(count($categories) > 0){
            foreach ($categories as $key => $category) {
                $categories[$key]['parents'] = $this->parents_category($category->id);
                $categories[$key]['childs'] = $this->get_childs($category,$default_value);
            }
        } else {
            if($default_value){
                if(in_array($category->id, $default_value)){
                    $this->category .= '<option value="'.$category->id.'" selected>';
                } else {
                    $this->category .= '<option value="'.$category->id.'">';    
                }
            } else {
                $this->category .= '<option value="'.$category->id.'">';
            }
            for ($i=count($category->parents) -1; $i >= 0 ; $i--) { 
                if($i != 0){
                    $this->category .= $category->parents[$i] . ' > ';
                } else {
                    $this->category .= $category->parents[$i];
                }
            }
            $this->category .= '</option>';

        }
        return $categories;
    }

    public function index()
    {
        $data['users'] = User::where('role',User::AHLI)->get();
        if(count($data['users']) > 0){
            foreach ($data['users'] as $key => $user) {
                $data['users'][$key]['n_category'] = count(explode(';', $user->ahli->id_category));
            }
        }
        return view('e-clinic.ahli.index',$data);
    }

    public function create()
    {
        $this->categories = '';
        $categories = Categories::where('parent_id',0)->get();
        foreach ($categories as $key => $category) {
            $categories[$key]['parents'] = $this->parents_category($category->id);
            $categories[$key]['childs'] = $this->get_childs($category);
        }
        $data['categories'] = $this->category;
        return view('e-clinic.ahli.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'name' => 'required',
            'password'=> 'required',
            'opd' => 'required',
            'nip' => 'required|numeric',
            'jabatan' => 'required',
            'hp' => 'required|numeric',
            'jenis_kelamin' => 'required',
            'category' => 'required',
        ],[
           'email.email' => 'Gunakan email yang valid',
           '*.required' => ':attribute harus terisi',
           '*.numeric' => ':attribute harus angka'
        ]);
        if ($validator->fails()) {
            $request->request->remove('category');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $email_exist = User::where('email',$request->input('email'))->get();
        if(count($email_exist) == 0){
            $user = new User;
            $user->name = $request->input('name');
            $user->password = bcrypt($request->input('password'));
            $user->email = $request->input('email');
            $now = Carbon::now();
            $user->role = User::AHLI;
            $user->created_by = Auth::user()->id;
            $user->save();

            $detail = new DetailUser;
            $detail->id_user = $user->id;
            $detail->opd = $request->input('opd');
            $detail->nip = $request->input('nip');
            $detail->jabatan = $request->input('jabatan');
            $detail->hp = $request->input('hp');
            $detail->jenis_kelamin = $request->input('jenis_kelamin');
            $detail->created_by = Auth::user()->id;
            $detail->save();

            $ahli = new Ahlis;
            $ahli->id_user = $user->id;
            $category = implode(";",$request->input('category'));
            $ahli->id_category = $category;
            $ahli->created_by = Auth::user()->id;
            $ahli->save();

            $cat = '';

            foreach ($request->input('category') as $key => $category) {
                $categories = $this->parents_category($category);
                for ($i = count($categories)-1; $i >= 0 ; $i--){
                  $cat = $cat . $categories[$i];
                  if($i != 0){
                    $cat .= ' - ';
                  }       
                }

                if($key != count($request->input('category'))-1){
                    $cat .= ', ';
                } else {
                    $cat .= ' ';
                }
            }

            $user_verification = new User_verification;
            $user_verification->token = $this->get_verification_code();
            

            $subject = "[e-clinic UPT PPK - BPKAD Provinsi Jawa Timur] Verifikasi email.";
            $email = $request->input('email');
            $name = $request->input('name');
            Mail::send('email.verify-ahli', ['name' => $user->name, 'verification_code' => $user_verification->token,'password' => $request->input('password'),'email' => $user->email, 'category' => $cat],
                function($mail) use ($email, $name, $subject){
                    $mail->from(env('MAIL_USERNAME'), "UPT PPK - BPKAD Provinsi Jawa Timur");
                    $mail->to($email, $name);
                    $mail->subject($subject);
                });
            $data['message'] = 'Terimakasih, Buka email anda untuk melengkapi pendaftaran.';

            $user->save();
            $user_verification->user_id = $user->id;
            $user_verification->save();

            return redirect('/admin/e-clinic/ahli')->with('create_success',true);
        } else {
            $request->request->remove('category');
            return redirect()->back()->with('email_error','Email sudah terdaftar.')->withInput($request->input());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['user'] = User::with('ahli')->findOrFail($id);
        $id_categories = explode(';',$data['user']->ahli->id_category);

        $cat = '<ol style="padding-left: 15px;margin-bottom:0px;">';
        foreach ($id_categories as $key => $category) {
            $cat .= '<li>';
            $categories = $this->parents_category($category);
            for ($i = count($categories)-1; $i >= 0 ; $i--){
              $cat = $cat . $categories[$i];
              if($i != 0){
                $cat .= ' - ';
              }       
            }
            $cat .= '</li>';
        }
        $cat .= '</ol>';
        $data['user']->ahli['categories'] = $cat;
        return view('e-clinic.ahli.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::with('ahli')->findOrFail($id);
        $this->categories = '';
        $categories = Categories::where('parent_id',0)->get();
        foreach ($categories as $key => $category) {
            $categories[$key]['parents'] = $this->parents_category($category->id);
            $categories[$key]['childs'] = $this->get_childs($category, explode(';', $data['user']->ahli->id_category));
        }
        $data['categories'] = $this->category;
        return view('e-clinic.ahli.edit', $data);
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'name' => 'required',
            'opd' => 'required',
            'nip' => 'required|numeric',
            'jabatan' => 'required',
            'hp' => 'required|numeric',
            'jenis_kelamin' => 'required',
            'category' => 'required',
        ],[
           'email.email' => 'Gunakan email yang valid',
           '*.required' => ':attribute harus terisi',
           '*.numeric' => ':attribute harus angka'
        ]);
        if ($validator->fails()) {
            $request->request->remove('category');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::findOrFail($id);
        if($user->email != $request->input('email')){
            $email_exist = User::where('email',$request->input('email'))->get();
            if(count($email_exist) > 0){
                $request->request->remove('category');
                return redirect()->back()->with('email_error', 'Email sudah terdaftar.')->withInput($request->input());
            }
        }
        
        $user->name = $request->input('name');
        if(!is_null($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->email = $request->input('email');
        $user->updated_by = Auth::user()->id;
        $user->save();

        $detail = $user->detail;
        $detail->opd = $request->input('opd');
        $detail->nip = $request->input('nip');
        $detail->hp = $request->input('hp');
        $detail->jabatan = $request->input('jabatan');
        $detail->jenis_kelamin = $request->input('jenis_kelamin');
        $detail->updated_by = Auth::user()->id;
        $detail->save();

        $ahli = Ahlis::where('id_user', $user->id)->first();
        $category = implode(";",$request->input('category'));
        $ahli->id_category = $category;
        $ahli->updated_by = Auth::user()->id;
        $ahli->save();

        return redirect('/admin/e-clinic/ahli/'.$id)->with('edit_success',true);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($id != 1){
            foreach ($user->ahli->respon as $key => $respon) {
                $respon->delete();
            }
            $user->ahli->delete();
            $user->delete();
            return redirect('/admin/e-clinic/ahli')->with('delete_success',true);    
        } else {
            return redirect('/admin/e-clinic/ahli')->with('delete_admin_error',true);    
        }
        
    }

    public function aktifkan($id)
    {
        $user = User::findOrFail($id);
        if($user){
            $user->is_verified = 1;
            $user->save();
            return redirect('/admin/e-clinic/ahli/'.$id)->with('aktif_success',true);  
        } else {
            return redirect()->back();
        }
    }

    public function non_aktifkan($id)
    {
        $user = User::findOrFail($id);
        if($user){
            $user->is_verified = -1;
            $user->save();
            return redirect('/admin/e-clinic/ahli/'.$id)->with('non_aktif_success',true);  
        } else {
            return redirect()->back();
        }
    }
}
