<?php

namespace App\Model\eClinic;

use Illuminate\Database\Eloquent\Model;

class Responders extends Model
{
    protected $table = 'responders';

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }

    public function responden()
    {
        return $this->hasOne('App\User', 'id','responder_id');
    }

    public function thread()
    {
        return $this->hasOne('App\Model\eClinic\Threads', 'id','thread_id');
    }
}
