<?php

namespace App\Model\eClinic;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'files';

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}
