<?php

namespace App\Model\eClinic;

use Illuminate\Database\Eloquent\Model;
use App\Model\eClinic\Files;
class Threads extends Model
{
    protected $table = 'threads';

	const NEW_QUESTION = 0;
	const QUESTION_FORWARDED = 1;
	const QUESTION_ANSWERED = 2;

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }

    public function responders()
    {
        return $this->hasMany('App\Model\eClinic\Responders', 'thread_id','id');
    }

    public function question_files()
    {
        if($this->file_question){
            $file_question = $this->file_question;
            $file_question = explode(';', $file_question);
            foreach ($file_question as $key => $value) {
                $file = Files::where('id',$value)->first();
                $file_question[$key] = URL_WEB_APLIKASI . $file->file;
            }

            return $file_question;
        } else{
            return [];
        }
    }

    public function answer_files()
    {
        if($this->file_answer){
            $file_answer = $this->file_answer;
            $file_answer = explode(';', $file_answer);
            foreach ($file_answer as $key => $value) {
                $file = Files::where('id',$value)->first();
                $file_answer[$key] = URL_WEB_APLIKASI . $file->file;
            }

            return $file_answer;
        } else{
            return [];
        }
    }
}
