<?php

namespace App\Model\eClinic;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ahlis extends Model
{
    protected $table = 'ahlis';

    protected $appends = array('full');

    protected function parents_category($category_id){
        $categories = Categories::where('id',$category_id)->first();
        if($categories){
            $categories_stack = [];
            while($categories->parent_id != 0){
                array_push($categories_stack,$categories->name);
                $categories = Categories::where('id',$categories->parent_id)->first();
            }
            array_push($categories_stack,$categories->name);
            return $categories_stack;
        }
    }

    public function getCategoriesAttribute()
    {
        $return = [];
        $id_categories = explode(';',$this->id_category);
        foreach ($id_categories as $key => $category) {
            $categories = $this->parents_category($category);
            if($categories){
                $cat = '';
                for ($i = count($categories)-1; $i >= 0 ; $i--){
                  $cat = $cat . $categories[$i];
                  if($i != 0){
                    $cat .= ' - ';
                  }       
                }
                $return[] = $cat;
            }
        }
      return $return;
    }

    public function ahli()
    {
        return $this->hasOne('App\User', 'id','id_user');
    }

    public function respon()
    {
        return $this->hasMany('App\Model\eClinic\Responders', 'responder_id','id_user');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}


