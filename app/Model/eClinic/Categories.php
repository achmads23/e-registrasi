<?php

namespace App\Model\eClinic;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
	use SoftDeletes;
    protected $table = 'categories';
    protected $dates = ['deleted_at'];

    public function childs()
    {
        return $this->hasMany('App\Model\eClinic\Categories', 'parent_id','id');
    }

    public function parent()
    {
        return $this->hasOne('App\Model\eClinic\Categories', 'id','parent_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}


