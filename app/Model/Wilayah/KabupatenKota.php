<?php

namespace App\Model\Wilayah;

use Illuminate\Database\Eloquent\Model;

class KabupatenKota extends Model
{
    protected $table = 'regencies';
}
