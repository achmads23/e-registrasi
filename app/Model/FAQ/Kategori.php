<?php

namespace App\Model\FAQ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lorisleiva\LaravelSearchString\Concerns\SearchString;

class Kategori extends Model
{
    protected $table = 'faq_kategori';

    public function child()
    {
        return $this->hasMany('App\Model\FAQ\Kategori', 'id_parent','id')->orderBy('urutan');
    }

    public function parent()
    {
        return $this->hasOne('App\Model\FAQ\Kategori', 'id','id_parent');
    }

    public function pertanyaan()
    {
        return $this->hasMany('App\Model\FAQ\Pertanyaan', 'id_faq_kategori','id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}


