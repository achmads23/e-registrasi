<?php

namespace App\Model\FAQ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pertanyaan extends Model
{
    protected $table = 'faq_pertanyaan';

    public function kategori()
    {
        return $this->hasOne('App\Model\FAQ\Kategori', 'id','id_faq_kategori');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id','created_by');
    }
}


