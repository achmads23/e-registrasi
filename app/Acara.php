<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Opd;
class Acara extends Model
{
    protected $table = 'acara';
    protected $primaryKey = 'id_acr';
    

    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function peserta()
    {
        return $this->hasMany('App\Peserta', 'id_acara', 'id_acr');
    }

    public function opds()
    {
        $id_opds = $this->id_opds;
        $id_opds = explode(';', $id_opds);
        array_pop($id_opds);
        foreach ($id_opds as $key => $value) {
            $opd = Opd::where('id',$value)->first();
            $id_opds[$key] = $opd;
        }

        return $id_opds;
    }

}
