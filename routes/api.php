<?php

use Illuminate\Http\Request;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('tes-firebase', function (Request $request) {
    $token = $request->token;
    $title =$request->title;
    $body =$request->body;
    $data =['jenis_notif' => 200];

    $res =  notification($title,$body,$data,$token);
    dd($res);
    return $res;
});

Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');
Route::post('recover', 'API\AuthController@recover');


Route::prefix('e-clinic/arsip')->group(function () {
	Route::get('threads/category/{id}', 'eClinic\API\Arsip\ThreadsController@index_by_category');

	Route::post('threads/find', 'eClinic\API\Arsip\ThreadsController@find');
	Route::get('threads/{id}', 'eClinic\API\Arsip\ThreadsController@show');
});

Route::get('e-clinic/category', 'eClinic\API\CategoriesController@index');
Route::get('e-clinic/category/{id}', 'eClinic\API\CategoriesController@show');

Route::group(['middleware' => ['android-api']], function() {
    Route::get('logout', 'API\AuthController@logout');

    Route::post('firebase/set', 'API\FirebaseController@set');
    Route::get('firebase/get', 'API\FirebaseController@get');

    Route::get('e-clinic/user', 'eClinic\API\UserController@index');

	Route::get('e-clinic/threads', 'eClinic\API\ThreadsController@index');
	Route::post('e-clinic/threads/find', 'eClinic\API\ThreadsController@find');
	Route::post('e-clinic/threads', 'eClinic\API\ThreadsController@create');

	Route::post('e-clinic/threads/{id}/rating', 'eClinic\API\ThreadsController@rating');
	Route::get('e-clinic/threads/{id}', 'eClinic\API\ThreadsController@show');
	Route::delete('e-clinic/threads/{id}', 'eClinic\API\ThreadsController@delete');
	Route::put('e-clinic/threads/{id}', 'eClinic\API\ThreadsController@update');

	Route::get('file/{id}', 'API\FilesController@index');
	Route::post('file', 'API\FilesController@upload');

	// ADMIN
	Route::prefix('e-clinic/admin')->group(function () {
		Route::get('threads', 'eClinic\API\Admin\ThreadsController@index');
		
		Route::post('threads/{id}/assign', 'eClinic\API\Admin\ThreadsController@assign');
		Route::delete('threads/{id}/responder/{id_responder}', 'eClinic\API\Admin\ThreadsController@delete_responder');

		Route::post('threads/{id}/conclusion', 'eClinic\API\Admin\ThreadsController@conclusion');

		Route::post('threads/find', 'eClinic\API\Admin\ThreadsController@find');
		Route::get('threads/{id}', 'eClinic\API\Admin\ThreadsController@show');
		Route::delete('threads/{id}', 'eClinic\API\Admin\ThreadsController@delete');

		Route::get('ahli/category/{id}', 'eClinic\API\Admin\AhliController@category');
	});


	// AHLI
	Route::prefix('e-clinic/ahli')->group(function () {
		Route::get('threads', 'eClinic\API\Ahli\ThreadsController@index');
		Route::get('threads/{id}', 'eClinic\API\Ahli\ThreadsController@show');

		Route::post('threads/{id}/answer', 'eClinic\API\Ahli\ThreadsController@answer');
		Route::post('threads/find', 'eClinic\API\Ahli\ThreadsController@find');
	});

	Route::prefix('bimtek')->group(function () {

		Route::prefix('undangan')->group(function () {
			Route::get('/', 'Bimtek\API\UndanganController@index');
			Route::get('/{id}', 'Bimtek\API\UndanganController@detail');
			Route::post('find', 'Bimtek\API\UndanganController@find');
		});

		Route::prefix('umum')->group(function () {
			Route::get('/', 'Bimtek\API\UmumController@index');
			Route::get('/{id}', 'Bimtek\API\UmumController@detail');
			Route::post('find', 'Bimtek\API\UmumController@find');
		});

		Route::prefix('acara')->group(function () {
			Route::post('cek-code', 'Bimtek\API\AcaraController@cek_code');
			Route::get('code/{code}', 'Bimtek\API\AcaraController@get_by_code');
			Route::get('/', 'Bimtek\API\AcaraController@list');
			Route::get('/{id}', 'Bimtek\API\AcaraController@show');
			Route::post('createUmum', 'Bimtek\API\AcaraController@createUmum');
			Route::post('createUndangan', 'Bimtek\API\AcaraController@createUndangan');
		});

		Route::post('cek-nip', 'Bimtek\API\AcaraController@cek_nip');
	});
});