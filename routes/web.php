<?php

use App\Model\FAQ\Kategori;
use App\Model\FAQ\Pertanyaan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pindah-data', 'PublicController@pindah_data')->name('pindah-data');

Route::get('/monitor', 'PublicController@monitor')->name('monitor');

Route::get('/', function () {
	$data['kategoris'] = Kategori::with(['parent','child','pertanyaan'])->where('id_parent',0)->orderBy('urutan','ASC')->get();
    return view('landing-page-new',$data);
})->name('index');

Route::post('klinik-center/kategori/like-dislike','KlinikCenterController@like_dislike')->name('klinik-center.like-dislike');
Route::get('klinik-center/kategori/{id}/{sub_id?}','KlinikCenterController@detail')->name('klinik-center.detail');
Route::get('klinik-center/pencarian/{cari?}','KlinikCenterController@search')->name('klinik-center.cari');
Route::get('klinik-center/kategori/sub/{id}','KlinikCenterController@detail_sub_kategori')->name('klinik-center.detail_sub_kategori');

Route::get('bimtek', function () {
    return view('bimtek');
})->name('bimtek');

Route::get('bimtek-grup', function () {
    return view('bimtek-grup');
})->name('bimtek-grup');
Route::get('bimtek-personal', function () {
    return view('bimtek-personal');
})->name('bimtek-personal');

Route::get('klinik-center', function () {
    return view('klinik-center');
})->name('klinik-center');

Route::get('pendampingan', function () {
    return view('pendampingan');
})->name('pendampingan');

Route::get('informasi', function () {
    return view('informasi');
})->name('informasi');

Route::prefix('bimtek')->group(function(){
	Route::get('undangan', function () {
	    return view('bimtek.undangan');
	})->name('bimtek.undangan');

	Route::get('acara', 'Bimtek\AcaraController@index')->name('bimtek.acara.index');
	Route::get('acara/{id}', 'Bimtek\AcaraController@show')->name('bimtek.acara.show');

	Route::get('umum', 'Bimtek\UmumController@index')->name('bimtek.umum');
	Route::post('umum', 'Bimtek\UmumController@post_index')->name('bimtek.umum.post');

	Route::get('informasi', function () {
	    return view('bimtek.informasi');
	})->name('bimtek.informasi');

	Route::post('pendaftaran', 'Bimtek\PendaftaranController@post_index')->name('bimtek.pendaftaran.index');

	Route::post('pendaftaran/register', 'Bimtek\PendaftaranController@post_register')->name('bimtek.pendaftaran.register');
	Route::post('pendaftaran/download', 'Bimtek\PendaftaranController@download')->name('pendaftaran.download');
	Route::get('pendaftaran/peserta/{id?}', 'Bimtek\PendaftaranController@peserta')->name('pendaftaran.peserta');

	Route::get('bimtek/check-in', 'Bimtek\PendaftaranController@bimtek_checkin')->name('pendaftaran.bimtek_checkin');
	Route::post('bimtek/check-in-token', 'Bimtek\PendaftaranController@bimtek_checkin_token')->name('pendaftaran.bimtek_checkin_token');
	Route::get('konfirmasi-perubahan-opd', 'Bimtek\PendaftaranController@konfirmasi_perubahan_opd')->name('pendaftaran.konfirmasi_perubahan_opd');
	Route::post('konfirmasi-perubahan-opd', 'Bimtek\PendaftaranController@post_konfirmasi_perubahan_opd')->name('pendaftaran.post_konfirmasi_perubahan_opd');
});

Route::get('/bimtek-scan', function () {
    return view('bimtek-scan-qr');
})->name('bimtek-scan');

Route::post('cek-code', 'AjaxController@cek_code')->name('ajax-cek-code');
Route::post('cek-peserta', 'AjaxController@cek_peserta')->name('ajax-cek-peserta');
Route::post('cek-bimtek-qr-code', 'AjaxController@cek_bimtek_qr_code')->name('ajax-cek-bimtek-qr-code');


Route::get('login', 'Frontend\AuthController@login')->name('public-login');
Route::get('forget-password', 'Frontend\AuthController@forget_password')->name('forget-password');
Route::post('forget-password', 'Frontend\AuthController@post_forget_password')->name('post-forget-password');
Route::get('reset-password/{token}', 'Frontend\AuthController@reset_password')->name('reset-password');
Route::post('reset-password/{token}', 'Frontend\AuthController@post_reset_password')->name('post-reset-password');
Route::post('login', 'Frontend\AuthController@post_login')->name('public-post-login');
Route::get('register', 'Frontend\AuthController@register')->name('public-register');
Route::post('register', 'Frontend\AuthController@post_register')->name('public-post-register');
Route::group(['middleware' => ['login']], function () {
	Route::get('logout', 'Frontend\AuthController@logout')->name('public-logout');

	Route::prefix('bimtek')->group(function(){
		Route::get('grup-umum', 'Bimtek\Grup\UmumController@index')->name('bimtek.grup-umum');
		Route::post('grup-umum', 'Bimtek\Grup\UmumController@register')->name('bimtek.grup-umum.register');

		Route::get('grup-undangan', 'Bimtek\Grup\UndanganController@index')->name('bimtek.grup-undangan');
		Route::post('grup-undangan', 'Bimtek\Grup\UndanganController@detail')->name('bimtek.grup-undangan.detail');
		Route::post('grup-undangan/submit', 'Bimtek\Grup\UndanganController@register')->name('bimtek.grup-undangan.register');
	});
});

Route::prefix('admin')->group(function () {
	$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
	$this->post('login', 'Auth\LoginController@login');

	Route::group(['middleware' => ['auth']], function () {
		Route::get('logout','AuthController@logout')->name('logout');
		Route::get('/', 'HomeController@index')->name('home');

		Route::group(['middleware' => ['admin']], function () {
			Route::get('/konfirmasi-opd', 'KonfirmasiPerubahanOpdController@index');	
			Route::get('/konfirmasi-opd/{id}', 'KonfirmasiPerubahanOpdController@detail');	
			Route::post('/konfirmasi-opd/{id}/tolak', 'KonfirmasiPerubahanOpdController@tolak');
			Route::post('/konfirmasi-opd/{id}/setuju', 'KonfirmasiPerubahanOpdController@setuju');	

			Route::post('/cek-spt', 'AcaraController@cek_spt')->name('admin/cek-spt');	
			Route::post('/force-check-in', 'AcaraController@force_check_in')->name('admin.force_check_in');	
			Route::post('/notify', 'AcaraController@notify')->name('admin.notify');	

			Route::get('acara/cetak-daftar-hadir/{id}', 'AcaraController@cetak_daftar_hadir')->name('admin.cetak-daftar-hadir');
			Route::get('acara/cetak-konfirmasi-peserta/{id}', 'AcaraController@cetak_konfirmasi_peserta')->name('admin.cetak-konfirmasi-peserta');
			Route::get('acara/cetak-undangan-umum/{id}', 'AcaraController@cetak_undangan_umum')->name('admin.cetak-undangan-umum');
			Route::get('acara/cetak-undangan-opd/{id}', 'AcaraController@cetak_undangan_opd')->name('admin.cetak-undangan-opd');
			Route::resource('acara', 'AcaraController');

			Route::get('e-clinic/laporan', 'eClinic\LaporanController@index');
			Route::post('e-clinic/laporan', 'eClinic\LaporanController@download');
			Route::post('e-clinic/rekap', 'eClinic\LaporanController@rekap');

			Route::post('e-clinic/ahli/{id}/aktifkan', 'eClinic\AhliController@aktifkan');
			Route::post('e-clinic/ahli/{id}/non-aktifkan', 'eClinic\AhliController@non_aktifkan');
			Route::resource('e-clinic/ahli', 'eClinic\AhliController');
			Route::post('monitor/{id}', 'AcaraController@set_monitor')->name('admin.acara.monitor');

			Route::prefix('kelola')->group(function () {
				Route::resource('user', 'Master\UserController');
				Route::resource('pegawai', 'Master\PegawaiController');
			});

			Route::get('konfirmasi-grup-undangan', 'Bimtek\KonfirmasiGrupUndanganController@index')->name('admin.bimtek.grup-undangan');
			Route::get('konfirmasi-grup-undangan/{id}/{id_acara}/detail', 'Bimtek\KonfirmasiGrupUndanganController@detail')->name('admin.bimtek.grup-undangan.detail');

			Route::get('konfirmasi-grup-undangan/{id}/{id_acara}', 'Bimtek\KonfirmasiGrupUndanganController@show')->name('admin.bimtek.grup-undangan.show');
			Route::post('konfirmasi-grup-undangan/{id}/{id_acara}', 'Bimtek\KonfirmasiGrupUndanganController@konfirmasi');

			Route::get('konfirmasi-grup-umum', 'Bimtek\KonfirmasiGrupUmumController@index')->name('admin.bimtek.grup-umum');
			Route::get('konfirmasi-grup-umum/{id}/{id_acara}/detail', 'Bimtek\KonfirmasiGrupUmumController@detail')->name('admin.bimtek.grup-umum.detail');
			Route::get('konfirmasi-grup-umum/{id}/{id_acara}', 'Bimtek\KonfirmasiGrupUmumController@show')->name('admin.bimtek.grup-umum.show');
			Route::post('konfirmasi-grup-umum/{id}/{id_acara}', 'Bimtek\KonfirmasiGrupUmumController@konfirmasi');
			

			Route::get('konfirmasi-umum', 'Bimtek\KonfirmasiUmumController@index')->name('admin.bimtek.umum');
			Route::get('konfirmasi-umum/{id}', 'Bimtek\KonfirmasiUmumController@show')->name('admin.bimtek.umum.show');

			Route::post('/konfirmasi-umum/{id}', 'Bimtek\KonfirmasiUmumController@konfirmasi');

			//FAQ AJAX
			Route::post('faq/{id}/ajax-pertanyaan', 'FAQController@ajax_pertanyaan');
			Route::post('faq/{id}/ajax-unlink', 'FAQController@ajax_unlink');
			Route::post('faq/{id}/ajax-link', 'FAQController@ajax_link');
			Route::post('faq/{id}/ajax-urutan', 'FAQController@ajax_urutan');

			Route::post('faq/ganti-urutan/{id}', 'FAQController@ganti_urutan');

			Route::post('faq/create-sub-kategori/{id?}', 'FAQController@store_sub_kategori');
			Route::get('faq/create-sub-kategori/{id?}', 'FAQController@create_sub_kategori')->name('admin.faq.create_sub_kategori');
			Route::put('faq/sub-kategori/{id}', 'FAQController@update_sub_kategori');
			Route::get('faq/sub-kategori/{id}/edit', 'FAQController@edit_sub_kategori');
			Route::get('faq/sub-kategori/{id}', 'FAQController@show_sub_kategori')->name('admin.faq.show_sub_kategori');
			Route::get('faq/sub-kategori', 'FAQController@sub_kategori')->name('admin.faq.sub-kategori');
			
			Route::get('faq/kategori', 'FAQController@kategori')->name('admin.faq');
			Route::delete('faq/{id}', 'FAQController@destroy');
			Route::put('faq/{id}', 'FAQController@update');
			Route::get('faq/{id}/edit', 'FAQController@edit');
			Route::post('faq/create/{id?}', 'FAQController@store');
			Route::get('faq/create/{id?}', 'FAQController@create')->name('admin.faq.create');
			Route::post('faq/{id}/pertanyaan/create', 'FAQController@pertanyaan_store')->name('admin.faq.pertanyaan.create');
			Route::delete('faq/{id}/pertanyaan/{id_pertanyaan}', 'FAQController@pertanyaan_destroy')->name('admin.faq.pertanyaan.destroy');
			Route::get('faq/{id}', 'FAQController@show')->name('admin.faq.show');

			Route::post('faq/attach-sub-kategori', 'FAQController@attach_sub_kategori')->name('admin.faq.attach-sub-kategori');
			Route::post('faq/detach-sub-kategori', 'FAQController@detach_sub_kategori')->name('admin.faq.detach-sub-kategori');
		});

		Route::group(['middleware' => ['pegawai' or 'admin']], function () {
			Route::post('/cek-spt', 'AcaraController@cek_spt')->name('admin/cek-spt');	
			Route::resource('acara', 'AcaraController',['only' => ['index','show']]);
		});
	});
});

// eCLINIC
Route::get('user/verify/{verification_code}', 'eClinic\IndexController@verifyUser');

use Illuminate\Support\Facades\Input;
// use URL;
Route::post('/upload_image', function() {
    $CKEditor = Input::get('CKEditor');
    $funcNum = Input::get('CKEditorFuncNum');
    $message = $url = '';
    if (Input::hasFile('upload')) {
        $file = Input::file('upload');
        if ($file->isValid()) {
            $filename = $file->getClientOriginalName();
            $file->move(public_path().'/storage/images/', $filename);
            $url = URL::to('/storage/images/' . $filename);
        } else {
            $message = 'An error occured while uploading the file.';
        }
    } else {
        $message = 'No file uploaded.';
    }
    echo '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
})->name('upload_image');